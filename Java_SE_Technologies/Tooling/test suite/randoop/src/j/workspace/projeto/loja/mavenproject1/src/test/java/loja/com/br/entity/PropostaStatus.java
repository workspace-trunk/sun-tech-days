import junit.framework.*;
import junit.textui.*;

public class PropostaStatus extends TestCase {

  public static void main(String[] args) {
    TestRunner runner = new TestRunner();
    TestResult result = runner.doRun(suite(), false);
    if (! result.wasSuccessful()) {
      System.exit(1);
    }
  }

  public PropostaStatus(String name) {
    super(name);
  }

  public static Test suite() {
    TestSuite result = new TestSuite();
    result.addTest(new TestSuite(PropostaStatus0.class));
    result.addTest(new TestSuite(PropostaStatus1.class));
    result.addTest(new TestSuite(PropostaStatus2.class));
    result.addTest(new TestSuite(PropostaStatus3.class));
    result.addTest(new TestSuite(PropostaStatus4.class));
    result.addTest(new TestSuite(PropostaStatus5.class));
    result.addTest(new TestSuite(PropostaStatus6.class));
    result.addTest(new TestSuite(PropostaStatus7.class));
    result.addTest(new TestSuite(PropostaStatus8.class));
    result.addTest(new TestSuite(PropostaStatus9.class));
    result.addTest(new TestSuite(PropostaStatus10.class));
    result.addTest(new TestSuite(PropostaStatus11.class));
    return result;
  }

}
