import junit.framework.*;
import junit.textui.*;

public class Categoria extends TestCase {

  public static void main(String[] args) {
    TestRunner runner = new TestRunner();
    TestResult result = runner.doRun(suite(), false);
    if (! result.wasSuccessful()) {
      System.exit(1);
    }
  }

  public Categoria(String name) {
    super(name);
  }

  public static Test suite() {
    TestSuite result = new TestSuite();
    result.addTest(new TestSuite(Categoria0.class));
    result.addTest(new TestSuite(Categoria1.class));
    result.addTest(new TestSuite(Categoria2.class));
    result.addTest(new TestSuite(Categoria3.class));
    result.addTest(new TestSuite(Categoria4.class));
    result.addTest(new TestSuite(Categoria5.class));
    result.addTest(new TestSuite(Categoria6.class));
    result.addTest(new TestSuite(Categoria7.class));
    result.addTest(new TestSuite(Categoria8.class));
    result.addTest(new TestSuite(Categoria9.class));
    result.addTest(new TestSuite(Categoria10.class));
    result.addTest(new TestSuite(Categoria11.class));
    result.addTest(new TestSuite(Categoria12.class));
    result.addTest(new TestSuite(Categoria13.class));
    result.addTest(new TestSuite(Categoria14.class));
    return result;
  }

}
