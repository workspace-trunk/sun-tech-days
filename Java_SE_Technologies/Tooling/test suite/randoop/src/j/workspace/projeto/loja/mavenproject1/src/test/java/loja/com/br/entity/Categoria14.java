
import junit.framework.*;

public class Categoria14 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test1"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var4 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var5 = var2.equals((java.lang.Object)var4);
    var2.setNome("");
    java.lang.Integer var8 = var2.getId();
    loja.com.br.entity.Categoria var10 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var11 = var10.getId();
    boolean var13 = var10.equals((java.lang.Object)(short)(-1));
    java.lang.Integer var14 = var10.getId();
    loja.com.br.entity.Categoria var16 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    java.lang.String var18 = var16.getNome();
    java.lang.String var19 = var16.getNome();
    java.lang.String var20 = var16.getNome();
    java.lang.String var21 = var16.toString();
    boolean var22 = var10.equals((java.lang.Object)var21);
    java.lang.Integer var23 = var10.getId();
    java.lang.String var24 = var10.toString();
    var10.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var27 = var2.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var28 = var2.getNome();
    var2.setId((java.lang.Integer)(-1));
    java.util.Collection var31 = var2.getProdutos();
    var2.setNome("");
    java.util.Collection var34 = var2.getProdutos();
    java.lang.Integer var35 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + ""+ "'", var28.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + (-1)+ "'", var35.equals((-1)));

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test2"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var4 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var5 = var2.equals((java.lang.Object)var4);
    java.util.Collection var6 = var4.getProdutos();
    java.util.Collection var7 = var4.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test3"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "");
    java.lang.String var3 = var2.getNome();
    java.util.Collection var4 = var2.getProdutos();
    java.lang.String var5 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + ""+ "'", var3.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.Categoria[ id=10 ]"));

  }

  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test4"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)100);
    java.lang.String var2 = var1.getNome();
    var1.setNome("loja.com.br.entity.Categoria[ id=100 ]");
    var1.setId((java.lang.Integer)0);
    java.lang.String var7 = var1.getNome();
    java.lang.String var8 = var1.toString();
    var1.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.util.Collection var11 = var1.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.Categoria[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test5"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.toString();
    java.util.Collection var5 = var2.getProdutos();
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.lang.Integer var9 = var7.getId();
    boolean var11 = var7.equals((java.lang.Object)(short)10);
    loja.com.br.entity.Categoria var12 = new loja.com.br.entity.Categoria();
    var12.setId((java.lang.Integer)10);
    boolean var16 = var12.equals((java.lang.Object)0.0f);
    java.lang.String var17 = var12.getNome();
    var12.setNome("");
    java.lang.String var20 = var12.toString();
    boolean var21 = var7.equals((java.lang.Object)var12);
    java.lang.String var22 = var7.getNome();
    java.lang.String var23 = var7.toString();
    var7.setNome("loja.com.br.entity.Categoria[ id=null ]");
    loja.com.br.entity.Categoria var28 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var29 = var28.getId();
    java.lang.String var30 = var28.toString();
    loja.com.br.entity.Categoria var31 = new loja.com.br.entity.Categoria();
    var31.setId((java.lang.Integer)10);
    boolean var35 = var31.equals((java.lang.Object)0.0f);
    java.lang.String var36 = var31.getNome();
    var31.setNome("");
    boolean var39 = var28.equals((java.lang.Object)var31);
    java.util.Collection var40 = var31.getProdutos();
    var31.setId((java.lang.Integer)0);
    var31.setNome("hi!");
    java.lang.String var45 = var31.toString();
    boolean var46 = var7.equals((java.lang.Object)var45);
    boolean var47 = var2.equals((java.lang.Object)var7);
    java.lang.Integer var48 = var7.getId();
    loja.com.br.entity.Categoria var51 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    loja.com.br.entity.Categoria var53 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.util.Collection var54 = var53.getProdutos();
    java.util.Collection var55 = var53.getProdutos();
    java.lang.String var56 = var53.getNome();
    loja.com.br.entity.Categoria var59 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var60 = var59.getId();
    var59.setId((java.lang.Integer)0);
    boolean var63 = var53.equals((java.lang.Object)var59);
    boolean var64 = var51.equals((java.lang.Object)var63);
    var51.setNome("loja.com.br.entity.Categoria[ id=1 ]");
    java.lang.Integer var67 = var51.getId();
    boolean var68 = var7.equals((java.lang.Object)var67);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 10+ "'", var3.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var23.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + (-1)+ "'", var29.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var30.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var45.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + (-1)+ "'", var48.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var60 + "' != '" + (-1)+ "'", var60.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var63 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var67 + "' != '" + (-1)+ "'", var67.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var68 == false);

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test6"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "loja.com.br.entity.Categoria[ id=1 ]");
    var2.setNome("loja.com.br.entity.Categoria[ id=1 ]");
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.util.Collection var8 = var7.getProdutos();
    java.lang.Integer var9 = var7.getId();
    var7.setNome("hi!");
    var7.setNome("");
    java.lang.Integer var14 = var7.getId();
    java.lang.Integer var15 = var7.getId();
    java.lang.String var16 = var7.getNome();
    java.lang.String var17 = var7.getNome();
    java.lang.String var18 = var7.getNome();
    loja.com.br.entity.Categoria var21 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "hi!");
    loja.com.br.entity.Categoria var22 = new loja.com.br.entity.Categoria();
    boolean var23 = var21.equals((java.lang.Object)var22);
    java.lang.Integer var24 = var21.getId();
    boolean var25 = var7.equals((java.lang.Object)var21);
    boolean var26 = var2.equals((java.lang.Object)var7);
    var7.setId((java.lang.Integer)0);
    java.lang.Integer var29 = var7.getId();
    java.util.Collection var30 = var7.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + ""+ "'", var16.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + ""+ "'", var18.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 0+ "'", var24.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 0+ "'", var29.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test7"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var4 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var5 = var2.equals((java.lang.Object)var4);
    var2.setNome("");
    java.lang.String var8 = var2.toString();
    loja.com.br.entity.Categoria var11 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var12 = var11.getId();
    var11.setNome("loja.com.br.entity.Categoria[ id=null ]");
    java.lang.String var15 = var11.getNome();
    loja.com.br.entity.Categoria var17 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.util.Collection var18 = var17.getProdutos();
    java.lang.String var19 = var17.getNome();
    loja.com.br.entity.Categoria var21 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var22 = var21.getId();
    boolean var24 = var21.equals((java.lang.Object)(short)(-1));
    java.lang.Integer var25 = var21.getId();
    loja.com.br.entity.Categoria var27 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var28 = var27.getId();
    java.lang.String var29 = var27.getNome();
    java.lang.String var30 = var27.getNome();
    java.lang.String var31 = var27.getNome();
    java.lang.String var32 = var27.toString();
    boolean var33 = var21.equals((java.lang.Object)var32);
    boolean var34 = var17.equals((java.lang.Object)var21);
    var21.setNome("loja.com.br.entity.Categoria[ id=null ]");
    boolean var37 = var11.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=null ]");
    loja.com.br.entity.Categoria var38 = new loja.com.br.entity.Categoria();
    var38.setId((java.lang.Integer)10);
    boolean var42 = var38.equals((java.lang.Object)0.0f);
    java.lang.String var43 = var38.getNome();
    var38.setNome("");
    boolean var46 = var11.equals((java.lang.Object)var38);
    boolean var47 = var2.equals((java.lang.Object)var46);
    java.lang.String var48 = var2.getNome();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var15.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var32.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + ""+ "'", var48.equals(""));

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test8"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    boolean var2 = var0.equals((java.lang.Object)(-1));
    java.lang.String var3 = var0.getNome();
    var0.setId((java.lang.Integer)0);
    var0.setId((java.lang.Integer)100);
    java.lang.String var8 = var0.getNome();
    var0.setNome("loja.com.br.entity.Categoria[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test9"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    boolean var2 = var0.equals((java.lang.Object)(-1));
    java.lang.String var3 = var0.toString();
    var0.setNome("");
    java.lang.Integer var6 = var0.getId();
    java.util.Collection var7 = var0.getProdutos();
    java.lang.Integer var8 = var0.getId();
    var0.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.Integer var11 = var0.getId();
    java.util.Collection var12 = var0.getProdutos();
    java.lang.String var13 = var0.toString();
    java.lang.Integer var14 = var0.getId();
    java.lang.String var15 = var0.toString();
    java.lang.Integer var16 = var0.getId();
    java.lang.String var17 = var0.getNome();
    java.lang.String var18 = var0.getNome();
    java.lang.String var19 = var0.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var13.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var15.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var19.equals("loja.com.br.entity.Categoria[ id=null ]"));

  }

  public void test10() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test10"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var3 = var2.getNome();
    var2.setId((java.lang.Integer)10);
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    var7.setNome("");
    java.lang.String var11 = var7.toString();
    var7.setNome("");
    boolean var14 = var2.equals((java.lang.Object)var7);
    var7.setId((java.lang.Integer)(-1));
    java.lang.String var17 = var7.getNome();
    var7.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));

  }

  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test11"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    var0.setId((java.lang.Integer)10);
    java.lang.String var3 = var0.getNome();
    var0.setId((java.lang.Integer)100);
    java.lang.Integer var6 = var0.getId();
    java.util.Collection var7 = var0.getProdutos();
    var0.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    var0.setNome("loja.com.br.entity.Categoria[ id=1 ]");
    boolean var13 = var0.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=-1 ]");
    loja.com.br.entity.Categoria var16 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var17 = var16.getId();
    java.lang.String var18 = var16.toString();
    loja.com.br.entity.Categoria var19 = new loja.com.br.entity.Categoria();
    var19.setId((java.lang.Integer)10);
    boolean var23 = var19.equals((java.lang.Object)0.0f);
    java.lang.String var24 = var19.getNome();
    var19.setNome("");
    boolean var27 = var16.equals((java.lang.Object)var19);
    java.util.Collection var28 = var19.getProdutos();
    var19.setNome("loja.com.br.entity.Categoria[ id=null ]");
    boolean var32 = var19.equals((java.lang.Object)(short)(-1));
    java.lang.String var33 = var19.toString();
    var19.setId((java.lang.Integer)1);
    boolean var36 = var0.equals((java.lang.Object)var19);
    java.util.Collection var37 = var19.getProdutos();
    var19.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);

  }

  public void test12() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test12"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)(short)10);
    java.lang.String var6 = var1.toString();
    var1.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var9 = var1.toString();
    java.util.Collection var10 = var1.getProdutos();
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.toString();
    loja.com.br.entity.Categoria var14 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.util.Collection var15 = var14.getProdutos();
    java.lang.String var16 = var14.getNome();
    loja.com.br.entity.Categoria var18 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var19 = var18.getId();
    boolean var21 = var18.equals((java.lang.Object)(short)(-1));
    java.lang.Integer var22 = var18.getId();
    loja.com.br.entity.Categoria var24 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var25 = var24.getId();
    java.lang.String var26 = var24.getNome();
    java.lang.String var27 = var24.getNome();
    java.lang.String var28 = var24.getNome();
    java.lang.String var29 = var24.toString();
    boolean var30 = var18.equals((java.lang.Object)var29);
    boolean var31 = var14.equals((java.lang.Object)var18);
    java.lang.Integer var32 = var18.getId();
    java.util.Collection var33 = var18.getProdutos();
    boolean var34 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.Categoria var35 = new loja.com.br.entity.Categoria();
    var35.setId((java.lang.Integer)10);
    java.lang.String var38 = var35.getNome();
    java.lang.Integer var39 = var35.getId();
    java.util.Collection var40 = var35.getProdutos();
    loja.com.br.entity.Categoria var41 = new loja.com.br.entity.Categoria();
    var41.setId((java.lang.Integer)10);
    java.lang.String var44 = var41.getNome();
    var41.setId((java.lang.Integer)100);
    java.lang.Integer var47 = var41.getId();
    boolean var48 = var35.equals((java.lang.Object)var41);
    java.lang.String var49 = var41.getNome();
    var41.setNome("loja.com.br.entity.Categoria[ id=10 ]");
    var41.setId((java.lang.Integer)(-1));
    java.lang.String var54 = var41.getNome();
    boolean var55 = var1.equals((java.lang.Object)var41);
    var41.setNome("loja.com.br.entity.Categoria[ id=null ]");
    var41.setId((java.lang.Integer)0);
    loja.com.br.entity.Categoria var60 = new loja.com.br.entity.Categoria();
    var60.setId((java.lang.Integer)10);
    boolean var64 = var60.equals((java.lang.Object)0.0f);
    var60.setId((java.lang.Integer)0);
    java.lang.String var67 = var60.getNome();
    java.lang.String var68 = var60.toString();
    var60.setId((java.lang.Integer)10);
    java.lang.String var71 = var60.toString();
    boolean var72 = var41.equals((java.lang.Object)var60);
    java.util.Collection var73 = var60.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + (-1)+ "'", var32.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + 10+ "'", var39.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + 100+ "'", var47.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var54.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var55 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var67);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var68.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var71 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var71.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var72 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var73);

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test13"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    var0.setId((java.lang.Integer)10);
    boolean var4 = var0.equals((java.lang.Object)0.0f);
    java.lang.String var5 = var0.getNome();
    var0.setNome("");
    java.lang.String var8 = var0.toString();
    java.lang.String var9 = var0.getNome();
    var0.setId((java.lang.Integer)10);
    var0.setId((java.lang.Integer)100);
    java.lang.Integer var14 = var0.getId();
    loja.com.br.entity.Categoria var16 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    boolean var19 = var16.equals((java.lang.Object)(short)(-1));
    boolean var20 = var0.equals((java.lang.Object)var16);
    java.lang.Integer var21 = var0.getId();
    java.lang.String var22 = var0.getNome();
    var0.setNome("loja.com.br.entity.Categoria[ id=10 ]");
    java.lang.Integer var25 = var0.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 100+ "'", var21.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + ""+ "'", var22.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));

  }

  public void test14() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test14"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.String var3 = var1.toString();
    var1.setNome("loja.com.br.entity.Categoria[ id=null ]");
    loja.com.br.entity.Categoria var8 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "loja.com.br.entity.Categoria[ id=100 ]");
    boolean var9 = var1.equals((java.lang.Object)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);

  }

  public void test15() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test15"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)100, "loja.com.br.entity.Categoria[ id=null ]");
    java.lang.String var3 = var2.toString();
    loja.com.br.entity.Categoria var6 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var7 = var6.getId();
    var6.setNome("loja.com.br.entity.Categoria[ id=null ]");
    var6.setNome("");
    var6.setId((java.lang.Integer)10);
    java.lang.String var14 = var6.toString();
    boolean var15 = var2.equals((java.lang.Object)var14);
    java.lang.Integer var16 = var2.getId();
    var2.setId((java.lang.Integer)100);
    java.lang.String var19 = var2.getNome();
    loja.com.br.entity.Categoria var20 = new loja.com.br.entity.Categoria();
    boolean var22 = var20.equals((java.lang.Object)(-1));
    java.lang.String var23 = var20.getNome();
    var20.setId((java.lang.Integer)0);
    var20.setId((java.lang.Integer)100);
    java.lang.String var28 = var20.getNome();
    java.lang.String var29 = var20.toString();
    java.util.Collection var30 = var20.getProdutos();
    var20.setNome("loja.com.br.entity.Categoria[ id=null ]");
    java.util.Collection var33 = var20.getProdutos();
    var20.setId((java.lang.Integer)100);
    java.util.Collection var36 = var20.getProdutos();
    boolean var37 = var2.equals((java.lang.Object)var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var14.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var19.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var29.equals("loja.com.br.entity.Categoria[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == true);

  }

  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test16"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var4 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var5 = var2.equals((java.lang.Object)var4);
    java.util.Collection var6 = var4.getProdutos();
    java.lang.String var7 = var4.getNome();
    java.lang.String var8 = var4.toString();
    java.util.Collection var9 = var4.getProdutos();
    var4.setId((java.lang.Integer)0);
    java.lang.String var12 = var4.getNome();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test17() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test17"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "");
    java.lang.String var3 = var2.getNome();
    var2.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var6 = var2.toString();
    var2.setNome("hi!");
    java.lang.Integer var9 = var2.getId();
    loja.com.br.entity.Categoria var11 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    var11.setId((java.lang.Integer)10);
    java.lang.Integer var14 = var11.getId();
    java.lang.String var15 = var11.toString();
    loja.com.br.entity.Categoria var18 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "hi!");
    loja.com.br.entity.Categoria var19 = new loja.com.br.entity.Categoria();
    boolean var20 = var18.equals((java.lang.Object)var19);
    var18.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    boolean var23 = var11.equals((java.lang.Object)var18);
    boolean var24 = var2.equals((java.lang.Object)var18);
    java.lang.String var25 = var2.toString();
    java.util.Collection var26 = var2.getProdutos();
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + ""+ "'", var3.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var15.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var25.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);

  }

  public void test18() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test18"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)(short)10);
    java.lang.String var6 = var1.toString();
    var1.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var9 = var1.toString();
    java.util.Collection var10 = var1.getProdutos();
    loja.com.br.entity.Categoria var12 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.lang.Integer var13 = var12.getId();
    boolean var14 = var1.equals((java.lang.Object)var13);
    java.lang.String var15 = var1.getNome();
    loja.com.br.entity.Categoria var17 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.util.Collection var18 = var17.getProdutos();
    java.lang.Integer var19 = var17.getId();
    var17.setNome("loja.com.br.entity.Categoria[ id=100 ]");
    loja.com.br.entity.Categoria var24 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var25 = var24.getNome();
    boolean var26 = var17.equals((java.lang.Object)var24);
    loja.com.br.entity.Categoria var27 = new loja.com.br.entity.Categoria();
    var27.setId((java.lang.Integer)10);
    boolean var31 = var27.equals((java.lang.Object)0.0f);
    java.lang.String var32 = var27.getNome();
    var27.setNome("");
    boolean var35 = var17.equals((java.lang.Object)var27);
    java.lang.String var36 = var27.toString();
    java.lang.String var37 = var27.toString();
    loja.com.br.entity.Categoria var40 = new loja.com.br.entity.Categoria((java.lang.Integer)100, "");
    java.lang.String var41 = var40.getNome();
    loja.com.br.entity.Categoria var44 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "");
    java.lang.String var45 = var44.getNome();
    var44.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    var44.setNome("");
    java.lang.String var50 = var44.getNome();
    loja.com.br.entity.Categoria var52 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var53 = var52.getId();
    java.lang.Integer var54 = var52.getId();
    var52.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var57 = var44.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var58 = var40.equals((java.lang.Object)var44);
    boolean var59 = var27.equals((java.lang.Object)var44);
    java.lang.Integer var60 = var44.getId();
    boolean var61 = var1.equals((java.lang.Object)var44);
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 1+ "'", var13.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var15.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 1+ "'", var19.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var37.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + ""+ "'", var41.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + ""+ "'", var45.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + ""+ "'", var50.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + (-1)+ "'", var53.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + (-1)+ "'", var54.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var59 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var60 + "' != '" + 0+ "'", var60.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);

  }

  public void test19() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test19"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    var0.setId((java.lang.Integer)10);
    boolean var4 = var0.equals((java.lang.Object)(-1));
    java.lang.String var5 = var0.toString();
    java.util.Collection var6 = var0.getProdutos();
    java.lang.Integer var7 = var0.getId();
    java.util.Collection var8 = var0.getProdutos();
    java.util.Collection var9 = var0.getProdutos();
    java.lang.String var10 = var0.getNome();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test20() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test20"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "");
    java.util.Collection var3 = var2.getProdutos();
    loja.com.br.entity.Categoria var6 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    java.lang.Integer var7 = var6.getId();
    java.lang.String var8 = var6.toString();
    boolean var9 = var2.equals((java.lang.Object)var6);
    var2.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    loja.com.br.entity.Categoria var12 = new loja.com.br.entity.Categoria();
    var12.setId((java.lang.Integer)10);
    java.lang.String var15 = var12.getNome();
    var12.setId((java.lang.Integer)100);
    java.lang.Integer var18 = var12.getId();
    loja.com.br.entity.Categoria var20 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var21 = var20.getId();
    java.lang.Integer var22 = var20.getId();
    var20.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var25 = var12.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=-1 ]");
    loja.com.br.entity.Categoria var27 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var28 = var27.getId();
    boolean var30 = var27.equals((java.lang.Object)(short)(-1));
    java.lang.Integer var31 = var27.getId();
    var27.setId((java.lang.Integer)100);
    java.util.Collection var34 = var27.getProdutos();
    var27.setNome("loja.com.br.entity.Categoria[ id=10 ]");
    loja.com.br.entity.Categoria var37 = new loja.com.br.entity.Categoria();
    boolean var39 = var37.equals((java.lang.Object)(-1));
    java.lang.String var40 = var37.getNome();
    var37.setId((java.lang.Integer)0);
    java.lang.String var43 = var37.toString();
    boolean var44 = var27.equals((java.lang.Object)var43);
    boolean var45 = var12.equals((java.lang.Object)var43);
    java.lang.Integer var46 = var12.getId();
    var12.setId((java.lang.Integer)0);
    boolean var49 = var2.equals((java.lang.Object)0);
    loja.com.br.entity.Categoria var52 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "hi!");
    loja.com.br.entity.Categoria var53 = new loja.com.br.entity.Categoria();
    boolean var54 = var52.equals((java.lang.Object)var53);
    loja.com.br.entity.Categoria var57 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "loja.com.br.entity.Categoria[ id=null ]");
    boolean var58 = var52.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=null ]");
    java.lang.String var59 = var52.getNome();
    var52.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var62 = var2.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var63 = var2.getNome();
    java.lang.Integer var64 = var2.getId();
    loja.com.br.entity.Categoria var66 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var67 = var66.getId();
    java.lang.Integer var68 = var66.getId();
    boolean var70 = var66.equals((java.lang.Object)(short)10);
    var66.setId((java.lang.Integer)100);
    var66.setNome("loja.com.br.entity.Categoria[ id=null ]");
    var66.setId((java.lang.Integer)0);
    java.lang.Integer var77 = var66.getId();
    var66.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var80 = var2.equals((java.lang.Object)var66);
    java.lang.String var81 = var66.getNome();
    java.lang.String var82 = var66.getNome();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 100+ "'", var18.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + (-1)+ "'", var31.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var43.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + 100+ "'", var46.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var54 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + "hi!"+ "'", var59.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var63 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var63.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var64 + "' != '" + 0+ "'", var64.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var67 + "' != '" + (-1)+ "'", var67.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + (-1)+ "'", var68.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var70 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var77 + "' != '" + 0+ "'", var77.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var80 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var81 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var81.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var82 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var82.equals("loja.com.br.entity.Categoria[ id=-1 ]"));

  }

  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test21"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    boolean var4 = var1.equals((java.lang.Object)(short)(-1));
    java.lang.Integer var5 = var1.getId();
    java.lang.String var6 = var1.getNome();
    java.lang.String var7 = var1.toString();
    var1.setNome("loja.com.br.entity.Categoria[ id=1 ]");
    java.lang.String var10 = var1.getNome();
    java.util.Collection var11 = var1.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.Categoria[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test22"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.toString();
    loja.com.br.entity.Categoria var5 = new loja.com.br.entity.Categoria();
    var5.setId((java.lang.Integer)10);
    boolean var9 = var5.equals((java.lang.Object)0.0f);
    java.lang.String var10 = var5.getNome();
    var5.setNome("");
    boolean var13 = var2.equals((java.lang.Object)var5);
    loja.com.br.entity.Categoria var16 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var18 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var19 = var16.equals((java.lang.Object)var18);
    var16.setNome("");
    java.lang.String var22 = var16.toString();
    loja.com.br.entity.Categoria var24 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var25 = var24.getId();
    java.lang.Integer var26 = var24.getId();
    boolean var28 = var24.equals((java.lang.Object)(short)10);
    java.lang.String var29 = var24.toString();
    var24.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var32 = var24.toString();
    java.util.Collection var33 = var24.getProdutos();
    loja.com.br.entity.Categoria var35 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.lang.Integer var36 = var35.getId();
    boolean var37 = var24.equals((java.lang.Object)var36);
    boolean var38 = var16.equals((java.lang.Object)var24);
    java.lang.String var39 = var24.toString();
    boolean var40 = var2.equals((java.lang.Object)var24);
    var24.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.util.Collection var43 = var24.getProdutos();
    java.lang.String var44 = var24.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var22.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var32.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 1+ "'", var36.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var39.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var44.equals("loja.com.br.entity.Categoria[ id=-1 ]"));

  }

  public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test23"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.util.Collection var3 = var2.getProdutos();
    java.lang.Integer var4 = var2.getId();
    var2.setNome("hi!");
    java.lang.String var7 = var2.toString();
    var2.setId((java.lang.Integer)10);
    loja.com.br.entity.Categoria var12 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "loja.com.br.entity.Categoria[ id=1 ]");
    var12.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    boolean var15 = var2.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=0 ]");
    java.lang.Integer var16 = var2.getId();
    var2.setId((java.lang.Integer)10);
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));

  }

  public void test24() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test24"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    var1.setNome("hi!");
    java.lang.Integer var4 = var1.getId();
    var1.setId((java.lang.Integer)1);
    loja.com.br.entity.Categoria var8 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.util.Collection var9 = var8.getProdutos();
    java.lang.Integer var10 = var8.getId();
    loja.com.br.entity.Categoria var11 = new loja.com.br.entity.Categoria();
    var11.setId((java.lang.Integer)10);
    java.lang.String var14 = var11.getNome();
    java.lang.Integer var15 = var11.getId();
    java.util.Collection var16 = var11.getProdutos();
    java.lang.String var17 = var11.getNome();
    boolean var18 = var8.equals((java.lang.Object)var11);
    var8.setNome("");
    java.lang.String var21 = var8.getNome();
    var8.setId((java.lang.Integer)100);
    var8.setId((java.lang.Integer)(-1));
    boolean var26 = var1.equals((java.lang.Object)(-1));
    var1.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 1+ "'", var10.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 10+ "'", var15.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + ""+ "'", var21.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);

  }

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test25"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.toString();
    loja.com.br.entity.Categoria var5 = new loja.com.br.entity.Categoria();
    var5.setId((java.lang.Integer)10);
    boolean var9 = var5.equals((java.lang.Object)0.0f);
    java.lang.String var10 = var5.getNome();
    var5.setNome("");
    boolean var13 = var2.equals((java.lang.Object)var5);
    java.lang.String var14 = var5.toString();
    java.lang.String var15 = var5.getNome();
    loja.com.br.entity.Categoria var18 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "loja.com.br.entity.Categoria[ id=-1 ]");
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.getNome();
    var18.setId((java.lang.Integer)0);
    java.lang.String var24 = var18.toString();
    var18.setNome("hi!");
    java.lang.String var27 = var18.toString();
    boolean var28 = var5.equals((java.lang.Object)var27);
    java.util.Collection var29 = var5.getProdutos();
    var5.setId((java.lang.Integer)100);
    java.lang.Integer var32 = var5.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var14.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + ""+ "'", var15.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var24.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var27.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 100+ "'", var32.equals(100));

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test26"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    boolean var4 = var1.equals((java.lang.Object)(short)(-1));
    java.lang.Integer var5 = var1.getId();
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.lang.String var9 = var7.getNome();
    java.lang.String var10 = var7.getNome();
    java.lang.String var11 = var7.getNome();
    java.lang.String var12 = var7.toString();
    boolean var13 = var1.equals((java.lang.Object)var12);
    java.lang.String var14 = var1.getNome();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test27"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    boolean var2 = var0.equals((java.lang.Object)(-1));
    java.lang.String var3 = var0.toString();
    var0.setNome("");
    java.lang.Integer var6 = var0.getId();
    java.util.Collection var7 = var0.getProdutos();
    java.lang.Integer var8 = var0.getId();
    var0.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var11 = var0.getNome();
    var0.setNome("loja.com.br.entity.Categoria[ id=100 ]");
    loja.com.br.entity.Categoria var16 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var18 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var19 = var16.equals((java.lang.Object)var18);
    var16.setNome("");
    var16.setNome("");
    java.lang.String var24 = var16.toString();
    boolean var25 = var0.equals((java.lang.Object)var24);
    var0.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var24.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);

  }

  public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test28"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var4 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var5 = var2.equals((java.lang.Object)var4);
    var2.setNome("");
    java.lang.String var8 = var2.toString();
    loja.com.br.entity.Categoria var10 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var11 = var10.getId();
    java.lang.Integer var12 = var10.getId();
    var10.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.util.Collection var15 = var10.getProdutos();
    boolean var16 = var2.equals((java.lang.Object)var10);
    java.lang.String var17 = var2.toString();
    java.lang.String var18 = var2.toString();
    java.util.Collection var19 = var2.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var17.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var18.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);

  }

  public void test29() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test29"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.util.Collection var3 = var2.getProdutos();
    java.lang.Integer var4 = var2.getId();
    var2.setNome("hi!");
    var2.setNome("");
    var2.setId((java.lang.Integer)10);
    var2.setNome("hi!");
    var2.setId((java.lang.Integer)100);
    var2.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));

  }

  public void test30() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test30"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    var0.setId((java.lang.Integer)10);
    java.lang.String var3 = var0.getNome();
    java.lang.Integer var4 = var0.getId();
    java.util.Collection var5 = var0.getProdutos();
    loja.com.br.entity.Categoria var6 = new loja.com.br.entity.Categoria();
    var6.setId((java.lang.Integer)10);
    java.lang.String var9 = var6.getNome();
    var6.setId((java.lang.Integer)100);
    java.lang.Integer var12 = var6.getId();
    boolean var13 = var0.equals((java.lang.Object)var6);
    java.lang.Integer var14 = var0.getId();
    java.lang.String var15 = var0.getNome();
    java.lang.String var16 = var0.getNome();
    java.util.Collection var17 = var0.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 10+ "'", var4.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test31() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test31"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    boolean var2 = var0.equals((java.lang.Object)(-1));
    java.lang.String var3 = var0.toString();
    var0.setNome("hi!");
    var0.setNome("loja.com.br.entity.Categoria[ id=null ]");
    loja.com.br.entity.Categoria var9 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    var9.setId((java.lang.Integer)10);
    java.lang.Integer var12 = var9.getId();
    java.lang.String var13 = var9.toString();
    var9.setNome("loja.com.br.entity.Categoria[ id=10 ]");
    var9.setNome("loja.com.br.entity.Categoria[ id=10 ]");
    loja.com.br.entity.Categoria var20 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "");
    var20.setNome("hi!");
    var20.setNome("hi!");
    java.util.Collection var25 = var20.getProdutos();
    var20.setNome("hi!");
    java.util.Collection var28 = var20.getProdutos();
    java.lang.Integer var29 = var20.getId();
    loja.com.br.entity.Categoria var32 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "");
    java.lang.String var33 = var32.getNome();
    var32.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    var32.setId((java.lang.Integer)100);
    java.lang.Integer var38 = var32.getId();
    boolean var39 = var20.equals((java.lang.Object)var32);
    boolean var40 = var9.equals((java.lang.Object)var20);
    boolean var41 = var0.equals((java.lang.Object)var20);
    var0.setId((java.lang.Integer)0);
    java.lang.String var44 = var0.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 10+ "'", var29.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + ""+ "'", var33.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + 100+ "'", var38.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var44.equals("loja.com.br.entity.Categoria[ id=0 ]"));

  }

  public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test32"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "loja.com.br.entity.Categoria[ id=10 ]");
    var2.setId((java.lang.Integer)0);
    java.lang.String var5 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var5.equals("loja.com.br.entity.Categoria[ id=0 ]"));

  }

  public void test33() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test33"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    boolean var2 = var0.equals((java.lang.Object)(-1));
    java.lang.String var3 = var0.toString();
    var0.setNome("");
    java.lang.Integer var6 = var0.getId();
    java.lang.String var7 = var0.getNome();
    java.lang.String var8 = var0.getNome();
    java.lang.String var9 = var0.toString();
    loja.com.br.entity.Categoria var11 = new loja.com.br.entity.Categoria((java.lang.Integer)10);
    boolean var12 = var0.equals((java.lang.Object)var11);
    loja.com.br.entity.Categoria var15 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "hi!");
    loja.com.br.entity.Categoria var16 = new loja.com.br.entity.Categoria();
    boolean var17 = var15.equals((java.lang.Object)var16);
    boolean var18 = var0.equals((java.lang.Object)var17);
    java.util.Collection var19 = var0.getProdutos();
    var0.setId((java.lang.Integer)1);
    var0.setNome("loja.com.br.entity.Categoria[ id=null ]");
    java.lang.String var24 = var0.toString();
    var0.setNome("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + ""+ "'", var7.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var9.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var24.equals("loja.com.br.entity.Categoria[ id=1 ]"));

  }

  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test34"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)10);
    java.lang.String var2 = var1.getNome();
    java.lang.Integer var3 = var1.getId();
    java.lang.String var4 = var1.getNome();
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var8 = var7.getId();
    java.lang.String var9 = var7.toString();
    boolean var11 = var7.equals((java.lang.Object)'#');
    java.lang.String var12 = var7.toString();
    boolean var13 = var1.equals((java.lang.Object)var12);
    java.util.Collection var14 = var1.getProdutos();
    java.lang.String var15 = var1.getNome();
    loja.com.br.entity.Categoria var16 = new loja.com.br.entity.Categoria();
    boolean var18 = var16.equals((java.lang.Object)(-1));
    boolean var20 = var16.equals((java.lang.Object)'#');
    var16.setId((java.lang.Integer)100);
    java.lang.String var23 = var16.getNome();
    java.util.Collection var24 = var16.getProdutos();
    loja.com.br.entity.Categoria var26 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var27 = var26.getId();
    java.lang.Integer var28 = var26.getId();
    boolean var30 = var26.equals((java.lang.Object)(short)10);
    loja.com.br.entity.Categoria var31 = new loja.com.br.entity.Categoria();
    var31.setId((java.lang.Integer)10);
    boolean var35 = var31.equals((java.lang.Object)0.0f);
    java.lang.String var36 = var31.getNome();
    var31.setNome("");
    java.lang.String var39 = var31.toString();
    boolean var40 = var26.equals((java.lang.Object)var31);
    loja.com.br.entity.Categoria var43 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "");
    var43.setNome("hi!");
    var43.setId((java.lang.Integer)0);
    boolean var48 = var26.equals((java.lang.Object)0);
    boolean var49 = var16.equals((java.lang.Object)0);
    boolean var50 = var1.equals((java.lang.Object)var16);
    java.util.Collection var51 = var16.getProdutos();
    java.lang.String var52 = var16.toString();
    java.lang.String var53 = var16.toString();
    var16.setNome("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 10+ "'", var3.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var39.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var52.equals("loja.com.br.entity.Categoria[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var53.equals("loja.com.br.entity.Categoria[ id=100 ]"));

  }

  public void test35() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test35"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.toString();
    boolean var6 = var2.equals((java.lang.Object)'#');
    java.lang.String var7 = var2.getNome();
    java.lang.String var8 = var2.toString();
    var2.setNome("hi!");
    var2.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    java.lang.Integer var13 = var2.getId();
    java.util.Collection var14 = var2.getProdutos();
    loja.com.br.entity.Categoria var15 = new loja.com.br.entity.Categoria();
    boolean var17 = var15.equals((java.lang.Object)(-1));
    java.lang.String var18 = var15.getNome();
    java.lang.String var19 = var15.getNome();
    var15.setNome("");
    java.lang.Integer var22 = var15.getId();
    loja.com.br.entity.Categoria var24 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var25 = var24.getId();
    java.lang.Integer var26 = var24.getId();
    boolean var28 = var24.equals((java.lang.Object)(short)10);
    java.lang.String var29 = var24.toString();
    var24.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var32 = var15.equals((java.lang.Object)var24);
    java.lang.String var33 = var24.getNome();
    java.lang.String var34 = var24.getNome();
    var24.setId((java.lang.Integer)10);
    java.util.Collection var37 = var24.getProdutos();
    boolean var38 = var2.equals((java.lang.Object)var24);
    var24.setNome("loja.com.br.entity.Categoria[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + ""+ "'", var7.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var33.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);

  }

  public void test36() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test36"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "");
    java.lang.String var3 = var2.getNome();
    var2.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var6 = var2.toString();
    java.lang.String var7 = var2.getNome();
    java.util.Collection var8 = var2.getProdutos();
    loja.com.br.entity.Categoria var10 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var11 = var10.getId();
    java.lang.Integer var12 = var10.getId();
    boolean var14 = var10.equals((java.lang.Object)(short)10);
    var10.setId((java.lang.Integer)100);
    var10.setNome("loja.com.br.entity.Categoria[ id=null ]");
    var10.setId((java.lang.Integer)0);
    java.lang.String var21 = var10.getNome();
    loja.com.br.entity.Categoria var23 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var24 = var23.getId();
    java.lang.Integer var25 = var23.getId();
    boolean var27 = var23.equals((java.lang.Object)(short)10);
    loja.com.br.entity.Categoria var28 = new loja.com.br.entity.Categoria();
    var28.setId((java.lang.Integer)10);
    boolean var32 = var28.equals((java.lang.Object)0.0f);
    java.lang.String var33 = var28.getNome();
    var28.setNome("");
    java.lang.String var36 = var28.toString();
    boolean var37 = var23.equals((java.lang.Object)var28);
    loja.com.br.entity.Categoria var40 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "");
    var40.setNome("hi!");
    var40.setId((java.lang.Integer)0);
    boolean var45 = var23.equals((java.lang.Object)0);
    var23.setId((java.lang.Integer)(-1));
    boolean var48 = var10.equals((java.lang.Object)var23);
    boolean var49 = var2.equals((java.lang.Object)var48);
    java.lang.String var50 = var2.toString();
    java.lang.Integer var51 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + ""+ "'", var3.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var21.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + (-1)+ "'", var24.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var50.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + 0+ "'", var51.equals(0));

  }

  public void test37() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test37"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.getNome();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)1);
    java.lang.Integer var11 = var1.getId();
    var1.setNome("loja.com.br.entity.Categoria[ id=null ]");
    java.lang.String var14 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var14.equals("loja.com.br.entity.Categoria[ id=1 ]"));

  }

  public void test38() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test38"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.toString();
    loja.com.br.entity.Categoria var5 = new loja.com.br.entity.Categoria();
    var5.setId((java.lang.Integer)10);
    boolean var9 = var5.equals((java.lang.Object)0.0f);
    java.lang.String var10 = var5.getNome();
    var5.setNome("");
    boolean var13 = var2.equals((java.lang.Object)var5);
    java.util.Collection var14 = var5.getProdutos();
    var5.setNome("loja.com.br.entity.Categoria[ id=null ]");
    loja.com.br.entity.Categoria var19 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "loja.com.br.entity.Categoria[ id=0 ]");
    boolean var20 = var5.equals((java.lang.Object)var19);
    var19.setNome("loja.com.br.entity.Categoria[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);

  }

  public void test39() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test39"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "loja.com.br.entity.Categoria[ id=10 ]");
    var2.setId((java.lang.Integer)0);
    java.util.Collection var5 = var2.getProdutos();
    java.lang.String var6 = var2.toString();
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria();
    var7.setId((java.lang.Integer)10);
    java.lang.String var10 = var7.getNome();
    java.lang.Integer var11 = var7.getId();
    java.lang.String var12 = var7.toString();
    var7.setNome("");
    var7.setId((java.lang.Integer)0);
    boolean var17 = var2.equals((java.lang.Object)0);
    loja.com.br.entity.Categoria var20 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "");
    java.lang.String var21 = var20.getNome();
    var20.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var24 = var20.toString();
    var20.setNome("hi!");
    java.lang.Integer var27 = var20.getId();
    var20.setNome("hi!");
    loja.com.br.entity.Categoria var30 = new loja.com.br.entity.Categoria();
    boolean var32 = var30.equals((java.lang.Object)(-1));
    java.lang.String var33 = var30.toString();
    var30.setNome("");
    java.lang.Integer var36 = var30.getId();
    var30.setNome("hi!");
    java.lang.String var39 = var30.toString();
    var30.setNome("loja.com.br.entity.Categoria[ id=1 ]");
    loja.com.br.entity.Categoria var43 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var44 = var43.getId();
    java.lang.Integer var45 = var43.getId();
    java.util.Collection var46 = var43.getProdutos();
    loja.com.br.entity.Categoria var49 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.util.Collection var50 = var49.getProdutos();
    java.lang.Integer var51 = var49.getId();
    var49.setId((java.lang.Integer)1);
    boolean var54 = var43.equals((java.lang.Object)var49);
    java.lang.String var55 = var43.getNome();
    java.util.Collection var56 = var43.getProdutos();
    boolean var57 = var30.equals((java.lang.Object)var43);
    boolean var58 = var20.equals((java.lang.Object)var43);
    var20.setNome("");
    java.util.Collection var61 = var20.getProdutos();
    loja.com.br.entity.Categoria var64 = new loja.com.br.entity.Categoria((java.lang.Integer)1, "hi!");
    loja.com.br.entity.Categoria var66 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var67 = var66.getId();
    java.lang.Integer var68 = var66.getId();
    boolean var70 = var66.equals((java.lang.Object)(short)10);
    var66.setId((java.lang.Integer)100);
    java.lang.String var73 = var66.toString();
    boolean var74 = var64.equals((java.lang.Object)var73);
    boolean var75 = var20.equals((java.lang.Object)var64);
    boolean var76 = var2.equals((java.lang.Object)var75);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + ""+ "'", var21.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var24.equals("loja.com.br.entity.Categoria[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 0+ "'", var27.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var33.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var39.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + (-1)+ "'", var44.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + (-1)+ "'", var45.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + (-1)+ "'", var51.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var54 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var67 + "' != '" + (-1)+ "'", var67.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + (-1)+ "'", var68.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var70 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var73 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var73.equals("loja.com.br.entity.Categoria[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var74 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var75 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var76 == false);

  }

  public void test40() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test40"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)(short)10);
    var1.setId((java.lang.Integer)100);
    var1.setNome("loja.com.br.entity.Categoria[ id=null ]");
    loja.com.br.entity.Categoria var12 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var14 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var15 = var12.equals((java.lang.Object)var14);
    var12.setNome("loja.com.br.entity.Categoria[ id=null ]");
    boolean var19 = var12.equals((java.lang.Object)1L);
    boolean var20 = var1.equals((java.lang.Object)var12);
    var12.setId((java.lang.Integer)100);
    var12.setNome("loja.com.br.entity.Categoria[ id=null ]");
    java.lang.Integer var25 = var12.getId();
    java.lang.Integer var26 = var12.getId();
    var12.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    loja.com.br.entity.Categoria var31 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var33 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var34 = var31.equals((java.lang.Object)var33);
    var31.setNome("");
    var31.setId((java.lang.Integer)10);
    java.lang.String var39 = var31.getNome();
    loja.com.br.entity.Categoria var40 = new loja.com.br.entity.Categoria();
    var40.setId((java.lang.Integer)10);
    boolean var44 = var40.equals((java.lang.Object)0.0f);
    java.lang.String var45 = var40.getNome();
    var40.setNome("");
    java.lang.String var48 = var40.toString();
    java.util.Collection var49 = var40.getProdutos();
    var40.setNome("");
    var40.setNome("loja.com.br.entity.Categoria[ id=null ]");
    var40.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    boolean var56 = var31.equals((java.lang.Object)"loja.com.br.entity.Categoria[ id=0 ]");
    boolean var57 = var12.equals((java.lang.Object)var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 100+ "'", var26.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + ""+ "'", var39.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var48.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var56 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);

  }

  public void test41() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test41"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    loja.com.br.entity.Categoria var4 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.util.Collection var5 = var4.getProdutos();
    java.util.Collection var6 = var4.getProdutos();
    java.lang.String var7 = var4.getNome();
    loja.com.br.entity.Categoria var10 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var11 = var10.getId();
    var10.setId((java.lang.Integer)0);
    boolean var14 = var4.equals((java.lang.Object)var10);
    boolean var15 = var2.equals((java.lang.Object)var14);
    var2.setNome("loja.com.br.entity.Categoria[ id=1 ]");
    java.lang.String var18 = var2.toString();
    java.lang.Integer var19 = var2.getId();
    java.lang.Integer var20 = var2.getId();
    java.lang.String var21 = var2.getNome();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var21.equals("loja.com.br.entity.Categoria[ id=1 ]"));

  }

  public void test42() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test42"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    var0.setId((java.lang.Integer)10);
    boolean var4 = var0.equals((java.lang.Object)0.0f);
    var0.setId((java.lang.Integer)0);
    loja.com.br.entity.Categoria var8 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    boolean var9 = var0.equals((java.lang.Object)(-1));
    var0.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    java.lang.String var12 = var0.getNome();
    var0.setNome("loja.com.br.entity.Categoria[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.Categoria[ id=0 ]"+ "'", var12.equals("loja.com.br.entity.Categoria[ id=0 ]"));

  }

  public void test43() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test43"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    var1.setNome("hi!");
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    java.lang.Integer var8 = var7.getId();
    java.lang.String var9 = var7.toString();
    loja.com.br.entity.Categoria var10 = new loja.com.br.entity.Categoria();
    var10.setId((java.lang.Integer)10);
    boolean var14 = var10.equals((java.lang.Object)0.0f);
    java.lang.String var15 = var10.getNome();
    var10.setNome("");
    boolean var18 = var7.equals((java.lang.Object)var10);
    java.lang.String var19 = var7.toString();
    boolean var20 = var1.equals((java.lang.Object)var7);
    java.lang.Integer var21 = var7.getId();
    java.lang.String var22 = var7.getNome();
    java.lang.String var23 = var7.getNome();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var19.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + ""+ "'", var22.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + ""+ "'", var23.equals(""));

  }

  public void test44() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test44"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.getNome();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)0);
    java.lang.String var9 = var1.getNome();
    java.util.Collection var10 = var1.getProdutos();
    java.lang.String var11 = var1.getNome();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);

  }

  public void test45() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test45"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    boolean var2 = var0.equals((java.lang.Object)(-1));
    java.lang.String var3 = var0.toString();
    var0.setNome("");
    java.lang.String var6 = var0.getNome();
    var0.setId((java.lang.Integer)(-1));
    java.lang.Integer var9 = var0.getId();
    java.util.Collection var10 = var0.getProdutos();
    java.lang.Integer var11 = var0.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));

  }

  public void test46() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test46"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.util.Collection var2 = var1.getProdutos();
    java.lang.Integer var3 = var1.getId();
    var1.setNome("loja.com.br.entity.Categoria[ id=100 ]");
    java.lang.String var6 = var1.toString();
    loja.com.br.entity.Categoria var7 = new loja.com.br.entity.Categoria();
    boolean var9 = var7.equals((java.lang.Object)(-1));
    java.lang.String var10 = var7.toString();
    var7.setNome("");
    java.lang.Integer var13 = var7.getId();
    java.lang.String var14 = var7.getNome();
    java.lang.String var15 = var7.getNome();
    java.lang.String var16 = var7.toString();
    boolean var17 = var1.equals((java.lang.Object)var16);
    java.lang.String var18 = var1.getNome();
    loja.com.br.entity.Categoria var19 = new loja.com.br.entity.Categoria();
    var19.setId((java.lang.Integer)10);
    java.lang.String var22 = var19.getNome();
    java.lang.Integer var23 = var19.getId();
    var19.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    boolean var26 = var1.equals((java.lang.Object)var19);
    loja.com.br.entity.Categoria var28 = new loja.com.br.entity.Categoria((java.lang.Integer)10);
    java.lang.String var29 = var28.getNome();
    java.lang.Integer var30 = var28.getId();
    java.lang.String var31 = var28.getNome();
    loja.com.br.entity.Categoria var33 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var34 = var33.getId();
    java.lang.Integer var35 = var33.getId();
    boolean var37 = var33.equals((java.lang.Object)(short)10);
    var33.setId((java.lang.Integer)100);
    var33.setNome("");
    boolean var42 = var28.equals((java.lang.Object)var33);
    loja.com.br.entity.Categoria var44 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.util.Collection var45 = var44.getProdutos();
    java.util.Collection var46 = var44.getProdutos();
    java.lang.String var47 = var44.getNome();
    loja.com.br.entity.Categoria var49 = new loja.com.br.entity.Categoria((java.lang.Integer)10);
    var49.setId((java.lang.Integer)10);
    java.lang.String var52 = var49.getNome();
    var49.setId((java.lang.Integer)10);
    var49.setId((java.lang.Integer)0);
    boolean var57 = var44.equals((java.lang.Object)var49);
    boolean var58 = var33.equals((java.lang.Object)var44);
    java.lang.Integer var59 = var44.getId();
    java.util.Collection var60 = var44.getProdutos();
    boolean var61 = var1.equals((java.lang.Object)var44);
    java.lang.String var62 = var1.toString();
    java.lang.String var63 = var1.toString();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var10.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + ""+ "'", var14.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + ""+ "'", var15.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var16.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.Categoria[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 10+ "'", var23.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + 10+ "'", var30.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + (-1)+ "'", var34.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + (-1)+ "'", var35.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var52);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + (-1)+ "'", var59.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var60);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var62 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var62.equals("loja.com.br.entity.Categoria[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var63 + "' != '" + "loja.com.br.entity.Categoria[ id=1 ]"+ "'", var63.equals("loja.com.br.entity.Categoria[ id=1 ]"));

  }

  public void test47() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test47"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    var0.setId((java.lang.Integer)10);
    java.lang.String var3 = var0.getNome();
    var0.setId((java.lang.Integer)100);
    java.lang.Integer var6 = var0.getId();
    java.util.Collection var7 = var0.getProdutos();
    java.util.Collection var8 = var0.getProdutos();
    var0.setId((java.lang.Integer)0);
    var0.setNome("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test48"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1), "");
    var2.setId((java.lang.Integer)10);
    var2.setId((java.lang.Integer)(-1));
    var2.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    java.lang.Integer var9 = var2.getId();
    java.lang.Integer var10 = var2.getId();
    loja.com.br.entity.Categoria var13 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var15 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var16 = var13.equals((java.lang.Object)var15);
    var13.setNome("");
    java.util.Collection var19 = var13.getProdutos();
    var13.setId((java.lang.Integer)0);
    boolean var22 = var2.equals((java.lang.Object)var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);

  }

  public void test49() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test49"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    boolean var2 = var0.equals((java.lang.Object)(-1));
    java.lang.String var3 = var0.toString();
    var0.setNome("");
    java.lang.Integer var6 = var0.getId();
    var0.setNome("hi!");
    var0.setNome("hi!");
    loja.com.br.entity.Categoria var13 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "hi!");
    var13.setId((java.lang.Integer)0);
    var13.setId((java.lang.Integer)1);
    boolean var18 = var0.equals((java.lang.Object)1);
    java.lang.String var19 = var0.toString();
    loja.com.br.entity.Categoria var21 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var22 = var21.getId();
    java.lang.Integer var23 = var21.getId();
    boolean var25 = var21.equals((java.lang.Object)(short)10);
    java.lang.String var26 = var21.toString();
    var21.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var29 = var21.toString();
    java.util.Collection var30 = var21.getProdutos();
    boolean var31 = var0.equals((java.lang.Object)var21);
    java.lang.String var32 = var21.getNome();
    java.lang.String var33 = var21.toString();
    var21.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var2 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var3.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.Categoria[ id=null ]"+ "'", var19.equals("loja.com.br.entity.Categoria[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var32.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var33.equals("loja.com.br.entity.Categoria[ id=-1 ]"));

  }

  public void test50() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test50"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)(short)10);
    java.lang.String var6 = var1.toString();
    var1.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var9 = var1.toString();
    java.util.Collection var10 = var1.getProdutos();
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.toString();
    loja.com.br.entity.Categoria var14 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    java.util.Collection var15 = var14.getProdutos();
    java.lang.String var16 = var14.getNome();
    loja.com.br.entity.Categoria var18 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var19 = var18.getId();
    boolean var21 = var18.equals((java.lang.Object)(short)(-1));
    java.lang.Integer var22 = var18.getId();
    loja.com.br.entity.Categoria var24 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var25 = var24.getId();
    java.lang.String var26 = var24.getNome();
    java.lang.String var27 = var24.getNome();
    java.lang.String var28 = var24.getNome();
    java.lang.String var29 = var24.toString();
    boolean var30 = var18.equals((java.lang.Object)var29);
    boolean var31 = var14.equals((java.lang.Object)var18);
    java.lang.Integer var32 = var18.getId();
    java.util.Collection var33 = var18.getProdutos();
    boolean var34 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var35 = var18.getId();
    java.lang.String var36 = var18.toString();
    loja.com.br.entity.Categoria var37 = new loja.com.br.entity.Categoria();
    var37.setId((java.lang.Integer)10);
    boolean var41 = var37.equals((java.lang.Object)0.0f);
    java.lang.String var42 = var37.getNome();
    var37.setNome("");
    java.lang.String var45 = var37.toString();
    java.lang.Integer var46 = var37.getId();
    java.lang.String var47 = var37.toString();
    java.lang.String var48 = var37.getNome();
    boolean var49 = var18.equals((java.lang.Object)var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + (-1)+ "'", var32.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + (-1)+ "'", var35.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var45.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + 10+ "'", var46.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var47.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + ""+ "'", var48.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);

  }

  public void test51() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test51"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.toString();
    java.lang.String var5 = var2.toString();
    java.lang.String var6 = var2.getNome();
    java.util.Collection var7 = var2.getProdutos();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 10+ "'", var3.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test52() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test52"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.getNome();
    java.lang.String var5 = var1.getNome();
    java.util.Collection var6 = var1.getProdutos();
    java.lang.Integer var7 = var1.getId();
    java.lang.Integer var8 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));

  }

  public void test53() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test53"); }


    loja.com.br.entity.Categoria var0 = new loja.com.br.entity.Categoria();
    var0.setId((java.lang.Integer)10);
    java.lang.String var3 = var0.getNome();
    java.lang.Integer var4 = var0.getId();
    java.util.Collection var5 = var0.getProdutos();
    loja.com.br.entity.Categoria var6 = new loja.com.br.entity.Categoria();
    var6.setId((java.lang.Integer)10);
    java.lang.String var9 = var6.getNome();
    var6.setId((java.lang.Integer)100);
    java.lang.Integer var12 = var6.getId();
    boolean var13 = var0.equals((java.lang.Object)var6);
    java.lang.String var14 = var6.getNome();
    var6.setNome("loja.com.br.entity.Categoria[ id=10 ]");
    var6.setId((java.lang.Integer)(-1));
    java.lang.String var19 = var6.getNome();
    java.lang.String var20 = var6.getNome();
    var6.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    var6.setNome("");
    var6.setNome("loja.com.br.entity.Categoria[ id=0 ]");
    var6.setId((java.lang.Integer)100);
    java.lang.String var29 = var6.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 10+ "'", var4.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.Categoria[ id=100 ]"+ "'", var29.equals("loja.com.br.entity.Categoria[ id=100 ]"));

  }

  public void test54() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test54"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)0, "");
    java.util.Collection var3 = var2.getProdutos();
    loja.com.br.entity.Categoria var6 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    java.lang.Integer var7 = var6.getId();
    java.lang.String var8 = var6.toString();
    boolean var9 = var2.equals((java.lang.Object)var6);
    java.lang.String var10 = var6.getNome();
    java.lang.Integer var11 = var6.getId();
    java.lang.String var12 = var6.getNome();
    java.lang.String var13 = var6.toString();
    var6.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.Categoria[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "hi!"+ "'", var10.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "hi!"+ "'", var12.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.Categoria[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.Categoria[ id=10 ]"));

  }

  public void test55() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test55"); }


    loja.com.br.entity.Categoria var1 = new loja.com.br.entity.Categoria((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)(short)10);
    java.lang.String var6 = var1.toString();
    var1.setNome("loja.com.br.entity.Categoria[ id=-1 ]");
    java.lang.String var9 = var1.toString();
    java.util.Collection var10 = var1.getProdutos();
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.toString();
    java.lang.Integer var13 = var1.getId();
    java.lang.String var14 = var1.getNome();
    java.lang.String var15 = var1.toString();
    java.lang.String var16 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var15.equals("loja.com.br.entity.Categoria[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.Categoria[ id=-1 ]"+ "'", var16.equals("loja.com.br.entity.Categoria[ id=-1 ]"));

  }

  public void test56() throws Throwable {

    if (debug) { System.out.println(); System.out.print("Categoria14.test56"); }


    loja.com.br.entity.Categoria var2 = new loja.com.br.entity.Categoria((java.lang.Integer)10, "hi!");
    loja.com.br.entity.Categoria var4 = new loja.com.br.entity.Categoria((java.lang.Integer)1);
    boolean var5 = var2.equals((java.lang.Object)var4);
    var2.setNome("");
    var2.setId((java.lang.Integer)1);
    var2.setId((java.lang.Integer)100);
    java.util.Collection var12 = var2.getProdutos();
    var2.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

}
