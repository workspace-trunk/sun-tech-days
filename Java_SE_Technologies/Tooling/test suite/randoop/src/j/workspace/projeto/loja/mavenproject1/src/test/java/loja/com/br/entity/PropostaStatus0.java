
import junit.framework.*;

public class PropostaStatus0 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test1"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.String var8 = var1.getStatus();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test2"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    var0.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var3 = var0.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test3"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var3 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));

  }

  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test4"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=-1 ]");

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test5"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    java.lang.String var11 = var4.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test6"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var7 = var1.getStatus();
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test7"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test8"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);

  }

  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test9"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);

  }

  public void test10() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test10"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    java.lang.String var6 = var1.getStatus();
    java.lang.Integer var7 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));

  }

  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test11"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var4.setId((java.lang.Integer)10);
    java.lang.String var7 = var4.toString();
    java.lang.String var8 = var4.toString();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var2.equals((java.lang.Object)var4);
    java.lang.String var11 = var4.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);

  }

  public void test12() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test12"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var18 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test13"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var3 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test14() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test14"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test15() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test15"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    var1.setStatus("");
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test16"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.String var8 = var1.getStatus();
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var11.setId((java.lang.Integer)10);
    var11.setStatus("");
    java.lang.Integer var16 = var11.getId();
    java.util.Collection var17 = var11.getPropostaCollection();
    java.lang.Integer var18 = var11.getId();
    boolean var19 = var1.equals((java.lang.Object)var11);
    java.lang.String var20 = var1.getStatus();
    java.lang.String var21 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 10+ "'", var18.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test17() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test17"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));

  }

  public void test18() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test18"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    java.lang.String var19 = var1.getStatus();
    java.util.Collection var20 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "hi!"+ "'", var19.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);

  }

  public void test19() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test19"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var2 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    java.lang.Integer var6 = var4.getId();
    java.lang.Integer var7 = var4.getId();
    boolean var8 = var1.equals((java.lang.Object)var4);
    java.lang.Integer var9 = var4.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));

  }

  public void test20() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test20"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var4 = var1.getStatus();
    java.lang.String var5 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test21"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var2 = var1.getStatus();
    java.lang.String var3 = var1.getStatus();
    java.util.Collection var4 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test22"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var13.setStatus("");
    java.lang.Integer var16 = var13.getId();
    java.lang.String var17 = var13.toString();
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var20 = var13.getId();
    boolean var21 = var1.equals((java.lang.Object)var13);
    java.lang.Integer var22 = var13.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));

  }

  public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test23"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var28 = var18.getId();
    java.util.Collection var29 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    java.lang.Integer var32 = var18.getId();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var35 = var18.getStatus();
    java.lang.String var36 = var18.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test24() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test24"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var1 = var0.getPropostaCollection();
    var0.setId((java.lang.Integer)100);
    var0.setId((java.lang.Integer)10);
    var0.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);

  }

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test25"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var7 = var6.toString();
    var6.setId((java.lang.Integer)(-1));
    boolean var10 = var2.equals((java.lang.Object)var6);
    java.lang.String var11 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test26"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setId((java.lang.Integer)1);
    java.lang.String var5 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test27"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var21 = var20.getId();
    java.util.Collection var22 = var20.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var25 = var24.getId();
    java.util.Collection var26 = var24.getPropostaCollection();
    var24.setId((java.lang.Integer)10);
    java.lang.String var29 = var24.toString();
    java.lang.String var30 = var24.getStatus();
    boolean var31 = var20.equals((java.lang.Object)var24);
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var34 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);

  }

  public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test28"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    var10.setStatus("hi!");
    var10.setId((java.lang.Integer)(-1));
    java.lang.Integer var23 = var10.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));

  }

  public void test29() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test29"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    var16.setId((java.lang.Integer)(-1));
    boolean var21 = var16.equals((java.lang.Object)(short)(-1));
    var16.setId((java.lang.Integer)10);
    java.lang.String var24 = var16.getStatus();
    boolean var25 = var10.equals((java.lang.Object)var16);
    boolean var26 = var2.equals((java.lang.Object)var10);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var29 = var10.getStatus();
    java.lang.Integer var30 = var10.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + 1+ "'", var30.equals(1));

  }

  public void test30() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test30"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.toString();
    java.lang.Integer var8 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));

  }

  public void test31() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test31"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var9.setId((java.lang.Integer)10);
    java.lang.String var12 = var9.toString();
    boolean var14 = var9.equals((java.lang.Object)(byte)100);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var17 = var1.equals((java.lang.Object)var9);
    var9.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);

  }

  public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test32"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var17 = var9.getPropostaCollection();
    java.util.Collection var18 = var9.getPropostaCollection();
    var9.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);

  }

  public void test33() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test33"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");

  }

  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test34"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.lang.String var7 = var1.toString();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var14 = var13.getId();
    boolean var15 = var9.equals((java.lang.Object)var13);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var17.setId((java.lang.Integer)10);
    java.lang.String var20 = var17.toString();
    boolean var22 = var17.equals((java.lang.Object)(byte)100);
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var25 = var9.equals((java.lang.Object)var17);
    var9.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var30 = var29.toString();
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var32.setStatus("");
    java.lang.Integer var35 = var32.getId();
    java.util.Collection var36 = var32.getPropostaCollection();
    java.lang.String var37 = var32.getStatus();
    boolean var38 = var29.equals((java.lang.Object)var32);
    var32.setStatus("");
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var43 = var42.getStatus();
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var46 = var32.equals((java.lang.Object)var42);
    boolean var47 = var9.equals((java.lang.Object)var42);
    boolean var48 = var1.equals((java.lang.Object)var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + (-1)+ "'", var35.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + ""+ "'", var37.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);

  }

  public void test35() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test35"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);

  }

  public void test36() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test36"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.util.Collection var23 = var1.getPropostaCollection();
    java.lang.String var24 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test37() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test37"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)100);
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));

  }

  public void test38() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test38"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    boolean var7 = var4.equals((java.lang.Object)(short)0);
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.Integer var9 = var4.getId();
    boolean var10 = var2.equals((java.lang.Object)var4);
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);

  }

  public void test39() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test39"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setStatus("");

  }

  public void test40() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test40"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.lang.String var4 = var1.toString();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var7 = var6.getId();
    java.lang.Integer var8 = var6.getId();
    java.lang.Integer var9 = var6.getId();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var13 = var11.equals((java.lang.Object)false);
    java.lang.String var14 = var11.toString();
    java.lang.String var15 = var11.getStatus();
    boolean var16 = var6.equals((java.lang.Object)var11);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    boolean var20 = var6.equals((java.lang.Object)0);
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var22.setId((java.lang.Integer)10);
    java.lang.String var25 = var22.toString();
    boolean var26 = var6.equals((java.lang.Object)var22);
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var29 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);

  }

  public void test41() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test41"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test42() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test42"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.util.Collection var5 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test43() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test43"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var10 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));

  }

  public void test44() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test44"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var12.setId((java.lang.Integer)10);
    java.lang.String var15 = var12.toString();
    java.lang.String var16 = var12.toString();
    var12.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var23 = var21.equals((java.lang.Object)(-1.0d));
    java.util.Collection var24 = var21.getPropostaCollection();
    var21.setId((java.lang.Integer)(-1));
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var29 = var12.equals((java.lang.Object)var21);
    boolean var30 = var1.equals((java.lang.Object)var12);
    var1.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);

  }

  public void test45() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test45"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var5 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test46() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test46"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)100);
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test47() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test47"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var13 = var1.toString();
    java.util.Collection var14 = var1.getPropostaCollection();
    java.lang.String var15 = var1.toString();
    java.util.Collection var16 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);

  }

  public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test48"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.util.Collection var7 = var1.getPropostaCollection();
    java.lang.String var8 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));

  }

  public void test49() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test49"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)10);
    java.lang.Integer var5 = var2.getId();
    var2.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));

  }

  public void test50() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test50"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)(-1));
    java.util.Collection var12 = var8.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var19 = var18.getId();
    boolean var20 = var14.equals((java.lang.Object)var18);
    java.lang.Integer var21 = var14.getId();
    boolean var22 = var8.equals((java.lang.Object)var21);
    var8.setId((java.lang.Integer)100);
    boolean var25 = var1.equals((java.lang.Object)100);
    java.lang.String var26 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 1+ "'", var21.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test51() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test51"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    boolean var9 = var1.equals((java.lang.Object)100);
    boolean var11 = var1.equals((java.lang.Object)(byte)1);
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var18 = var17.getId();
    boolean var19 = var13.equals((java.lang.Object)var17);
    java.lang.Integer var20 = var13.getId();
    java.util.Collection var21 = var13.getPropostaCollection();
    var13.setId((java.lang.Integer)1);
    boolean var24 = var1.equals((java.lang.Object)var13);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var27 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 100+ "'", var18.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 1+ "'", var20.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);

  }

  public void test52() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test52"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var2 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var5 = var1.toString();
    java.lang.String var6 = var1.getStatus();
    java.lang.Integer var7 = var1.getId();
    java.util.Collection var8 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test53() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test53"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    var5.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    boolean var17 = var11.equals((java.lang.Object)var15);
    java.lang.Integer var18 = var11.getId();
    var11.setId((java.lang.Integer)10);
    java.lang.Integer var21 = var11.getId();
    var11.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var26 = var25.getId();
    java.util.Collection var27 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)10);
    var25.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var33.setStatus("");
    java.lang.Integer var36 = var33.getId();
    java.util.Collection var37 = var33.getPropostaCollection();
    boolean var38 = var25.equals((java.lang.Object)var33);
    var25.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var47 = var46.getId();
    boolean var48 = var42.equals((java.lang.Object)var46);
    java.lang.String var49 = var42.getStatus();
    java.lang.String var50 = var42.getStatus();
    boolean var51 = var25.equals((java.lang.Object)var42);
    var25.setId((java.lang.Integer)1);
    boolean var54 = var11.equals((java.lang.Object)var25);
    java.lang.Integer var55 = var25.getId();
    boolean var56 = var5.equals((java.lang.Object)var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 1+ "'", var18.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 10+ "'", var21.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + (-1)+ "'", var36.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + 100+ "'", var47.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var49.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var50.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var54 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + 1+ "'", var55.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var56 == false);

  }

  public void test54() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test54"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    java.lang.String var4 = var2.getStatus();
    java.util.Collection var5 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test55() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test55"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var12 = var11.toString();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("");
    java.lang.Integer var17 = var14.getId();
    java.util.Collection var18 = var14.getPropostaCollection();
    java.lang.String var19 = var14.getStatus();
    boolean var20 = var11.equals((java.lang.Object)var14);
    boolean var21 = var1.equals((java.lang.Object)var11);
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var23.setId((java.lang.Integer)100);
    java.lang.String var26 = var23.getStatus();
    java.util.Collection var27 = var23.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var31 = var29.equals((java.lang.Object)false);
    boolean var32 = var23.equals((java.lang.Object)var29);
    java.lang.String var33 = var29.getStatus();
    java.lang.Integer var34 = var29.getId();
    boolean var35 = var1.equals((java.lang.Object)var29);
    java.lang.Integer var36 = var29.getId();
    java.lang.Integer var37 = var29.getId();
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + 0+ "'", var34.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 0+ "'", var36.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 0+ "'", var37.equals(0));

  }

  public void test56() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test56"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.Integer var1 = var0.getId();
    java.lang.Integer var2 = var0.getId();
    java.lang.String var3 = var0.getStatus();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.util.Collection var11 = var5.getPropostaCollection();
    boolean var12 = var0.equals((java.lang.Object)var5);
    java.lang.String var13 = var0.getStatus();
    java.lang.String var14 = var0.toString();
    java.lang.String var15 = var0.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test57() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test57"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test58() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test58"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setId((java.lang.Integer)10);
    var6.setStatus("");
    java.lang.Integer var11 = var6.getId();
    java.util.Collection var12 = var6.getPropostaCollection();
    boolean var13 = var2.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var18 = var17.getPropostaCollection();
    var17.setId((java.lang.Integer)100);
    java.lang.Integer var21 = var17.getId();
    boolean var22 = var16.equals((java.lang.Object)var17);
    java.lang.String var23 = var17.toString();
    java.lang.Integer var24 = var17.getId();
    boolean var25 = var2.equals((java.lang.Object)var24);
    java.lang.Integer var26 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 100+ "'", var21.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 100+ "'", var24.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));

  }

  public void test59() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test59"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var7.setStatus("");
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var12.setId((java.lang.Integer)0);
    boolean var15 = var7.equals((java.lang.Object)0);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var18 = var7.getStatus();
    boolean var19 = var1.equals((java.lang.Object)var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);

  }

  public void test60() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test60"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setId((java.lang.Integer)10);
    java.lang.String var9 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    java.lang.String var16 = var11.toString();
    java.lang.String var17 = var11.getStatus();
    var11.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var22 = var21.toString();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var24.setStatus("");
    java.lang.Integer var27 = var24.getId();
    java.util.Collection var28 = var24.getPropostaCollection();
    java.lang.String var29 = var24.getStatus();
    boolean var30 = var21.equals((java.lang.Object)var24);
    boolean var31 = var11.equals((java.lang.Object)var21);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var33.setId((java.lang.Integer)100);
    java.lang.String var36 = var33.getStatus();
    java.util.Collection var37 = var33.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var41 = var39.equals((java.lang.Object)false);
    boolean var42 = var33.equals((java.lang.Object)var39);
    java.lang.String var43 = var39.getStatus();
    java.lang.Integer var44 = var39.getId();
    boolean var45 = var11.equals((java.lang.Object)var39);
    java.lang.String var46 = var11.toString();
    boolean var47 = var2.equals((java.lang.Object)var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + ""+ "'", var29.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + 0+ "'", var44.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);

  }

  public void test61() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test61"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    java.lang.Integer var6 = var1.getId();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    java.lang.Integer var10 = var8.getId();
    java.lang.Integer var11 = var8.getId();
    java.lang.String var12 = var8.getStatus();
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var16.setStatus("");
    java.lang.Integer var19 = var16.getId();
    java.lang.String var20 = var16.getStatus();
    java.lang.String var21 = var16.toString();
    java.lang.Integer var22 = var16.getId();
    java.lang.String var23 = var16.getStatus();
    boolean var24 = var8.equals((java.lang.Object)var23);
    boolean var25 = var1.equals((java.lang.Object)var23);
    java.util.Collection var26 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 1+ "'", var6.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + ""+ "'", var23.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);

  }

  public void test62() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test62"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    var1.setId((java.lang.Integer)10);
    java.util.Collection var9 = var1.getPropostaCollection();
    java.util.Collection var10 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test63() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test63"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var15 = var1.getId();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 0+ "'", var15.equals(0));

  }

  public void test64() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test64"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.lang.String var5 = var1.getStatus();
    java.lang.Integer var6 = var1.getId();
    java.lang.String var7 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test65() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test65"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test66() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test66"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var5 = var2.getStatus();
    java.lang.Integer var6 = var2.getId();
    var2.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));

  }

  public void test67() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test67"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var9.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    java.lang.String var17 = var12.toString();
    boolean var18 = var9.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var22 = var12.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var23 = var1.equals((java.lang.Object)var12);
    java.lang.String var24 = var12.toString();
    var12.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test68() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test68"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    boolean var6 = var1.equals((java.lang.Object)(byte)100);
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test69() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test69"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.getStatus();
    var2.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "hi!"+ "'", var8.equals("hi!"));

  }

  public void test70() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test70"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.lang.String var9 = var1.toString();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    var11.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.util.Collection var23 = var19.getPropostaCollection();
    boolean var24 = var11.equals((java.lang.Object)var19);
    var11.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var33 = var32.getId();
    boolean var34 = var28.equals((java.lang.Object)var32);
    java.lang.String var35 = var28.getStatus();
    java.lang.String var36 = var28.getStatus();
    boolean var37 = var11.equals((java.lang.Object)var28);
    java.lang.String var38 = var28.getStatus();
    boolean var39 = var1.equals((java.lang.Object)var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 100+ "'", var33.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == true);

  }

  public void test71() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test71"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    java.lang.String var5 = var2.getStatus();
    var2.setId((java.lang.Integer)0);
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test72() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test72"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.Integer var8 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var11 = var1.getPropostaCollection();
    java.lang.Integer var12 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));

  }

  public void test73() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test73"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    boolean var6 = var1.equals((java.lang.Object)(byte)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var9 = var1.getStatus();
    java.lang.Integer var10 = var1.getId();
    java.lang.String var11 = var1.toString();
    java.util.Collection var12 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test74() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test74"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.getStatus();
    java.lang.Integer var9 = var2.getId();
    java.lang.String var10 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 10+ "'", var9.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test75() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test75"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.util.Collection var28 = var1.getPropostaCollection();
    java.lang.String var29 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "hi!"+ "'", var29.equals("hi!"));

  }

  public void test76() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test76"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.util.Collection var23 = var1.getPropostaCollection();
    java.util.Collection var24 = var1.getPropostaCollection();
    java.lang.String var25 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "hi!"+ "'", var25.equals("hi!"));

  }

  public void test77() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test77"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var15 = var14.getId();
    java.lang.Integer var16 = var14.getId();
    java.lang.Integer var17 = var14.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var21 = var19.equals((java.lang.Object)false);
    java.lang.String var22 = var19.toString();
    java.lang.String var23 = var19.getStatus();
    boolean var24 = var14.equals((java.lang.Object)var19);
    boolean var25 = var1.equals((java.lang.Object)var14);
    boolean var27 = var14.equals((java.lang.Object)10);
    java.lang.String var28 = var14.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test78() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test78"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    boolean var12 = var9.equals((java.lang.Object)(short)0);
    var9.setStatus("");
    boolean var15 = var1.equals((java.lang.Object)var9);
    java.lang.Integer var16 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));

  }

  public void test79() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test79"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var6 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test80() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test80"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var9.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    java.lang.String var17 = var12.toString();
    boolean var18 = var9.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var22 = var12.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var23 = var1.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var30 = var29.getId();
    boolean var31 = var25.equals((java.lang.Object)var29);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var33.setId((java.lang.Integer)10);
    java.lang.String var36 = var33.toString();
    boolean var38 = var33.equals((java.lang.Object)(byte)100);
    var33.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var41 = var25.equals((java.lang.Object)var33);
    java.lang.String var42 = var25.toString();
    boolean var43 = var12.equals((java.lang.Object)var25);
    java.util.Collection var44 = var12.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + 100+ "'", var30.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var42.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);

  }

  public void test81() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test81"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test82() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test82"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.Integer var6 = var1.getId();
    java.lang.Integer var7 = var1.getId();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var12 = var9.toString();
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var15 = var9.getStatus();
    var9.setId((java.lang.Integer)(-1));
    boolean var18 = var1.equals((java.lang.Object)(-1));
    java.lang.String var19 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));

  }

  public void test83() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test83"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var13 = var12.getId();
    java.util.Collection var14 = var12.getPropostaCollection();
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var17 = var12.getId();
    java.lang.String var18 = var12.toString();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var21.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var26 = var25.getId();
    java.util.Collection var27 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)10);
    var25.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var33.setStatus("");
    java.lang.Integer var36 = var33.getId();
    java.util.Collection var37 = var33.getPropostaCollection();
    boolean var38 = var25.equals((java.lang.Object)var33);
    var25.setId((java.lang.Integer)10);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var43 = var21.equals((java.lang.Object)var25);
    java.lang.Integer var44 = var21.getId();
    boolean var45 = var12.equals((java.lang.Object)var21);
    boolean var46 = var1.equals((java.lang.Object)var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + (-1)+ "'", var36.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + 1+ "'", var44.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);

  }

  public void test84() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test84"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    var7.setId((java.lang.Integer)(-1));
    boolean var12 = var7.equals((java.lang.Object)(short)(-1));
    var7.setId((java.lang.Integer)10);
    java.lang.String var15 = var7.getStatus();
    boolean var16 = var1.equals((java.lang.Object)var7);
    var7.setId((java.lang.Integer)100);
    java.lang.String var19 = var7.getStatus();
    java.util.Collection var20 = var7.getPropostaCollection();
    var7.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);

  }

  public void test85() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test85"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.Integer var8 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var11 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test86() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test86"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    java.lang.String var9 = var6.toString();
    java.lang.String var10 = var6.getStatus();
    boolean var11 = var1.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    boolean var15 = var1.equals((java.lang.Object)0);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var17.setId((java.lang.Integer)10);
    java.lang.String var20 = var17.toString();
    boolean var21 = var1.equals((java.lang.Object)var17);
    java.lang.Integer var22 = var17.getId();
    java.lang.String var23 = var17.toString();
    java.lang.Integer var24 = var17.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 10+ "'", var22.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 10+ "'", var24.equals(10));

  }

  public void test87() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test87"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    var5.setId((java.lang.Integer)(-1));
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var14 = var5.toString();
    boolean var15 = var1.equals((java.lang.Object)var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);

  }

  public void test88() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test88"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.util.Collection var9 = var7.getPropostaCollection();
    var7.setId((java.lang.Integer)10);
    java.lang.String var12 = var7.toString();
    java.lang.String var13 = var7.getStatus();
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)1);
    java.lang.String var20 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test89() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test89"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var6.setId((java.lang.Integer)0);
    boolean var9 = var1.equals((java.lang.Object)0);
    java.lang.String var10 = var1.getStatus();
    java.lang.String var11 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test90() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test90"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var12 = var11.toString();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("");
    java.lang.Integer var17 = var14.getId();
    java.util.Collection var18 = var14.getPropostaCollection();
    java.lang.String var19 = var14.getStatus();
    boolean var20 = var11.equals((java.lang.Object)var14);
    boolean var21 = var1.equals((java.lang.Object)var11);
    java.util.Collection var22 = var1.getPropostaCollection();
    java.util.Collection var23 = var1.getPropostaCollection();
    java.lang.Integer var24 = var1.getId();
    java.lang.String var25 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 0+ "'", var24.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test91() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test91"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var11 = var10.getId();
    boolean var12 = var1.equals((java.lang.Object)var11);
    java.lang.String var13 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 100+ "'", var11.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test92() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test92"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.Integer var6 = var1.getId();
    var1.setStatus("hi!");
    java.lang.Integer var9 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));

  }

  public void test93() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test93"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    java.lang.Integer var9 = var1.getId();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    var11.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.util.Collection var23 = var19.getPropostaCollection();
    boolean var24 = var11.equals((java.lang.Object)var19);
    var11.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var33 = var32.getId();
    boolean var34 = var28.equals((java.lang.Object)var32);
    java.lang.String var35 = var28.getStatus();
    java.lang.String var36 = var28.getStatus();
    boolean var37 = var11.equals((java.lang.Object)var28);
    boolean var38 = var1.equals((java.lang.Object)var37);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var41 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 100+ "'", var33.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + (-1)+ "'", var41.equals((-1)));

  }

  public void test94() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test94"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    var1.setId((java.lang.Integer)100);
    java.lang.String var6 = var1.getStatus();
    java.util.Collection var7 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test95() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test95"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var15 = var1.getId();
    java.util.Collection var16 = var1.getPropostaCollection();
    java.util.Collection var17 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 10+ "'", var15.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test96() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test96"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var9 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test97() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test97"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.util.Collection var10 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test98() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test98"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.Integer var5 = var1.getId();
    boolean var7 = var1.equals((java.lang.Object)'#');
    java.lang.Integer var8 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 100+ "'", var8.equals(100));

  }

  public void test99() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test99"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var9 = var7.equals((java.lang.Object)false);
    boolean var10 = var1.equals((java.lang.Object)var7);
    java.lang.String var11 = var7.getStatus();
    java.lang.String var12 = var7.getStatus();
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test100() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test100"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    var1.setStatus("hi!");
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    java.lang.String var8 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "hi!"+ "'", var7.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "hi!"+ "'", var8.equals("hi!"));

  }

  public void test101() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test101"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var5.setStatus("");
    java.lang.Integer var8 = var5.getId();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.String var10 = var5.toString();
    boolean var11 = var2.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var5.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var16 = var5.toString();
    java.lang.String var17 = var5.getStatus();
    var5.setStatus("hi!");
    java.util.Collection var20 = var5.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);

  }

  public void test102() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test102"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    java.util.Collection var4 = var2.getPropostaCollection();
    java.lang.String var5 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test103() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test103"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    java.lang.Integer var6 = var1.getId();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    java.lang.Integer var10 = var8.getId();
    java.lang.Integer var11 = var8.getId();
    java.lang.String var12 = var8.getStatus();
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var16.setStatus("");
    java.lang.Integer var19 = var16.getId();
    java.lang.String var20 = var16.getStatus();
    java.lang.String var21 = var16.toString();
    java.lang.Integer var22 = var16.getId();
    java.lang.String var23 = var16.getStatus();
    boolean var24 = var8.equals((java.lang.Object)var23);
    boolean var25 = var1.equals((java.lang.Object)var23);
    java.lang.Integer var26 = var1.getId();
    java.lang.String var27 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 1+ "'", var6.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + ""+ "'", var23.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 1+ "'", var26.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test104() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test104"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var8 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test105() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test105"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.toString();
    boolean var23 = var18.equals((java.lang.Object)(byte)100);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var26 = var10.equals((java.lang.Object)var18);
    boolean var27 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    var29.setId((java.lang.Integer)(-1));
    boolean var34 = var29.equals((java.lang.Object)'4');
    boolean var35 = var1.equals((java.lang.Object)var29);
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);

  }

  public void test106() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test106"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var6 = var4.equals((java.lang.Object)false);
    boolean var8 = var4.equals((java.lang.Object)10.0d);
    boolean var9 = var2.equals((java.lang.Object)var8);
    java.lang.String var10 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var14 = var13.toString();
    var13.setId((java.lang.Integer)(-1));
    boolean var17 = var2.equals((java.lang.Object)var13);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == true);

  }

  public void test107() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test107"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    java.lang.String var7 = var1.getStatus();
    java.lang.String var8 = var1.toString();
    java.lang.String var9 = var1.getStatus();
    java.lang.String var10 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "hi!"+ "'", var7.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test108() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test108"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var5 = var1.getId();
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 0+ "'", var5.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test109() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test109"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test110() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test110"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var2.setId((java.lang.Integer)100);

  }

  public void test111() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test111"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test112() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test112"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var26 = var25.getId();
    java.util.Collection var27 = var25.getPropostaCollection();
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var30 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)(-1));
    boolean var33 = var1.equals((java.lang.Object)var25);
    java.lang.String var34 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test113() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test113"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var11.setId((java.lang.Integer)100);
    boolean var16 = var1.equals((java.lang.Object)100);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var19 = var18.getPropostaCollection();
    java.lang.Integer var20 = var18.getId();
    boolean var22 = var18.equals((java.lang.Object)'4');
    var18.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=null ]");
    var27.setId((java.lang.Integer)0);
    boolean var30 = var18.equals((java.lang.Object)var27);
    boolean var31 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var34 = var33.getId();
    java.util.Collection var35 = var33.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var38 = var37.getId();
    java.util.Collection var39 = var37.getPropostaCollection();
    var37.setId((java.lang.Integer)10);
    java.lang.String var42 = var37.toString();
    java.lang.String var43 = var37.getStatus();
    boolean var44 = var33.equals((java.lang.Object)var37);
    var33.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var33.setStatus("");
    boolean var49 = var1.equals((java.lang.Object)var33);
    loja.com.br.entity.PropostaStatus var52 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var52.setId((java.lang.Integer)(-1));
    var52.setId((java.lang.Integer)100);
    boolean var57 = var33.equals((java.lang.Object)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 1+ "'", var20.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + (-1)+ "'", var34.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + (-1)+ "'", var38.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var42.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);

  }

  public void test114() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test114"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test115() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test115"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    var1.setStatus("");
    java.lang.String var20 = var1.getStatus();
    java.lang.String var21 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + ""+ "'", var21.equals(""));

  }

  public void test116() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test116"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var4 = var1.getId();
    java.lang.Integer var5 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 10+ "'", var4.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));

  }

  public void test117() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test117"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    var1.setId((java.lang.Integer)0);
    java.lang.String var10 = var1.toString();
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test118() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test118"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var5.getStatus();
    java.lang.Integer var11 = var5.getId();
    java.lang.String var12 = var5.toString();
    boolean var13 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var14 = var5.getId();
    var5.setId((java.lang.Integer)0);
    java.lang.Integer var17 = var5.getId();
    java.lang.Integer var18 = var5.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 0+ "'", var17.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 0+ "'", var18.equals(0));

  }

  public void test119() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test119"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test120() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test120"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    boolean var9 = var1.equals((java.lang.Object)1.0f);
    var1.setId((java.lang.Integer)100);
    java.lang.String var12 = var1.getStatus();
    java.util.Collection var13 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);

  }

  public void test121() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test121"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    boolean var7 = var4.equals((java.lang.Object)(short)0);
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.Integer var9 = var4.getId();
    boolean var10 = var2.equals((java.lang.Object)var4);
    java.lang.Integer var11 = var4.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));

  }

  public void test122() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test122"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    java.util.Collection var3 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var6 = var2.getId();
    java.util.Collection var7 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test123() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test123"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.String var5 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.String var10 = var9.toString();
    boolean var11 = var1.equals((java.lang.Object)var10);
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test124() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test124"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.lang.Integer var5 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var8 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test125() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test125"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.Integer var5 = var1.getId();
    boolean var7 = var1.equals((java.lang.Object)'#');
    java.lang.String var8 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test126() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test126"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    java.lang.String var9 = var1.getStatus();
    java.lang.String var10 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test127() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test127"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var3 = var2.getId();
    java.lang.Integer var4 = var2.getId();
    var2.setId((java.lang.Integer)0);
    java.util.Collection var7 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 1+ "'", var4.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test128() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test128"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var7 = var6.getId();
    java.lang.Integer var8 = var6.getId();
    java.util.Collection var9 = var6.getPropostaCollection();
    java.lang.String var10 = var6.toString();
    boolean var11 = var1.equals((java.lang.Object)var6);
    java.lang.String var12 = var6.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 1+ "'", var7.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test129() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test129"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var10 = var1.equals((java.lang.Object)100);
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var14 = var13.getId();
    java.lang.String var15 = var13.getStatus();
    boolean var16 = var1.equals((java.lang.Object)var15);
    java.util.Collection var17 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var19 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)100);
    java.lang.String var22 = var18.getStatus();
    java.lang.Integer var23 = var18.getId();
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var28 = var26.equals((java.lang.Object)(-1.0d));
    java.util.Collection var29 = var26.getPropostaCollection();
    java.lang.Integer var30 = var26.getId();
    boolean var31 = var18.equals((java.lang.Object)var26);
    boolean var32 = var1.equals((java.lang.Object)var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + 100+ "'", var30.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);

  }

  public void test130() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test130"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var6.setId((java.lang.Integer)0);
    boolean var9 = var1.equals((java.lang.Object)0);
    java.lang.String var10 = var1.getStatus();
    var1.setStatus("");
    java.lang.String var13 = var1.getStatus();
    java.lang.Integer var14 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + ""+ "'", var13.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));

  }

  public void test131() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test131"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    boolean var12 = var9.equals((java.lang.Object)(short)0);
    var9.setStatus("");
    boolean var15 = var1.equals((java.lang.Object)var9);
    java.util.Collection var16 = var9.getPropostaCollection();
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var25 = var24.getId();
    boolean var26 = var20.equals((java.lang.Object)var24);
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var28.setId((java.lang.Integer)10);
    java.lang.String var31 = var28.toString();
    boolean var33 = var28.equals((java.lang.Object)(byte)100);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var36 = var20.equals((java.lang.Object)var28);
    var20.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var41 = var40.toString();
    loja.com.br.entity.PropostaStatus var43 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var43.setStatus("");
    java.lang.Integer var46 = var43.getId();
    java.util.Collection var47 = var43.getPropostaCollection();
    java.lang.String var48 = var43.getStatus();
    boolean var49 = var40.equals((java.lang.Object)var43);
    var43.setStatus("");
    loja.com.br.entity.PropostaStatus var53 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var54 = var53.getStatus();
    var53.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var57 = var43.equals((java.lang.Object)var53);
    boolean var58 = var20.equals((java.lang.Object)var53);
    boolean var59 = var9.equals((java.lang.Object)var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var31.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + (-1)+ "'", var46.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + ""+ "'", var48.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var59 == false);

  }

  public void test132() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test132"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var8.setStatus("");
    boolean var12 = var8.equals((java.lang.Object)"hi!");
    java.lang.String var13 = var8.getStatus();
    java.lang.String var14 = var8.getStatus();
    java.lang.Integer var15 = var8.getId();
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    java.util.Collection var19 = var17.getPropostaCollection();
    var17.setId((java.lang.Integer)10);
    var17.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var25.setStatus("");
    java.lang.Integer var28 = var25.getId();
    java.util.Collection var29 = var25.getPropostaCollection();
    boolean var30 = var17.equals((java.lang.Object)var25);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var33 = var25.toString();
    var25.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var37.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var41 = var37.equals((java.lang.Object)false);
    boolean var42 = var25.equals((java.lang.Object)var37);
    boolean var43 = var8.equals((java.lang.Object)var25);
    java.lang.Integer var44 = var8.getId();
    boolean var45 = var1.equals((java.lang.Object)var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + ""+ "'", var13.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + ""+ "'", var14.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + (-1)+ "'", var44.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);

  }

  public void test133() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test133"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test134() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test134"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test135() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test135"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var8 = var1.toString();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test136() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test136"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test137() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test137"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setId((java.lang.Integer)10);
    java.util.Collection var6 = var2.getPropostaCollection();
    java.util.Collection var7 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test138() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test138"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    var1.setStatus("");
    java.lang.String var9 = var1.getStatus();
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));

  }

  public void test139() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test139"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var5 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test140() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test140"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setId((java.lang.Integer)100);
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.String var6 = var2.toString();
    java.lang.String var7 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test141() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test141"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setId((java.lang.Integer)(-1));
    java.lang.String var6 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test142() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test142"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);

  }

  public void test143() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test143"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var5 = var1.equals((java.lang.Object)false);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)100);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var15 = var14.toString();
    var14.setId((java.lang.Integer)1);
    boolean var18 = var1.equals((java.lang.Object)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);

  }

  public void test144() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test144"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)(-1));
    java.lang.Integer var9 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));

  }

  public void test145() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test145"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "hi!");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var5 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 1+ "'", var5.equals(1));

  }

  public void test146() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test146"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    java.lang.String var7 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var15 = var10.getId();
    java.lang.String var16 = var10.getStatus();
    java.lang.Integer var17 = var10.getId();
    boolean var18 = var1.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var20.setId((java.lang.Integer)100);
    java.lang.String var23 = var20.getStatus();
    var20.setId((java.lang.Integer)10);
    java.lang.Integer var26 = var20.getId();
    java.lang.String var27 = var20.toString();
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var20.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var34 = var33.getId();
    java.lang.Integer var35 = var33.getId();
    java.lang.Integer var36 = var33.getId();
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var40 = var38.equals((java.lang.Object)false);
    java.lang.String var41 = var38.toString();
    java.lang.String var42 = var38.getStatus();
    boolean var43 = var33.equals((java.lang.Object)var38);
    boolean var44 = var20.equals((java.lang.Object)var33);
    boolean var45 = var10.equals((java.lang.Object)var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "hi!"+ "'", var7.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 10+ "'", var15.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 10+ "'", var17.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + (-1)+ "'", var34.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + (-1)+ "'", var35.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + (-1)+ "'", var36.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);

  }

  public void test147() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test147"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    java.lang.String var9 = var6.toString();
    java.lang.String var10 = var6.getStatus();
    boolean var11 = var1.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    boolean var15 = var1.equals((java.lang.Object)0);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var17.setId((java.lang.Integer)10);
    java.lang.String var20 = var17.toString();
    boolean var21 = var1.equals((java.lang.Object)var17);
    java.lang.String var22 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test148() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test148"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test149() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test149"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    java.lang.Integer var4 = var2.getId();
    var2.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 10+ "'", var4.equals(10));

  }

  public void test150() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test150"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    boolean var9 = var1.equals((java.lang.Object)100);
    boolean var11 = var1.equals((java.lang.Object)(byte)1);
    java.lang.Integer var12 = var1.getId();
    java.lang.String var13 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test151() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test151"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    java.util.Collection var4 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);

  }

  public void test152() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test152"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setId((java.lang.Integer)0);
    boolean var13 = var1.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var15.setStatus("");
    boolean var19 = var15.equals((java.lang.Object)"hi!");
    java.lang.String var20 = var15.getStatus();
    var15.setId((java.lang.Integer)100);
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var25 = var15.getStatus();
    boolean var26 = var10.equals((java.lang.Object)var15);
    java.lang.String var27 = var10.getStatus();
    java.lang.String var28 = var10.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test153() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test153"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var6.setId((java.lang.Integer)0);
    boolean var9 = var1.equals((java.lang.Object)0);
    java.lang.String var10 = var1.getStatus();
    java.util.Collection var11 = var1.getPropostaCollection();
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + ""+ "'", var12.equals(""));

  }

  public void test154() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test154"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);

  }

  public void test155() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test155"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var8 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var14 = var11.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    java.util.Collection var16 = var1.getPropostaCollection();
    java.lang.Integer var17 = var1.getId();
    java.lang.Integer var18 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 0+ "'", var17.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 0+ "'", var18.equals(0));

  }

  public void test156() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test156"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var9 = var1.getId();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var14 = var11.toString();
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var11.setId((java.lang.Integer)1);
    java.util.Collection var19 = var11.getPropostaCollection();
    java.util.Collection var20 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var11);
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == true);

  }

  public void test157() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test157"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.Integer var9 = var1.getId();
    java.lang.Integer var10 = var1.getId();
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 10+ "'", var9.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test158() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test158"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var3 = var2.getStatus();
    java.lang.String var4 = var2.getStatus();
    java.lang.Integer var5 = var2.getId();
    java.lang.String var6 = var2.getStatus();
    java.util.Collection var7 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "hi!"+ "'", var3.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "hi!"+ "'", var4.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "hi!"+ "'", var6.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test159() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test159"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.String var8 = var1.getStatus();
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var11.setId((java.lang.Integer)10);
    var11.setStatus("");
    java.lang.Integer var16 = var11.getId();
    java.util.Collection var17 = var11.getPropostaCollection();
    java.lang.Integer var18 = var11.getId();
    boolean var19 = var1.equals((java.lang.Object)var11);
    java.lang.String var20 = var11.getStatus();
    java.util.Collection var21 = var11.getPropostaCollection();
    java.lang.String var22 = var11.getStatus();
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 10+ "'", var18.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + ""+ "'", var22.equals(""));

  }

  public void test160() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test160"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setStatus("");
    java.lang.Integer var9 = var6.getId();
    java.lang.String var10 = var6.getStatus();
    java.lang.String var11 = var6.toString();
    var6.setStatus("");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var16.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var16.setId((java.lang.Integer)100);
    boolean var21 = var6.equals((java.lang.Object)100);
    java.lang.Integer var22 = var6.getId();
    boolean var23 = var1.equals((java.lang.Object)var6);
    java.util.Collection var24 = var6.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);

  }

  public void test161() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test161"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var11 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));

  }

  public void test162() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test162"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var4 = var3.getPropostaCollection();
    var3.setId((java.lang.Integer)100);
    java.lang.Integer var7 = var3.getId();
    boolean var8 = var2.equals((java.lang.Object)var3);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var11.setId((java.lang.Integer)(-1));
    var11.setStatus("hi!");
    boolean var16 = var3.equals((java.lang.Object)"hi!");
    java.lang.String var17 = var3.getStatus();
    var3.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test163() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test163"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    java.lang.Integer var10 = var1.getId();
    java.lang.Integer var11 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));

  }

  public void test164() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test164"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    java.lang.String var9 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var12 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test165() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test165"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setId((java.lang.Integer)10);
    var14.setStatus("");
    java.lang.Integer var19 = var14.getId();
    java.util.Collection var20 = var14.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var23 = var22.getId();
    boolean var25 = var22.equals((java.lang.Object)(short)0);
    var22.setStatus("");
    boolean var28 = var14.equals((java.lang.Object)var22);
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var31 = var30.getId();
    boolean var33 = var30.equals((java.lang.Object)(short)0);
    java.util.Collection var34 = var30.getPropostaCollection();
    java.lang.Integer var35 = var30.getId();
    boolean var36 = var14.equals((java.lang.Object)var30);
    java.lang.Integer var37 = var30.getId();
    boolean var38 = var1.equals((java.lang.Object)var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + (-1)+ "'", var31.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + (-1)+ "'", var35.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + (-1)+ "'", var37.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);

  }

  public void test166() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test166"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var8 = var1.getId();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    java.lang.String var17 = var10.getStatus();
    java.util.Collection var18 = var10.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var20.setId((java.lang.Integer)10);
    var20.setStatus("");
    java.lang.Integer var25 = var20.getId();
    java.util.Collection var26 = var20.getPropostaCollection();
    java.lang.Integer var27 = var20.getId();
    boolean var28 = var10.equals((java.lang.Object)var20);
    java.lang.String var29 = var20.getStatus();
    boolean var30 = var1.equals((java.lang.Object)var20);
    var20.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 10+ "'", var25.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 10+ "'", var27.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + ""+ "'", var29.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);

  }

  public void test167() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test167"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.String var6 = var2.toString();
    java.lang.String var7 = var2.toString();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var14 = var13.getId();
    boolean var15 = var9.equals((java.lang.Object)var13);
    java.lang.Integer var16 = var9.getId();
    java.lang.Integer var17 = var9.getId();
    java.lang.String var18 = var9.getStatus();
    boolean var19 = var2.equals((java.lang.Object)var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 1+ "'", var16.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 1+ "'", var17.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);

  }

  public void test168() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test168"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test169() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test169"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var4 = var3.getPropostaCollection();
    var3.setId((java.lang.Integer)100);
    java.lang.Integer var7 = var3.getId();
    boolean var8 = var2.equals((java.lang.Object)var3);
    java.lang.Integer var9 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));

  }

  public void test170() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test170"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.util.Collection var8 = var1.getPropostaCollection();
    var1.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test171() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test171"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var2.setId((java.lang.Integer)0);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var7 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 0+ "'", var7.equals(0));

  }

  public void test172() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test172"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var1 = var0.getPropostaCollection();
    var0.setId((java.lang.Integer)100);
    java.lang.String var4 = var0.getStatus();
    java.lang.Integer var5 = var0.getId();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var10 = var8.equals((java.lang.Object)(-1.0d));
    java.util.Collection var11 = var8.getPropostaCollection();
    java.lang.Integer var12 = var8.getId();
    boolean var13 = var0.equals((java.lang.Object)var8);
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var16 = var8.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);

  }

  public void test173() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test173"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    boolean var14 = var11.equals((java.lang.Object)(short)0);
    var11.setStatus("");
    boolean var17 = var1.equals((java.lang.Object)"");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);

  }

  public void test174() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test174"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.Integer var9 = var1.getId();
    var1.setStatus("hi!");
    java.util.Collection var12 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var15 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 10+ "'", var9.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test175() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test175"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test176() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test176"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var12 = var8.equals((java.lang.Object)false);
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var15 = var8.toString();
    boolean var16 = var1.equals((java.lang.Object)var8);
    java.util.Collection var17 = var8.getPropostaCollection();
    java.lang.Integer var18 = var8.getId();
    java.lang.String var19 = var8.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test177() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test177"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var9 = var1.getId();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    java.lang.String var16 = var11.toString();
    java.lang.String var17 = var11.getStatus();
    var11.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var22 = var21.toString();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var24.setStatus("");
    java.lang.Integer var27 = var24.getId();
    java.util.Collection var28 = var24.getPropostaCollection();
    java.lang.String var29 = var24.getStatus();
    boolean var30 = var21.equals((java.lang.Object)var24);
    boolean var31 = var11.equals((java.lang.Object)var21);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var33.setId((java.lang.Integer)100);
    java.lang.String var36 = var33.getStatus();
    java.util.Collection var37 = var33.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var41 = var39.equals((java.lang.Object)false);
    boolean var42 = var33.equals((java.lang.Object)var39);
    java.lang.String var43 = var39.getStatus();
    java.lang.Integer var44 = var39.getId();
    boolean var45 = var11.equals((java.lang.Object)var39);
    java.lang.Integer var46 = var39.getId();
    java.lang.Integer var47 = var39.getId();
    boolean var48 = var1.equals((java.lang.Object)var39);
    var39.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + ""+ "'", var29.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + 0+ "'", var44.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + 0+ "'", var46.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + 0+ "'", var47.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == true);

  }

  public void test178() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test178"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var7 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test179() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test179"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");

  }

  public void test180() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test180"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 1+ "'", var2.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));

  }

  public void test181() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test181"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var1 = var0.getPropostaCollection();
    var0.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.Integer var11 = var5.getId();
    boolean var13 = var5.equals((java.lang.Object)100);
    var5.setId((java.lang.Integer)0);
    boolean var16 = var0.equals((java.lang.Object)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);

  }

  public void test182() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test182"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var19 = var18.getId();
    boolean var20 = var13.equals((java.lang.Object)var19);
    boolean var21 = var1.equals((java.lang.Object)var13);
    var13.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == true);

  }

  public void test183() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test183"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var9 = var8.getPropostaCollection();
    var8.setId((java.lang.Integer)100);
    java.lang.Integer var12 = var8.getId();
    boolean var13 = var7.equals((java.lang.Object)var8);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var16.setId((java.lang.Integer)(-1));
    var16.setStatus("hi!");
    boolean var21 = var8.equals((java.lang.Object)"hi!");
    boolean var22 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var23 = var1.toString();
    java.lang.String var24 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test184() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test184"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var7 = var1.getStatus();
    java.lang.String var8 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test185() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test185"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.String var12 = var11.toString();
    boolean var13 = var1.equals((java.lang.Object)var11);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var16 = var11.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));

  }

  public void test186() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test186"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var18 = var15.getStatus();
    java.lang.Integer var19 = var15.getId();
    boolean var20 = var1.equals((java.lang.Object)var15);
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var22.setId((java.lang.Integer)100);
    java.lang.String var25 = var22.getStatus();
    java.lang.String var26 = var22.getStatus();
    var22.setStatus("");
    boolean var29 = var15.equals((java.lang.Object)"");
    java.util.Collection var30 = var15.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 0+ "'", var19.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);

  }

  public void test187() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test187"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    java.util.Collection var4 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setId((java.lang.Integer)10);
    var6.setStatus("");
    java.lang.Integer var11 = var6.getId();
    java.util.Collection var12 = var6.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var15 = var14.getId();
    boolean var17 = var14.equals((java.lang.Object)(short)0);
    var14.setStatus("");
    boolean var20 = var6.equals((java.lang.Object)var14);
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var23 = var22.getId();
    boolean var25 = var22.equals((java.lang.Object)(short)0);
    java.util.Collection var26 = var22.getPropostaCollection();
    java.lang.Integer var27 = var22.getId();
    boolean var28 = var6.equals((java.lang.Object)var22);
    java.lang.Integer var29 = var22.getId();
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var32 = var2.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + (-1)+ "'", var29.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);

  }

  public void test188() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test188"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.lang.Integer var7 = var5.getId();
    java.lang.Integer var8 = var5.getId();
    var5.setId((java.lang.Integer)10);
    boolean var11 = var2.equals((java.lang.Object)var5);
    java.lang.Integer var12 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));

  }

  public void test189() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test189"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setId((java.lang.Integer)0);
    boolean var13 = var1.equals((java.lang.Object)var10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);

  }

  public void test190() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test190"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var3 = var2.getId();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setId((java.lang.Integer)100);
    java.lang.String var8 = var5.getStatus();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.Integer var10 = var5.getId();
    java.lang.String var11 = var5.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var14 = var13.getPropostaCollection();
    java.lang.String var15 = var13.getStatus();
    boolean var16 = var5.equals((java.lang.Object)var13);
    boolean var17 = var2.equals((java.lang.Object)var16);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 100+ "'", var10.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);

  }

  public void test191() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test191"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    var11.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.util.Collection var23 = var19.getPropostaCollection();
    boolean var24 = var11.equals((java.lang.Object)var19);
    var11.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var30 = var29.toString();
    java.lang.Integer var31 = var29.getId();
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var34 = var29.toString();
    boolean var35 = var11.equals((java.lang.Object)var29);
    boolean var36 = var1.equals((java.lang.Object)var11);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + 10+ "'", var31.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == true);

  }

  public void test192() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test192"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setStatus("");
    java.lang.Integer var9 = var6.getId();
    java.lang.String var10 = var6.getStatus();
    java.lang.String var11 = var6.toString();
    var6.setStatus("");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var16.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var16.setId((java.lang.Integer)100);
    boolean var21 = var6.equals((java.lang.Object)100);
    java.lang.Integer var22 = var6.getId();
    boolean var23 = var1.equals((java.lang.Object)var6);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);

  }

  public void test193() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test193"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.String var6 = var1.toString();
    var1.setStatus("");
    java.lang.Integer var9 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));

  }

  public void test194() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test194"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.util.Collection var2 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var5 = var1.getPropostaCollection();
    java.util.Collection var6 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);

  }

  public void test195() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test195"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=1 ]");
    var2.setId((java.lang.Integer)0);
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.Integer var6 = var2.getId();
    java.util.Collection var7 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test196() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test196"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    boolean var9 = var1.equals((java.lang.Object)1.0f);
    var1.setId((java.lang.Integer)100);
    java.lang.Integer var12 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));

  }

  public void test197() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test197"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setId((java.lang.Integer)10);
    var6.setStatus("");
    java.lang.Integer var11 = var6.getId();
    java.util.Collection var12 = var6.getPropostaCollection();
    boolean var13 = var2.equals((java.lang.Object)var6);
    java.util.Collection var14 = var2.getPropostaCollection();
    java.lang.String var15 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "hi!"+ "'", var15.equals("hi!"));

  }

  public void test198() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test198"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test199() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test199"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var9 = var1.getId();
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));

  }

  public void test200() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test200"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.Integer var6 = var1.getId();
    java.lang.String var7 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test201() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test201"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)(-1));
    java.util.Collection var12 = var8.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var19 = var18.getId();
    boolean var20 = var14.equals((java.lang.Object)var18);
    java.lang.Integer var21 = var14.getId();
    boolean var22 = var8.equals((java.lang.Object)var21);
    var8.setId((java.lang.Integer)100);
    boolean var25 = var1.equals((java.lang.Object)100);
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 1+ "'", var21.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);

  }

  public void test202() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test202"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var6.setId((java.lang.Integer)0);
    boolean var9 = var1.equals((java.lang.Object)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var12 = var1.getStatus();
    java.lang.String var13 = var1.toString();
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "hi!");
    var16.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var20.setStatus("");
    java.lang.Integer var23 = var20.getId();
    java.lang.String var24 = var20.getStatus();
    java.lang.String var25 = var20.toString();
    var20.setStatus("");
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var30.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var30.setId((java.lang.Integer)100);
    boolean var35 = var20.equals((java.lang.Object)100);
    boolean var36 = var16.equals((java.lang.Object)var20);
    boolean var37 = var1.equals((java.lang.Object)var36);
    java.lang.String var38 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + ""+ "'", var24.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test203() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test203"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    boolean var9 = var1.equals((java.lang.Object)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var12 = var1.getStatus();
    java.util.Collection var13 = var1.getPropostaCollection();
    java.util.Collection var14 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var18 = var17.toString();
    java.lang.Integer var19 = var17.getId();
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);

  }

  public void test204() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test204"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    boolean var17 = var11.equals((java.lang.Object)var15);
    java.util.Collection var18 = var15.getPropostaCollection();
    java.util.Collection var19 = var15.getPropostaCollection();
    boolean var20 = var1.equals((java.lang.Object)var15);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);

  }

  public void test205() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test205"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.String var23 = var21.getStatus();
    java.util.Collection var24 = var21.getPropostaCollection();
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var27 = var21.getId();
    java.lang.String var28 = var21.getStatus();
    java.lang.Integer var29 = var21.getId();
    java.util.Collection var30 = var21.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + (-1)+ "'", var29.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);

  }

  public void test206() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test206"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setId((java.lang.Integer)100);
    java.lang.String var10 = var7.getStatus();
    var7.setId((java.lang.Integer)10);
    java.lang.String var13 = var7.toString();
    java.util.Collection var14 = var7.getPropostaCollection();
    boolean var15 = var1.equals((java.lang.Object)var7);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);

  }

  public void test207() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test207"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var20 = var19.getId();
    var19.setId((java.lang.Integer)(-1));
    java.util.Collection var23 = var19.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var30 = var29.getId();
    boolean var31 = var25.equals((java.lang.Object)var29);
    java.lang.Integer var32 = var25.getId();
    boolean var33 = var19.equals((java.lang.Object)var32);
    java.util.Collection var34 = var19.getPropostaCollection();
    boolean var35 = var1.equals((java.lang.Object)var19);
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + 100+ "'", var30.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 1+ "'", var32.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == true);

  }

  public void test208() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test208"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.String var5 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test209() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test209"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    boolean var5 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test210() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test210"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test211() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test211"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var3 = var2.getStatus();
    java.lang.String var4 = var2.getStatus();
    java.lang.Integer var5 = var2.getId();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setId((java.lang.Integer)100);
    java.lang.String var10 = var7.getStatus();
    java.util.Collection var11 = var7.getPropostaCollection();
    java.lang.Integer var12 = var7.getId();
    java.util.Collection var13 = var7.getPropostaCollection();
    boolean var14 = var2.equals((java.lang.Object)var7);
    java.util.Collection var15 = var7.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "hi!"+ "'", var3.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "hi!"+ "'", var4.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test212() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test212"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setId((java.lang.Integer)100);
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.String var6 = var2.toString();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test213() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test213"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test214() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test214"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var6 = var1.getStatus();
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 1+ "'", var7.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test215() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test215"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    boolean var4 = var1.equals((java.lang.Object)(short)0);
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.Integer var6 = var1.getId();
    java.lang.Integer var7 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));

  }

  public void test216() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test216"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);

  }

  public void test217() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test217"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var20 = var19.toString();
    java.lang.Integer var21 = var19.getId();
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var24 = var19.toString();
    boolean var25 = var1.equals((java.lang.Object)var19);
    java.lang.String var26 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 10+ "'", var21.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "hi!"+ "'", var26.equals("hi!"));

  }

  public void test218() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test218"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    java.lang.Integer var9 = var1.getId();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    var11.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.util.Collection var23 = var19.getPropostaCollection();
    boolean var24 = var11.equals((java.lang.Object)var19);
    var11.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var33 = var32.getId();
    boolean var34 = var28.equals((java.lang.Object)var32);
    java.lang.String var35 = var28.getStatus();
    java.lang.String var36 = var28.getStatus();
    boolean var37 = var11.equals((java.lang.Object)var28);
    boolean var38 = var1.equals((java.lang.Object)var37);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var41 = var1.getStatus();
    java.lang.String var42 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 100+ "'", var33.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var42.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test219() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test219"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    java.lang.String var18 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + ""+ "'", var18.equals(""));

  }

  public void test220() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test220"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var6.setId((java.lang.Integer)0);
    boolean var9 = var1.equals((java.lang.Object)0);
    java.lang.String var10 = var1.getStatus();
    var1.setStatus("");
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    java.util.Collection var18 = var16.getPropostaCollection();
    var16.setId((java.lang.Integer)10);
    java.lang.String var21 = var16.toString();
    java.lang.String var22 = var16.getStatus();
    var16.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var27 = var26.toString();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var29.setStatus("");
    java.lang.Integer var32 = var29.getId();
    java.util.Collection var33 = var29.getPropostaCollection();
    java.lang.String var34 = var29.getStatus();
    boolean var35 = var26.equals((java.lang.Object)var29);
    boolean var36 = var16.equals((java.lang.Object)var26);
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var38.setId((java.lang.Integer)100);
    java.lang.String var41 = var38.getStatus();
    java.util.Collection var42 = var38.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var46 = var44.equals((java.lang.Object)false);
    boolean var47 = var38.equals((java.lang.Object)var44);
    java.lang.String var48 = var44.getStatus();
    java.lang.Integer var49 = var44.getId();
    boolean var50 = var16.equals((java.lang.Object)var44);
    boolean var51 = var1.equals((java.lang.Object)var44);
    var44.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + (-1)+ "'", var32.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + ""+ "'", var34.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + 0+ "'", var49.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);

  }

  public void test221() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test221"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var27 = var26.toString();
    boolean var29 = var26.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var30 = var26.getStatus();
    boolean var31 = var1.equals((java.lang.Object)var26);
    var26.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == true);

  }

  public void test222() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test222"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.Integer var7 = var1.getId();
    java.util.Collection var8 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test223() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test223"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var13 = var6.getId();
    boolean var14 = var2.equals((java.lang.Object)var13);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setStatus("hi!");
    java.lang.Integer var19 = var2.getId();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var22 = var21.toString();
    java.lang.String var23 = var21.getStatus();
    var21.setStatus("hi!");
    var21.setStatus("");
    boolean var28 = var2.equals((java.lang.Object)"");
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus();
    java.lang.String var30 = var29.toString();
    java.util.Collection var31 = var29.getPropostaCollection();
    var29.setId((java.lang.Integer)0);
    boolean var34 = var2.equals((java.lang.Object)0);
    var2.setStatus("");
    java.lang.String var37 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 1+ "'", var13.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 1+ "'", var19.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test224() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test224"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.util.Collection var8 = var5.getPropostaCollection();
    java.lang.Integer var9 = var5.getId();
    var5.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));

  }

  public void test225() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test225"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var5.setStatus("");
    java.lang.Integer var8 = var5.getId();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.String var10 = var5.toString();
    boolean var11 = var2.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var5.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var16 = var5.toString();
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test226() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test226"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var2.setId((java.lang.Integer)100);
    java.lang.String var5 = var2.getStatus();
    java.lang.Integer var6 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));

  }

  public void test227() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test227"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.getStatus();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.Integer var9 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));

  }

  public void test228() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test228"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.util.Collection var8 = var5.getPropostaCollection();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.String var10 = var5.getStatus();
    var5.setId((java.lang.Integer)10);
    var5.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test229() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test229"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    var1.setId((java.lang.Integer)100);
    java.lang.Integer var6 = var1.getId();
    java.lang.Integer var7 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));

  }

  public void test230() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test230"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.String var9 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var12 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test231() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test231"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var2.setId((java.lang.Integer)100);
    var2.setId((java.lang.Integer)1);
    var2.setId((java.lang.Integer)1);
    var2.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var20.setId((java.lang.Integer)1);
    java.lang.Integer var23 = var20.getId();
    boolean var24 = var2.equals((java.lang.Object)var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 1+ "'", var23.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);

  }

  public void test232() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test232"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var17 = var9.getPropostaCollection();
    java.util.Collection var18 = var9.getPropostaCollection();
    var9.setStatus("");
    java.lang.String var21 = var9.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test233() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test233"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.getStatus();
    java.lang.String var13 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test234() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test234"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var7 = var6.getId();
    java.lang.Integer var8 = var6.getId();
    java.util.Collection var9 = var6.getPropostaCollection();
    java.lang.String var10 = var6.toString();
    boolean var11 = var1.equals((java.lang.Object)var6);
    var6.setId((java.lang.Integer)100);
    java.lang.String var14 = var6.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 1+ "'", var7.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test235() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test235"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test236() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test236"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    java.lang.String var5 = var2.getStatus();
    java.lang.Integer var6 = var2.getId();
    java.util.Collection var7 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 1+ "'", var6.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test237() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test237"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    java.util.Collection var11 = var9.getPropostaCollection();
    var9.setId((java.lang.Integer)10);
    var9.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var17.setStatus("");
    java.lang.Integer var20 = var17.getId();
    java.util.Collection var21 = var17.getPropostaCollection();
    boolean var22 = var9.equals((java.lang.Object)var17);
    var9.setId((java.lang.Integer)10);
    boolean var26 = var9.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var30 = var9.equals((java.lang.Object)var29);
    java.lang.Integer var31 = var29.getId();
    java.lang.String var32 = var29.getStatus();
    boolean var33 = var1.equals((java.lang.Object)var32);
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + (-1)+ "'", var31.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);

  }

  public void test238() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test238"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var4 = var1.getStatus();
    java.lang.Integer var5 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 0+ "'", var5.equals(0));

  }

  public void test239() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test239"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    var16.setId((java.lang.Integer)(-1));
    boolean var21 = var16.equals((java.lang.Object)(short)(-1));
    var16.setId((java.lang.Integer)10);
    java.lang.String var24 = var16.getStatus();
    boolean var25 = var10.equals((java.lang.Object)var16);
    boolean var26 = var2.equals((java.lang.Object)var10);
    java.util.Collection var27 = var10.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var32 = var29.toString();
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var35 = var29.toString();
    java.lang.String var36 = var29.toString();
    var29.setId((java.lang.Integer)1);
    boolean var39 = var10.equals((java.lang.Object)var29);
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var43 = var42.toString();
    loja.com.br.entity.PropostaStatus var45 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var45.setStatus("");
    java.lang.Integer var48 = var45.getId();
    java.util.Collection var49 = var45.getPropostaCollection();
    java.lang.String var50 = var45.toString();
    boolean var51 = var42.equals((java.lang.Object)var45);
    java.lang.String var52 = var42.getStatus();
    boolean var53 = var10.equals((java.lang.Object)var42);
    java.lang.Integer var54 = var42.getId();
    var42.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var43.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + (-1)+ "'", var48.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var50.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var52.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + 100+ "'", var54.equals(100));

  }

  public void test240() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test240"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var2 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    java.lang.Integer var6 = var4.getId();
    java.lang.Integer var7 = var4.getId();
    boolean var8 = var1.equals((java.lang.Object)var4);
    java.lang.String var9 = var1.getStatus();
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test241() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test241"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var8 = var7.getId();
    boolean var9 = var2.equals((java.lang.Object)var8);
    java.lang.String var10 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 100+ "'", var8.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test242() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test242"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.String var3 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test243() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test243"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    boolean var6 = var1.equals((java.lang.Object)(byte)100);
    java.lang.String var7 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test244() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test244"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setId((java.lang.Integer)10);
    var6.setStatus("");
    java.lang.Integer var11 = var6.getId();
    java.util.Collection var12 = var6.getPropostaCollection();
    boolean var13 = var2.equals((java.lang.Object)var6);
    java.util.Collection var14 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var18 = var17.toString();
    java.lang.Integer var19 = var17.getId();
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var2.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);

  }

  public void test245() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test245"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var11 = var1.getStatus();
    java.lang.String var12 = var1.toString();
    java.lang.Integer var13 = var1.getId();
    var1.setId((java.lang.Integer)1);
    java.lang.String var16 = var1.toString();
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var21 = var18.toString();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var18.setId((java.lang.Integer)0);
    boolean var26 = var1.equals((java.lang.Object)0);
    java.util.Collection var27 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 1+ "'", var13.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);

  }

  public void test246() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test246"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var13 = var6.getId();
    boolean var14 = var2.equals((java.lang.Object)var13);
    java.lang.String var15 = var2.toString();
    var2.setStatus("hi!");
    var2.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 1+ "'", var13.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test247() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test247"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    java.lang.String var7 = var1.getStatus();
    java.lang.String var8 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var11 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "hi!"+ "'", var7.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test248() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test248"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.Integer var23 = var21.getId();
    boolean var25 = var21.equals((java.lang.Object)10.0d);
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var28 = var21.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test249() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test249"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    java.lang.String var6 = var1.getStatus();
    boolean var8 = var1.equals((java.lang.Object)false);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var11 = var10.getId();
    java.util.Collection var12 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)10);
    boolean var15 = var1.equals((java.lang.Object)10);
    java.lang.Integer var16 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));

  }

  public void test250() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test250"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.util.Collection var9 = var7.getPropostaCollection();
    var7.setId((java.lang.Integer)10);
    java.lang.String var12 = var7.toString();
    java.lang.String var13 = var7.getStatus();
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var22 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);

  }

  public void test251() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test251"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var9 = var1.getId();
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.util.Collection var14 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test252() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test252"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var5 = var2.getStatus();
    java.lang.Integer var6 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setId((java.lang.Integer)0);
    java.lang.String var11 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test253() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test253"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var12 = var11.getId();
    boolean var13 = var7.equals((java.lang.Object)var11);
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    java.lang.Integer var16 = var1.getId();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));

  }

  public void test254() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test254"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var15 = var1.getStatus();
    boolean var17 = var1.equals((java.lang.Object)1.0d);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);

  }

  public void test255() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test255"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 1+ "'", var2.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));

  }

  public void test256() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test256"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var4 = var3.getId();
    java.util.Collection var5 = var3.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.util.Collection var9 = var7.getPropostaCollection();
    var7.setId((java.lang.Integer)10);
    java.lang.String var12 = var7.toString();
    java.lang.String var13 = var7.getStatus();
    boolean var14 = var3.equals((java.lang.Object)var7);
    boolean var15 = var1.equals((java.lang.Object)var14);
    java.lang.String var16 = var1.toString();
    java.lang.Integer var17 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 100+ "'", var17.equals(100));

  }

  public void test257() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test257"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    boolean var6 = var1.equals((java.lang.Object)(byte)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var9 = var1.getStatus();
    java.lang.String var10 = var1.toString();
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test258() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test258"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var9 = var7.equals((java.lang.Object)false);
    boolean var10 = var1.equals((java.lang.Object)var7);
    java.util.Collection var11 = var7.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);

  }

  public void test259() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test259"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.lang.Integer var9 = var1.getId();
    java.lang.String var10 = var1.getStatus();
    java.lang.String var11 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 1+ "'", var9.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test260() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test260"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setId((java.lang.Integer)100);
    java.util.Collection var7 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var10 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test261() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test261"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test262() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test262"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.util.Collection var12 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var15 = var14.getPropostaCollection();
    java.lang.Integer var16 = var14.getId();
    boolean var18 = var14.equals((java.lang.Object)'4');
    boolean var20 = var14.equals((java.lang.Object)0L);
    boolean var21 = var1.equals((java.lang.Object)0L);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 1+ "'", var16.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);

  }

  public void test263() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test263"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    java.lang.String var3 = var2.getStatus();
    java.util.Collection var4 = var2.getPropostaCollection();
    java.util.Collection var5 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + ""+ "'", var3.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test264() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test264"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var9.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    java.lang.String var17 = var12.toString();
    boolean var18 = var9.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var22 = var12.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var23 = var1.equals((java.lang.Object)var12);
    java.util.Collection var24 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var27 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);

  }

  public void test265() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test265"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var4 = var3.getId();
    java.util.Collection var5 = var3.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.util.Collection var9 = var7.getPropostaCollection();
    var7.setId((java.lang.Integer)10);
    java.lang.String var12 = var7.toString();
    java.lang.String var13 = var7.getStatus();
    boolean var14 = var3.equals((java.lang.Object)var7);
    boolean var15 = var1.equals((java.lang.Object)var14);
    java.lang.String var16 = var1.toString();
    java.lang.String var17 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test266() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test266"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var3 = var2.getStatus();
    java.lang.String var4 = var2.getStatus();
    java.lang.Integer var5 = var2.getId();
    java.lang.String var6 = var2.getStatus();
    java.lang.String var7 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "hi!"+ "'", var3.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "hi!"+ "'", var4.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "hi!"+ "'", var6.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test267() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test267"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 0+ "'", var3.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test268() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test268"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setId((java.lang.Integer)100);
    java.lang.String var13 = var10.getStatus();
    var10.setId((java.lang.Integer)10);
    java.lang.Integer var16 = var10.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var21 = var19.equals((java.lang.Object)'#');
    var19.setStatus("hi!");
    boolean var24 = var10.equals((java.lang.Object)var19);
    var10.setId((java.lang.Integer)10);
    boolean var27 = var1.equals((java.lang.Object)var10);
    var10.setId((java.lang.Integer)10);
    java.lang.String var30 = var10.toString();
    java.util.Collection var31 = var10.getPropostaCollection();
    java.lang.String var32 = var10.toString();
    java.lang.String var33 = var10.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);

  }

  public void test269() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test269"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var9 = var7.equals((java.lang.Object)false);
    boolean var10 = var1.equals((java.lang.Object)var7);
    var1.setStatus("");
    java.util.Collection var13 = var1.getPropostaCollection();
    java.lang.String var14 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test270() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test270"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var9.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    java.lang.String var17 = var12.toString();
    boolean var18 = var9.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var22 = var12.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var23 = var1.equals((java.lang.Object)var12);
    java.lang.String var24 = var12.toString();
    var12.setId((java.lang.Integer)10);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var29 = var12.getPropostaCollection();
    java.lang.String var30 = var12.getStatus();
    java.lang.String var31 = var12.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var31.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test271() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test271"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var17 = var9.getPropostaCollection();
    java.util.Collection var18 = var9.getPropostaCollection();
    var9.setStatus("");
    java.lang.String var21 = var9.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + ""+ "'", var21.equals(""));

  }

  public void test272() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test272"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)0);
    java.lang.String var10 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test273() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test273"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    java.lang.String var4 = var2.getStatus();
    java.lang.Integer var5 = var2.getId();
    java.util.Collection var6 = var2.getPropostaCollection();
    java.util.Collection var7 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test274() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test274"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    java.util.Collection var17 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var19.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var23.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var23.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var28 = var23.getStatus();
    java.lang.Integer var29 = var23.getId();
    java.lang.String var30 = var23.toString();
    boolean var31 = var19.equals((java.lang.Object)var23);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var34 = var33.getId();
    var33.setId((java.lang.Integer)(-1));
    boolean var38 = var33.equals((java.lang.Object)'4');
    java.lang.String var39 = var33.toString();
    java.lang.String var40 = var33.toString();
    var33.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var44.setId((java.lang.Integer)10);
    java.lang.String var47 = var44.toString();
    java.lang.String var48 = var44.toString();
    var44.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var53 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var55 = var53.equals((java.lang.Object)(-1.0d));
    java.util.Collection var56 = var53.getPropostaCollection();
    var53.setId((java.lang.Integer)(-1));
    var53.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var61 = var44.equals((java.lang.Object)var53);
    boolean var62 = var33.equals((java.lang.Object)var44);
    boolean var63 = var23.equals((java.lang.Object)var62);
    boolean var64 = var1.equals((java.lang.Object)var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 1+ "'", var29.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + (-1)+ "'", var34.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var48.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var55 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var63 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);

  }

  public void test275() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test275"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var12 = var11.getId();
    boolean var13 = var7.equals((java.lang.Object)var11);
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    java.util.Collection var16 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    java.util.Collection var19 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var22.setId((java.lang.Integer)(-1));
    var22.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var28.setStatus("");
    boolean var32 = var28.equals((java.lang.Object)"hi!");
    java.lang.String var33 = var28.getStatus();
    var28.setId((java.lang.Integer)100);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var38 = var28.getId();
    java.lang.String var39 = var28.toString();
    boolean var40 = var22.equals((java.lang.Object)var39);
    boolean var41 = var1.equals((java.lang.Object)var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + ""+ "'", var33.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + 100+ "'", var38.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);

  }

  public void test276() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test276"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.toString();
    boolean var23 = var18.equals((java.lang.Object)(byte)100);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var26 = var10.equals((java.lang.Object)var18);
    boolean var27 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    var29.setId((java.lang.Integer)(-1));
    boolean var34 = var29.equals((java.lang.Object)'4');
    boolean var35 = var1.equals((java.lang.Object)var29);
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var40 = var38.equals((java.lang.Object)(-1.0d));
    java.util.Collection var41 = var38.getPropostaCollection();
    var38.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var47 = var46.toString();
    var46.setStatus("hi!");
    java.util.Collection var50 = var46.getPropostaCollection();
    var46.setStatus("hi!");
    boolean var53 = var38.equals((java.lang.Object)var46);
    java.util.Collection var54 = var46.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var56 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var58 = var56.equals((java.lang.Object)false);
    boolean var60 = var56.equals((java.lang.Object)10.0d);
    boolean var61 = var46.equals((java.lang.Object)10.0d);
    boolean var62 = var29.equals((java.lang.Object)var46);
    var46.setId((java.lang.Integer)100);
    java.lang.Integer var65 = var46.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var65 + "' != '" + 100+ "'", var65.equals(100));

  }

  public void test277() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test277"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test278() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test278"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.String var9 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test279() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test279"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.toString();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var10 = var8.equals((java.lang.Object)(-1.0d));
    java.lang.String var11 = var8.getStatus();
    java.lang.Integer var12 = var8.getId();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var17 = var14.toString();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var14.setId((java.lang.Integer)1);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var24 = var14.getStatus();
    java.lang.String var25 = var14.toString();
    boolean var26 = var8.equals((java.lang.Object)var25);
    boolean var27 = var1.equals((java.lang.Object)var25);
    java.lang.Integer var28 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));

  }

  public void test280() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test280"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var3 = var2.toString();
    java.lang.Integer var4 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setStatus("hi!");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var11 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 100+ "'", var4.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test281() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test281"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var9 = var1.toString();
    java.lang.Integer var10 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var13 = var1.getPropostaCollection();
    java.util.Collection var14 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test282() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test282"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.String var4 = var1.toString();
    java.lang.Integer var5 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));

  }

  public void test283() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test283"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    java.lang.String var8 = var2.toString();
    java.lang.String var9 = var2.toString();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    var11.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.util.Collection var23 = var19.getPropostaCollection();
    boolean var24 = var11.equals((java.lang.Object)var19);
    var11.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var33 = var32.getId();
    boolean var34 = var28.equals((java.lang.Object)var32);
    java.lang.String var35 = var28.getStatus();
    java.lang.String var36 = var28.getStatus();
    boolean var37 = var11.equals((java.lang.Object)var28);
    var11.setId((java.lang.Integer)1);
    boolean var40 = var2.equals((java.lang.Object)1);
    java.lang.Integer var41 = var2.getId();
    var2.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 100+ "'", var33.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + (-1)+ "'", var41.equals((-1)));

  }

  public void test284() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test284"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var5.getStatus();
    java.lang.Integer var11 = var5.getId();
    java.lang.String var12 = var5.toString();
    boolean var13 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var14 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var17 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test285() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test285"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var6 = var4.equals((java.lang.Object)false);
    boolean var8 = var4.equals((java.lang.Object)10.0d);
    boolean var9 = var2.equals((java.lang.Object)var8);
    java.lang.String var10 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var14 = var13.toString();
    var13.setId((java.lang.Integer)(-1));
    boolean var17 = var2.equals((java.lang.Object)var13);
    java.lang.String var18 = var13.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test286() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test286"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    boolean var4 = var1.equals((java.lang.Object)(short)0);
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.Integer var6 = var1.getId();
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test287() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test287"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    boolean var5 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var11 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    java.util.Collection var14 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    java.util.Collection var18 = var16.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var21 = var20.getId();
    java.util.Collection var22 = var20.getPropostaCollection();
    var20.setId((java.lang.Integer)10);
    java.lang.String var25 = var20.toString();
    java.lang.String var26 = var20.getStatus();
    boolean var27 = var16.equals((java.lang.Object)var20);
    var16.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var16.setStatus("");
    java.lang.String var32 = var16.getStatus();
    java.util.Collection var33 = var16.getPropostaCollection();
    boolean var34 = var1.equals((java.lang.Object)var16);
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var37.setId((java.lang.Integer)100);
    java.lang.String var40 = var37.getStatus();
    java.lang.String var41 = var37.toString();
    boolean var42 = var1.equals((java.lang.Object)var37);
    java.lang.Integer var43 = var37.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + ""+ "'", var32.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + ""+ "'", var40.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + 100+ "'", var43.equals(100));

  }

  public void test288() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test288"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setId((java.lang.Integer)0);
    boolean var13 = var1.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var15.setStatus("");
    boolean var19 = var15.equals((java.lang.Object)"hi!");
    java.lang.String var20 = var15.getStatus();
    var15.setId((java.lang.Integer)100);
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var25 = var15.getStatus();
    boolean var26 = var10.equals((java.lang.Object)var15);
    java.lang.String var27 = var10.getStatus();
    java.lang.Integer var28 = var10.getId();
    java.lang.Integer var29 = var10.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 0+ "'", var28.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 0+ "'", var29.equals(0));

  }

  public void test289() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test289"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    java.util.Collection var3 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var6 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var9 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test290() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test290"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setId((java.lang.Integer)100);
    java.lang.String var10 = var7.getStatus();
    var7.setId((java.lang.Integer)10);
    java.lang.String var13 = var7.toString();
    java.util.Collection var14 = var7.getPropostaCollection();
    boolean var15 = var1.equals((java.lang.Object)var7);
    java.util.Collection var16 = var7.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var19.setId((java.lang.Integer)0);
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var24 = var7.equals((java.lang.Object)var19);
    var7.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);

  }

  public void test291() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test291"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    var1.setStatus("");
    java.lang.String var7 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + ""+ "'", var7.equals(""));

  }

  public void test292() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test292"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var8 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test293() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test293"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=1 ]");
    var2.setId((java.lang.Integer)10);

  }

  public void test294() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test294"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.util.Collection var9 = var7.getPropostaCollection();
    var7.setId((java.lang.Integer)10);
    java.lang.String var12 = var7.toString();
    java.lang.String var13 = var7.getStatus();
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var22 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var24.setId((java.lang.Integer)10);
    java.lang.String var27 = var24.toString();
    java.lang.String var28 = var24.toString();
    var24.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var33 = var32.getId();
    java.util.Collection var34 = var32.getPropostaCollection();
    var32.setId((java.lang.Integer)10);
    java.lang.String var37 = var32.toString();
    java.lang.Integer var38 = var32.getId();
    var32.setStatus("");
    var32.setStatus("hi!");
    java.lang.String var43 = var32.getStatus();
    boolean var44 = var24.equals((java.lang.Object)var32);
    boolean var45 = var1.equals((java.lang.Object)var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + (-1)+ "'", var33.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + 10+ "'", var38.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + "hi!"+ "'", var43.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);

  }

  public void test295() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test295"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var12.setId((java.lang.Integer)10);
    java.lang.String var15 = var12.toString();
    java.lang.String var16 = var12.toString();
    var12.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var23 = var21.equals((java.lang.Object)(-1.0d));
    java.util.Collection var24 = var21.getPropostaCollection();
    var21.setId((java.lang.Integer)(-1));
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var29 = var12.equals((java.lang.Object)var21);
    boolean var30 = var1.equals((java.lang.Object)var12);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var35 = var12.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test296() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test296"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.Integer var8 = var2.getId();
    java.lang.String var9 = var2.getStatus();
    var2.setId((java.lang.Integer)(-1));
    java.lang.String var12 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "hi!"+ "'", var12.equals("hi!"));

  }

  public void test297() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test297"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.toString();
    var1.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test298() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test298"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.String var23 = var21.getStatus();
    java.util.Collection var24 = var21.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var27 = var26.getId();
    java.util.Collection var28 = var26.getPropostaCollection();
    var26.setId((java.lang.Integer)10);
    var26.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var34 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var34.setStatus("");
    java.lang.Integer var37 = var34.getId();
    java.util.Collection var38 = var34.getPropostaCollection();
    boolean var39 = var26.equals((java.lang.Object)var34);
    var26.setId((java.lang.Integer)10);
    boolean var42 = var21.equals((java.lang.Object)var26);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var45 = var44.getId();
    java.lang.Integer var46 = var44.getId();
    java.lang.Integer var47 = var44.getId();
    java.lang.String var48 = var44.toString();
    boolean var49 = var26.equals((java.lang.Object)var44);
    var44.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + (-1)+ "'", var37.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + (-1)+ "'", var45.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + (-1)+ "'", var46.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + (-1)+ "'", var47.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var48.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);

  }

  public void test299() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test299"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    boolean var7 = var4.equals((java.lang.Object)(short)0);
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.Integer var9 = var4.getId();
    boolean var10 = var2.equals((java.lang.Object)var4);
    java.lang.String var11 = var4.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test300() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test300"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test301() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test301"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.String var23 = var21.getStatus();
    java.util.Collection var24 = var21.getPropostaCollection();
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var27 = var21.getId();
    var21.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));

  }

  public void test302() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test302"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.Integer var9 = var1.getId();
    java.lang.Integer var10 = var1.getId();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var13 = var12.getId();
    java.util.Collection var14 = var12.getPropostaCollection();
    var12.setId((java.lang.Integer)10);
    var12.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var20.setStatus("");
    java.lang.Integer var23 = var20.getId();
    java.util.Collection var24 = var20.getPropostaCollection();
    boolean var25 = var12.equals((java.lang.Object)var20);
    var12.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var34 = var33.getId();
    boolean var35 = var29.equals((java.lang.Object)var33);
    java.lang.String var36 = var29.getStatus();
    java.lang.String var37 = var29.getStatus();
    boolean var38 = var12.equals((java.lang.Object)var29);
    java.lang.Integer var39 = var29.getId();
    java.util.Collection var40 = var29.getPropostaCollection();
    var29.setId((java.lang.Integer)10);
    boolean var43 = var1.equals((java.lang.Object)var29);
    java.util.Collection var44 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 10+ "'", var9.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + 100+ "'", var34.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + 1+ "'", var39.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);

  }

  public void test303() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test303"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    java.util.Collection var19 = var10.getPropostaCollection();
    java.lang.String var20 = var10.toString();
    java.util.Collection var21 = var10.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var25 = var24.getId();
    java.lang.String var26 = var24.getStatus();
    var24.setId((java.lang.Integer)1);
    boolean var29 = var10.equals((java.lang.Object)var24);
    java.util.Collection var30 = var24.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);

  }

  public void test304() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test304"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test305() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test305"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var6 = var4.equals((java.lang.Object)false);
    boolean var8 = var4.equals((java.lang.Object)10.0d);
    boolean var9 = var2.equals((java.lang.Object)var8);
    java.lang.String var10 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var14 = var13.toString();
    var13.setId((java.lang.Integer)(-1));
    boolean var17 = var2.equals((java.lang.Object)var13);
    var2.setId((java.lang.Integer)0);
    java.lang.String var20 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test306() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test306"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)1);
    java.lang.String var12 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    java.lang.String var15 = var1.toString();
    java.util.Collection var16 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);

  }

  public void test307() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test307"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    java.lang.String var8 = var1.toString();
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    var11.setId((java.lang.Integer)(-1));
    boolean var16 = var11.equals((java.lang.Object)'4');
    java.lang.String var17 = var11.toString();
    java.lang.String var18 = var11.toString();
    var11.setId((java.lang.Integer)0);
    var11.setId((java.lang.Integer)1);
    java.util.Collection var23 = var11.getPropostaCollection();
    boolean var24 = var1.equals((java.lang.Object)var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);

  }

  public void test308() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test308"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var26 = var25.getId();
    java.util.Collection var27 = var25.getPropostaCollection();
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var30 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)(-1));
    boolean var33 = var1.equals((java.lang.Object)var25);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);

  }

  public void test309() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test309"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    java.lang.String var15 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    java.util.Collection var19 = var17.getPropostaCollection();
    var17.setId((java.lang.Integer)10);
    var17.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var25.setStatus("");
    java.lang.Integer var28 = var25.getId();
    java.util.Collection var29 = var25.getPropostaCollection();
    boolean var30 = var17.equals((java.lang.Object)var25);
    var17.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var36 = var35.toString();
    java.lang.Integer var37 = var35.getId();
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var40 = var35.toString();
    boolean var41 = var17.equals((java.lang.Object)var35);
    java.lang.Integer var42 = var17.getId();
    boolean var43 = var1.equals((java.lang.Object)var17);
    var17.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 10+ "'", var37.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + 10+ "'", var42.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);

  }

  public void test310() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test310"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var11 = var1.toString();
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test311() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test311"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test312() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test312"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var5 = var2.getId();
    java.util.Collection var6 = var2.getPropostaCollection();
    java.lang.String var7 = var2.getStatus();
    java.util.Collection var8 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test313() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test313"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var12 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test314() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test314"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var15 = var1.getId();
    java.lang.String var16 = var1.getStatus();
    java.lang.Integer var17 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 0+ "'", var15.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 0+ "'", var17.equals(0));

  }

  public void test315() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test315"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("hi!");
    var2.setId((java.lang.Integer)1);
    var2.setId((java.lang.Integer)10);
    java.lang.Integer var10 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));

  }

  public void test316() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test316"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    java.lang.Integer var6 = var1.getId();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var8.setId((java.lang.Integer)10);
    java.lang.String var11 = var8.toString();
    boolean var13 = var8.equals((java.lang.Object)(byte)100);
    var8.setId((java.lang.Integer)10);
    boolean var16 = var1.equals((java.lang.Object)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);

  }

  public void test317() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test317"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var2.setId((java.lang.Integer)0);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var7 = var2.getStatus();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var2.setId((java.lang.Integer)100);
    java.lang.Integer var12 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));

  }

  public void test318() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test318"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("hi!");
    java.util.Collection var6 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);

  }

  public void test319() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test319"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var4 = var3.getPropostaCollection();
    var3.setId((java.lang.Integer)100);
    java.lang.Integer var7 = var3.getId();
    boolean var8 = var2.equals((java.lang.Object)var3);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var11.setId((java.lang.Integer)(-1));
    var11.setStatus("hi!");
    boolean var16 = var3.equals((java.lang.Object)"hi!");
    java.lang.String var17 = var3.getStatus();
    var3.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test320() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test320"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test321() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test321"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.Integer var1 = var0.getId();
    java.lang.String var2 = var0.toString();
    java.lang.String var3 = var0.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test322() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test322"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var8 = var1.getId();
    java.lang.String var9 = var1.toString();
    java.lang.Integer var10 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 0+ "'", var10.equals(0));

  }

  public void test323() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test323"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var8.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var13 = var12.getId();
    java.util.Collection var14 = var12.getPropostaCollection();
    var12.setId((java.lang.Integer)10);
    var12.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var20.setStatus("");
    java.lang.Integer var23 = var20.getId();
    java.util.Collection var24 = var20.getPropostaCollection();
    boolean var25 = var12.equals((java.lang.Object)var20);
    var12.setId((java.lang.Integer)10);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var30 = var8.equals((java.lang.Object)var12);
    boolean var31 = var1.equals((java.lang.Object)var12);
    java.lang.String var32 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test324() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test324"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    java.lang.String var9 = var1.getStatus();
    java.lang.String var10 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    java.lang.Integer var13 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));

  }

  public void test325() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test325"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)10);
    java.lang.String var14 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test326() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test326"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var5.getStatus();
    java.lang.Integer var11 = var5.getId();
    java.lang.String var12 = var5.toString();
    boolean var13 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var14 = var5.getId();
    var5.setId((java.lang.Integer)0);
    java.lang.Integer var17 = var5.getId();
    java.lang.String var18 = var5.getStatus();
    java.lang.String var19 = var5.toString();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var21 = var20.getPropostaCollection();
    var20.setId((java.lang.Integer)100);
    java.lang.String var24 = var20.getStatus();
    java.lang.Integer var25 = var20.getId();
    boolean var26 = var5.equals((java.lang.Object)var20);
    java.util.Collection var27 = var5.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 0+ "'", var17.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);

  }

  public void test327() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test327"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.String var5 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test328() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test328"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    boolean var7 = var1.equals((java.lang.Object)0L);
    var1.setId((java.lang.Integer)100);
    var1.setId((java.lang.Integer)1);
    java.lang.Integer var12 = var1.getId();
    java.util.Collection var13 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 1+ "'", var12.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);

  }

  public void test329() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test329"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var10 = var5.getPropostaCollection();
    boolean var11 = var1.equals((java.lang.Object)var5);
    java.lang.String var12 = var5.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test330() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test330"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var2.setId((java.lang.Integer)0);
    java.lang.String var5 = var2.toString();
    java.lang.Integer var6 = var2.getId();
    java.lang.Integer var7 = var2.getId();
    java.lang.Integer var8 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 0+ "'", var7.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));

  }

  public void test331() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test331"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.util.Collection var28 = var1.getPropostaCollection();
    java.lang.Integer var29 = var1.getId();
    var1.setStatus("");
    java.lang.String var32 = var1.getStatus();
    java.lang.String var33 = var1.toString();
    var1.setId((java.lang.Integer)0);
    java.lang.String var36 = var1.toString();
    boolean var38 = var1.equals((java.lang.Object)'#');
    java.lang.String var39 = var1.getStatus();
    java.lang.String var40 = var1.toString();
    java.lang.String var41 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 10+ "'", var29.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + ""+ "'", var32.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + ""+ "'", var39.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + ""+ "'", var41.equals(""));

  }

  public void test332() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test332"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.util.Collection var9 = var1.getPropostaCollection();
    java.util.Collection var10 = var1.getPropostaCollection();
    java.lang.Integer var11 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));

  }

  public void test333() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test333"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var18 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.util.Collection var21 = var1.getPropostaCollection();
    java.lang.Integer var22 = var1.getId();
    java.lang.String var23 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 1+ "'", var22.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);

  }

  public void test334() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test334"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    boolean var5 = var2.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=1 ]");
    var2.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    java.util.Collection var17 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)10);
    java.lang.String var20 = var15.toString();
    java.lang.String var21 = var15.getStatus();
    boolean var22 = var11.equals((java.lang.Object)var15);
    boolean var23 = var9.equals((java.lang.Object)var22);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var26 = var9.toString();
    boolean var27 = var2.equals((java.lang.Object)var26);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var32 = var31.toString();
    loja.com.br.entity.PropostaStatus var34 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var34.setStatus("");
    java.lang.Integer var37 = var34.getId();
    java.util.Collection var38 = var34.getPropostaCollection();
    java.lang.String var39 = var34.getStatus();
    boolean var40 = var31.equals((java.lang.Object)var34);
    boolean var41 = var2.equals((java.lang.Object)var40);
    var2.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + (-1)+ "'", var37.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + ""+ "'", var39.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);

  }

  public void test335() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test335"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var12 = var11.toString();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("");
    java.lang.Integer var17 = var14.getId();
    java.util.Collection var18 = var14.getPropostaCollection();
    java.lang.String var19 = var14.getStatus();
    boolean var20 = var11.equals((java.lang.Object)var14);
    boolean var21 = var1.equals((java.lang.Object)var11);
    java.util.Collection var22 = var1.getPropostaCollection();
    java.util.Collection var23 = var1.getPropostaCollection();
    java.lang.Integer var24 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var29 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 0+ "'", var24.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test336() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test336"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var14 = var1.getId();
    java.lang.String var15 = var1.getStatus();
    java.lang.String var16 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 0+ "'", var14.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test337() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test337"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var6 = var4.equals((java.lang.Object)false);
    boolean var8 = var4.equals((java.lang.Object)10.0d);
    boolean var9 = var2.equals((java.lang.Object)var8);
    java.lang.String var10 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var14 = var13.toString();
    var13.setId((java.lang.Integer)(-1));
    boolean var17 = var2.equals((java.lang.Object)var13);
    var2.setId((java.lang.Integer)0);
    var2.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == true);

  }

  public void test338() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test338"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var6 = var5.getId();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var11 = var9.equals((java.lang.Object)(-1.0d));
    java.util.Collection var12 = var9.getPropostaCollection();
    var9.setId((java.lang.Integer)(-1));
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var9.setStatus("hi!");
    boolean var19 = var5.equals((java.lang.Object)var9);
    boolean var20 = var2.equals((java.lang.Object)var19);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);

  }

  public void test339() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test339"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var9 = var1.getId();
    var1.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var16 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)100);
    java.lang.Integer var19 = var15.getId();
    boolean var20 = var14.equals((java.lang.Object)var15);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var23 = var1.equals((java.lang.Object)var14);
    java.lang.Integer var24 = var14.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 0+ "'", var24.equals(0));

  }

  public void test340() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test340"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setId((java.lang.Integer)100);
    java.lang.String var13 = var10.getStatus();
    var10.setId((java.lang.Integer)10);
    java.lang.Integer var16 = var10.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var21 = var19.equals((java.lang.Object)'#');
    var19.setStatus("hi!");
    boolean var24 = var10.equals((java.lang.Object)var19);
    var10.setId((java.lang.Integer)10);
    boolean var27 = var1.equals((java.lang.Object)var10);
    var10.setId((java.lang.Integer)10);
    java.lang.String var30 = var10.toString();
    java.util.Collection var31 = var10.getPropostaCollection();
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var10.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);

  }

  public void test341() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test341"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    boolean var12 = var9.equals((java.lang.Object)(short)0);
    var9.setStatus("");
    boolean var15 = var1.equals((java.lang.Object)var9);
    java.lang.String var16 = var9.getStatus();
    java.lang.String var17 = var9.toString();
    java.lang.String var18 = var9.getStatus();
    java.lang.String var19 = var9.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + ""+ "'", var16.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + ""+ "'", var18.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));

  }

  public void test342() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test342"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var17 = var15.equals((java.lang.Object)'#');
    var15.setStatus("hi!");
    boolean var20 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var21 = var1.getStatus();
    java.util.Collection var22 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);

  }

  public void test343() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test343"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.toString();
    boolean var23 = var18.equals((java.lang.Object)(byte)100);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var26 = var10.equals((java.lang.Object)var18);
    boolean var27 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    var29.setId((java.lang.Integer)(-1));
    boolean var34 = var29.equals((java.lang.Object)'4');
    boolean var35 = var1.equals((java.lang.Object)var29);
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var40 = var38.equals((java.lang.Object)(-1.0d));
    java.util.Collection var41 = var38.getPropostaCollection();
    var38.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var47 = var46.toString();
    var46.setStatus("hi!");
    java.util.Collection var50 = var46.getPropostaCollection();
    var46.setStatus("hi!");
    boolean var53 = var38.equals((java.lang.Object)var46);
    java.util.Collection var54 = var46.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var56 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var58 = var56.equals((java.lang.Object)false);
    boolean var60 = var56.equals((java.lang.Object)10.0d);
    boolean var61 = var46.equals((java.lang.Object)10.0d);
    boolean var62 = var29.equals((java.lang.Object)var46);
    var46.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var65 = var46.getId();
    loja.com.br.entity.PropostaStatus var67 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var68 = var67.toString();
    loja.com.br.entity.PropostaStatus var70 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var70.setStatus("");
    java.lang.Integer var73 = var70.getId();
    java.util.Collection var74 = var70.getPropostaCollection();
    java.lang.String var75 = var70.getStatus();
    boolean var76 = var67.equals((java.lang.Object)var70);
    var70.setStatus("");
    loja.com.br.entity.PropostaStatus var80 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var81 = var80.getStatus();
    var80.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var84 = var70.equals((java.lang.Object)var80);
    var80.setStatus("");
    java.util.Collection var87 = var80.getPropostaCollection();
    java.lang.String var88 = var80.getStatus();
    boolean var89 = var46.equals((java.lang.Object)var88);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var65 + "' != '" + 10+ "'", var65.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var68.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var73 + "' != '" + (-1)+ "'", var73.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var74);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var75 + "' != '" + ""+ "'", var75.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var76 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var81);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var84 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var87);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var88 + "' != '" + ""+ "'", var88.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var89 == false);

  }

  public void test344() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test344"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var2.setId((java.lang.Integer)100);
    java.lang.String var5 = var2.getStatus();
    java.lang.String var6 = var2.toString();
    java.lang.Integer var7 = var2.getId();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.Integer var10 = var9.getId();
    boolean var11 = var2.equals((java.lang.Object)var10);
    java.lang.String var12 = var2.toString();
    java.lang.Integer var13 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 0+ "'", var10.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 100+ "'", var13.equals(100));

  }

  public void test345() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test345"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.String var23 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "hi!"+ "'", var23.equals("hi!"));

  }

  public void test346() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test346"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=1 ]");
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var14 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var15 = var1.getPropostaCollection();
    java.lang.Integer var16 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 0+ "'", var16.equals(0));

  }

  public void test347() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test347"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.String var12 = var11.toString();
    boolean var13 = var1.equals((java.lang.Object)var11);
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);

  }

  public void test348() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test348"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var8 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));

  }

  public void test349() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test349"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("hi!");
    java.util.Collection var6 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)100);
    java.lang.String var9 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));

  }

  public void test350() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test350"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.String var6 = var2.toString();
    java.lang.String var7 = var2.getStatus();
    java.lang.Integer var8 = var2.getId();
    java.util.Collection var9 = var2.getPropostaCollection();
    java.lang.Integer var10 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));

  }

  public void test351() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test351"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.Integer var9 = var1.getId();
    java.lang.Integer var10 = var1.getId();
    var1.setStatus("");
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var15 = var1.toString();
    java.util.Collection var16 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 10+ "'", var9.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);

  }

  public void test352() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test352"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var17 = var9.getPropostaCollection();
    java.util.Collection var18 = var9.getPropostaCollection();
    var9.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);

  }

  public void test353() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test353"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    var1.setStatus("");
    java.util.Collection var10 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var17 = var16.getId();
    boolean var18 = var12.equals((java.lang.Object)var16);
    java.lang.Integer var19 = var12.getId();
    java.util.Collection var20 = var12.getPropostaCollection();
    java.lang.String var21 = var12.toString();
    var12.setStatus("");
    boolean var24 = var1.equals((java.lang.Object)"");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 100+ "'", var17.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 1+ "'", var19.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);

  }

  public void test354() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test354"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var19 = var1.getId();
    java.lang.String var20 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var26 = var25.getPropostaCollection();
    java.lang.Integer var27 = var25.getId();
    boolean var29 = var25.equals((java.lang.Object)'4');
    boolean var30 = var23.equals((java.lang.Object)var29);
    boolean var31 = var1.equals((java.lang.Object)var23);
    java.util.Collection var32 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 1+ "'", var27.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);

  }

  public void test355() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test355"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.Integer var23 = var21.getId();
    java.lang.String var24 = var21.getStatus();
    java.util.Collection var25 = var21.getPropostaCollection();
    java.util.Collection var26 = var21.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);

  }

  public void test356() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test356"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.getStatus();
    java.lang.String var4 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test357() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test357"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var10 = var1.getId();
    java.lang.String var11 = var1.toString();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 0+ "'", var10.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test358() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test358"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test359() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test359"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var3 = var2.getStatus();
    java.lang.String var4 = var2.toString();
    java.lang.String var5 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test360() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test360"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var13 = var10.getStatus();
    java.lang.Integer var14 = var10.getId();
    boolean var15 = var1.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var24 = var17.getStatus();
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var29 = var10.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 0+ "'", var14.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);

  }

  public void test361() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test361"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var15.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var21 = var20.toString();
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var23.setStatus("");
    java.lang.Integer var26 = var23.getId();
    java.util.Collection var27 = var23.getPropostaCollection();
    java.lang.String var28 = var23.toString();
    boolean var29 = var20.equals((java.lang.Object)var23);
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var33 = var23.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var34 = var23.getPropostaCollection();
    boolean var35 = var15.equals((java.lang.Object)var23);
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var38 = var37.getPropostaCollection();
    java.lang.Integer var39 = var37.getId();
    boolean var41 = var37.equals((java.lang.Object)'4');
    var37.setId((java.lang.Integer)10);
    java.util.Collection var44 = var37.getPropostaCollection();
    boolean var45 = var15.equals((java.lang.Object)var37);
    java.lang.String var46 = var37.toString();
    boolean var47 = var1.equals((java.lang.Object)var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + 1+ "'", var39.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);

  }

  public void test362() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test362"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.getStatus();
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)100);
    java.util.Collection var9 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test363() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test363"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setId((java.lang.Integer)100);
    boolean var8 = var2.equals((java.lang.Object)var5);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var11 = var2.toString();
    var2.setStatus("hi!");
    java.lang.Integer var14 = var2.getId();
    java.util.Collection var15 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test364() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test364"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    java.lang.String var15 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test365() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test365"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    var15.setId((java.lang.Integer)(-1));
    java.util.Collection var19 = var15.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var26 = var25.getId();
    boolean var27 = var21.equals((java.lang.Object)var25);
    java.lang.Integer var28 = var21.getId();
    boolean var29 = var15.equals((java.lang.Object)var28);
    java.util.Collection var30 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)0);
    boolean var33 = var1.equals((java.lang.Object)0);
    java.lang.String var34 = var1.toString();
    loja.com.br.entity.PropostaStatus var36 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var37 = var36.toString();
    java.lang.String var38 = var36.getStatus();
    java.util.Collection var39 = var36.getPropostaCollection();
    java.lang.String var40 = var36.getStatus();
    var36.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var45 = var44.getId();
    java.util.Collection var46 = var44.getPropostaCollection();
    var44.setId((java.lang.Integer)10);
    var44.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var52 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var52.setStatus("");
    java.lang.Integer var55 = var52.getId();
    java.util.Collection var56 = var52.getPropostaCollection();
    boolean var57 = var44.equals((java.lang.Object)var52);
    var44.setId((java.lang.Integer)10);
    boolean var61 = var44.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var64 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var65 = var44.equals((java.lang.Object)var64);
    java.lang.Integer var66 = var64.getId();
    java.lang.String var67 = var64.getStatus();
    boolean var68 = var36.equals((java.lang.Object)var67);
    java.lang.Integer var69 = var36.getId();
    boolean var70 = var1.equals((java.lang.Object)var69);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 100+ "'", var26.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + (-1)+ "'", var45.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + (-1)+ "'", var55.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var65 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var66 + "' != '" + (-1)+ "'", var66.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var67 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var67.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var68 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var69 + "' != '" + 0+ "'", var69.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var70 == false);

  }

  public void test366() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test366"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var15 = var1.equals((java.lang.Object)(-1));
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    java.lang.Integer var19 = var17.getId();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var22 = var21.getId();
    java.util.Collection var23 = var21.getPropostaCollection();
    var21.setId((java.lang.Integer)10);
    var21.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var29.setStatus("");
    java.lang.Integer var32 = var29.getId();
    java.util.Collection var33 = var29.getPropostaCollection();
    boolean var34 = var21.equals((java.lang.Object)var29);
    var21.setId((java.lang.Integer)10);
    boolean var38 = var21.equals((java.lang.Object)(byte)10);
    java.lang.String var39 = var21.getStatus();
    boolean var40 = var17.equals((java.lang.Object)var21);
    java.lang.String var41 = var17.getStatus();
    var17.setStatus("");
    boolean var44 = var1.equals((java.lang.Object)"");
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + (-1)+ "'", var32.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "hi!"+ "'", var39.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);

  }

  public void test367() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test367"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var7 = var6.getId();
    java.util.Collection var8 = var6.getPropostaCollection();
    var6.setId((java.lang.Integer)10);
    var6.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("");
    java.lang.Integer var17 = var14.getId();
    java.util.Collection var18 = var14.getPropostaCollection();
    boolean var19 = var6.equals((java.lang.Object)var14);
    var6.setId((java.lang.Integer)10);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var24 = var2.equals((java.lang.Object)var6);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var27 = var6.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);

  }

  public void test368() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test368"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test369() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test369"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var5.setStatus("");
    java.lang.Integer var8 = var5.getId();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.String var10 = var5.toString();
    boolean var11 = var2.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var5.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var16 = var5.getStatus();
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.Integer var25 = var18.getId();
    java.util.Collection var26 = var18.getPropostaCollection();
    java.lang.String var27 = var18.toString();
    boolean var28 = var5.equals((java.lang.Object)var27);
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var33 = var32.getPropostaCollection();
    var32.setId((java.lang.Integer)100);
    java.lang.Integer var36 = var32.getId();
    boolean var37 = var31.equals((java.lang.Object)var32);
    boolean var38 = var5.equals((java.lang.Object)var37);
    java.lang.Integer var39 = var5.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + ""+ "'", var16.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 1+ "'", var25.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 100+ "'", var36.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + (-1)+ "'", var39.equals((-1)));

  }

  public void test370() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test370"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var8.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var21 = var18.getId();
    boolean var22 = var8.equals((java.lang.Object)var21);
    boolean var23 = var1.equals((java.lang.Object)var8);
    java.lang.Integer var24 = var8.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 10+ "'", var21.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 0+ "'", var24.equals(0));

  }

  public void test371() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test371"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var13 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test372() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test372"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.util.Collection var2 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var6 = var1.equals((java.lang.Object)' ');
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);

  }

  public void test373() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test373"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var8 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.lang.String var11 = var1.getStatus();
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test374() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test374"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var9 = var1.getId();
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test375() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test375"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.String var1 = var0.toString();
    java.util.Collection var2 = var0.getPropostaCollection();
    java.lang.String var3 = var0.getStatus();
    java.lang.Integer var4 = var0.getId();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var9 = var7.equals((java.lang.Object)'#');
    var7.setStatus("hi!");
    java.lang.Integer var12 = var7.getId();
    java.lang.String var13 = var7.getStatus();
    java.lang.String var14 = var7.getStatus();
    java.lang.String var15 = var7.getStatus();
    boolean var16 = var0.equals((java.lang.Object)var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var1.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "hi!"+ "'", var13.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "hi!"+ "'", var14.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "hi!"+ "'", var15.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);

  }

  public void test376() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test376"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var9 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test377() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test377"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var28 = var18.getId();
    java.util.Collection var29 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    java.lang.Integer var32 = var18.getId();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var38 = var37.toString();
    var37.setStatus("hi!");
    java.lang.String var41 = var37.toString();
    boolean var42 = var18.equals((java.lang.Object)var41);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var44.setId((java.lang.Integer)100);
    java.lang.String var47 = var44.getStatus();
    var44.setId((java.lang.Integer)10);
    java.lang.Integer var50 = var44.getId();
    java.lang.String var51 = var44.toString();
    loja.com.br.entity.PropostaStatus var53 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var53.setId((java.lang.Integer)100);
    java.lang.String var56 = var53.getStatus();
    var53.setId((java.lang.Integer)10);
    java.lang.Integer var59 = var53.getId();
    loja.com.br.entity.PropostaStatus var62 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var64 = var62.equals((java.lang.Object)'#');
    var62.setStatus("hi!");
    boolean var67 = var53.equals((java.lang.Object)var62);
    var53.setId((java.lang.Integer)10);
    boolean var70 = var44.equals((java.lang.Object)var53);
    loja.com.br.entity.PropostaStatus var72 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var72.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var75 = var72.toString();
    var72.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var78 = var72.getStatus();
    java.lang.String var79 = var72.toString();
    java.lang.String var80 = var72.toString();
    boolean var81 = var44.equals((java.lang.Object)var80);
    boolean var82 = var18.equals((java.lang.Object)var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + 10+ "'", var50.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var51.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + 10+ "'", var59.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var67 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var70 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var75 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var75.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var78 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var78.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var79 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var79.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var80 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var80.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var81 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var82 == true);

  }

  public void test378() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test378"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var2.setId((java.lang.Integer)100);
    var2.setId((java.lang.Integer)1);
    var2.setId((java.lang.Integer)1);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test379() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test379"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    boolean var4 = var1.equals((java.lang.Object)(short)0);
    java.lang.String var5 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test380() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test380"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var9 = var7.equals((java.lang.Object)false);
    boolean var11 = var7.equals((java.lang.Object)10.0d);
    java.lang.String var12 = var7.toString();
    boolean var13 = var2.equals((java.lang.Object)var7);
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);

  }

  public void test381() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test381"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var12 = var10.equals((java.lang.Object)'#');
    var10.setStatus("hi!");
    boolean var15 = var1.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var19 = var18.toString();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var21.setStatus("");
    java.lang.Integer var24 = var21.getId();
    java.util.Collection var25 = var21.getPropostaCollection();
    java.lang.String var26 = var21.toString();
    boolean var27 = var18.equals((java.lang.Object)var21);
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var31 = var21.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var32 = var10.equals((java.lang.Object)var31);
    java.lang.String var33 = var10.toString();
    var10.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + (-1)+ "'", var24.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test382() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test382"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var2.setId((java.lang.Integer)100);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var2.setStatus("");
    java.lang.String var9 = var2.toString();
    java.lang.String var10 = var2.getStatus();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));

  }

  public void test383() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test383"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setId((java.lang.Integer)1);
    java.util.Collection var17 = var9.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test384() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test384"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    var1.setId((java.lang.Integer)10);
    java.lang.String var25 = var1.getStatus();
    java.lang.Integer var26 = var1.getId();
    java.lang.String var27 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "hi!"+ "'", var25.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test385() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test385"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    boolean var9 = var1.equals((java.lang.Object)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var12 = var1.getStatus();
    java.util.Collection var13 = var1.getPropostaCollection();
    java.util.Collection var14 = var1.getPropostaCollection();
    java.util.Collection var15 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test386() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test386"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    java.lang.String var6 = var1.getStatus();
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);

  }

  public void test387() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test387"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.getStatus();
    var2.setId((java.lang.Integer)1);
    java.lang.String var7 = var2.toString();
    java.lang.Integer var8 = var2.getId();
    java.lang.String var9 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test388() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test388"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.Integer var5 = var1.getId();
    java.util.Collection var6 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)(-1));
    boolean var13 = var8.equals((java.lang.Object)'4');
    java.lang.String var14 = var8.getStatus();
    boolean var15 = var1.equals((java.lang.Object)var8);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var21 = var20.getId();
    java.lang.String var22 = var20.getStatus();
    java.lang.String var23 = var20.getStatus();
    java.lang.Integer var24 = var20.getId();
    boolean var25 = var1.equals((java.lang.Object)var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 1+ "'", var21.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 1+ "'", var24.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);

  }

  public void test389() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test389"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var7.setId((java.lang.Integer)10);
    java.lang.String var10 = var7.toString();
    java.lang.String var11 = var7.toString();
    java.lang.String var12 = var7.getStatus();
    boolean var13 = var1.equals((java.lang.Object)var7);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var19.setId((java.lang.Integer)100);
    boolean var22 = var16.equals((java.lang.Object)var19);
    boolean var23 = var1.equals((java.lang.Object)var19);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var26 = var25.getId();
    var25.setId((java.lang.Integer)(-1));
    java.util.Collection var29 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)10);
    java.lang.Integer var32 = var25.getId();
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var35 = var19.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);

  }

  public void test390() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test390"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var10 = var1.getId();
    java.lang.Integer var11 = var1.getId();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var13.setId((java.lang.Integer)10);
    java.lang.String var16 = var13.toString();
    boolean var18 = var13.equals((java.lang.Object)(byte)100);
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var21 = var13.getStatus();
    var13.setId((java.lang.Integer)10);
    boolean var24 = var1.equals((java.lang.Object)var13);
    var13.setId((java.lang.Integer)0);
    java.util.Collection var27 = var13.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);

  }

  public void test391() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test391"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var9 = var8.getPropostaCollection();
    var8.setId((java.lang.Integer)100);
    java.lang.Integer var12 = var8.getId();
    boolean var13 = var7.equals((java.lang.Object)var8);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var16.setId((java.lang.Integer)(-1));
    var16.setStatus("hi!");
    boolean var21 = var8.equals((java.lang.Object)"hi!");
    boolean var22 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var23 = var1.toString();
    java.lang.String var24 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + ""+ "'", var24.equals(""));

  }

  public void test392() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test392"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.getStatus();
    java.util.Collection var6 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var10 = var9.toString();
    java.lang.String var11 = var9.toString();
    java.lang.String var12 = var9.toString();
    boolean var13 = var1.equals((java.lang.Object)var9);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    var15.setId((java.lang.Integer)0);
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var15.setId((java.lang.Integer)100);
    boolean var23 = var9.equals((java.lang.Object)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);

  }

  public void test393() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test393"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var5.getStatus();
    java.lang.Integer var11 = var5.getId();
    java.lang.String var12 = var5.toString();
    boolean var13 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    var15.setId((java.lang.Integer)(-1));
    boolean var20 = var15.equals((java.lang.Object)'4');
    java.lang.String var21 = var15.toString();
    java.lang.String var22 = var15.toString();
    var15.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var26.setId((java.lang.Integer)10);
    java.lang.String var29 = var26.toString();
    java.lang.String var30 = var26.toString();
    var26.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var37 = var35.equals((java.lang.Object)(-1.0d));
    java.util.Collection var38 = var35.getPropostaCollection();
    var35.setId((java.lang.Integer)(-1));
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var43 = var26.equals((java.lang.Object)var35);
    boolean var44 = var15.equals((java.lang.Object)var26);
    boolean var45 = var5.equals((java.lang.Object)var44);
    java.lang.String var46 = var5.getStatus();
    loja.com.br.entity.PropostaStatus var49 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var49.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var53 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var53.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var53.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var53.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var60 = var53.getId();
    boolean var61 = var49.equals((java.lang.Object)var60);
    var49.setId((java.lang.Integer)1);
    var49.setId((java.lang.Integer)1);
    boolean var66 = var5.equals((java.lang.Object)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var60 + "' != '" + 1+ "'", var60.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var66 == false);

  }

  public void test394() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test394"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var8 = var1.getId();
    java.lang.String var9 = var1.getStatus();
    java.lang.Integer var10 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 0+ "'", var10.equals(0));

  }

  public void test395() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test395"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var14 = var13.getId();
    boolean var15 = var9.equals((java.lang.Object)var13);
    java.lang.Integer var16 = var9.getId();
    var9.setId((java.lang.Integer)10);
    java.lang.Integer var19 = var9.getId();
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var22 = var9.getStatus();
    boolean var23 = var1.equals((java.lang.Object)var22);
    java.lang.String var24 = var1.getStatus();
    java.lang.String var25 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 1+ "'", var16.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test396() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test396"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    var4.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var15 = var14.getStatus();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var18 = var4.equals((java.lang.Object)var14);
    var14.setId((java.lang.Integer)100);
    var14.setId((java.lang.Integer)(-1));
    java.lang.Integer var23 = var14.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));

  }

  public void test397() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test397"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var6 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test398() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test398"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    var1.setStatus("hi!");
    java.util.Collection var7 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);

  }

  public void test399() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test399"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.String var8 = var1.getStatus();
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var11.setId((java.lang.Integer)10);
    var11.setStatus("");
    java.lang.Integer var16 = var11.getId();
    java.util.Collection var17 = var11.getPropostaCollection();
    java.lang.Integer var18 = var11.getId();
    boolean var19 = var1.equals((java.lang.Object)var11);
    java.lang.String var20 = var11.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 10+ "'", var18.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test400() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test400"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var8 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var11 = var1.getId();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var16 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)100);
    java.lang.Integer var19 = var15.getId();
    boolean var20 = var14.equals((java.lang.Object)var15);
    var15.setId((java.lang.Integer)(-1));
    java.lang.String var23 = var15.toString();
    boolean var24 = var1.equals((java.lang.Object)var15);
    java.util.Collection var25 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);

  }

  public void test401() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test401"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.toString();
    boolean var23 = var18.equals((java.lang.Object)(byte)100);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var26 = var10.equals((java.lang.Object)var18);
    boolean var27 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    var29.setId((java.lang.Integer)(-1));
    boolean var34 = var29.equals((java.lang.Object)'4');
    boolean var35 = var1.equals((java.lang.Object)var29);
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var40 = var39.getId();
    java.util.Collection var41 = var39.getPropostaCollection();
    var39.setId((java.lang.Integer)10);
    var39.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var47 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var47.setStatus("");
    java.lang.Integer var50 = var47.getId();
    java.util.Collection var51 = var47.getPropostaCollection();
    boolean var52 = var39.equals((java.lang.Object)var47);
    java.lang.String var53 = var39.toString();
    java.util.Collection var54 = var39.getPropostaCollection();
    java.lang.Integer var55 = var39.getId();
    boolean var56 = var1.equals((java.lang.Object)var39);
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + (-1)+ "'", var40.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + (-1)+ "'", var50.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var53.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + 10+ "'", var55.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var56 == false);

  }

  public void test402() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test402"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    java.lang.String var9 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test403() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test403"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    boolean var7 = var1.equals((java.lang.Object)0L);
    java.lang.Integer var8 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));

  }

  public void test404() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test404"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setId((java.lang.Integer)100);
    boolean var8 = var2.equals((java.lang.Object)var5);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);

  }

  public void test405() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test405"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    var1.setId((java.lang.Integer)100);
    java.lang.String var10 = var1.toString();
    java.lang.String var11 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var17 = var13.equals((java.lang.Object)false);
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var20 = var13.toString();
    boolean var21 = var1.equals((java.lang.Object)var13);
    java.lang.String var22 = var13.toString();
    java.lang.String var23 = var13.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + ""+ "'", var11.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test406() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test406"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("hi!");
    java.util.Collection var6 = var2.getPropostaCollection();
    var2.setStatus("hi!");
    java.lang.String var9 = var2.getStatus();
    java.lang.String var10 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test407() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test407"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.lang.Integer var17 = var1.getId();
    var1.setStatus("");
    java.lang.String var20 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));

  }

  public void test408() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test408"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var2 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setStatus("");
    java.lang.Integer var9 = var6.getId();
    java.util.Collection var10 = var6.getPropostaCollection();
    java.lang.String var11 = var6.toString();
    java.lang.String var12 = var6.toString();
    boolean var13 = var1.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var17 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var18 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);

  }

  public void test409() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test409"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var15 = var1.getId();
    java.lang.String var16 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 0+ "'", var15.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test410() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test410"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var18 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.util.Collection var21 = var1.getPropostaCollection();
    java.lang.Integer var22 = var1.getId();
    java.lang.Integer var23 = var1.getId();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 1+ "'", var22.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 1+ "'", var23.equals(1));

  }

  public void test411() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test411"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.Integer var5 = var1.getId();
    var1.setStatus("");
    java.lang.String var8 = var1.toString();
    java.lang.Integer var9 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 1+ "'", var2.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 1+ "'", var5.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 1+ "'", var9.equals(1));

  }

  public void test412() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test412"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));

  }

  public void test413() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test413"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.getStatus();
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);

  }

  public void test414() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test414"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    java.util.Collection var3 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test415() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test415"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.String var1 = var0.getStatus();
    java.util.Collection var2 = var0.getPropostaCollection();
    var0.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);

  }

  public void test416() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test416"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    java.lang.Integer var11 = var1.getId();
    java.util.Collection var12 = var1.getPropostaCollection();
    java.lang.String var13 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test417() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test417"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)1);
    var1.setStatus("");
    java.lang.String var10 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));

  }

  public void test418() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test418"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var3 = var2.getId();
    java.util.Collection var4 = var2.getPropostaCollection();
    java.lang.String var5 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test419() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test419"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var9.setId((java.lang.Integer)10);
    java.lang.String var12 = var9.toString();
    boolean var14 = var9.equals((java.lang.Object)(byte)100);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var17 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    java.util.Collection var20 = var1.getPropostaCollection();
    var1.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);

  }

  public void test420() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test420"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    java.lang.String var9 = var6.toString();
    java.lang.String var10 = var6.getStatus();
    boolean var11 = var1.equals((java.lang.Object)var6);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    java.util.Collection var17 = var15.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var20 = var19.getId();
    java.util.Collection var21 = var19.getPropostaCollection();
    var19.setId((java.lang.Integer)10);
    java.lang.String var24 = var19.toString();
    java.lang.String var25 = var19.getStatus();
    boolean var26 = var15.equals((java.lang.Object)var19);
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var15.setStatus("");
    java.util.Collection var31 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)1);
    boolean var34 = var1.equals((java.lang.Object)var15);
    var15.setId((java.lang.Integer)(-1));
    java.lang.String var37 = var15.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test421() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test421"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    java.lang.Integer var4 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setId((java.lang.Integer)1);
    java.lang.String var9 = var2.toString();
    java.lang.String var10 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 10+ "'", var4.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test422() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test422"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var10 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test423() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test423"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setId((java.lang.Integer)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test424() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test424"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    java.lang.Integer var11 = var1.getId();
    java.util.Collection var12 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test425() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test425"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var5 = var2.getId();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var10 = var7.toString();
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var7.setId((java.lang.Integer)1);
    java.util.Collection var15 = var7.getPropostaCollection();
    java.lang.String var16 = var7.getStatus();
    boolean var17 = var2.equals((java.lang.Object)var7);
    java.lang.Integer var18 = var7.getId();
    java.util.Collection var19 = var7.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 0+ "'", var5.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 1+ "'", var18.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);

  }

  public void test426() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test426"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)100);
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test427() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test427"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    java.lang.Integer var10 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 1+ "'", var10.equals(1));

  }

  public void test428() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test428"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)100);
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var12.setId((java.lang.Integer)100);
    java.lang.String var15 = var12.getStatus();
    java.lang.Integer var16 = var12.getId();
    java.util.Collection var17 = var12.getPropostaCollection();
    var12.setStatus("hi!");
    boolean var20 = var1.equals((java.lang.Object)"hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);

  }

  public void test429() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test429"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var8 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.lang.String var11 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var14 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test430() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test430"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    var4.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var15 = var14.getStatus();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var18 = var4.equals((java.lang.Object)var14);
    var4.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);

  }

  public void test431() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test431"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    java.lang.Integer var14 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.String var17 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test432() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test432"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    var5.setId((java.lang.Integer)(-1));
    java.util.Collection var10 = var5.getPropostaCollection();
    java.lang.String var11 = var5.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);

  }

  public void test433() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test433"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setId((java.lang.Integer)(-1));
    java.lang.Integer var6 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));

  }

  public void test434() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test434"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var8.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var21 = var18.getId();
    boolean var22 = var8.equals((java.lang.Object)var21);
    boolean var23 = var1.equals((java.lang.Object)var8);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var32 = var25.getStatus();
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var37 = var8.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var38 = var8.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 10+ "'", var21.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);

  }

  public void test435() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test435"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.Integer var8 = var2.getId();
    java.lang.String var9 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var11.setId((java.lang.Integer)10);
    java.lang.String var14 = var11.toString();
    java.lang.String var15 = var11.toString();
    var11.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var22 = var20.equals((java.lang.Object)(-1.0d));
    java.util.Collection var23 = var20.getPropostaCollection();
    var20.setId((java.lang.Integer)(-1));
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var28 = var11.equals((java.lang.Object)var20);
    var20.setStatus("hi!");
    var20.setId((java.lang.Integer)10);
    java.lang.Integer var33 = var20.getId();
    boolean var34 = var2.equals((java.lang.Object)var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 10+ "'", var33.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);

  }

  public void test436() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test436"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var7 = var6.getId();
    java.util.Collection var8 = var6.getPropostaCollection();
    var6.setId((java.lang.Integer)10);
    var6.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("");
    java.lang.Integer var17 = var14.getId();
    java.util.Collection var18 = var14.getPropostaCollection();
    boolean var19 = var6.equals((java.lang.Object)var14);
    var6.setId((java.lang.Integer)10);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var24 = var2.equals((java.lang.Object)var6);
    java.lang.Integer var25 = var2.getId();
    java.lang.String var26 = var2.getStatus();
    var2.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 1+ "'", var25.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test437() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test437"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var15 = var1.getStatus();
    boolean var17 = var1.equals((java.lang.Object)1.0d);
    java.lang.String var18 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var21 = var20.getId();
    java.util.Collection var22 = var20.getPropostaCollection();
    var20.setId((java.lang.Integer)10);
    java.lang.String var25 = var20.toString();
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var28 = var20.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var30.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var34 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var35 = var34.getId();
    boolean var36 = var30.equals((java.lang.Object)var34);
    java.util.Collection var37 = var34.getPropostaCollection();
    java.util.Collection var38 = var34.getPropostaCollection();
    boolean var39 = var20.equals((java.lang.Object)var34);
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var42 = var1.equals((java.lang.Object)var20);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var45 = var44.getId();
    boolean var47 = var44.equals((java.lang.Object)(short)0);
    var44.setStatus("");
    java.lang.String var50 = var44.getStatus();
    var44.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var53 = var20.equals((java.lang.Object)var44);
    java.lang.String var54 = var20.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + 100+ "'", var35.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + (-1)+ "'", var45.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + ""+ "'", var50.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test438() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test438"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var18 = var15.getStatus();
    java.lang.Integer var19 = var15.getId();
    boolean var20 = var1.equals((java.lang.Object)var15);
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 0+ "'", var19.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);

  }

  public void test439() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test439"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)100);
    java.lang.String var11 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test440() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test440"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var9 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test441() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test441"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var7 = var6.getPropostaCollection();
    var6.setId((java.lang.Integer)100);
    boolean var10 = var1.equals((java.lang.Object)var6);
    java.lang.String var11 = var6.toString();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var15 = var14.getStatus();
    java.lang.Integer var16 = var14.getId();
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var19 = var18.getId();
    java.util.Collection var20 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    var18.setStatus("hi!");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var18.setId((java.lang.Integer)10);
    boolean var29 = var14.equals((java.lang.Object)10);
    boolean var30 = var6.equals((java.lang.Object)var14);
    java.lang.String var31 = var6.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 0+ "'", var16.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var31.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test442() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test442"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    java.util.Collection var4 = var2.getPropostaCollection();
    java.lang.String var5 = var2.toString();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var9 = var8.getPropostaCollection();
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var15 = var13.equals((java.lang.Object)false);
    boolean var17 = var13.equals((java.lang.Object)10.0d);
    java.lang.String var18 = var13.toString();
    boolean var19 = var8.equals((java.lang.Object)var13);
    java.lang.String var20 = var13.getStatus();
    java.util.Collection var21 = var13.getPropostaCollection();
    boolean var22 = var2.equals((java.lang.Object)var13);
    java.lang.String var23 = var13.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);

  }

  public void test443() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test443"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.String var8 = var1.getStatus();
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var11.setId((java.lang.Integer)10);
    var11.setStatus("");
    java.lang.Integer var16 = var11.getId();
    java.util.Collection var17 = var11.getPropostaCollection();
    java.lang.Integer var18 = var11.getId();
    boolean var19 = var1.equals((java.lang.Object)var11);
    java.util.Collection var20 = var1.getPropostaCollection();
    java.lang.String var21 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var23.setId((java.lang.Integer)100);
    java.lang.String var26 = var23.getStatus();
    var23.setId((java.lang.Integer)10);
    java.lang.Integer var29 = var23.getId();
    java.lang.String var30 = var23.toString();
    var23.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var23.setId((java.lang.Integer)1);
    java.lang.String var35 = var23.getStatus();
    boolean var36 = var1.equals((java.lang.Object)var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 10+ "'", var18.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 10+ "'", var29.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);

  }

  public void test444() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test444"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var1.setId((java.lang.Integer)1);
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test445() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test445"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var8 = var1.getId();
    java.lang.String var9 = var1.toString();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test446() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test446"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.String var23 = var21.getStatus();
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test447() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test447"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    var18.setStatus("hi!");
    java.lang.Integer var30 = var18.getId();
    java.lang.Integer var31 = var18.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + 1+ "'", var30.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + 1+ "'", var31.equals(1));

  }

  public void test448() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test448"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var12 = var11.getId();
    boolean var13 = var7.equals((java.lang.Object)var11);
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var20 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 100+ "'", var20.equals(100));

  }

  public void test449() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test449"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    boolean var5 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var15 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 100+ "'", var11.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test450() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test450"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var4 = var3.getPropostaCollection();
    var3.setId((java.lang.Integer)100);
    java.lang.Integer var7 = var3.getId();
    boolean var8 = var2.equals((java.lang.Object)var3);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var11 = var2.getStatus();
    java.util.Collection var12 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)1);
    java.util.Collection var15 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test451() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test451"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)100);
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.toString();
    java.util.Collection var10 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test452() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test452"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    var4.setStatus("");
    var4.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var15 = var4.getPropostaCollection();
    java.lang.String var16 = var4.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test453() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test453"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    java.lang.Integer var10 = var1.getId();
    java.lang.String var11 = var1.toString();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test454() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test454"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    java.lang.String var7 = var1.getStatus();
    java.lang.String var8 = var1.toString();
    java.lang.String var9 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "hi!"+ "'", var7.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));

  }

  public void test455() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test455"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    var1.setStatus("");
    java.lang.String var20 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var23 = var22.getId();
    var22.setId((java.lang.Integer)(-1));
    java.util.Collection var26 = var22.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var33 = var32.getId();
    boolean var34 = var28.equals((java.lang.Object)var32);
    java.lang.Integer var35 = var28.getId();
    boolean var36 = var22.equals((java.lang.Object)var35);
    java.util.Collection var37 = var22.getPropostaCollection();
    boolean var38 = var1.equals((java.lang.Object)var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 100+ "'", var33.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + 1+ "'", var35.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == true);

  }

  public void test456() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test456"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var13 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var16 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test457() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test457"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test458() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test458"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    java.lang.String var9 = var6.toString();
    java.lang.String var10 = var6.getStatus();
    boolean var11 = var1.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    boolean var15 = var1.equals((java.lang.Object)0);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var17.setId((java.lang.Integer)10);
    java.lang.String var20 = var17.toString();
    boolean var21 = var1.equals((java.lang.Object)var17);
    java.lang.String var22 = var1.getStatus();
    java.util.Collection var23 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);

  }

  public void test459() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test459"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var7 = var6.toString();
    var6.setId((java.lang.Integer)(-1));
    boolean var10 = var2.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setId((java.lang.Integer)10);
    var12.setStatus("");
    java.lang.String var17 = var12.getStatus();
    var12.setId((java.lang.Integer)10);
    java.lang.String var20 = var12.toString();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var23 = var22.getId();
    java.util.Collection var24 = var22.getPropostaCollection();
    var22.setId((java.lang.Integer)10);
    var22.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var30.setStatus("");
    java.lang.Integer var33 = var30.getId();
    java.util.Collection var34 = var30.getPropostaCollection();
    boolean var35 = var22.equals((java.lang.Object)var30);
    var22.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var41 = var40.toString();
    java.lang.Integer var42 = var40.getId();
    var40.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var45 = var40.toString();
    boolean var46 = var22.equals((java.lang.Object)var40);
    boolean var47 = var12.equals((java.lang.Object)var22);
    boolean var48 = var2.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var50 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var50.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var50.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var57 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var58 = var50.equals((java.lang.Object)10);
    var50.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var61 = var50.getPropostaCollection();
    var50.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var64 = var2.equals((java.lang.Object)var50);
    var50.setId((java.lang.Integer)10);
    java.lang.Integer var67 = var50.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + (-1)+ "'", var33.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + 10+ "'", var42.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var45.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var67 + "' != '" + 10+ "'", var67.equals(10));

  }

  public void test460() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test460"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var8 = var7.getStatus();
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var11 = var7.toString();
    java.util.Collection var12 = var7.getPropostaCollection();
    boolean var13 = var2.equals((java.lang.Object)var7);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == true);

  }

  public void test461() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test461"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    var10.setStatus("hi!");
    var10.setId((java.lang.Integer)(-1));
    java.lang.String var23 = var10.getStatus();
    java.lang.String var24 = var10.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "hi!"+ "'", var23.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "hi!"+ "'", var24.equals("hi!"));

  }

  public void test462() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test462"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var11 = var1.equals((java.lang.Object)var8);
    java.lang.Integer var12 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));

  }

  public void test463() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test463"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var5.setStatus("");
    java.lang.Integer var8 = var5.getId();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.String var10 = var5.toString();
    boolean var11 = var2.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var5.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var18 = var5.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));

  }

  public void test464() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test464"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.toString();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    var11.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.util.Collection var23 = var19.getPropostaCollection();
    boolean var24 = var11.equals((java.lang.Object)var19);
    var11.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var30 = var29.toString();
    java.lang.Integer var31 = var29.getId();
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var34 = var29.toString();
    boolean var35 = var11.equals((java.lang.Object)var29);
    boolean var36 = var1.equals((java.lang.Object)var11);
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + 10+ "'", var31.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == true);

  }

  public void test465() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test465"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test466() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test466"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.Integer var1 = var0.getId();
    java.lang.Integer var2 = var0.getId();
    java.lang.String var3 = var0.getStatus();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.util.Collection var11 = var5.getPropostaCollection();
    boolean var12 = var0.equals((java.lang.Object)var5);
    var5.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);

  }

  public void test467() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test467"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    var1.setStatus("hi!");
    var1.setStatus("hi!");
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.Integer var9 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));

  }

  public void test468() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test468"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    java.lang.Integer var8 = var2.getId();
    var2.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));

  }

  public void test469() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test469"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    java.lang.Integer var11 = var1.getId();
    java.util.Collection var12 = var1.getPropostaCollection();
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test470() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test470"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("hi!");
    java.util.Collection var6 = var2.getPropostaCollection();
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.getStatus();
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "hi!"+ "'", var8.equals("hi!"));

  }

  public void test471() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test471"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setId((java.lang.Integer)100);
    java.lang.String var13 = var10.getStatus();
    var10.setId((java.lang.Integer)10);
    java.lang.Integer var16 = var10.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var21 = var19.equals((java.lang.Object)'#');
    var19.setStatus("hi!");
    boolean var24 = var10.equals((java.lang.Object)var19);
    var10.setId((java.lang.Integer)10);
    boolean var27 = var1.equals((java.lang.Object)var10);
    var10.setId((java.lang.Integer)10);
    java.lang.String var30 = var10.toString();
    java.util.Collection var31 = var10.getPropostaCollection();
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);

  }

  public void test472() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test472"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)1);
    java.lang.Integer var11 = var1.getId();
    java.lang.Integer var12 = var1.getId();
    java.lang.Integer var13 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 1+ "'", var12.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 1+ "'", var13.equals(1));

  }

  public void test473() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test473"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.toString();
    java.util.Collection var9 = var2.getPropostaCollection();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test474() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test474"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    var1.setId((java.lang.Integer)10);
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test475() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test475"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var15 = var1.getId();
    java.lang.String var16 = var1.getStatus();
    java.lang.String var17 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 0+ "'", var15.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test476() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test476"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var4 = var3.getPropostaCollection();
    var3.setId((java.lang.Integer)100);
    java.lang.Integer var7 = var3.getId();
    boolean var8 = var2.equals((java.lang.Object)var3);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var11.setId((java.lang.Integer)(-1));
    var11.setStatus("hi!");
    boolean var16 = var3.equals((java.lang.Object)"hi!");
    java.lang.String var17 = var3.getStatus();
    java.lang.String var18 = var3.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test477() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test477"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    java.util.Collection var11 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var14 = var13.getId();
    var13.setId((java.lang.Integer)(-1));
    java.util.Collection var17 = var13.getPropostaCollection();
    boolean var18 = var1.equals((java.lang.Object)var13);
    var13.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);

  }

  public void test478() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test478"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var10 = var5.getPropostaCollection();
    boolean var11 = var1.equals((java.lang.Object)var5);
    java.lang.String var12 = var5.toString();
    java.lang.Integer var13 = var5.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));

  }

  public void test479() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test479"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)(-1));

  }

  public void test480() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test480"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var13 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test481() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test481"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var1 = var0.getPropostaCollection();
    var0.setId((java.lang.Integer)100);
    var0.setId((java.lang.Integer)10);
    java.lang.String var6 = var0.getStatus();
    java.lang.String var7 = var0.toString();
    java.lang.String var8 = var0.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);

  }

  public void test482() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test482"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)10);
    java.lang.String var5 = var2.getStatus();
    java.lang.Integer var6 = var2.getId();
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));

  }

  public void test483() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test483"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    boolean var12 = var9.equals((java.lang.Object)(short)0);
    var9.setStatus("");
    boolean var15 = var1.equals((java.lang.Object)var9);
    java.lang.String var16 = var9.toString();
    java.lang.String var17 = var9.getStatus();
    var9.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));

  }

  public void test484() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test484"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var3 = var2.getId();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setId((java.lang.Integer)100);
    java.lang.String var8 = var5.getStatus();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.Integer var10 = var5.getId();
    java.lang.String var11 = var5.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var14 = var13.getPropostaCollection();
    java.lang.String var15 = var13.getStatus();
    boolean var16 = var5.equals((java.lang.Object)var13);
    boolean var17 = var2.equals((java.lang.Object)var16);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var21.setId((java.lang.Integer)10);
    var21.setStatus("");
    java.lang.Integer var26 = var21.getId();
    java.util.Collection var27 = var21.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    boolean var32 = var29.equals((java.lang.Object)(short)0);
    var29.setStatus("");
    boolean var35 = var21.equals((java.lang.Object)var29);
    java.lang.String var36 = var29.getStatus();
    java.lang.String var37 = var29.getStatus();
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var40 = var39.getId();
    java.lang.Integer var41 = var39.getId();
    java.lang.Integer var42 = var39.getId();
    var39.setId((java.lang.Integer)10);
    boolean var45 = var29.equals((java.lang.Object)var39);
    java.util.Collection var46 = var39.getPropostaCollection();
    java.lang.String var47 = var39.getStatus();
    boolean var48 = var2.equals((java.lang.Object)var39);
    java.util.Collection var49 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 100+ "'", var10.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + ""+ "'", var36.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + ""+ "'", var37.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + (-1)+ "'", var40.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + (-1)+ "'", var41.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + (-1)+ "'", var42.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);

  }

  public void test485() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test485"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    boolean var9 = var1.equals((java.lang.Object)1.0f);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var12.setId((java.lang.Integer)100);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var12.setStatus("");
    java.lang.String var19 = var12.toString();
    java.lang.String var20 = var12.getStatus();
    boolean var21 = var1.equals((java.lang.Object)var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);

  }

  public void test486() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test486"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var6 = var1.getStatus();
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.toString();
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 1+ "'", var7.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test487() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test487"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var11 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var14 = var13.toString();
    boolean var16 = var13.equals((java.lang.Object)"");
    boolean var17 = var1.equals((java.lang.Object)var13);
    java.lang.Integer var18 = var13.getId();
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 100+ "'", var18.equals(100));

  }

  public void test488() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test488"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.getStatus();
    java.lang.Integer var4 = var2.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var7 = var6.getId();
    java.util.Collection var8 = var6.getPropostaCollection();
    var6.setId((java.lang.Integer)10);
    var6.setStatus("hi!");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var6.setId((java.lang.Integer)10);
    boolean var17 = var2.equals((java.lang.Object)10);
    java.lang.String var18 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 0+ "'", var4.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test489() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test489"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setId((java.lang.Integer)100);
    java.lang.String var13 = var10.getStatus();
    var10.setId((java.lang.Integer)10);
    java.lang.Integer var16 = var10.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var21 = var19.equals((java.lang.Object)'#');
    var19.setStatus("hi!");
    boolean var24 = var10.equals((java.lang.Object)var19);
    var10.setId((java.lang.Integer)10);
    boolean var27 = var1.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus();
    java.lang.String var29 = var28.toString();
    java.util.Collection var30 = var28.getPropostaCollection();
    java.lang.String var31 = var28.toString();
    java.util.Collection var32 = var28.getPropostaCollection();
    java.util.Collection var33 = var28.getPropostaCollection();
    boolean var34 = var1.equals((java.lang.Object)var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var31.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);

  }

  public void test490() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test490"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var14 = var1.getPropostaCollection();
    java.lang.Integer var15 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 1+ "'", var15.equals(1));

  }

  public void test491() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test491"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    var1.setStatus("");
    java.lang.Integer var10 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));

  }

  public void test492() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test492"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var7 = var6.toString();
    var6.setId((java.lang.Integer)(-1));
    boolean var10 = var2.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setId((java.lang.Integer)10);
    var12.setStatus("");
    java.lang.String var17 = var12.getStatus();
    var12.setId((java.lang.Integer)10);
    java.lang.String var20 = var12.toString();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var23 = var22.getId();
    java.util.Collection var24 = var22.getPropostaCollection();
    var22.setId((java.lang.Integer)10);
    var22.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var30.setStatus("");
    java.lang.Integer var33 = var30.getId();
    java.util.Collection var34 = var30.getPropostaCollection();
    boolean var35 = var22.equals((java.lang.Object)var30);
    var22.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var41 = var40.toString();
    java.lang.Integer var42 = var40.getId();
    var40.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var45 = var40.toString();
    boolean var46 = var22.equals((java.lang.Object)var40);
    boolean var47 = var12.equals((java.lang.Object)var22);
    boolean var48 = var2.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var50 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var50.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var50.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var57 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var58 = var50.equals((java.lang.Object)10);
    var50.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var61 = var50.getPropostaCollection();
    var50.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var64 = var2.equals((java.lang.Object)var50);
    var50.setId((java.lang.Integer)10);
    java.util.Collection var67 = var50.getPropostaCollection();
    var50.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + (-1)+ "'", var33.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + 10+ "'", var42.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var45.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var67);

  }

  public void test493() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test493"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var9 = var1.toString();
    java.lang.Integer var10 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));

  }

  public void test494() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test494"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test495() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test495"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    var1.setStatus("");
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "hi!");
    boolean var15 = var1.equals((java.lang.Object)var14);
    java.lang.String var16 = var14.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "hi!"+ "'", var16.equals("hi!"));

  }

  public void test496() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test496"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    var16.setId((java.lang.Integer)(-1));
    boolean var21 = var16.equals((java.lang.Object)(short)(-1));
    var16.setId((java.lang.Integer)10);
    java.lang.String var24 = var16.getStatus();
    boolean var25 = var10.equals((java.lang.Object)var16);
    boolean var26 = var2.equals((java.lang.Object)var10);
    java.lang.Integer var27 = var10.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 1+ "'", var27.equals(1));

  }

  public void test497() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test497"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.Integer var8 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var11 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var15 = var14.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var19 = var18.toString();
    var18.setId((java.lang.Integer)(-1));
    boolean var22 = var14.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.String var25 = var24.toString();
    java.util.Collection var26 = var24.getPropostaCollection();
    boolean var27 = var18.equals((java.lang.Object)var24);
    boolean var28 = var1.equals((java.lang.Object)var24);
    java.lang.String var29 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test498() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test498"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.getStatus();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.util.Collection var10 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var13 = var1.getStatus();
    java.lang.String var14 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + ""+ "'", var13.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test499() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test499"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    java.lang.String var5 = var2.toString();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test500() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus0.test500"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var1.toString();
    java.lang.String var11 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

}
