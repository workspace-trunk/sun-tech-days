
import junit.framework.*;

public class PropostaStatus11 extends TestCase {

  public static boolean debug = false;

  public void test1() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test1"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var5.setStatus("");
    java.lang.Integer var8 = var5.getId();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.String var10 = var5.toString();
    boolean var11 = var2.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var5.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var16 = var5.toString();
    java.lang.String var17 = var5.getStatus();
    var5.setId((java.lang.Integer)10);
    java.lang.String var20 = var5.toString();
    var5.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test2() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test2"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var1 = var0.getPropostaCollection();
    var0.setStatus("hi!");
    java.lang.Integer var4 = var0.getId();
    java.lang.Integer var5 = var0.getId();
    var0.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var8 = var0.toString();
    java.util.Collection var9 = var0.getPropostaCollection();
    java.lang.String var10 = var0.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test3() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test3"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var9 = var1.getId();
    var1.setId((java.lang.Integer)100);
    java.lang.String var12 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test4() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test4"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.getStatus();
    java.lang.String var4 = var2.toString();
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setStatus("hi!");
    java.util.Collection var8 = var2.getPropostaCollection();
    java.lang.String var9 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test5() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test5"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var8 = var1.getId();
    java.lang.Integer var9 = var1.getId();
    java.lang.String var10 = var1.getStatus();
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.toString();
    java.lang.String var13 = var1.getStatus();
    java.util.Collection var14 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 1+ "'", var9.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test6() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test6"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var10 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test7() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test7"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var5.getStatus();
    java.lang.Integer var11 = var5.getId();
    java.lang.String var12 = var5.toString();
    boolean var13 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    var15.setId((java.lang.Integer)(-1));
    boolean var20 = var15.equals((java.lang.Object)'4');
    java.lang.String var21 = var15.toString();
    java.lang.String var22 = var15.toString();
    var15.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var26.setId((java.lang.Integer)10);
    java.lang.String var29 = var26.toString();
    java.lang.String var30 = var26.toString();
    var26.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var37 = var35.equals((java.lang.Object)(-1.0d));
    java.util.Collection var38 = var35.getPropostaCollection();
    var35.setId((java.lang.Integer)(-1));
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var43 = var26.equals((java.lang.Object)var35);
    boolean var44 = var15.equals((java.lang.Object)var26);
    boolean var45 = var5.equals((java.lang.Object)var44);
    java.lang.String var46 = var5.getStatus();
    var5.setId((java.lang.Integer)1);
    java.util.Collection var49 = var5.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);

  }

  public void test8() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test8"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.String var11 = var1.getStatus();
    java.lang.String var12 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var15 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test9() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test9"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.lang.Integer var7 = var2.getId();
    java.lang.String var8 = var2.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    var16.setId((java.lang.Integer)(-1));
    boolean var21 = var16.equals((java.lang.Object)(short)(-1));
    var16.setId((java.lang.Integer)10);
    java.lang.String var24 = var16.getStatus();
    boolean var25 = var10.equals((java.lang.Object)var16);
    boolean var26 = var2.equals((java.lang.Object)var10);
    java.util.Collection var27 = var10.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var32 = var29.toString();
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var35 = var29.toString();
    java.lang.String var36 = var29.toString();
    var29.setId((java.lang.Integer)1);
    boolean var39 = var10.equals((java.lang.Object)var29);
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var45 = var44.getPropostaCollection();
    java.lang.Integer var46 = var44.getId();
    boolean var48 = var44.equals((java.lang.Object)'4');
    boolean var49 = var42.equals((java.lang.Object)var48);
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var52 = var42.getPropostaCollection();
    boolean var53 = var10.equals((java.lang.Object)var42);
    java.lang.Integer var54 = var42.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + 1+ "'", var46.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var52);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + 100+ "'", var54.equals(100));

  }

  public void test10() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test10"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    var5.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var13.setStatus("");
    java.lang.Integer var16 = var13.getId();
    java.util.Collection var17 = var13.getPropostaCollection();
    boolean var18 = var5.equals((java.lang.Object)var13);
    var5.setId((java.lang.Integer)10);
    boolean var22 = var5.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var26 = var5.equals((java.lang.Object)var25);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var29 = var25.toString();
    java.lang.Integer var30 = var25.getId();
    java.lang.String var31 = var25.toString();
    boolean var32 = var1.equals((java.lang.Object)var31);
    java.lang.String var33 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var31.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test11() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test11"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    var2.setStatus("");
    java.util.Collection var7 = var2.getPropostaCollection();
    java.util.Collection var8 = var2.getPropostaCollection();
    java.lang.String var9 = var2.getStatus();
    java.lang.Integer var10 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 1+ "'", var10.equals(1));

  }

  public void test12() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test12"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    var15.setId((java.lang.Integer)(-1));
    java.util.Collection var19 = var15.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var26 = var25.getId();
    boolean var27 = var21.equals((java.lang.Object)var25);
    java.lang.Integer var28 = var21.getId();
    boolean var29 = var15.equals((java.lang.Object)var28);
    java.util.Collection var30 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)0);
    boolean var33 = var1.equals((java.lang.Object)0);
    java.lang.String var34 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var37 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 100+ "'", var26.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test13() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test13"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.getStatus();
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.Integer var6 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));

  }

  public void test14() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test14"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var2 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setStatus("");
    java.lang.Integer var9 = var6.getId();
    java.util.Collection var10 = var6.getPropostaCollection();
    java.lang.String var11 = var6.toString();
    java.lang.String var12 = var6.toString();
    boolean var13 = var1.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var17 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var22.setId((java.lang.Integer)100);
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var22.setStatus("");
    java.lang.String var29 = var22.toString();
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var33 = var32.getStatus();
    boolean var34 = var22.equals((java.lang.Object)var33);
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus();
    java.lang.Integer var36 = var35.getId();
    java.lang.Integer var37 = var35.getId();
    java.lang.String var38 = var35.getStatus();
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var41 = var40.getId();
    java.util.Collection var42 = var40.getPropostaCollection();
    var40.setId((java.lang.Integer)10);
    java.lang.String var45 = var40.toString();
    java.util.Collection var46 = var40.getPropostaCollection();
    boolean var47 = var35.equals((java.lang.Object)var40);
    boolean var48 = var22.equals((java.lang.Object)var35);
    boolean var49 = var1.equals((java.lang.Object)var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + (-1)+ "'", var41.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var45.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);

  }

  public void test15() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test15"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var17 = var9.toString();
    java.util.Collection var18 = var9.getPropostaCollection();
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);

  }

  public void test16() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test16"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var9 = var1.getPropostaCollection();
    java.util.Collection var10 = var1.getPropostaCollection();
    java.lang.Integer var11 = var1.getId();
    java.util.Collection var12 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test17() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test17"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var4 = var3.getId();
    var3.setId((java.lang.Integer)(-1));
    boolean var8 = var3.equals((java.lang.Object)'4');
    java.lang.String var9 = var3.toString();
    java.lang.String var10 = var3.toString();
    boolean var11 = var1.equals((java.lang.Object)var10);
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);

  }

  public void test18() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test18"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    var1.setStatus("");
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test19() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test19"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var8 = var1.toString();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var12 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test20() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test20"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    java.lang.Integer var8 = var1.getId();
    java.lang.Integer var9 = var1.getId();
    java.lang.Integer var10 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 100+ "'", var8.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 100+ "'", var10.equals(100));

  }

  public void test21() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test21"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setId((java.lang.Integer)100);
    java.lang.String var13 = var10.getStatus();
    var10.setId((java.lang.Integer)10);
    java.lang.Integer var16 = var10.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var21 = var19.equals((java.lang.Object)'#');
    var19.setStatus("hi!");
    boolean var24 = var10.equals((java.lang.Object)var19);
    var10.setId((java.lang.Integer)10);
    boolean var27 = var1.equals((java.lang.Object)var10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var30 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test22() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test22"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var9.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    java.lang.String var17 = var12.toString();
    boolean var18 = var9.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var22 = var12.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var23 = var1.equals((java.lang.Object)var12);
    java.lang.String var24 = var12.toString();
    var12.setId((java.lang.Integer)10);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var12.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var32.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var32.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var32.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var39 = var32.getId();
    java.util.Collection var40 = var32.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var45 = var42.toString();
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var42.setId((java.lang.Integer)1);
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var52 = var32.equals((java.lang.Object)var42);
    boolean var53 = var12.equals((java.lang.Object)var42);
    java.lang.String var54 = var12.getStatus();
    loja.com.br.entity.PropostaStatus var57 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    java.lang.Integer var58 = var57.getId();
    var57.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var61 = var57.getStatus();
    boolean var62 = var12.equals((java.lang.Object)var57);
    java.util.Collection var63 = var57.getPropostaCollection();
    java.util.Collection var64 = var57.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + 1+ "'", var39.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var45.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var58 + "' != '" + 100+ "'", var58.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var61 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var61.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var63);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var64);

  }

  public void test23() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test23"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    boolean var5 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var6 = var1.getStatus();
    java.lang.String var7 = var1.getStatus();
    java.lang.Integer var8 = var1.getId();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var11 = var10.getId();
    java.util.Collection var12 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)10);
    var10.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var18.setStatus("");
    java.lang.Integer var21 = var18.getId();
    java.util.Collection var22 = var18.getPropostaCollection();
    boolean var23 = var10.equals((java.lang.Object)var18);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var26 = var18.toString();
    var18.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var30.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var34 = var30.equals((java.lang.Object)false);
    boolean var35 = var18.equals((java.lang.Object)var30);
    boolean var36 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var37 = var1.getId();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + ""+ "'", var7.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + (-1)+ "'", var37.equals((-1)));

  }

  public void test24() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test24"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var2 = var1.getId();
    java.lang.String var3 = var1.toString();
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 1+ "'", var2.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 1+ "'", var4.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test25() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test25"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var9 = var1.getId();
    var1.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var16 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)100);
    java.lang.Integer var19 = var15.getId();
    boolean var20 = var14.equals((java.lang.Object)var15);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var23 = var1.equals((java.lang.Object)var14);
    java.lang.Integer var24 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 1+ "'", var24.equals(1));

  }

  public void test26() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test26"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.Integer var8 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var11 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.String var14 = var1.toString();
    java.lang.String var15 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test27() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test27"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    java.lang.Integer var3 = var2.getId();
    var2.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var12 = var7.getStatus();
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var15 = var7.getId();
    boolean var16 = var2.equals((java.lang.Object)var7);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 1+ "'", var15.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);

  }

  public void test28() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test28"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var7 = var6.getId();
    java.lang.Integer var8 = var6.getId();
    java.lang.Integer var9 = var6.getId();
    var6.setStatus("");
    java.util.Collection var12 = var6.getPropostaCollection();
    java.lang.String var13 = var6.getStatus();
    var6.setId((java.lang.Integer)10);
    var6.setId((java.lang.Integer)0);
    boolean var18 = var1.equals((java.lang.Object)0);
    java.util.Collection var19 = var1.getPropostaCollection();
    java.lang.String var20 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + ""+ "'", var13.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test29() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test29"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var9 = var1.equals((java.lang.Object)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var12 = var1.toString();
    java.lang.String var13 = var1.toString();
    java.lang.Integer var14 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));

  }

  public void test30() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test30"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    java.lang.String var19 = var1.getStatus();
    java.util.Collection var20 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    var23.setId((java.lang.Integer)0);
    boolean var26 = var1.equals((java.lang.Object)0);
    java.lang.Integer var27 = var1.getId();
    java.util.Collection var28 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var31 = var1.getId();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "hi!"+ "'", var19.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 10+ "'", var27.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + 10+ "'", var31.equals(10));

  }

  public void test31() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test31"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var26 = var25.getId();
    java.util.Collection var27 = var25.getPropostaCollection();
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var30 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)(-1));
    boolean var33 = var1.equals((java.lang.Object)var25);
    var25.setStatus("hi!");
    java.util.Collection var36 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)0);
    var25.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);

  }

  public void test32() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test32"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var2 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    java.lang.Integer var6 = var4.getId();
    java.lang.Integer var7 = var4.getId();
    boolean var8 = var1.equals((java.lang.Object)var4);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var10.setStatus("");
    java.lang.Integer var13 = var10.getId();
    java.lang.Integer var14 = var10.getId();
    java.lang.Integer var15 = var10.getId();
    java.util.Collection var16 = var10.getPropostaCollection();
    boolean var17 = var1.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setId((java.lang.Integer)10);
    var19.setStatus("");
    java.lang.Integer var24 = var19.getId();
    java.util.Collection var25 = var19.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var28 = var27.getId();
    boolean var30 = var27.equals((java.lang.Object)(short)0);
    var27.setStatus("");
    boolean var33 = var19.equals((java.lang.Object)var27);
    java.lang.String var34 = var27.getStatus();
    loja.com.br.entity.PropostaStatus var36 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var37 = var36.getId();
    var36.setId((java.lang.Integer)(-1));
    var36.setId((java.lang.Integer)100);
    java.lang.String var42 = var36.toString();
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    boolean var45 = var36.equals((java.lang.Object)(-1));
    java.lang.String var46 = var36.toString();
    java.lang.String var47 = var36.toString();
    boolean var48 = var27.equals((java.lang.Object)var47);
    loja.com.br.entity.PropostaStatus var51 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var52 = var51.toString();
    java.lang.String var53 = var51.getStatus();
    java.lang.Integer var54 = var51.getId();
    java.lang.String var55 = var51.toString();
    boolean var56 = var27.equals((java.lang.Object)var51);
    loja.com.br.entity.PropostaStatus var58 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var59 = var58.getId();
    var58.setId((java.lang.Integer)(-1));
    boolean var63 = var58.equals((java.lang.Object)'4');
    java.lang.String var64 = var58.toString();
    var58.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var69 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var69.setId((java.lang.Integer)100);
    boolean var72 = var58.equals((java.lang.Object)var69);
    var69.setId((java.lang.Integer)0);
    var69.setStatus("");
    java.lang.String var77 = var69.getStatus();
    boolean var78 = var51.equals((java.lang.Object)var69);
    boolean var79 = var1.equals((java.lang.Object)var69);
    var1.setId((java.lang.Integer)0);
    java.lang.String var82 = var1.toString();
    loja.com.br.entity.PropostaStatus var84 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var85 = var84.getPropostaCollection();
    var84.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var89 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var89.setStatus("");
    java.lang.Integer var92 = var89.getId();
    java.util.Collection var93 = var89.getPropostaCollection();
    java.lang.String var94 = var89.toString();
    java.lang.String var95 = var89.toString();
    boolean var96 = var84.equals((java.lang.Object)var95);
    java.lang.Integer var97 = var84.getId();
    boolean var98 = var1.equals((java.lang.Object)var84);
    java.util.Collection var99 = var84.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 10+ "'", var24.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + ""+ "'", var34.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + (-1)+ "'", var37.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var42.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var52.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var53.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + (-1)+ "'", var54.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var55.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var56 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + (-1)+ "'", var59.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var63 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var64 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var64.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var72 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var77 + "' != '" + ""+ "'", var77.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var78 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var79 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var82 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var82.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var85);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var92 + "' != '" + (-1)+ "'", var92.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var93);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var94 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var94.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var95 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var95.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var96 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var97 + "' != '" + 0+ "'", var97.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var98 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var99);

  }

  public void test33() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test33"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    java.lang.Integer var3 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var6 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var8.setStatus("");
    java.lang.Integer var11 = var8.getId();
    java.lang.String var12 = var8.getStatus();
    java.lang.String var13 = var8.toString();
    java.lang.Integer var14 = var8.getId();
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var17 = var8.getId();
    java.lang.Integer var18 = var8.getId();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var20.setId((java.lang.Integer)10);
    java.lang.String var23 = var20.toString();
    boolean var25 = var20.equals((java.lang.Object)(byte)100);
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var28 = var20.getStatus();
    var20.setId((java.lang.Integer)10);
    boolean var31 = var8.equals((java.lang.Object)var20);
    boolean var32 = var2.equals((java.lang.Object)var8);
    var8.setId((java.lang.Integer)1);
    java.util.Collection var35 = var8.getPropostaCollection();
    java.lang.Integer var36 = var8.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + ""+ "'", var12.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 1+ "'", var36.equals(1));

  }

  public void test34() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test34"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    var1.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var8 = var7.toString();
    java.util.Collection var9 = var7.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    boolean var17 = var11.equals((java.lang.Object)var15);
    java.lang.Integer var18 = var11.getId();
    var11.setId((java.lang.Integer)10);
    java.lang.String var21 = var11.toString();
    boolean var22 = var7.equals((java.lang.Object)var21);
    boolean var23 = var1.equals((java.lang.Object)var21);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 1+ "'", var18.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);

  }

  public void test35() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test35"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var4 = var2.equals((java.lang.Object)'#');
    var2.setStatus("hi!");
    java.util.Collection var7 = var2.getPropostaCollection();
    java.lang.Integer var8 = var2.getId();
    java.lang.Integer var9 = var2.getId();
    java.util.Collection var10 = var2.getPropostaCollection();
    java.lang.Integer var11 = var2.getId();
    java.lang.String var12 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 10+ "'", var9.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "hi!"+ "'", var12.equals("hi!"));

  }

  public void test36() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test36"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=1 ]");
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var14 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var15 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var18 = var1.getPropostaCollection();
    java.lang.String var19 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test37() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test37"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setId((java.lang.Integer)100);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.util.Collection var7 = var2.getPropostaCollection();
    java.lang.String var8 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test38() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test38"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var15 = var1.getId();
    java.lang.String var16 = var1.getStatus();
    java.lang.Integer var17 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var20 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 0+ "'", var15.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 0+ "'", var17.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test39() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test39"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    boolean var6 = var1.equals((java.lang.Object)(byte)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var9 = var1.getStatus();
    java.util.Collection var10 = var1.getPropostaCollection();
    java.lang.String var11 = var1.getStatus();
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    java.util.Collection var19 = var17.getPropostaCollection();
    var17.setId((java.lang.Integer)10);
    var17.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var25.setStatus("");
    java.lang.Integer var28 = var25.getId();
    java.util.Collection var29 = var25.getPropostaCollection();
    boolean var30 = var17.equals((java.lang.Object)var25);
    var17.setId((java.lang.Integer)10);
    boolean var34 = var17.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var38 = var17.equals((java.lang.Object)var37);
    java.lang.Integer var39 = var37.getId();
    java.lang.String var40 = var37.getStatus();
    var37.setId((java.lang.Integer)0);
    boolean var43 = var1.equals((java.lang.Object)0);
    java.lang.String var44 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + (-1)+ "'", var39.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + "hi!"+ "'", var44.equals("hi!"));

  }

  public void test40() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test40"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setId((java.lang.Integer)0);
    boolean var13 = var1.equals((java.lang.Object)var10);
    java.util.Collection var14 = var10.getPropostaCollection();
    var10.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var25 = var24.getId();
    var24.setId((java.lang.Integer)(-1));
    boolean var29 = var24.equals((java.lang.Object)(short)(-1));
    var24.setId((java.lang.Integer)10);
    java.lang.String var32 = var24.getStatus();
    boolean var33 = var18.equals((java.lang.Object)var24);
    var24.setId((java.lang.Integer)100);
    java.lang.Integer var36 = var24.getId();
    var24.setId((java.lang.Integer)1);
    boolean var39 = var10.equals((java.lang.Object)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 100+ "'", var36.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);

  }

  public void test41() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test41"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.Integer var23 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus();
    var26.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var29 = var26.getPropostaCollection();
    java.util.Collection var30 = var26.getPropostaCollection();
    java.util.Collection var31 = var26.getPropostaCollection();
    var26.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var34 = var1.equals((java.lang.Object)var26);
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var38.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var38.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var45 = var44.getId();
    var44.setId((java.lang.Integer)(-1));
    boolean var49 = var44.equals((java.lang.Object)(short)(-1));
    var44.setId((java.lang.Integer)10);
    java.lang.String var52 = var44.getStatus();
    boolean var53 = var38.equals((java.lang.Object)var44);
    var44.setId((java.lang.Integer)100);
    java.lang.String var56 = var44.getStatus();
    loja.com.br.entity.PropostaStatus var58 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var59 = var58.getId();
    java.util.Collection var60 = var58.getPropostaCollection();
    var58.setId((java.lang.Integer)10);
    var58.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var66 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var66.setStatus("");
    java.lang.Integer var69 = var66.getId();
    java.util.Collection var70 = var66.getPropostaCollection();
    boolean var71 = var58.equals((java.lang.Object)var66);
    var58.setId((java.lang.Integer)10);
    boolean var75 = var58.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var78 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var79 = var58.equals((java.lang.Object)var78);
    java.lang.Integer var80 = var78.getId();
    java.lang.String var81 = var78.getStatus();
    boolean var82 = var44.equals((java.lang.Object)var78);
    boolean var83 = var1.equals((java.lang.Object)var78);
    java.lang.String var84 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 10+ "'", var23.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + (-1)+ "'", var45.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var52);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + (-1)+ "'", var59.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var60);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var69 + "' != '" + (-1)+ "'", var69.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var70);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var71 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var75 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var79 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var80 + "' != '" + (-1)+ "'", var80.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var81 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var81.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var82 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var83 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var84 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var84.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test42() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test42"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    java.util.Collection var17 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)10);
    var15.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var23.setStatus("");
    java.lang.Integer var26 = var23.getId();
    java.util.Collection var27 = var23.getPropostaCollection();
    boolean var28 = var15.equals((java.lang.Object)var23);
    var15.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var32.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var36 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var37 = var36.getId();
    boolean var38 = var32.equals((java.lang.Object)var36);
    java.lang.String var39 = var32.getStatus();
    java.lang.String var40 = var32.getStatus();
    boolean var41 = var15.equals((java.lang.Object)var32);
    var15.setId((java.lang.Integer)1);
    boolean var44 = var1.equals((java.lang.Object)var15);
    java.lang.Integer var45 = var15.getId();
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var15.setId((java.lang.Integer)0);
    java.lang.String var50 = var15.toString();
    java.util.Collection var51 = var15.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 100+ "'", var37.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + 1+ "'", var45.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var50.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);

  }

  public void test43() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test43"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setId((java.lang.Integer)10);
    java.lang.Integer var6 = var2.getId();
    java.lang.String var7 = var2.toString();
    var2.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var12.setId((java.lang.Integer)(-1));
    boolean var15 = var2.equals((java.lang.Object)var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);

  }

  public void test44() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test44"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    var5.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var13.setStatus("");
    java.lang.Integer var16 = var13.getId();
    java.util.Collection var17 = var13.getPropostaCollection();
    boolean var18 = var5.equals((java.lang.Object)var13);
    var5.setId((java.lang.Integer)10);
    boolean var22 = var5.equals((java.lang.Object)(byte)10);
    java.lang.String var23 = var5.getStatus();
    boolean var24 = var1.equals((java.lang.Object)var5);
    java.lang.String var25 = var5.toString();
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var29.setId((java.lang.Integer)100);
    java.lang.String var32 = var29.getStatus();
    java.util.Collection var33 = var29.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var37 = var35.equals((java.lang.Object)false);
    boolean var38 = var29.equals((java.lang.Object)var35);
    java.lang.String var39 = var35.getStatus();
    java.util.Collection var40 = var35.getPropostaCollection();
    boolean var41 = var5.equals((java.lang.Object)var35);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var47 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var47.setId((java.lang.Integer)100);
    boolean var50 = var44.equals((java.lang.Object)var47);
    var44.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var53 = var35.equals((java.lang.Object)var44);
    java.lang.String var54 = var44.getStatus();
    java.lang.String var55 = var44.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "hi!"+ "'", var23.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var55.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test45() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test45"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var7 = var1.getStatus();
    java.lang.String var8 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test46() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test46"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setId((java.lang.Integer)0);
    boolean var13 = var1.equals((java.lang.Object)var10);
    var1.setStatus("");
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);

  }

  public void test47() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test47"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var28 = var18.getId();
    java.util.Collection var29 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    java.util.Collection var32 = var18.getPropostaCollection();
    java.lang.String var33 = var18.toString();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var36 = var18.toString();
    java.lang.String var37 = var18.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test48() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test48"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    java.lang.Integer var5 = var2.getId();
    java.lang.Integer var6 = var2.getId();
    var2.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 1+ "'", var5.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 1+ "'", var6.equals(1));

  }

  public void test49() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test49"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    java.util.Collection var11 = var9.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var14 = var13.getId();
    java.util.Collection var15 = var13.getPropostaCollection();
    var13.setId((java.lang.Integer)10);
    java.lang.String var18 = var13.toString();
    java.lang.String var19 = var13.getStatus();
    boolean var20 = var9.equals((java.lang.Object)var13);
    var9.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var26 = var9.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var9.setId((java.lang.Integer)1);
    boolean var29 = var1.equals((java.lang.Object)var9);
    java.lang.String var30 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    java.lang.String var33 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "hi!"+ "'", var30.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test50() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test50"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var2.setId((java.lang.Integer)100);
    var2.setId((java.lang.Integer)1);
    var2.setId((java.lang.Integer)1);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var20 = var2.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);

  }

  public void test51() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test51"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    java.lang.String var9 = var6.toString();
    java.lang.String var10 = var6.getStatus();
    boolean var11 = var1.equals((java.lang.Object)var6);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var14 = var6.getId();
    java.lang.String var15 = var6.toString();
    java.util.Collection var16 = var6.getPropostaCollection();
    java.lang.Integer var17 = var6.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 0+ "'", var14.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 0+ "'", var17.equals(0));

  }

  public void test52() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test52"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setId((java.lang.Integer)100);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.util.Collection var7 = var2.getPropostaCollection();
    java.util.Collection var8 = var2.getPropostaCollection();
    java.lang.String var9 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    var11.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.util.Collection var23 = var19.getPropostaCollection();
    boolean var24 = var11.equals((java.lang.Object)var19);
    var11.setId((java.lang.Integer)10);
    boolean var28 = var11.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var32 = var11.equals((java.lang.Object)var31);
    java.lang.String var33 = var31.getStatus();
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var36 = var35.getPropostaCollection();
    java.lang.Integer var37 = var35.getId();
    boolean var39 = var35.equals((java.lang.Object)'4');
    loja.com.br.entity.PropostaStatus var41 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var42 = var41.getId();
    java.util.Collection var43 = var41.getPropostaCollection();
    var41.setId((java.lang.Integer)10);
    java.lang.String var46 = var41.toString();
    java.lang.String var47 = var41.getStatus();
    java.lang.Integer var48 = var41.getId();
    boolean var49 = var35.equals((java.lang.Object)var48);
    var35.setId((java.lang.Integer)1);
    java.lang.String var52 = var35.toString();
    boolean var53 = var31.equals((java.lang.Object)var35);
    var35.setId((java.lang.Integer)1);
    java.util.Collection var56 = var35.getPropostaCollection();
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var59 = var2.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 1+ "'", var37.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + (-1)+ "'", var42.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + 10+ "'", var48.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var52.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var59 == false);

  }

  public void test53() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test53"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var15 = var1.getId();
    java.util.Collection var16 = var1.getPropostaCollection();
    java.lang.Integer var17 = var1.getId();
    java.lang.String var18 = var1.toString();
    java.lang.String var19 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test54() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test54"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var17 = var9.toString();
    java.util.Collection var18 = var9.getPropostaCollection();
    var9.setId((java.lang.Integer)10);
    java.util.Collection var21 = var9.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);

  }

  public void test55() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test55"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.toString();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var12 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var15 = var1.getId();
    java.lang.Integer var16 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 0+ "'", var12.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 0+ "'", var15.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 0+ "'", var16.equals(0));

  }

  public void test56() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test56"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.String var6 = var1.getStatus();
    java.lang.String var7 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "hi!");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var10.setId((java.lang.Integer)1);
    boolean var15 = var1.equals((java.lang.Object)1);
    java.lang.String var16 = var1.getStatus();
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + ""+ "'", var16.equals(""));

  }

  public void test57() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test57"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);

  }

  public void test58() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test58"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var8 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    java.lang.Integer var11 = var1.getId();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var13.setId((java.lang.Integer)10);
    java.lang.String var16 = var13.toString();
    java.lang.String var17 = var13.toString();
    var13.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var24 = var22.equals((java.lang.Object)(-1.0d));
    java.util.Collection var25 = var22.getPropostaCollection();
    var22.setId((java.lang.Integer)(-1));
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var30 = var13.equals((java.lang.Object)var22);
    java.util.Collection var31 = var22.getPropostaCollection();
    java.lang.String var32 = var22.toString();
    java.util.Collection var33 = var22.getPropostaCollection();
    var22.setId((java.lang.Integer)10);
    java.lang.String var36 = var22.toString();
    boolean var37 = var1.equals((java.lang.Object)var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);

  }

  public void test59() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test59"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test60() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test60"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var2.setId((java.lang.Integer)100);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.util.Collection var7 = var2.getPropostaCollection();
    java.util.Collection var8 = var2.getPropostaCollection();
    java.lang.String var9 = var2.getStatus();
    java.lang.String var10 = var2.toString();
    java.util.Collection var11 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)1);
    java.lang.Integer var14 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));

  }

  public void test61() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test61"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    java.lang.Integer var3 = var2.getId();
    var2.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var7.setId((java.lang.Integer)10);
    var7.setStatus("");
    java.lang.Integer var12 = var7.getId();
    java.util.Collection var13 = var7.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    boolean var18 = var15.equals((java.lang.Object)(short)0);
    var15.setStatus("");
    boolean var21 = var7.equals((java.lang.Object)var15);
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var24 = var23.getId();
    boolean var26 = var23.equals((java.lang.Object)(short)0);
    java.util.Collection var27 = var23.getPropostaCollection();
    java.lang.Integer var28 = var23.getId();
    boolean var29 = var7.equals((java.lang.Object)var23);
    java.lang.String var30 = var23.getStatus();
    java.lang.String var31 = var23.toString();
    var23.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var40 = var39.getId();
    boolean var41 = var35.equals((java.lang.Object)var39);
    java.util.Collection var42 = var39.getPropostaCollection();
    java.util.Collection var43 = var39.getPropostaCollection();
    java.util.Collection var44 = var39.getPropostaCollection();
    boolean var45 = var23.equals((java.lang.Object)var39);
    java.lang.String var46 = var39.getStatus();
    boolean var47 = var2.equals((java.lang.Object)var39);
    loja.com.br.entity.PropostaStatus var48 = new loja.com.br.entity.PropostaStatus();
    var48.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var51 = var48.getPropostaCollection();
    java.util.Collection var52 = var48.getPropostaCollection();
    java.util.Collection var53 = var48.getPropostaCollection();
    java.util.Collection var54 = var48.getPropostaCollection();
    boolean var55 = var2.equals((java.lang.Object)var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + (-1)+ "'", var24.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var31.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + 100+ "'", var40.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var52);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var55 == false);

  }

  public void test62() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test62"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    var1.setStatus("hi!");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var11 = var10.getId();
    boolean var12 = var1.equals((java.lang.Object)var11);
    java.util.Collection var13 = var1.getPropostaCollection();
    java.util.Collection var14 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 100+ "'", var11.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test63() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test63"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var12 = var10.equals((java.lang.Object)'#');
    var10.setStatus("hi!");
    boolean var15 = var1.equals((java.lang.Object)var10);
    java.lang.String var16 = var10.toString();
    java.lang.Integer var17 = var10.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 10+ "'", var17.equals(10));

  }

  public void test64() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test64"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var23.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var27.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var27.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var27.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var34 = var27.getId();
    boolean var35 = var23.equals((java.lang.Object)var34);
    var23.setId((java.lang.Integer)1);
    java.util.Collection var38 = var23.getPropostaCollection();
    java.lang.String var39 = var23.toString();
    java.lang.String var40 = var23.toString();
    boolean var41 = var10.equals((java.lang.Object)var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + 1+ "'", var34.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);

  }

  public void test65() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test65"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var2.setId((java.lang.Integer)(-1));
    java.lang.String var5 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var8 = var7.getPropostaCollection();
    java.lang.Integer var9 = var7.getId();
    boolean var11 = var7.equals((java.lang.Object)'4');
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var14 = var7.getStatus();
    java.lang.String var15 = var7.getStatus();
    boolean var16 = var2.equals((java.lang.Object)var7);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var19.setId((java.lang.Integer)100);
    boolean var24 = var2.equals((java.lang.Object)100);
    java.lang.String var25 = var2.toString();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    java.util.Collection var31 = var29.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var34 = var33.getId();
    java.util.Collection var35 = var33.getPropostaCollection();
    var33.setId((java.lang.Integer)10);
    java.lang.String var38 = var33.toString();
    java.lang.String var39 = var33.getStatus();
    boolean var40 = var29.equals((java.lang.Object)var33);
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var43 = var29.getId();
    java.lang.String var44 = var29.toString();
    boolean var45 = var2.equals((java.lang.Object)var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 1+ "'", var9.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + (-1)+ "'", var34.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + (-1)+ "'", var43.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var44.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);

  }

  public void test66() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test66"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    java.lang.Integer var6 = var1.getId();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    java.lang.Integer var10 = var8.getId();
    java.lang.Integer var11 = var8.getId();
    java.lang.String var12 = var8.getStatus();
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var16.setStatus("");
    java.lang.Integer var19 = var16.getId();
    java.lang.String var20 = var16.getStatus();
    java.lang.String var21 = var16.toString();
    java.lang.Integer var22 = var16.getId();
    java.lang.String var23 = var16.getStatus();
    boolean var24 = var8.equals((java.lang.Object)var23);
    boolean var25 = var1.equals((java.lang.Object)var23);
    java.lang.Integer var26 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var29 = var1.getId();
    java.util.Collection var30 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 1+ "'", var6.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + ""+ "'", var23.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 1+ "'", var26.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 1+ "'", var29.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);

  }

  public void test67() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test67"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var7 = var1.getPropostaCollection();
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.util.Collection var10 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var13 = var12.getId();
    java.util.Collection var14 = var12.getPropostaCollection();
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var17 = var12.getStatus();
    var12.setStatus("hi!");
    var12.setId((java.lang.Integer)10);
    boolean var22 = var1.equals((java.lang.Object)var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);

  }

  public void test68() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test68"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    boolean var6 = var1.equals((java.lang.Object)(byte)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var9 = var1.getStatus();
    java.util.Collection var10 = var1.getPropostaCollection();
    java.lang.String var11 = var1.getStatus();
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)100);
    java.lang.String var16 = var1.toString();
    java.lang.String var17 = var1.toString();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.lang.String var23 = var19.getStatus();
    java.lang.String var24 = var19.toString();
    java.lang.Integer var25 = var19.getId();
    var19.setId((java.lang.Integer)100);
    java.lang.String var28 = var19.toString();
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var34 = var33.toString();
    var33.setStatus("hi!");
    java.lang.String var37 = var33.toString();
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var41 = var40.toString();
    java.lang.String var42 = var40.getStatus();
    java.lang.String var43 = var40.getStatus();
    boolean var44 = var33.equals((java.lang.Object)var43);
    var33.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var47 = var33.getId();
    boolean var48 = var19.equals((java.lang.Object)var47);
    boolean var49 = var1.equals((java.lang.Object)var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + ""+ "'", var23.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var42.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var43.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + 10+ "'", var47.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);

  }

  public void test69() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test69"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.String var1 = var0.toString();
    java.util.Collection var2 = var0.getPropostaCollection();
    java.lang.String var3 = var0.getStatus();
    var0.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var1.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);

  }

  public void test70() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test70"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    java.lang.String var2 = var1.toString();
    java.lang.String var3 = var1.getStatus();
    var1.setStatus("hi!");
    java.lang.String var6 = var1.toString();
    var1.setId((java.lang.Integer)100);
    java.lang.String var9 = var1.getStatus();
    java.lang.Integer var10 = var1.getId();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.lang.String var16 = var12.getStatus();
    java.lang.String var17 = var12.toString();
    var12.setStatus("");
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var26 = var21.getStatus();
    java.lang.Integer var27 = var21.getId();
    java.lang.String var28 = var21.getStatus();
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var31 = var21.getId();
    boolean var32 = var12.equals((java.lang.Object)var31);
    boolean var33 = var1.equals((java.lang.Object)var32);
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var38 = var35.toString();
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var35.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var45 = var44.getId();
    java.util.Collection var46 = var44.getPropostaCollection();
    var44.setId((java.lang.Integer)10);
    var44.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var52 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var52.setStatus("");
    java.lang.Integer var55 = var52.getId();
    java.util.Collection var56 = var52.getPropostaCollection();
    boolean var57 = var44.equals((java.lang.Object)var52);
    var44.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var61 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var61.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var65 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var66 = var65.getId();
    boolean var67 = var61.equals((java.lang.Object)var65);
    java.lang.String var68 = var61.getStatus();
    java.lang.String var69 = var61.getStatus();
    boolean var70 = var44.equals((java.lang.Object)var61);
    boolean var71 = var35.equals((java.lang.Object)var44);
    java.lang.Integer var72 = var44.getId();
    var44.setId((java.lang.Integer)10);
    java.lang.String var75 = var44.getStatus();
    java.lang.Integer var76 = var44.getId();
    boolean var77 = var1.equals((java.lang.Object)var76);
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 100+ "'", var10.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + ""+ "'", var16.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 1+ "'", var27.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + 1+ "'", var31.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + (-1)+ "'", var45.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + (-1)+ "'", var55.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var66 + "' != '" + 100+ "'", var66.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var67 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var68.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var69 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var69.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var70 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var71 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var72 + "' != '" + 10+ "'", var72.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var75 + "' != '" + "hi!"+ "'", var75.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var76 + "' != '" + 10+ "'", var76.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var77 == false);

  }

  public void test71() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test71"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var18 = var15.getStatus();
    java.lang.Integer var19 = var15.getId();
    boolean var20 = var1.equals((java.lang.Object)var15);
    java.lang.String var21 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var26 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 0+ "'", var19.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test72() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test72"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var3 = var2.getId();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setId((java.lang.Integer)100);
    java.lang.String var8 = var5.getStatus();
    java.util.Collection var9 = var5.getPropostaCollection();
    java.lang.Integer var10 = var5.getId();
    java.lang.String var11 = var5.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var14 = var13.getPropostaCollection();
    java.lang.String var15 = var13.getStatus();
    boolean var16 = var5.equals((java.lang.Object)var13);
    boolean var17 = var2.equals((java.lang.Object)var16);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var21.setId((java.lang.Integer)10);
    var21.setStatus("");
    java.lang.Integer var26 = var21.getId();
    java.util.Collection var27 = var21.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    boolean var32 = var29.equals((java.lang.Object)(short)0);
    var29.setStatus("");
    boolean var35 = var21.equals((java.lang.Object)var29);
    java.lang.String var36 = var29.getStatus();
    java.lang.String var37 = var29.getStatus();
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var40 = var39.getId();
    java.lang.Integer var41 = var39.getId();
    java.lang.Integer var42 = var39.getId();
    var39.setId((java.lang.Integer)10);
    boolean var45 = var29.equals((java.lang.Object)var39);
    java.util.Collection var46 = var39.getPropostaCollection();
    java.lang.String var47 = var39.getStatus();
    boolean var48 = var2.equals((java.lang.Object)var39);
    java.lang.String var49 = var2.toString();
    java.util.Collection var50 = var2.getPropostaCollection();
    java.util.Collection var51 = var2.getPropostaCollection();
    java.lang.Integer var52 = var2.getId();
    loja.com.br.entity.PropostaStatus var54 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var55 = var54.getId();
    java.lang.Integer var56 = var54.getId();
    java.lang.Integer var57 = var54.getId();
    java.lang.String var58 = var54.getStatus();
    var54.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var61 = var54.getPropostaCollection();
    java.lang.String var62 = var54.toString();
    java.lang.String var63 = var54.toString();
    boolean var64 = var2.equals((java.lang.Object)var63);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 100+ "'", var10.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + ""+ "'", var36.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + ""+ "'", var37.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + (-1)+ "'", var40.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + (-1)+ "'", var41.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + (-1)+ "'", var42.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var49.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + 100+ "'", var52.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + (-1)+ "'", var55.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var56 + "' != '" + (-1)+ "'", var56.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + (-1)+ "'", var57.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var58);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var62 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var62.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var63 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var63.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);

  }

  public void test73() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test73"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.Integer var5 = var1.getId();
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var9 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test74() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test74"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    var10.setStatus("hi!");
    var10.setId((java.lang.Integer)10);
    var10.setStatus("");
    java.lang.String var25 = var10.getStatus();
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var28.setId((java.lang.Integer)(-1));
    boolean var31 = var10.equals((java.lang.Object)var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + ""+ "'", var25.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);

  }

  public void test75() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test75"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.String var6 = var2.toString();
    java.lang.String var7 = var2.toString();
    var2.setStatus("");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    var11.setId((java.lang.Integer)10);
    java.util.Collection var16 = var11.getPropostaCollection();
    java.lang.String var17 = var11.toString();
    var11.setId((java.lang.Integer)0);
    java.lang.Integer var20 = var11.getId();
    var11.setId((java.lang.Integer)100);
    boolean var23 = var2.equals((java.lang.Object)var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 0+ "'", var20.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);

  }

  public void test76() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test76"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var22.setId((java.lang.Integer)100);
    boolean var25 = var1.equals((java.lang.Object)100);
    var1.setId((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var30 = var1.getId();
    java.util.Collection var31 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var34 = var33.getId();
    java.util.Collection var35 = var33.getPropostaCollection();
    var33.setId((java.lang.Integer)10);
    java.lang.String var38 = var33.toString();
    java.lang.Integer var39 = var33.getId();
    java.util.Collection var40 = var33.getPropostaCollection();
    java.lang.String var41 = var33.toString();
    var33.setStatus("");
    var33.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var46 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=10 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var49 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + (-1)+ "'", var34.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + 10+ "'", var39.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);

  }

  public void test77() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test77"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var9 = var8.getPropostaCollection();
    var8.setId((java.lang.Integer)100);
    java.lang.Integer var12 = var8.getId();
    boolean var13 = var7.equals((java.lang.Object)var8);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var16 = var7.getStatus();
    var7.setId((java.lang.Integer)0);
    boolean var19 = var2.equals((java.lang.Object)0);
    var2.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);

  }

  public void test78() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test78"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test79() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test79"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var3 = var2.getStatus();
    java.lang.Integer var4 = var2.getId();
    java.lang.String var5 = var2.toString();
    java.lang.Integer var6 = var2.getId();
    var2.setStatus("");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var11 = var10.getId();
    java.util.Collection var12 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)10);
    var10.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var18.setStatus("");
    java.lang.Integer var21 = var18.getId();
    java.util.Collection var22 = var18.getPropostaCollection();
    boolean var23 = var10.equals((java.lang.Object)var18);
    var10.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var27.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var32 = var31.getId();
    boolean var33 = var27.equals((java.lang.Object)var31);
    java.lang.String var34 = var27.getStatus();
    java.lang.String var35 = var27.getStatus();
    boolean var36 = var10.equals((java.lang.Object)var27);
    java.lang.Integer var37 = var27.getId();
    java.util.Collection var38 = var27.getPropostaCollection();
    var27.setId((java.lang.Integer)10);
    java.lang.String var41 = var27.toString();
    java.lang.String var42 = var27.toString();
    boolean var43 = var2.equals((java.lang.Object)var27);
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var46.setId((java.lang.Integer)(-1));
    java.util.Collection var49 = var46.getPropostaCollection();
    java.lang.String var50 = var46.toString();
    java.lang.String var51 = var46.getStatus();
    java.lang.Integer var52 = var46.getId();
    java.lang.Integer var53 = var46.getId();
    java.lang.String var54 = var46.toString();
    java.lang.String var55 = var46.getStatus();
    var46.setId((java.lang.Integer)100);
    boolean var58 = var27.equals((java.lang.Object)var46);
    java.lang.String var59 = var46.getStatus();
    java.lang.Integer var60 = var46.getId();
    java.lang.String var61 = var46.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 10+ "'", var4.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 100+ "'", var32.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 1+ "'", var37.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var42.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var50.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var51.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + (-1)+ "'", var52.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + (-1)+ "'", var53.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var55.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var59.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var60 + "' != '" + 100+ "'", var60.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var61 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var61.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test80() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test80"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    java.lang.String var24 = var1.toString();
    java.lang.Integer var25 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 10+ "'", var25.equals(10));

  }

  public void test81() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test81"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var21 = var20.getId();
    java.util.Collection var22 = var20.getPropostaCollection();
    var20.setId((java.lang.Integer)10);
    var20.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var29 = var28.getId();
    java.util.Collection var30 = var28.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var33 = var32.getId();
    java.util.Collection var34 = var32.getPropostaCollection();
    var32.setId((java.lang.Integer)10);
    java.lang.String var37 = var32.toString();
    java.lang.String var38 = var32.getStatus();
    boolean var39 = var28.equals((java.lang.Object)var32);
    var28.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var45 = var28.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    var28.setId((java.lang.Integer)1);
    boolean var48 = var20.equals((java.lang.Object)var28);
    java.lang.String var49 = var20.toString();
    boolean var50 = var1.equals((java.lang.Object)var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + (-1)+ "'", var29.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + (-1)+ "'", var33.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var49.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == true);

  }

  public void test82() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test82"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var12 = var11.getId();
    boolean var13 = var7.equals((java.lang.Object)var11);
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)1);
    java.lang.String var22 = var1.toString();
    java.lang.Integer var23 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 1+ "'", var23.equals(1));

  }

  public void test83() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test83"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var5 = var2.getId();
    java.lang.String var6 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    java.util.Collection var10 = var8.getPropostaCollection();
    var8.setId((java.lang.Integer)10);
    java.lang.String var13 = var8.toString();
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var22 = var21.getId();
    boolean var23 = var17.equals((java.lang.Object)var21);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var25.setId((java.lang.Integer)10);
    java.lang.String var28 = var25.toString();
    boolean var30 = var25.equals((java.lang.Object)(byte)100);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var33 = var17.equals((java.lang.Object)var25);
    boolean var34 = var8.equals((java.lang.Object)var25);
    loja.com.br.entity.PropostaStatus var36 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var37 = var36.getId();
    var36.setId((java.lang.Integer)(-1));
    boolean var41 = var36.equals((java.lang.Object)'4');
    boolean var42 = var8.equals((java.lang.Object)var36);
    loja.com.br.entity.PropostaStatus var45 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var47 = var45.equals((java.lang.Object)(-1.0d));
    java.util.Collection var48 = var45.getPropostaCollection();
    var45.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var53 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var54 = var53.toString();
    var53.setStatus("hi!");
    java.util.Collection var57 = var53.getPropostaCollection();
    var53.setStatus("hi!");
    boolean var60 = var45.equals((java.lang.Object)var53);
    java.util.Collection var61 = var53.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var63 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var65 = var63.equals((java.lang.Object)false);
    boolean var67 = var63.equals((java.lang.Object)10.0d);
    boolean var68 = var53.equals((java.lang.Object)10.0d);
    boolean var69 = var36.equals((java.lang.Object)var53);
    var53.setId((java.lang.Integer)100);
    java.util.Collection var72 = var53.getPropostaCollection();
    java.lang.Integer var73 = var53.getId();
    boolean var74 = var2.equals((java.lang.Object)var53);
    loja.com.br.entity.PropostaStatus var76 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var77 = var76.getId();
    java.util.Collection var78 = var76.getPropostaCollection();
    var76.setId((java.lang.Integer)10);
    java.util.Collection var81 = var76.getPropostaCollection();
    java.lang.String var82 = var76.toString();
    var76.setStatus("");
    java.util.Collection var85 = var76.getPropostaCollection();
    boolean var86 = var53.equals((java.lang.Object)var76);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 100+ "'", var22.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + (-1)+ "'", var37.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var57);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var65 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var67 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var68 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var69 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var72);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var73 + "' != '" + 100+ "'", var73.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var74 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var77 + "' != '" + (-1)+ "'", var77.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var78);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var81);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var82 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var82.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var85);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var86 == false);

  }

  public void test84() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test84"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    boolean var5 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.toString();
    java.lang.String var13 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var18 = var15.toString();
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var15.setId((java.lang.Integer)1);
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var25 = var15.getStatus();
    java.lang.String var26 = var15.toString();
    java.util.Collection var27 = var15.getPropostaCollection();
    java.util.Collection var28 = var15.getPropostaCollection();
    boolean var29 = var1.equals((java.lang.Object)var15);
    var15.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 100+ "'", var11.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);

  }

  public void test85() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test85"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var9 = var7.equals((java.lang.Object)false);
    boolean var10 = var1.equals((java.lang.Object)var7);
    java.util.Collection var11 = var1.getPropostaCollection();
    java.util.Collection var12 = var1.getPropostaCollection();
    java.lang.String var13 = var1.getStatus();
    java.util.Collection var14 = var1.getPropostaCollection();
    java.lang.String var15 = var1.toString();
    java.lang.String var16 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);

  }

  public void test86() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test86"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var3 = var2.getId();
    var2.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var7 = var6.getPropostaCollection();
    var6.setId((java.lang.Integer)100);
    java.lang.String var10 = var6.getStatus();
    java.lang.String var11 = var6.getStatus();
    var6.setId((java.lang.Integer)10);
    java.lang.String var14 = var6.getStatus();
    java.lang.String var15 = var6.getStatus();
    boolean var16 = var2.equals((java.lang.Object)var6);
    java.util.Collection var17 = var2.getPropostaCollection();
    java.lang.Integer var18 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));

  }

  public void test87() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test87"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.lang.String var7 = var1.toString();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var10 = var9.getPropostaCollection();
    java.lang.Integer var11 = var9.getId();
    boolean var13 = var9.equals((java.lang.Object)'4');
    var9.setId((java.lang.Integer)10);
    java.util.Collection var16 = var9.getPropostaCollection();
    boolean var17 = var1.equals((java.lang.Object)var9);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var24 = var23.getId();
    boolean var25 = var19.equals((java.lang.Object)var23);
    java.lang.Integer var26 = var19.getId();
    java.util.Collection var27 = var19.getPropostaCollection();
    var19.setId((java.lang.Integer)1);
    java.lang.String var30 = var19.getStatus();
    var19.setId((java.lang.Integer)100);
    java.lang.String var33 = var19.toString();
    boolean var34 = var9.equals((java.lang.Object)var19);
    java.util.Collection var35 = var9.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 100+ "'", var24.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 1+ "'", var26.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);

  }

  public void test88() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test88"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var6 = var1.getId();
    java.lang.String var7 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var10.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var15 = var14.getId();
    java.util.Collection var16 = var14.getPropostaCollection();
    var14.setId((java.lang.Integer)10);
    var14.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var22.setStatus("");
    java.lang.Integer var25 = var22.getId();
    java.util.Collection var26 = var22.getPropostaCollection();
    boolean var27 = var14.equals((java.lang.Object)var22);
    var14.setId((java.lang.Integer)10);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var32 = var10.equals((java.lang.Object)var14);
    java.lang.Integer var33 = var10.getId();
    boolean var34 = var1.equals((java.lang.Object)var10);
    java.lang.Integer var35 = var1.getId();
    java.lang.String var36 = var1.toString();
    java.lang.Integer var37 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 1+ "'", var33.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + (-1)+ "'", var35.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + (-1)+ "'", var37.equals((-1)));

  }

  public void test89() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test89"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    java.lang.String var11 = var4.getStatus();
    var4.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + ""+ "'", var11.equals(""));

  }

  public void test90() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test90"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var8 = var1.getId();
    java.lang.String var9 = var1.toString();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    boolean var17 = var11.equals((java.lang.Object)var15);
    java.lang.String var18 = var11.getStatus();
    java.util.Collection var19 = var11.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var21.setId((java.lang.Integer)10);
    var21.setStatus("");
    java.lang.Integer var26 = var21.getId();
    java.util.Collection var27 = var21.getPropostaCollection();
    java.lang.Integer var28 = var21.getId();
    boolean var29 = var11.equals((java.lang.Object)var21);
    java.lang.String var30 = var11.getStatus();
    var11.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var36 = var35.toString();
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var39 = var35.getStatus();
    boolean var40 = var11.equals((java.lang.Object)var35);
    boolean var41 = var1.equals((java.lang.Object)var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 10+ "'", var28.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);

  }

  public void test91() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test91"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var11 = var1.getStatus();
    java.lang.String var12 = var1.toString();
    java.lang.Integer var13 = var1.getId();
    var1.setId((java.lang.Integer)1);
    java.lang.String var16 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var19 = var1.getPropostaCollection();
    java.lang.String var20 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 1+ "'", var13.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test92() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test92"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var8 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var14 = var11.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)(-1));
    java.lang.Integer var20 = var1.getId();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var23 = var22.getId();
    var22.setId((java.lang.Integer)(-1));
    boolean var27 = var22.equals((java.lang.Object)(short)(-1));
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    var29.setId((java.lang.Integer)(-1));
    java.util.Collection var33 = var29.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var35.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var40 = var39.getId();
    boolean var41 = var35.equals((java.lang.Object)var39);
    java.lang.Integer var42 = var35.getId();
    boolean var43 = var29.equals((java.lang.Object)var42);
    var29.setId((java.lang.Integer)100);
    boolean var46 = var22.equals((java.lang.Object)100);
    var22.setId((java.lang.Integer)0);
    var22.setId((java.lang.Integer)0);
    java.lang.String var51 = var22.getStatus();
    boolean var52 = var1.equals((java.lang.Object)var22);
    java.lang.Integer var53 = var1.getId();
    java.lang.String var54 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + 100+ "'", var40.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + 1+ "'", var42.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + (-1)+ "'", var53.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test93() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test93"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var11 = var10.getId();
    java.util.Collection var12 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)10);
    var10.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var18.setStatus("");
    java.lang.Integer var21 = var18.getId();
    java.util.Collection var22 = var18.getPropostaCollection();
    boolean var23 = var10.equals((java.lang.Object)var18);
    var10.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var27.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var32 = var31.getId();
    boolean var33 = var27.equals((java.lang.Object)var31);
    java.lang.String var34 = var27.getStatus();
    java.lang.String var35 = var27.getStatus();
    boolean var36 = var10.equals((java.lang.Object)var27);
    boolean var37 = var1.equals((java.lang.Object)var10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var40 = var1.getId();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + (-1)+ "'", var11.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 100+ "'", var32.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + 0+ "'", var40.equals(0));

  }

  public void test94() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test94"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    boolean var12 = var9.equals((java.lang.Object)(short)0);
    var9.setStatus("");
    boolean var15 = var1.equals((java.lang.Object)var9);
    java.lang.String var16 = var9.toString();
    java.lang.Integer var17 = var9.getId();
    java.lang.String var18 = var9.getStatus();
    java.lang.String var19 = var9.toString();
    var9.setId((java.lang.Integer)0);
    var9.setId((java.lang.Integer)100);
    var9.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + ""+ "'", var18.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test95() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test95"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test96() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test96"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.util.Collection var28 = var1.getPropostaCollection();
    java.lang.Integer var29 = var1.getId();
    java.lang.Integer var30 = var1.getId();
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 10+ "'", var29.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + 10+ "'", var30.equals(10));

  }

  public void test97() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test97"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.util.Collection var7 = var1.getPropostaCollection();
    java.lang.String var8 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "hi!");
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var17.setStatus("");
    java.lang.Integer var20 = var17.getId();
    java.lang.String var21 = var17.getStatus();
    java.lang.String var22 = var17.toString();
    var17.setStatus("");
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var27.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var27.setId((java.lang.Integer)100);
    boolean var32 = var17.equals((java.lang.Object)100);
    boolean var33 = var13.equals((java.lang.Object)var17);
    boolean var34 = var1.equals((java.lang.Object)var33);
    var1.setId((java.lang.Integer)(-1));
    var1.setStatus("hi!");
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + ""+ "'", var21.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);

  }

  public void test98() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test98"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var5.getId();
    java.lang.String var9 = var5.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 100+ "'", var8.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test99() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test99"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    java.util.Collection var6 = var4.getPropostaCollection();
    var4.setId((java.lang.Integer)10);
    var4.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    boolean var17 = var4.equals((java.lang.Object)var12);
    var4.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var26 = var25.getId();
    boolean var27 = var21.equals((java.lang.Object)var25);
    java.lang.String var28 = var21.getStatus();
    java.lang.String var29 = var21.getStatus();
    boolean var30 = var4.equals((java.lang.Object)var21);
    java.lang.String var31 = var4.getStatus();
    java.util.Collection var32 = var4.getPropostaCollection();
    java.util.Collection var33 = var4.getPropostaCollection();
    boolean var34 = var2.equals((java.lang.Object)var4);
    java.util.Collection var35 = var4.getPropostaCollection();
    java.lang.String var36 = var4.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 100+ "'", var26.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "hi!"+ "'", var31.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "hi!"+ "'", var36.equals("hi!"));

  }

  public void test100() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test100"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setId((java.lang.Integer)100);
    java.lang.String var10 = var7.getStatus();
    var7.setId((java.lang.Integer)10);
    java.lang.String var13 = var7.toString();
    java.util.Collection var14 = var7.getPropostaCollection();
    boolean var15 = var1.equals((java.lang.Object)var7);
    var7.setStatus("hi!");
    java.lang.Integer var18 = var7.getId();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var21 = var20.getId();
    java.util.Collection var22 = var20.getPropostaCollection();
    var20.setId((java.lang.Integer)10);
    var20.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var28.setStatus("");
    java.lang.Integer var31 = var28.getId();
    java.util.Collection var32 = var28.getPropostaCollection();
    boolean var33 = var20.equals((java.lang.Object)var28);
    var20.setId((java.lang.Integer)10);
    boolean var37 = var20.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var41 = var20.equals((java.lang.Object)var40);
    java.util.Collection var42 = var20.getPropostaCollection();
    java.util.Collection var43 = var20.getPropostaCollection();
    java.lang.String var44 = var20.toString();
    java.lang.String var45 = var20.toString();
    loja.com.br.entity.PropostaStatus var47 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var48 = var47.getId();
    var47.setId((java.lang.Integer)(-1));
    boolean var52 = var47.equals((java.lang.Object)(short)(-1));
    var47.setId((java.lang.Integer)10);
    java.lang.String var55 = var47.getStatus();
    var47.setStatus("");
    boolean var58 = var20.equals((java.lang.Object)var47);
    boolean var59 = var7.equals((java.lang.Object)var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 10+ "'", var18.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + (-1)+ "'", var31.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var44.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var45.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + (-1)+ "'", var48.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var59 == true);

  }

  public void test101() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test101"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    java.lang.String var9 = var6.toString();
    java.lang.String var10 = var6.getStatus();
    boolean var11 = var1.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    boolean var15 = var1.equals((java.lang.Object)0);
    java.lang.Integer var16 = var1.getId();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var21 = var1.getStatus();
    java.util.Collection var22 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var25 = var1.getId();
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 0+ "'", var25.equals(0));

  }

  public void test102() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test102"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)1);
    java.lang.String var16 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    java.util.Collection var19 = var1.getPropostaCollection();
    java.util.Collection var20 = var1.getPropostaCollection();
    java.lang.String var21 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test103() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test103"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var18 = var1.getId();
    java.lang.String var19 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var25 = var22.getStatus();
    java.lang.Integer var26 = var22.getId();
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var29 = var28.getId();
    java.util.Collection var30 = var28.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var33 = var32.getId();
    java.util.Collection var34 = var32.getPropostaCollection();
    var32.setId((java.lang.Integer)10);
    java.lang.String var37 = var32.toString();
    java.lang.String var38 = var32.getStatus();
    boolean var39 = var28.equals((java.lang.Object)var32);
    var28.setId((java.lang.Integer)0);
    boolean var42 = var22.equals((java.lang.Object)var28);
    java.lang.Integer var43 = var22.getId();
    boolean var44 = var1.equals((java.lang.Object)var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 0+ "'", var26.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + (-1)+ "'", var29.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + (-1)+ "'", var33.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + 0+ "'", var43.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);

  }

  public void test104() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test104"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)1);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setId((java.lang.Integer)100);
    java.lang.Integer var16 = var1.getId();
    java.lang.String var17 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test105() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test105"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    java.lang.String var3 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var6 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var9 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test106() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test106"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    java.lang.String var12 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var17 = var15.equals((java.lang.Object)'#');
    var15.setStatus("hi!");
    boolean var20 = var1.equals((java.lang.Object)"hi!");
    java.lang.String var21 = var1.getStatus();
    java.util.Collection var22 = var1.getPropostaCollection();
    java.lang.String var23 = var1.toString();
    java.util.Collection var24 = var1.getPropostaCollection();
    java.lang.String var25 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test107() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test107"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var11 = var10.toString();
    var10.setStatus("hi!");
    java.util.Collection var14 = var10.getPropostaCollection();
    var10.setStatus("hi!");
    boolean var17 = var2.equals((java.lang.Object)var10);
    java.util.Collection var18 = var10.getPropostaCollection();
    java.lang.String var19 = var10.toString();
    java.lang.String var20 = var10.getStatus();
    java.lang.String var21 = var10.getStatus();
    java.lang.String var22 = var10.toString();
    var10.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "hi!"+ "'", var20.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "hi!"+ "'", var21.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test108() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test108"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.Integer var23 = var21.getId();
    var21.setId((java.lang.Integer)1);
    java.lang.String var26 = var21.toString();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var33 = var32.getId();
    java.lang.String var34 = var32.getStatus();
    boolean var35 = var29.equals((java.lang.Object)var34);
    java.lang.String var36 = var29.toString();
    boolean var37 = var21.equals((java.lang.Object)var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 100+ "'", var33.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);

  }

  public void test109() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test109"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var8 = var7.getId();
    java.util.Collection var9 = var7.getPropostaCollection();
    var7.setId((java.lang.Integer)10);
    java.util.Collection var12 = var7.getPropostaCollection();
    java.lang.String var13 = var7.toString();
    var7.setStatus("");
    java.util.Collection var16 = var7.getPropostaCollection();
    java.lang.String var17 = var7.getStatus();
    boolean var18 = var1.equals((java.lang.Object)var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);

  }

  public void test110() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test110"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var12 = var11.getId();
    boolean var13 = var7.equals((java.lang.Object)var11);
    java.lang.Integer var14 = var7.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    java.util.Collection var16 = var1.getPropostaCollection();
    java.lang.String var17 = var1.toString();
    java.util.Collection var18 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var25 = var24.getId();
    boolean var26 = var20.equals((java.lang.Object)var24);
    java.lang.Integer var27 = var20.getId();
    var20.setId((java.lang.Integer)10);
    var20.setId((java.lang.Integer)(-1));
    java.lang.String var32 = var20.toString();
    java.lang.String var33 = var20.getStatus();
    var20.setId((java.lang.Integer)1);
    boolean var36 = var1.equals((java.lang.Object)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 1+ "'", var27.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);

  }

  public void test111() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test111"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var6.setId((java.lang.Integer)0);
    boolean var9 = var1.equals((java.lang.Object)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var12 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var15 = var1.getPropostaCollection();
    var1.setStatus("hi!");
    java.lang.String var18 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test112() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test112"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var3 = var2.getStatus();
    var2.setId((java.lang.Integer)1);
    java.lang.Integer var6 = var2.getId();
    java.lang.String var7 = var2.toString();
    java.lang.Integer var8 = var2.getId();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    java.lang.Integer var17 = var10.getId();
    var10.setId((java.lang.Integer)10);
    java.lang.Integer var20 = var10.getId();
    java.lang.String var21 = var10.toString();
    boolean var22 = var2.equals((java.lang.Object)var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 1+ "'", var6.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 1+ "'", var17.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 10+ "'", var20.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);

  }

  public void test113() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test113"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)0);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var9 = var1.getId();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setId((java.lang.Integer)0);
    java.lang.String var16 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test114() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test114"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var9 = var1.getId();
    java.util.Collection var10 = var1.getPropostaCollection();
    java.lang.String var11 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    java.lang.String var14 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test115() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test115"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var10 = var9.getPropostaCollection();
    boolean var11 = var5.equals((java.lang.Object)var9);
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var15 = var13.equals((java.lang.Object)false);
    java.util.Collection var16 = var13.getPropostaCollection();
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var22 = var21.toString();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var24.setStatus("");
    java.lang.Integer var27 = var24.getId();
    java.util.Collection var28 = var24.getPropostaCollection();
    java.lang.String var29 = var24.toString();
    boolean var30 = var21.equals((java.lang.Object)var24);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var34 = var24.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var35 = var13.equals((java.lang.Object)var24);
    var24.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var38 = var5.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var39 = var5.toString();
    loja.com.br.entity.PropostaStatus var41 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var42 = var41.getId();
    var41.setId((java.lang.Integer)(-1));
    java.util.Collection var45 = var41.getPropostaCollection();
    java.lang.String var46 = var41.toString();
    var41.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var41.setId((java.lang.Integer)10);
    boolean var51 = var5.equals((java.lang.Object)var41);
    java.lang.Integer var52 = var41.getId();
    java.lang.String var53 = var41.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var42 + "' != '" + (-1)+ "'", var42.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + 10+ "'", var52.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var53.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test116() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test116"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var3 = var2.getStatus();
    java.lang.String var4 = var2.toString();
    java.lang.Integer var5 = var2.getId();
    var2.setId((java.lang.Integer)(-1));
    java.lang.String var8 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 1+ "'", var5.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test117() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test117"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.util.Collection var10 = var1.getPropostaCollection();
    java.util.Collection var11 = var1.getPropostaCollection();
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test118() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test118"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var13 = var6.getId();
    boolean var14 = var2.equals((java.lang.Object)var13);
    java.lang.Integer var15 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var20 = var2.toString();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var22.setId((java.lang.Integer)100);
    java.lang.String var25 = var22.getStatus();
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var34 = var33.getPropostaCollection();
    var33.setId((java.lang.Integer)100);
    java.lang.Integer var37 = var33.getId();
    boolean var38 = var32.equals((java.lang.Object)var33);
    java.lang.String var39 = var33.toString();
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "");
    boolean var43 = var33.equals((java.lang.Object)"");
    java.lang.String var44 = var33.toString();
    boolean var45 = var22.equals((java.lang.Object)var33);
    boolean var46 = var2.equals((java.lang.Object)var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 1+ "'", var13.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 1+ "'", var15.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 100+ "'", var37.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var44.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);

  }

  public void test119() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test119"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    java.lang.String var9 = var1.toString();
    java.lang.Integer var10 = var1.getId();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var17 = var15.equals((java.lang.Object)false);
    boolean var19 = var15.equals((java.lang.Object)10.0d);
    boolean var20 = var13.equals((java.lang.Object)var19);
    java.util.Collection var21 = var13.getPropostaCollection();
    java.lang.Integer var22 = var13.getId();
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var26 = var25.toString();
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var29 = var25.getId();
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var32 = var31.getId();
    var31.setId((java.lang.Integer)0);
    java.lang.Integer var35 = var31.getId();
    boolean var36 = var25.equals((java.lang.Object)var31);
    boolean var37 = var13.equals((java.lang.Object)var36);
    java.lang.Integer var38 = var13.getId();
    boolean var39 = var1.equals((java.lang.Object)var38);
    java.lang.String var40 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 100+ "'", var10.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 10+ "'", var29.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 100+ "'", var32.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + 0+ "'", var35.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + (-1)+ "'", var38.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test120() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test120"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var12 = var11.toString();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("");
    java.lang.Integer var17 = var14.getId();
    java.util.Collection var18 = var14.getPropostaCollection();
    java.lang.String var19 = var14.getStatus();
    boolean var20 = var11.equals((java.lang.Object)var14);
    boolean var21 = var1.equals((java.lang.Object)var11);
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var23.setId((java.lang.Integer)100);
    java.lang.String var26 = var23.getStatus();
    java.util.Collection var27 = var23.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var31 = var29.equals((java.lang.Object)false);
    boolean var32 = var23.equals((java.lang.Object)var29);
    java.lang.String var33 = var29.getStatus();
    java.lang.Integer var34 = var29.getId();
    boolean var35 = var1.equals((java.lang.Object)var29);
    java.lang.Integer var36 = var29.getId();
    java.lang.String var37 = var29.toString();
    java.lang.Integer var38 = var29.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + 0+ "'", var34.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 0+ "'", var36.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + 0+ "'", var38.equals(0));

  }

  public void test121() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test121"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.toString();
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var12 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var15 = var1.getId();
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var17.setId((java.lang.Integer)10);
    java.lang.String var20 = var17.toString();
    java.lang.String var21 = var17.toString();
    var17.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var28 = var26.equals((java.lang.Object)(-1.0d));
    java.util.Collection var29 = var26.getPropostaCollection();
    var26.setId((java.lang.Integer)(-1));
    var26.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var34 = var17.equals((java.lang.Object)var26);
    java.util.Collection var35 = var26.getPropostaCollection();
    java.lang.Integer var36 = var26.getId();
    java.lang.String var37 = var26.getStatus();
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var40 = var39.getId();
    java.util.Collection var41 = var39.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var43 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var44 = var43.getId();
    java.util.Collection var45 = var43.getPropostaCollection();
    var43.setId((java.lang.Integer)10);
    java.lang.String var48 = var43.toString();
    java.lang.String var49 = var43.getStatus();
    boolean var50 = var39.equals((java.lang.Object)var43);
    var39.setId((java.lang.Integer)0);
    java.lang.String var53 = var39.getStatus();
    java.lang.String var54 = var39.toString();
    boolean var55 = var26.equals((java.lang.Object)var54);
    java.lang.String var56 = var26.toString();
    java.lang.String var57 = var26.getStatus();
    boolean var58 = var1.equals((java.lang.Object)var26);
    java.lang.String var59 = var1.getStatus();
    java.util.Collection var60 = var1.getPropostaCollection();
    java.util.Collection var61 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 0+ "'", var12.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 0+ "'", var15.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + (-1)+ "'", var36.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + (-1)+ "'", var40.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + (-1)+ "'", var44.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var48.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var55 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var56 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var56.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var57.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var59.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var60);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);

  }

  public void test122() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test122"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.getStatus();
    java.lang.String var8 = var1.getStatus();
    java.lang.String var9 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);

  }

  public void test123() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test123"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var3 = var2.toString();
    java.lang.Integer var4 = var2.getId();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 100+ "'", var4.equals(100));

  }

  public void test124() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test124"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.util.Collection var28 = var1.getPropostaCollection();
    java.lang.Integer var29 = var1.getId();
    var1.setStatus("");
    java.lang.String var32 = var1.getStatus();
    java.lang.String var33 = var1.toString();
    var1.setId((java.lang.Integer)0);
    java.lang.String var36 = var1.toString();
    java.lang.Integer var37 = var1.getId();
    java.lang.String var38 = var1.toString();
    java.util.Collection var39 = var1.getPropostaCollection();
    java.lang.Integer var40 = var1.getId();
    loja.com.br.entity.PropostaStatus var43 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var44 = var43.getId();
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var46.setId((java.lang.Integer)100);
    java.lang.String var49 = var46.getStatus();
    java.util.Collection var50 = var46.getPropostaCollection();
    java.lang.Integer var51 = var46.getId();
    java.lang.String var52 = var46.getStatus();
    loja.com.br.entity.PropostaStatus var54 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var55 = var54.getPropostaCollection();
    java.lang.String var56 = var54.getStatus();
    boolean var57 = var46.equals((java.lang.Object)var54);
    boolean var58 = var43.equals((java.lang.Object)var57);
    java.lang.String var59 = var43.getStatus();
    java.lang.String var60 = var43.getStatus();
    boolean var61 = var1.equals((java.lang.Object)var60);
    java.lang.String var62 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 10+ "'", var29.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + ""+ "'", var32.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 0+ "'", var37.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + 0+ "'", var40.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + 100+ "'", var44.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + 100+ "'", var51.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var52);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var59.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var60 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var60.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var62 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var62.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test125() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test125"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    var5.setId((java.lang.Integer)(-1));
    var5.setId((java.lang.Integer)0);
    java.lang.Integer var14 = var5.getId();
    boolean var15 = var1.equals((java.lang.Object)var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 0+ "'", var14.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);

  }

  public void test126() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test126"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus();
    java.lang.Integer var8 = var7.getId();
    java.lang.Integer var9 = var7.getId();
    java.lang.String var10 = var7.getStatus();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var13 = var12.getId();
    java.util.Collection var14 = var12.getPropostaCollection();
    var12.setId((java.lang.Integer)10);
    java.lang.String var17 = var12.toString();
    java.util.Collection var18 = var12.getPropostaCollection();
    boolean var19 = var7.equals((java.lang.Object)var12);
    java.lang.Integer var20 = var12.getId();
    boolean var21 = var1.equals((java.lang.Object)var12);
    java.lang.Integer var22 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 10+ "'", var20.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 0+ "'", var22.equals(0));

  }

  public void test127() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test127"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.String var6 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)(-1));
    java.lang.String var11 = var1.toString();
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var18 = var17.getId();
    var17.setId((java.lang.Integer)0);
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    boolean var26 = var17.equals((java.lang.Object)"");
    boolean var27 = var1.equals((java.lang.Object)"");
    java.util.Collection var28 = var1.getPropostaCollection();
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + ""+ "'", var6.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + 100+ "'", var18.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);

  }

  public void test128() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test128"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var8.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var13 = var12.getId();
    java.util.Collection var14 = var12.getPropostaCollection();
    var12.setId((java.lang.Integer)10);
    var12.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var20.setStatus("");
    java.lang.Integer var23 = var20.getId();
    java.util.Collection var24 = var20.getPropostaCollection();
    boolean var25 = var12.equals((java.lang.Object)var20);
    var12.setId((java.lang.Integer)10);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var30 = var8.equals((java.lang.Object)var12);
    boolean var31 = var1.equals((java.lang.Object)var12);
    var12.setId((java.lang.Integer)(-1));
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == true);

  }

  public void test129() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test129"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("hi!");
    java.lang.String var7 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var15 = var10.getId();
    java.lang.String var16 = var10.getStatus();
    java.lang.Integer var17 = var10.getId();
    boolean var18 = var1.equals((java.lang.Object)var10);
    java.util.Collection var19 = var10.getPropostaCollection();
    java.lang.String var20 = var10.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "hi!"+ "'", var7.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 10+ "'", var15.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 10+ "'", var17.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test130() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test130"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var26 = var25.getId();
    java.util.Collection var27 = var25.getPropostaCollection();
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var30 = var25.getPropostaCollection();
    var25.setId((java.lang.Integer)(-1));
    boolean var33 = var1.equals((java.lang.Object)var25);
    java.lang.String var34 = var25.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test131() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test131"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var28 = var18.getId();
    java.util.Collection var29 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    java.lang.Integer var32 = var18.getId();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var35 = var18.getPropostaCollection();
    java.lang.String var36 = var18.getStatus();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var39 = var18.getStatus();
    java.util.Collection var40 = var18.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var35);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);

  }

  public void test132() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test132"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var6 = var4.equals((java.lang.Object)false);
    boolean var8 = var4.equals((java.lang.Object)10.0d);
    boolean var9 = var2.equals((java.lang.Object)var8);
    java.lang.String var10 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var14 = var13.toString();
    var13.setId((java.lang.Integer)(-1));
    boolean var17 = var2.equals((java.lang.Object)var13);
    var2.setId((java.lang.Integer)0);
    java.lang.String var20 = var2.getStatus();
    java.lang.String var21 = var2.toString();
    java.lang.String var22 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + ""+ "'", var20.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test133() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test133"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    boolean var5 = var1.equals((java.lang.Object)"hi!");
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);

  }

  public void test134() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test134"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    var1.setId((java.lang.Integer)10);
    java.lang.String var9 = var1.getStatus();
    var1.setStatus("");
    java.lang.String var12 = var1.getStatus();
    java.lang.Integer var13 = var1.getId();
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    java.util.Collection var19 = var17.getPropostaCollection();
    var17.setId((java.lang.Integer)10);
    var17.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var25.setStatus("");
    java.lang.Integer var28 = var25.getId();
    java.util.Collection var29 = var25.getPropostaCollection();
    boolean var30 = var17.equals((java.lang.Object)var25);
    var17.setId((java.lang.Integer)10);
    boolean var34 = var17.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var38 = var17.equals((java.lang.Object)var37);
    java.lang.Integer var39 = var17.getId();
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus();
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var45 = var42.getPropostaCollection();
    java.util.Collection var46 = var42.getPropostaCollection();
    java.util.Collection var47 = var42.getPropostaCollection();
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var50 = var17.equals((java.lang.Object)var42);
    boolean var51 = var1.equals((java.lang.Object)var50);
    loja.com.br.entity.PropostaStatus var53 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var54 = var53.getId();
    java.util.Collection var55 = var53.getPropostaCollection();
    var53.setId((java.lang.Integer)10);
    var53.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var61 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var61.setStatus("");
    java.lang.Integer var64 = var61.getId();
    java.util.Collection var65 = var61.getPropostaCollection();
    boolean var66 = var53.equals((java.lang.Object)var61);
    var53.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var70 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var70.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var74 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var75 = var74.getId();
    boolean var76 = var70.equals((java.lang.Object)var74);
    java.lang.String var77 = var70.getStatus();
    java.lang.String var78 = var70.getStatus();
    boolean var79 = var53.equals((java.lang.Object)var70);
    java.lang.Integer var80 = var70.getId();
    java.util.Collection var81 = var70.getPropostaCollection();
    var70.setId((java.lang.Integer)10);
    java.lang.Integer var84 = var70.getId();
    var70.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var87 = var70.getPropostaCollection();
    java.lang.String var88 = var70.getStatus();
    java.lang.String var89 = var70.toString();
    boolean var90 = var1.equals((java.lang.Object)var89);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + ""+ "'", var12.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 10+ "'", var13.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + (-1)+ "'", var28.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + 10+ "'", var39.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var47);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + (-1)+ "'", var54.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var64 + "' != '" + (-1)+ "'", var64.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var65);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var66 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var75 + "' != '" + 100+ "'", var75.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var76 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var77 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var77.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var78 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var78.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var79 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var80 + "' != '" + 1+ "'", var80.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var81);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var84 + "' != '" + 10+ "'", var84.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var87);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var88 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var88.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var89 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var89.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var90 == false);

  }

  public void test135() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test135"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "hi!");
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setStatus("");
    java.lang.Integer var9 = var6.getId();
    java.lang.String var10 = var6.getStatus();
    java.lang.String var11 = var6.toString();
    var6.setStatus("");
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var16.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var16.setId((java.lang.Integer)100);
    boolean var21 = var6.equals((java.lang.Object)100);
    boolean var22 = var2.equals((java.lang.Object)var6);
    java.util.Collection var23 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    var2.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    java.util.Collection var31 = var29.getPropostaCollection();
    var29.setId((java.lang.Integer)10);
    java.util.Collection var34 = var29.getPropostaCollection();
    var29.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=1 ]");
    var39.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var42 = var29.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var43 = var2.equals((java.lang.Object)var29);
    java.lang.String var44 = var29.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);

  }

  public void test136() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test136"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    java.lang.String var4 = var2.getStatus();
    java.lang.Integer var5 = var2.getId();
    java.util.Collection var6 = var2.getPropostaCollection();
    java.lang.String var7 = var2.getStatus();
    java.lang.String var8 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test137() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test137"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    java.lang.Integer var8 = var2.getId();
    java.lang.String var9 = var2.toString();
    java.lang.String var10 = var2.toString();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test138() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test138"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=1 ]");
    var2.setStatus("");
    var2.setId((java.lang.Integer)100);
    java.lang.String var7 = var2.toString();
    var2.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test139() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test139"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var4.setId((java.lang.Integer)10);
    java.lang.String var7 = var4.toString();
    java.lang.String var8 = var4.toString();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var2.equals((java.lang.Object)var4);
    java.lang.String var11 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test140() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test140"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var13.setStatus("");
    java.lang.Integer var16 = var13.getId();
    java.lang.String var17 = var13.toString();
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var20 = var13.getId();
    boolean var21 = var1.equals((java.lang.Object)var13);
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var24.setId((java.lang.Integer)0);
    java.lang.String var27 = var24.toString();
    boolean var28 = var13.equals((java.lang.Object)var24);
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    var31.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var34 = var31.getId();
    loja.com.br.entity.PropostaStatus var36 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var36.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var39 = var36.toString();
    var36.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var36.setId((java.lang.Integer)1);
    java.util.Collection var44 = var36.getPropostaCollection();
    java.lang.String var45 = var36.getStatus();
    boolean var46 = var31.equals((java.lang.Object)var36);
    java.lang.String var47 = var36.toString();
    boolean var48 = var13.equals((java.lang.Object)var47);
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var52 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var52.setId((java.lang.Integer)100);
    java.lang.String var55 = var52.getStatus();
    java.lang.String var56 = var52.getStatus();
    var52.setId((java.lang.Integer)(-1));
    boolean var59 = var13.equals((java.lang.Object)(-1));
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var62 = var13.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + 0+ "'", var34.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var45 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var45.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var59 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var62);

  }

  public void test141() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test141"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "");
    var6.setId((java.lang.Integer)0);
    boolean var9 = var1.equals((java.lang.Object)0);
    java.lang.String var10 = var1.getStatus();
    var1.setStatus("");
    var1.setId((java.lang.Integer)(-1));
    java.lang.Integer var15 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));

  }

  public void test142() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test142"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    var2.setId((java.lang.Integer)1);
    java.lang.Integer var5 = var2.getId();
    var2.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 1+ "'", var5.equals(1));

  }

  public void test143() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test143"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    java.lang.String var18 = var1.getStatus();
    java.util.Collection var19 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var22 = var1.getId();
    java.lang.String var23 = var1.toString();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + ""+ "'", var18.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 10+ "'", var22.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test144() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test144"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);

  }

  public void test145() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test145"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    boolean var5 = var1.equals((java.lang.Object)10.0d);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var8 = var1.getId();
    java.lang.String var9 = var1.toString();
    java.util.Collection var10 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);

  }

  public void test146() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test146"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    java.lang.Integer var8 = var1.getId();
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.getStatus();
    java.lang.String var11 = var1.getStatus();
    java.lang.Integer var12 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));

  }

  public void test147() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test147"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var2 = var1.getPropostaCollection();
    java.lang.Integer var3 = var1.getId();
    boolean var5 = var1.equals((java.lang.Object)'4');
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);

  }

  public void test148() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test148"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("");
    java.lang.Integer var12 = var1.getId();
    var1.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 1+ "'", var12.equals(1));

  }

  public void test149() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test149"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);

  }

  public void test150() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test150"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.util.Collection var17 = var1.getPropostaCollection();
    java.lang.String var18 = var1.getStatus();
    var1.setId((java.lang.Integer)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + ""+ "'", var18.equals(""));

  }

  public void test151() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test151"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var3 = var2.toString();
    java.lang.String var4 = var2.toString();
    java.lang.Integer var5 = var2.getId();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var9 = var8.getStatus();
    java.lang.String var10 = var8.getStatus();
    java.lang.Integer var11 = var8.getId();
    boolean var12 = var2.equals((java.lang.Object)var8);
    java.util.Collection var13 = var2.getPropostaCollection();
    java.lang.String var14 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var17 = var16.toString();
    boolean var19 = var16.equals((java.lang.Object)"");
    boolean var20 = var2.equals((java.lang.Object)var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 10+ "'", var5.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "hi!"+ "'", var9.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "hi!"+ "'", var10.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "hi!"+ "'", var14.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var19 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);

  }

  public void test152() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test152"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var9 = var1.equals((java.lang.Object)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var12 = var1.toString();
    java.lang.String var13 = var1.getStatus();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.getStatus();
    java.lang.String var22 = var18.toString();
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var25.setId((java.lang.Integer)100);
    var25.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var25.setStatus("");
    boolean var32 = var18.equals((java.lang.Object)"");
    java.lang.String var33 = var18.toString();
    boolean var34 = var1.equals((java.lang.Object)var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + ""+ "'", var21.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);

  }

  public void test153() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test153"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var17 = var9.toString();
    var9.setId((java.lang.Integer)0);
    var9.setStatus("hi!");
    java.lang.String var22 = var9.toString();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var24.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var29 = var28.getId();
    boolean var30 = var24.equals((java.lang.Object)var28);
    java.lang.Integer var31 = var24.getId();
    var24.setId((java.lang.Integer)10);
    java.lang.Integer var34 = var24.getId();
    java.lang.String var35 = var24.getStatus();
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var40 = var38.equals((java.lang.Object)'#');
    var38.setStatus("hi!");
    boolean var43 = var24.equals((java.lang.Object)"hi!");
    java.lang.String var44 = var24.getStatus();
    boolean var45 = var9.equals((java.lang.Object)var24);
    loja.com.br.entity.PropostaStatus var47 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var48 = var47.getId();
    var47.setId((java.lang.Integer)(-1));
    boolean var52 = var47.equals((java.lang.Object)(short)(-1));
    var47.setId((java.lang.Integer)10);
    java.lang.String var55 = var47.getStatus();
    var47.setStatus("");
    var47.setStatus("hi!");
    boolean var60 = var9.equals((java.lang.Object)"hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 100+ "'", var29.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + 1+ "'", var31.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + 10+ "'", var34.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var44.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + (-1)+ "'", var48.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == false);

  }

  public void test154() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test154"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.String var6 = var2.toString();
    java.lang.String var7 = var2.getStatus();
    java.lang.Integer var8 = var2.getId();
    java.lang.String var9 = var2.getStatus();
    java.lang.String var10 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test155() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test155"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.toString();
    boolean var23 = var18.equals((java.lang.Object)(byte)100);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var26 = var10.equals((java.lang.Object)var18);
    boolean var27 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var30 = var29.getId();
    var29.setId((java.lang.Integer)(-1));
    boolean var34 = var29.equals((java.lang.Object)'4');
    boolean var35 = var1.equals((java.lang.Object)var29);
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var40 = var38.equals((java.lang.Object)(-1.0d));
    java.util.Collection var41 = var38.getPropostaCollection();
    var38.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var47 = var46.toString();
    var46.setStatus("hi!");
    java.util.Collection var50 = var46.getPropostaCollection();
    var46.setStatus("hi!");
    boolean var53 = var38.equals((java.lang.Object)var46);
    java.util.Collection var54 = var46.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var56 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var58 = var56.equals((java.lang.Object)false);
    boolean var60 = var56.equals((java.lang.Object)10.0d);
    boolean var61 = var46.equals((java.lang.Object)10.0d);
    boolean var62 = var29.equals((java.lang.Object)var46);
    java.lang.Integer var63 = var46.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + (-1)+ "'", var30.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var61 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var63 + "' != '" + 10+ "'", var63.equals(10));

  }

  public void test156() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test156"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    var1.setId((java.lang.Integer)1);
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test157() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test157"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.toString();
    java.util.Collection var8 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var14.setId((java.lang.Integer)100);
    boolean var17 = var11.equals((java.lang.Object)var14);
    boolean var18 = var1.equals((java.lang.Object)var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 1+ "'", var2.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);

  }

  public void test158() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test158"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var1 = var0.getPropostaCollection();
    var0.setId((java.lang.Integer)100);
    java.lang.String var4 = var0.getStatus();
    java.lang.String var5 = var0.getStatus();
    java.lang.String var6 = var0.toString();
    java.util.Collection var7 = var0.getPropostaCollection();
    java.lang.Integer var8 = var0.getId();
    java.lang.Integer var9 = var0.getId();
    var0.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 100+ "'", var8.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));

  }

  public void test159() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test159"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var5 = var1.equals((java.lang.Object)false);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var1.setStatus("");
    java.lang.String var10 = var1.getStatus();
    java.lang.String var11 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var15 = var1.equals((java.lang.Object)var14);
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var5 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + ""+ "'", var11.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);

  }

  public void test160() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test160"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var26 = var18.getId();
    java.lang.Integer var27 = var18.getId();
    java.lang.String var28 = var18.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 0+ "'", var26.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 0+ "'", var27.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test161() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test161"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var6 = var2.getId();
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)0);
    java.lang.Integer var12 = var8.getId();
    boolean var13 = var2.equals((java.lang.Object)var8);
    java.lang.Integer var14 = var2.getId();
    java.lang.String var15 = var2.getStatus();
    var2.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 0+ "'", var12.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 10+ "'", var14.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test162() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test162"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var12 = var11.toString();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("");
    java.lang.Integer var17 = var14.getId();
    java.util.Collection var18 = var14.getPropostaCollection();
    java.lang.String var19 = var14.getStatus();
    boolean var20 = var11.equals((java.lang.Object)var14);
    boolean var21 = var1.equals((java.lang.Object)var11);
    java.util.Collection var22 = var1.getPropostaCollection();
    java.util.Collection var23 = var1.getPropostaCollection();
    java.lang.Integer var24 = var1.getId();
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 0+ "'", var24.equals(0));

  }

  public void test163() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test163"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var3 = var2.getId();
    java.lang.String var4 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    boolean var10 = var6.equals((java.lang.Object)10.0d);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var13 = var6.getStatus();
    boolean var14 = var2.equals((java.lang.Object)var13);
    java.lang.String var15 = var2.toString();
    java.lang.Integer var16 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 100+ "'", var3.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var13.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));

  }

  public void test164() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test164"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.lang.String var9 = var1.toString();
    java.lang.String var10 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    boolean var16 = var12.equals((java.lang.Object)"hi!");
    java.lang.String var17 = var12.getStatus();
    var12.setId((java.lang.Integer)100);
    java.lang.Integer var20 = var12.getId();
    boolean var21 = var1.equals((java.lang.Object)var20);
    java.util.Collection var22 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 100+ "'", var20.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);

  }

  public void test165() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test165"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.Integer var7 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var12 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);

  }

  public void test166() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test166"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    var4.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var15 = var14.getStatus();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var18 = var4.equals((java.lang.Object)var14);
    var14.setId((java.lang.Integer)0);
    var14.setId((java.lang.Integer)1);
    java.lang.String var23 = var14.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test167() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test167"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("hi!");
    java.lang.String var23 = var1.toString();
    java.lang.String var24 = var1.toString();
    java.lang.String var25 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "hi!"+ "'", var25.equals("hi!"));

  }

  public void test168() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test168"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var4 = var2.equals((java.lang.Object)(-1.0d));
    java.util.Collection var5 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var11 = var10.toString();
    var10.setStatus("hi!");
    java.util.Collection var14 = var10.getPropostaCollection();
    var10.setStatus("hi!");
    boolean var17 = var2.equals((java.lang.Object)var10);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var22 = var19.toString();
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var19.setId((java.lang.Integer)1);
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var29 = var19.getStatus();
    java.lang.String var30 = var19.toString();
    var19.setId((java.lang.Integer)10);
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var35 = var19.getId();
    boolean var36 = var10.equals((java.lang.Object)var19);
    java.lang.String var37 = var19.getStatus();
    java.lang.String var38 = var19.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + 10+ "'", var35.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test169() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test169"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.lang.String var5 = var1.getStatus();
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.toString();
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.String var10 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + ""+ "'", var5.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test170() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test170"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var3 = var2.toString();
    java.util.Collection var4 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var6.setId((java.lang.Integer)10);
    var6.setStatus("");
    java.lang.String var11 = var6.getStatus();
    var6.setId((java.lang.Integer)10);
    boolean var14 = var2.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var16.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var21 = var20.getId();
    boolean var22 = var16.equals((java.lang.Object)var20);
    java.lang.Integer var23 = var16.getId();
    java.util.Collection var24 = var16.getPropostaCollection();
    var16.setStatus("");
    java.lang.Integer var27 = var16.getId();
    loja.com.br.entity.PropostaStatus var28 = new loja.com.br.entity.PropostaStatus();
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var31 = var28.getPropostaCollection();
    java.util.Collection var32 = var28.getPropostaCollection();
    java.util.Collection var33 = var28.getPropostaCollection();
    boolean var34 = var16.equals((java.lang.Object)var28);
    var28.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var37 = var2.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var38 = var2.getId();
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var41 = var40.getId();
    java.util.Collection var42 = var40.getPropostaCollection();
    var40.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var45 = var2.equals((java.lang.Object)var40);
    java.lang.String var46 = var40.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + ""+ "'", var11.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 100+ "'", var21.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 1+ "'", var23.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 1+ "'", var27.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + 10+ "'", var38.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + (-1)+ "'", var41.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test171() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test171"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    var15.setId((java.lang.Integer)(-1));
    java.util.Collection var19 = var15.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var25 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var26 = var25.getId();
    boolean var27 = var21.equals((java.lang.Object)var25);
    java.lang.Integer var28 = var21.getId();
    boolean var29 = var15.equals((java.lang.Object)var28);
    java.util.Collection var30 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)0);
    boolean var33 = var1.equals((java.lang.Object)0);
    java.lang.String var34 = var1.toString();
    java.lang.String var35 = var1.toString();
    java.lang.Integer var36 = var1.getId();
    java.lang.String var37 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 100+ "'", var26.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + (-1)+ "'", var36.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);

  }

  public void test172() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test172"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var8 = var6.equals((java.lang.Object)false);
    java.lang.String var9 = var6.toString();
    java.lang.String var10 = var6.getStatus();
    boolean var11 = var1.equals((java.lang.Object)var6);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    boolean var15 = var1.equals((java.lang.Object)0);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var17.setId((java.lang.Integer)10);
    java.lang.String var20 = var17.toString();
    boolean var21 = var1.equals((java.lang.Object)var17);
    java.lang.Integer var22 = var17.getId();
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var25 = var17.getStatus();
    java.lang.String var26 = var17.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 10+ "'", var22.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test173() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test173"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)(-1));
    java.util.Collection var12 = var8.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var19 = var18.getId();
    boolean var20 = var14.equals((java.lang.Object)var18);
    java.lang.Integer var21 = var14.getId();
    boolean var22 = var8.equals((java.lang.Object)var21);
    var8.setId((java.lang.Integer)100);
    boolean var25 = var1.equals((java.lang.Object)100);
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)0);
    java.lang.String var30 = var1.getStatus();
    var1.setId((java.lang.Integer)(-1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 100+ "'", var19.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 1+ "'", var21.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);

  }

  public void test174() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test174"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.String var1 = var0.toString();
    java.util.Collection var2 = var0.getPropostaCollection();
    var0.setId((java.lang.Integer)0);
    java.lang.String var5 = var0.getStatus();
    java.lang.Integer var6 = var0.getId();
    var0.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var9 = var0.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var1 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var1.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 0+ "'", var6.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 0+ "'", var9.equals(0));

  }

  public void test175() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test175"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    java.lang.String var19 = var18.toString();
    java.lang.String var20 = var18.toString();
    var18.setId((java.lang.Integer)0);
    boolean var23 = var1.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var27 = var26.toString();
    boolean var29 = var26.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var30 = var26.getStatus();
    boolean var31 = var1.equals((java.lang.Object)var26);
    var26.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var26.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == true);

  }

  public void test176() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test176"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    var4.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var15 = var14.getStatus();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var18 = var4.equals((java.lang.Object)var14);
    java.lang.Integer var19 = var14.getId();
    java.lang.String var20 = var14.getStatus();
    java.util.Collection var21 = var14.getPropostaCollection();
    java.lang.String var22 = var14.getStatus();
    java.lang.String var23 = var14.getStatus();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test177() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test177"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var9.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    java.lang.String var17 = var12.toString();
    boolean var18 = var9.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var22 = var12.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var23 = var1.equals((java.lang.Object)var12);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var26 = var12.getId();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var31 = var29.equals((java.lang.Object)(-1.0d));
    java.util.Collection var32 = var29.getPropostaCollection();
    var29.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var38 = var37.toString();
    var37.setStatus("hi!");
    java.util.Collection var41 = var37.getPropostaCollection();
    var37.setStatus("hi!");
    boolean var44 = var29.equals((java.lang.Object)var37);
    java.util.Collection var45 = var37.getPropostaCollection();
    java.lang.String var46 = var37.toString();
    java.lang.String var47 = var37.getStatus();
    java.lang.String var48 = var37.getStatus();
    java.lang.String var49 = var37.toString();
    loja.com.br.entity.PropostaStatus var51 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var52 = var51.toString();
    loja.com.br.entity.PropostaStatus var54 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var54.setStatus("");
    java.lang.Integer var57 = var54.getId();
    java.util.Collection var58 = var54.getPropostaCollection();
    java.lang.String var59 = var54.getStatus();
    boolean var60 = var51.equals((java.lang.Object)var54);
    java.lang.String var61 = var51.getStatus();
    var51.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var64 = var37.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var65 = var12.equals((java.lang.Object)var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "hi!"+ "'", var47.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + "hi!"+ "'", var48.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var49.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var52.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + (-1)+ "'", var57.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var58);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + ""+ "'", var59.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var65 == false);

  }

  public void test178() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test178"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.String var23 = var21.getStatus();
    java.util.Collection var24 = var21.getPropostaCollection();
    java.util.Collection var25 = var21.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var27.setId((java.lang.Integer)10);
    java.lang.String var30 = var27.toString();
    java.lang.String var31 = var27.toString();
    var27.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var36 = var35.getId();
    java.util.Collection var37 = var35.getPropostaCollection();
    var35.setId((java.lang.Integer)10);
    java.lang.String var40 = var35.toString();
    java.lang.Integer var41 = var35.getId();
    var35.setStatus("");
    var35.setStatus("hi!");
    java.lang.String var46 = var35.getStatus();
    boolean var47 = var27.equals((java.lang.Object)var35);
    java.lang.String var48 = var27.toString();
    boolean var49 = var21.equals((java.lang.Object)var48);
    loja.com.br.entity.PropostaStatus var51 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var51.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var55 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var56 = var55.getId();
    boolean var57 = var51.equals((java.lang.Object)var55);
    java.lang.String var58 = var51.getStatus();
    java.util.Collection var59 = var51.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var61 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var61.setId((java.lang.Integer)10);
    var61.setStatus("");
    java.lang.Integer var66 = var61.getId();
    java.util.Collection var67 = var61.getPropostaCollection();
    java.lang.Integer var68 = var61.getId();
    boolean var69 = var51.equals((java.lang.Object)var61);
    java.lang.String var70 = var51.getStatus();
    var51.setId((java.lang.Integer)(-1));
    java.lang.String var73 = var51.toString();
    var51.setId((java.lang.Integer)100);
    boolean var76 = var21.equals((java.lang.Object)100);
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var31.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + (-1)+ "'", var36.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + 10+ "'", var41.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "hi!"+ "'", var46.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var48.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var56 + "' != '" + 100+ "'", var56.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var58 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var58.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var59);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var66 + "' != '" + 10+ "'", var66.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var67);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + 10+ "'", var68.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var69 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var70 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var70.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var73 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var73.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var76 == false);

  }

  public void test179() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test179"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    boolean var18 = var1.equals((java.lang.Object)(byte)10);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=10 ]");
    boolean var22 = var1.equals((java.lang.Object)var21);
    java.lang.String var23 = var21.getStatus();
    java.util.Collection var24 = var21.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var27 = var26.getId();
    java.util.Collection var28 = var26.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var31 = var30.getId();
    java.util.Collection var32 = var30.getPropostaCollection();
    var30.setId((java.lang.Integer)10);
    java.lang.String var35 = var30.toString();
    java.lang.String var36 = var30.getStatus();
    boolean var37 = var26.equals((java.lang.Object)var30);
    java.util.Collection var38 = var26.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var41 = var40.getId();
    var40.setId((java.lang.Integer)(-1));
    java.util.Collection var44 = var40.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var46.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var50 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var51 = var50.getId();
    boolean var52 = var46.equals((java.lang.Object)var50);
    java.lang.Integer var53 = var46.getId();
    boolean var54 = var40.equals((java.lang.Object)var53);
    java.util.Collection var55 = var40.getPropostaCollection();
    var40.setId((java.lang.Integer)0);
    boolean var58 = var26.equals((java.lang.Object)0);
    java.lang.String var59 = var26.toString();
    boolean var60 = var21.equals((java.lang.Object)var26);
    var21.setStatus("");
    var21.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var65 = var21.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + (-1)+ "'", var31.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var35.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + (-1)+ "'", var41.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var44);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + 100+ "'", var51.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + 1+ "'", var53.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var54 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var55);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var59.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var65 + "' != '" + (-1)+ "'", var65.equals((-1)));

  }

  public void test180() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test180"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var17 = var9.toString();
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var9.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test181() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test181"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var19 = var1.getId();
    java.lang.String var20 = var1.getStatus();
    java.lang.String var21 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var25 = var1.equals((java.lang.Object)var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);

  }

  public void test182() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test182"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("");
    java.lang.String var17 = var1.getStatus();
    var1.setStatus("");
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var24 = var1.getPropostaCollection();
    java.lang.String var25 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + ""+ "'", var17.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test183() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test183"); }


    loja.com.br.entity.PropostaStatus var0 = new loja.com.br.entity.PropostaStatus();
    java.lang.Integer var1 = var0.getId();
    java.lang.Integer var2 = var0.getId();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var5 = var4.getId();
    var4.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var11 = var4.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var17 = var14.getId();
    boolean var18 = var4.equals((java.lang.Object)var17);
    var4.setId((java.lang.Integer)(-1));
    java.lang.String var21 = var4.toString();
    java.lang.String var22 = var4.getStatus();
    boolean var23 = var0.equals((java.lang.Object)var4);
    var0.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var1);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 10+ "'", var17.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);

  }

  public void test184() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test184"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)(short)(-1));
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var15 = var8.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var21 = var18.getId();
    boolean var22 = var8.equals((java.lang.Object)var21);
    boolean var23 = var1.equals((java.lang.Object)var8);
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var27 = var26.getPropostaCollection();
    java.lang.String var28 = var26.getStatus();
    boolean var29 = var1.equals((java.lang.Object)var26);
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 10+ "'", var21.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);

  }

  public void test185() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test185"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)1);
    java.util.Collection var13 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)0);
    var1.setId((java.lang.Integer)0);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);

  }

  public void test186() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test186"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var28 = var18.getId();
    java.util.Collection var29 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    java.lang.Integer var32 = var18.getId();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var37 = var18.getStatus();
    java.lang.String var38 = var18.toString();
    var18.setId((java.lang.Integer)(-1));
    java.lang.String var41 = var18.toString();
    java.util.Collection var42 = var18.getPropostaCollection();
    java.lang.String var43 = var18.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var43.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test187() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test187"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.Integer var7 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var14 = var13.getId();
    var13.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var20 = var13.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var23.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var26 = var23.getId();
    boolean var27 = var13.equals((java.lang.Object)var26);
    var13.setId((java.lang.Integer)(-1));
    java.lang.String var30 = var13.toString();
    var13.setStatus("hi!");
    java.lang.String var33 = var13.toString();
    java.lang.String var34 = var13.toString();
    boolean var35 = var1.equals((java.lang.Object)var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);

  }

  public void test188() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test188"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var5.getStatus();
    java.lang.Integer var11 = var5.getId();
    java.lang.String var12 = var5.toString();
    boolean var13 = var1.equals((java.lang.Object)var5);
    java.util.Collection var14 = var1.getPropostaCollection();
    java.util.Collection var15 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var12.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test189() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test189"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "");
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var6 = var4.equals((java.lang.Object)false);
    boolean var8 = var4.equals((java.lang.Object)10.0d);
    boolean var9 = var2.equals((java.lang.Object)var8);
    java.lang.String var10 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var14 = var13.toString();
    var13.setId((java.lang.Integer)(-1));
    boolean var17 = var2.equals((java.lang.Object)var13);
    java.lang.String var18 = var2.toString();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var20.setId((java.lang.Integer)100);
    java.lang.String var23 = var20.getStatus();
    var20.setId((java.lang.Integer)10);
    java.lang.Integer var26 = var20.getId();
    java.lang.String var27 = var20.toString();
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var29.setId((java.lang.Integer)100);
    java.lang.String var32 = var29.getStatus();
    var29.setId((java.lang.Integer)10);
    java.lang.Integer var35 = var29.getId();
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var40 = var38.equals((java.lang.Object)'#');
    var38.setStatus("hi!");
    boolean var43 = var29.equals((java.lang.Object)var38);
    var29.setId((java.lang.Integer)10);
    boolean var46 = var20.equals((java.lang.Object)var29);
    var29.setId((java.lang.Integer)10);
    java.lang.String var49 = var29.toString();
    boolean var50 = var2.equals((java.lang.Object)var49);
    java.lang.String var51 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + ""+ "'", var10.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var35 + "' != '" + 10+ "'", var35.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var43 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var49.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var51.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test190() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test190"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    boolean var17 = var11.equals((java.lang.Object)var15);
    java.util.Collection var18 = var15.getPropostaCollection();
    java.util.Collection var19 = var15.getPropostaCollection();
    boolean var20 = var1.equals((java.lang.Object)var15);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var25 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var29.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var34 = var33.getId();
    boolean var35 = var29.equals((java.lang.Object)var33);
    java.lang.String var36 = var29.getStatus();
    java.util.Collection var37 = var29.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var39.setId((java.lang.Integer)10);
    var39.setStatus("");
    java.lang.Integer var44 = var39.getId();
    java.util.Collection var45 = var39.getPropostaCollection();
    java.lang.Integer var46 = var39.getId();
    boolean var47 = var29.equals((java.lang.Object)var39);
    java.lang.String var48 = var39.getStatus();
    java.util.Collection var49 = var39.getPropostaCollection();
    java.lang.String var50 = var39.toString();
    java.lang.Integer var51 = var39.getId();
    boolean var52 = var1.equals((java.lang.Object)var51);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + 100+ "'", var34.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var36.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + 10+ "'", var44.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + 10+ "'", var46.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + ""+ "'", var48.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var50.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + 10+ "'", var51.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);

  }

  public void test191() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test191"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var3 = var1.equals((java.lang.Object)false);
    java.util.Collection var4 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var10 = var9.toString();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var12.setStatus("");
    java.lang.Integer var15 = var12.getId();
    java.util.Collection var16 = var12.getPropostaCollection();
    java.lang.String var17 = var12.toString();
    boolean var18 = var9.equals((java.lang.Object)var12);
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var22 = var12.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var23 = var1.equals((java.lang.Object)var12);
    java.lang.String var24 = var12.toString();
    var12.setId((java.lang.Integer)10);
    java.lang.String var27 = var12.toString();
    java.util.Collection var28 = var12.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var3 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + (-1)+ "'", var15.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);

  }

  public void test192() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test192"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("");
    java.lang.Integer var4 = var1.getId();
    java.util.Collection var5 = var1.getPropostaCollection();
    java.lang.Integer var6 = var1.getId();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var9.setId((java.lang.Integer)100);
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var14 = var9.getStatus();
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    var16.setId((java.lang.Integer)(-1));
    var16.setId((java.lang.Integer)100);
    boolean var22 = var9.equals((java.lang.Object)100);
    boolean var23 = var1.equals((java.lang.Object)100);
    java.util.Collection var24 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);

  }

  public void test193() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test193"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    java.lang.String var3 = var1.toString();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.util.Collection var5 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);

  }

  public void test194() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test194"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var12 = var11.getId();
    boolean var13 = var7.equals((java.lang.Object)var11);
    java.lang.Integer var14 = var7.getId();
    var7.setId((java.lang.Integer)10);
    java.lang.Integer var17 = var7.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.lang.String var23 = var19.toString();
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var26 = var19.getId();
    boolean var27 = var7.equals((java.lang.Object)var19);
    java.util.Collection var28 = var19.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var32 = var31.getStatus();
    boolean var33 = var19.equals((java.lang.Object)var32);
    boolean var34 = var1.equals((java.lang.Object)var19);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 10+ "'", var17.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == true);

  }

  public void test195() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test195"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.lang.Integer var4 = var1.getId();
    var1.setStatus("");
    java.util.Collection var7 = var1.getPropostaCollection();
    java.lang.String var8 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1, "hi!");
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var17.setStatus("");
    java.lang.Integer var20 = var17.getId();
    java.lang.String var21 = var17.getStatus();
    java.lang.String var22 = var17.toString();
    var17.setStatus("");
    loja.com.br.entity.PropostaStatus var27 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var27.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var27.setId((java.lang.Integer)100);
    boolean var32 = var17.equals((java.lang.Object)100);
    boolean var33 = var13.equals((java.lang.Object)var17);
    boolean var34 = var1.equals((java.lang.Object)var33);
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var38.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var42.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var47 = var42.getStatus();
    java.lang.Integer var48 = var42.getId();
    java.lang.String var49 = var42.toString();
    boolean var50 = var38.equals((java.lang.Object)var42);
    var38.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.util.Collection var53 = var38.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var55 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var55.setId((java.lang.Integer)10);
    java.lang.String var58 = var55.toString();
    java.lang.String var59 = var55.toString();
    java.lang.String var60 = var55.getStatus();
    var55.setStatus("hi!");
    java.util.Collection var63 = var55.getPropostaCollection();
    boolean var64 = var38.equals((java.lang.Object)var55);
    boolean var65 = var1.equals((java.lang.Object)var38);
    java.lang.String var66 = var38.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + (-1)+ "'", var3.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + (-1)+ "'", var4.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + ""+ "'", var8.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + ""+ "'", var21.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var32 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + 1+ "'", var48.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var49.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var58 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var58.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var59.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var60);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var63);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var65 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var66 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var66.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test196() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test196"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    java.lang.Integer var14 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var17 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test197() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test197"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    var4.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var15 = var14.getStatus();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var18 = var4.equals((java.lang.Object)var14);
    var14.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var27 = var26.getId();
    boolean var28 = var22.equals((java.lang.Object)var26);
    java.lang.Integer var29 = var22.getId();
    java.util.Collection var30 = var22.getPropostaCollection();
    var22.setStatus("");
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var36 = var22.equals((java.lang.Object)(-1));
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var39 = var38.getId();
    java.util.Collection var40 = var38.getPropostaCollection();
    var38.setId((java.lang.Integer)10);
    var38.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var46.setStatus("");
    java.lang.Integer var49 = var46.getId();
    java.util.Collection var50 = var46.getPropostaCollection();
    boolean var51 = var38.equals((java.lang.Object)var46);
    var38.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var56 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var57 = var56.toString();
    java.lang.Integer var58 = var56.getId();
    var56.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var61 = var56.toString();
    boolean var62 = var38.equals((java.lang.Object)var56);
    java.lang.String var63 = var38.getStatus();
    boolean var64 = var22.equals((java.lang.Object)var38);
    boolean var65 = var14.equals((java.lang.Object)var22);
    var14.setStatus("hi!");
    java.lang.String var68 = var14.toString();
    java.lang.String var69 = var14.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 100+ "'", var27.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 1+ "'", var29.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + (-1)+ "'", var39.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + (-1)+ "'", var49.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var57.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var58 + "' != '" + 10+ "'", var58.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var61 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var61.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var63 + "' != '" + "hi!"+ "'", var63.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var65 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var68.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var69 + "' != '" + "hi!"+ "'", var69.equals("hi!"));

  }

  public void test198() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test198"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var3 = var2.getPropostaCollection();
    java.lang.String var4 = var2.toString();
    java.lang.String var5 = var2.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test199() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test199"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    java.util.Collection var4 = var1.getPropostaCollection();
    java.lang.Integer var5 = var1.getId();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var7.setId((java.lang.Integer)10);
    java.util.Collection var10 = var7.getPropostaCollection();
    var7.setStatus("");
    var7.setId((java.lang.Integer)0);
    boolean var15 = var1.equals((java.lang.Object)var7);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    java.util.Collection var19 = var17.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var22 = var21.getId();
    java.util.Collection var23 = var21.getPropostaCollection();
    var21.setId((java.lang.Integer)10);
    java.lang.String var26 = var21.toString();
    java.lang.String var27 = var21.getStatus();
    boolean var28 = var17.equals((java.lang.Object)var21);
    var17.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var17.setStatus("");
    java.util.Collection var33 = var17.getPropostaCollection();
    var17.setStatus("");
    java.lang.String var36 = var17.getStatus();
    var17.setId((java.lang.Integer)10);
    boolean var39 = var7.equals((java.lang.Object)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 1+ "'", var2.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 1+ "'", var5.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + ""+ "'", var36.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);

  }

  public void test200() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test200"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var6 = var1.getStatus();
    var1.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=null ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=null ]"));

  }

  public void test201() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test201"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var7.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var12 = var11.getId();
    boolean var13 = var7.equals((java.lang.Object)var11);
    java.lang.Integer var14 = var7.getId();
    var7.setId((java.lang.Integer)10);
    java.lang.Integer var17 = var7.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var19.setStatus("");
    java.lang.Integer var22 = var19.getId();
    java.lang.String var23 = var19.toString();
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var26 = var19.getId();
    boolean var27 = var7.equals((java.lang.Object)var19);
    java.util.Collection var28 = var19.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var32 = var31.getStatus();
    boolean var33 = var19.equals((java.lang.Object)var32);
    boolean var34 = var1.equals((java.lang.Object)var19);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 100+ "'", var12.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var13 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 1+ "'", var14.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 10+ "'", var17.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var23.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + (-1)+ "'", var26.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var32.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var33 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == true);

  }

  public void test202() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test202"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.String var7 = var1.getStatus();
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    java.lang.Integer var10 = var1.getId();
    java.lang.String var11 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.util.Collection var14 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test203() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test203"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.Integer var5 = var1.getId();
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.getStatus();
    java.util.Collection var8 = var1.getPropostaCollection();
    java.lang.String var9 = var1.toString();
    var1.setId((java.lang.Integer)1);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));

  }

  public void test204() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test204"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var6 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "");
    java.util.Collection var10 = var9.getPropostaCollection();
    java.util.Collection var11 = var9.getPropostaCollection();
    java.lang.String var12 = var9.getStatus();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var17 = var14.toString();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var14.setId((java.lang.Integer)1);
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var24 = var14.getStatus();
    java.lang.String var25 = var14.toString();
    java.lang.Integer var26 = var14.getId();
    var14.setId((java.lang.Integer)1);
    java.lang.String var29 = var14.toString();
    boolean var30 = var9.equals((java.lang.Object)var29);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var33.setId((java.lang.Integer)10);
    java.lang.Integer var36 = var33.getId();
    java.lang.String var37 = var33.getStatus();
    java.lang.String var38 = var33.toString();
    java.lang.String var39 = var33.getStatus();
    boolean var40 = var9.equals((java.lang.Object)var39);
    boolean var41 = var1.equals((java.lang.Object)var39);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + ""+ "'", var12.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var17.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 1+ "'", var26.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 10+ "'", var36.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var39.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var41 == false);

  }

  public void test205() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test205"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    java.util.Collection var11 = var9.getPropostaCollection();
    var9.setId((java.lang.Integer)10);
    java.lang.String var14 = var9.toString();
    java.lang.Integer var15 = var9.getId();
    var9.setStatus("");
    var9.setStatus("hi!");
    java.lang.String var20 = var9.getStatus();
    boolean var21 = var1.equals((java.lang.Object)var9);
    java.lang.String var22 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 10+ "'", var15.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "hi!"+ "'", var20.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "hi!"+ "'", var22.equals("hi!"));

  }

  public void test206() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test206"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var7 = var1.getId();
    java.lang.String var8 = var1.toString();
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setId((java.lang.Integer)100);
    java.lang.String var13 = var10.getStatus();
    var10.setId((java.lang.Integer)10);
    java.lang.Integer var16 = var10.getId();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "hi!");
    boolean var21 = var19.equals((java.lang.Object)'#');
    var19.setStatus("hi!");
    boolean var24 = var10.equals((java.lang.Object)var19);
    var10.setId((java.lang.Integer)10);
    boolean var27 = var1.equals((java.lang.Object)var10);
    var10.setId((java.lang.Integer)10);
    java.lang.String var30 = var10.toString();
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var33.setId((java.lang.Integer)0);
    java.lang.Integer var36 = var33.getId();
    boolean var37 = var10.equals((java.lang.Object)var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + 0+ "'", var36.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);

  }

  public void test207() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test207"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    var2.setStatus("");
    java.util.Collection var7 = var2.getPropostaCollection();
    java.util.Collection var8 = var2.getPropostaCollection();
    java.util.Collection var9 = var2.getPropostaCollection();
    var2.setId((java.lang.Integer)(-1));
    java.lang.Integer var12 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));

  }

  public void test208() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test208"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)10);
    var1.setStatus("");
    java.lang.Integer var6 = var1.getId();
    java.util.Collection var7 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var10 = var9.getId();
    boolean var12 = var9.equals((java.lang.Object)(short)0);
    var9.setStatus("");
    boolean var15 = var1.equals((java.lang.Object)var9);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    boolean var20 = var17.equals((java.lang.Object)(short)0);
    java.util.Collection var21 = var17.getPropostaCollection();
    java.lang.Integer var22 = var17.getId();
    boolean var23 = var1.equals((java.lang.Object)var17);
    java.lang.Integer var24 = var17.getId();
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var27 = var26.toString();
    var26.setId((java.lang.Integer)(-1));
    var26.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var33.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var37 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var38 = var37.getId();
    boolean var39 = var33.equals((java.lang.Object)var37);
    java.lang.String var40 = var33.getStatus();
    java.util.Collection var41 = var33.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var43 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var43.setId((java.lang.Integer)10);
    var43.setStatus("");
    java.lang.Integer var48 = var43.getId();
    java.util.Collection var49 = var43.getPropostaCollection();
    java.lang.Integer var50 = var43.getId();
    boolean var51 = var33.equals((java.lang.Object)var43);
    java.lang.String var52 = var33.getStatus();
    boolean var53 = var26.equals((java.lang.Object)var33);
    boolean var54 = var17.equals((java.lang.Object)var33);
    java.lang.String var55 = var17.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + (-1)+ "'", var10.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + (-1)+ "'", var24.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var27.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + 100+ "'", var38.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var39 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + 10+ "'", var48.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + 10+ "'", var50.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var52.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var54 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var55.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test209() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test209"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var28 = var18.getId();
    java.util.Collection var29 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    java.lang.Integer var32 = var18.getId();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var37 = var18.getId();
    var18.setId((java.lang.Integer)(-1));
    java.lang.Integer var40 = var18.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + 10+ "'", var37.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + (-1)+ "'", var40.equals((-1)));

  }

  public void test210() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test210"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var2 = var1.toString();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var4.setStatus("");
    java.lang.Integer var7 = var4.getId();
    java.util.Collection var8 = var4.getPropostaCollection();
    java.lang.String var9 = var4.getStatus();
    boolean var10 = var1.equals((java.lang.Object)var4);
    var4.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var15 = var14.getStatus();
    var14.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var18 = var4.equals((java.lang.Object)var14);
    var14.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var26 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var27 = var26.getId();
    boolean var28 = var22.equals((java.lang.Object)var26);
    java.lang.Integer var29 = var22.getId();
    java.util.Collection var30 = var22.getPropostaCollection();
    var22.setStatus("");
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var36 = var22.equals((java.lang.Object)(-1));
    loja.com.br.entity.PropostaStatus var38 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var39 = var38.getId();
    java.util.Collection var40 = var38.getPropostaCollection();
    var38.setId((java.lang.Integer)10);
    var38.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var46.setStatus("");
    java.lang.Integer var49 = var46.getId();
    java.util.Collection var50 = var46.getPropostaCollection();
    boolean var51 = var38.equals((java.lang.Object)var46);
    var38.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var56 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var57 = var56.toString();
    java.lang.Integer var58 = var56.getId();
    var56.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var61 = var56.toString();
    boolean var62 = var38.equals((java.lang.Object)var56);
    java.lang.String var63 = var38.getStatus();
    boolean var64 = var22.equals((java.lang.Object)var38);
    boolean var65 = var14.equals((java.lang.Object)var22);
    java.lang.Integer var66 = var22.getId();
    var22.setStatus("");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + ""+ "'", var9.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 100+ "'", var27.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 1+ "'", var29.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + (-1)+ "'", var39.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + (-1)+ "'", var49.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var57.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var58 + "' != '" + 10+ "'", var58.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var61 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var61.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var63 + "' != '" + "hi!"+ "'", var63.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var64 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var65 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var66 + "' != '" + 1+ "'", var66.equals(1));

  }

  public void test211() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test211"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var11.setId((java.lang.Integer)(-1));
    java.util.Collection var14 = var11.getPropostaCollection();
    java.lang.String var15 = var11.toString();
    java.lang.String var16 = var11.getStatus();
    java.lang.Integer var17 = var11.getId();
    java.util.Collection var18 = var11.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var21 = var20.getId();
    java.util.Collection var22 = var20.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var25 = var24.getId();
    java.util.Collection var26 = var24.getPropostaCollection();
    var24.setId((java.lang.Integer)10);
    java.lang.String var29 = var24.toString();
    java.lang.String var30 = var24.getStatus();
    boolean var31 = var20.equals((java.lang.Object)var24);
    var20.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var20.setStatus("");
    java.util.Collection var36 = var20.getPropostaCollection();
    java.lang.String var37 = var20.getStatus();
    java.lang.String var38 = var20.toString();
    loja.com.br.entity.PropostaStatus var40 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var41 = var40.getStatus();
    java.lang.String var42 = var40.getStatus();
    var40.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var45 = var40.getPropostaCollection();
    boolean var46 = var20.equals((java.lang.Object)var40);
    boolean var47 = var11.equals((java.lang.Object)var20);
    java.lang.Integer var48 = var20.getId();
    boolean var49 = var1.equals((java.lang.Object)var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var16.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + (-1)+ "'", var21.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + (-1)+ "'", var25.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var30);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + ""+ "'", var37.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var38.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var42);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var46 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var47 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + (-1)+ "'", var48.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var49 == false);

  }

  public void test212() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test212"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    boolean var9 = var1.equals((java.lang.Object)1.0f);
    var1.setId((java.lang.Integer)100);
    var1.setId((java.lang.Integer)10);
    var1.setId((java.lang.Integer)0);
    java.util.Collection var16 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);

  }

  public void test213() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test213"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)(-1));
    java.util.Collection var5 = var2.getPropostaCollection();
    java.lang.String var6 = var2.toString();
    java.lang.String var7 = var2.getStatus();
    java.lang.Integer var8 = var2.getId();
    java.util.Collection var9 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var12 = var11.getId();
    java.util.Collection var13 = var11.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var16 = var15.getId();
    java.util.Collection var17 = var15.getPropostaCollection();
    var15.setId((java.lang.Integer)10);
    java.lang.String var20 = var15.toString();
    java.lang.String var21 = var15.getStatus();
    boolean var22 = var11.equals((java.lang.Object)var15);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var11.setStatus("");
    java.util.Collection var27 = var11.getPropostaCollection();
    java.lang.String var28 = var11.getStatus();
    java.lang.String var29 = var11.toString();
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    java.lang.String var32 = var31.getStatus();
    java.lang.String var33 = var31.getStatus();
    var31.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var36 = var31.getPropostaCollection();
    boolean var37 = var11.equals((java.lang.Object)var31);
    boolean var38 = var2.equals((java.lang.Object)var11);
    java.lang.Integer var39 = var11.getId();
    java.util.Collection var40 = var11.getPropostaCollection();
    java.lang.String var41 = var11.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + (-1)+ "'", var16.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + ""+ "'", var28.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var37 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + (-1)+ "'", var39.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var40);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var41 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var41.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));

  }

  public void test214() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test214"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var9 = var8.getId();
    var8.setId((java.lang.Integer)0);
    java.lang.Integer var12 = var8.getId();
    java.lang.Integer var13 = var8.getId();
    boolean var14 = var1.equals((java.lang.Object)var8);
    java.lang.String var15 = var8.getStatus();
    var8.setId((java.lang.Integer)(-1));
    var8.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 100+ "'", var2.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 100+ "'", var9.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 0+ "'", var12.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + 0+ "'", var13.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);

  }

  public void test215() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test215"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    var1.setStatus("");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var12 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.String var15 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var15.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test216() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test216"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    var2.setId((java.lang.Integer)1);
    var2.setStatus("");
    java.util.Collection var7 = var2.getPropostaCollection();
    java.util.Collection var8 = var2.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var10 = var9.getPropostaCollection();
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var9.setStatus("");
    java.util.Collection var15 = var9.getPropostaCollection();
    var9.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var9.setId((java.lang.Integer)1);
    boolean var20 = var2.equals((java.lang.Object)var9);
    java.util.Collection var21 = var9.getPropostaCollection();
    java.util.Collection var22 = var9.getPropostaCollection();
    var9.setId((java.lang.Integer)10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var10);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);

  }

  public void test217() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test217"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    java.lang.Integer var7 = var1.getId();
    java.lang.Integer var8 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var11 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var15 = var14.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var19 = var18.toString();
    var18.setId((java.lang.Integer)(-1));
    boolean var22 = var14.equals((java.lang.Object)var18);
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.String var25 = var24.toString();
    java.util.Collection var26 = var24.getPropostaCollection();
    boolean var27 = var18.equals((java.lang.Object)var24);
    boolean var28 = var1.equals((java.lang.Object)var24);
    java.lang.String var29 = var1.toString();
    loja.com.br.entity.PropostaStatus var31 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var31.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var34 = var31.toString();
    var31.setStatus("hi!");
    var31.setId((java.lang.Integer)0);
    java.lang.Integer var39 = var31.getId();
    var31.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var44 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var45 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var46 = var45.getPropostaCollection();
    var45.setId((java.lang.Integer)100);
    java.lang.Integer var49 = var45.getId();
    boolean var50 = var44.equals((java.lang.Object)var45);
    var44.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var53 = var31.equals((java.lang.Object)var44);
    java.lang.String var54 = var44.toString();
    boolean var55 = var1.equals((java.lang.Object)var54);
    java.util.Collection var56 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var59 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.String var60 = var59.toString();
    java.lang.Integer var61 = var59.getId();
    loja.com.br.entity.PropostaStatus var63 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var64 = var63.getId();
    java.util.Collection var65 = var63.getPropostaCollection();
    var63.setId((java.lang.Integer)10);
    java.lang.String var68 = var63.toString();
    java.lang.String var69 = var63.getStatus();
    var63.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var73 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var74 = var73.toString();
    loja.com.br.entity.PropostaStatus var76 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var76.setStatus("");
    java.lang.Integer var79 = var76.getId();
    java.util.Collection var80 = var76.getPropostaCollection();
    java.lang.String var81 = var76.getStatus();
    boolean var82 = var73.equals((java.lang.Object)var76);
    boolean var83 = var63.equals((java.lang.Object)var73);
    java.util.Collection var84 = var63.getPropostaCollection();
    java.util.Collection var85 = var63.getPropostaCollection();
    java.lang.Integer var86 = var63.getId();
    var63.setId((java.lang.Integer)(-1));
    java.lang.Integer var89 = var63.getId();
    boolean var90 = var59.equals((java.lang.Object)var89);
    boolean var91 = var1.equals((java.lang.Object)var90);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 10+ "'", var7.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var22 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var34.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + 0+ "'", var39.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var46);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + 100+ "'", var49.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var53 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var54.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var55 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var60 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var60.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var61 + "' != '" + 100+ "'", var61.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var64 + "' != '" + (-1)+ "'", var64.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var65);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var68.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var69);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var74 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var74.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var79 + "' != '" + (-1)+ "'", var79.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var80);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var81 + "' != '" + ""+ "'", var81.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var82 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var83 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var84);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var85);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var86 + "' != '" + 0+ "'", var86.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var89 + "' != '" + (-1)+ "'", var89.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var90 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var91 == false);

  }

  public void test218() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test218"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setStatus("");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1), "loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var15 = var1.equals((java.lang.Object)(-1));
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var18 = var17.getId();
    java.lang.Integer var19 = var17.getId();
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var22 = var21.getId();
    java.util.Collection var23 = var21.getPropostaCollection();
    var21.setId((java.lang.Integer)10);
    var21.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var29 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var29.setStatus("");
    java.lang.Integer var32 = var29.getId();
    java.util.Collection var33 = var29.getPropostaCollection();
    boolean var34 = var21.equals((java.lang.Object)var29);
    var21.setId((java.lang.Integer)10);
    boolean var38 = var21.equals((java.lang.Object)(byte)10);
    java.lang.String var39 = var21.getStatus();
    boolean var40 = var17.equals((java.lang.Object)var21);
    java.lang.String var41 = var17.getStatus();
    var17.setStatus("");
    boolean var44 = var1.equals((java.lang.Object)"");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var48 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var48.setStatus("");
    java.lang.Integer var51 = var48.getId();
    java.lang.String var52 = var48.getStatus();
    java.lang.String var53 = var48.toString();
    java.lang.Integer var54 = var48.getId();
    var48.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.Integer var57 = var48.getId();
    java.lang.Integer var58 = var48.getId();
    loja.com.br.entity.PropostaStatus var60 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var60.setId((java.lang.Integer)10);
    java.lang.String var63 = var60.toString();
    boolean var65 = var60.equals((java.lang.Object)(byte)100);
    var60.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var68 = var60.getStatus();
    var60.setId((java.lang.Integer)10);
    boolean var71 = var48.equals((java.lang.Object)var60);
    java.lang.String var72 = var48.getStatus();
    java.lang.String var73 = var48.getStatus();
    java.lang.Integer var74 = var48.getId();
    java.lang.Integer var75 = var48.getId();
    boolean var76 = var1.equals((java.lang.Object)var48);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + (-1)+ "'", var18.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + (-1)+ "'", var19.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + (-1)+ "'", var22.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + (-1)+ "'", var32.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + "hi!"+ "'", var39.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var40 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + (-1)+ "'", var51.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var52 + "' != '" + ""+ "'", var52.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var53.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var54 + "' != '" + (-1)+ "'", var54.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var57 + "' != '" + (-1)+ "'", var57.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var58 + "' != '" + (-1)+ "'", var58.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var63 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var63.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var65 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var68 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var68.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var71 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var72 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var72.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var73 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var73.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var74 + "' != '" + (-1)+ "'", var74.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var75 + "' != '" + (-1)+ "'", var75.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var76 == false);

  }

  public void test219() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test219"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.toString();
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var6 = var2.getId();
    java.util.Collection var7 = var2.getPropostaCollection();
    java.lang.Integer var8 = var2.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 10+ "'", var6.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 10+ "'", var8.equals(10));

  }

  public void test220() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test220"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    boolean var6 = var1.equals((java.lang.Object)(byte)100);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var9 = var1.getStatus();
    java.lang.Integer var10 = var1.getId();
    java.util.Collection var11 = var1.getPropostaCollection();
    java.lang.Integer var12 = var1.getId();
    java.util.Collection var13 = var1.getPropostaCollection();
    java.util.Collection var14 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + 10+ "'", var10.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 10+ "'", var12.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);

  }

  public void test221() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test221"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setId((java.lang.Integer)100);
    java.lang.String var4 = var1.getStatus();
    java.util.Collection var5 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var7 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var9 = var7.equals((java.lang.Object)false);
    boolean var10 = var1.equals((java.lang.Object)var7);
    java.util.Collection var11 = var1.getPropostaCollection();
    var1.setStatus("hi!");
    java.lang.Integer var14 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var5);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var9 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var10 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + 100+ "'", var14.equals(100));

  }

  public void test222() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test222"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var3 = var2.getStatus();
    java.lang.Integer var4 = var2.getId();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var7 = var6.getId();
    java.util.Collection var8 = var6.getPropostaCollection();
    var6.setId((java.lang.Integer)10);
    var6.setStatus("hi!");
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var6.setId((java.lang.Integer)10);
    boolean var17 = var2.equals((java.lang.Object)10);
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var20 = var19.getId();
    java.util.Collection var21 = var19.getPropostaCollection();
    var19.setId((java.lang.Integer)10);
    java.lang.String var24 = var19.toString();
    java.lang.String var25 = var19.getStatus();
    java.lang.Integer var26 = var19.getId();
    java.util.Collection var27 = var19.getPropostaCollection();
    boolean var28 = var2.equals((java.lang.Object)var19);
    java.lang.String var29 = var19.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var3.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + 0+ "'", var4.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + (-1)+ "'", var20.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 10+ "'", var26.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var27);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));

  }

  public void test223() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test223"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    boolean var17 = var11.equals((java.lang.Object)var15);
    java.util.Collection var18 = var15.getPropostaCollection();
    java.util.Collection var19 = var15.getPropostaCollection();
    boolean var20 = var1.equals((java.lang.Object)var15);
    var15.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var15.setId((java.lang.Integer)100);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);

  }

  public void test224() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test224"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    java.util.Collection var13 = var1.getPropostaCollection();
    java.lang.String var14 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var17 = var16.getPropostaCollection();
    var16.setId((java.lang.Integer)0);
    java.lang.Integer var20 = var16.getId();
    boolean var21 = var1.equals((java.lang.Object)var16);
    java.lang.Integer var22 = var16.getId();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var24.setStatus("");
    java.lang.Integer var27 = var24.getId();
    java.lang.String var28 = var24.toString();
    var24.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.Integer var31 = var24.getId();
    var24.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var24.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var36 = var16.equals((java.lang.Object)var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + 0+ "'", var20.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var21 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 0+ "'", var22.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var31 + "' != '" + (-1)+ "'", var31.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var36 == false);

  }

  public void test225() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test225"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var7 = var6.getId();
    java.lang.Integer var8 = var6.getId();
    java.lang.Integer var9 = var6.getId();
    var6.setStatus("");
    java.util.Collection var12 = var6.getPropostaCollection();
    java.lang.String var13 = var6.getStatus();
    var6.setId((java.lang.Integer)10);
    var6.setId((java.lang.Integer)0);
    boolean var18 = var1.equals((java.lang.Object)0);
    java.util.Collection var19 = var1.getPropostaCollection();
    java.lang.String var20 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var23 = var22.getId();
    java.util.Collection var24 = var22.getPropostaCollection();
    var22.setId((java.lang.Integer)10);
    var22.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var30.setStatus("");
    java.lang.Integer var33 = var30.getId();
    java.util.Collection var34 = var30.getPropostaCollection();
    boolean var35 = var22.equals((java.lang.Object)var30);
    var22.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var39 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var39.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var43 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var44 = var43.getId();
    boolean var45 = var39.equals((java.lang.Object)var43);
    java.lang.String var46 = var39.getStatus();
    java.lang.String var47 = var39.getStatus();
    boolean var48 = var22.equals((java.lang.Object)var39);
    java.lang.String var49 = var22.getStatus();
    java.util.Collection var50 = var22.getPropostaCollection();
    java.lang.String var51 = var22.toString();
    var22.setId((java.lang.Integer)100);
    var22.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var58 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var60 = var58.equals((java.lang.Object)(-1.0d));
    java.util.Collection var61 = var58.getPropostaCollection();
    var58.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var66 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var67 = var66.toString();
    var66.setStatus("hi!");
    java.util.Collection var70 = var66.getPropostaCollection();
    var66.setStatus("hi!");
    boolean var73 = var58.equals((java.lang.Object)var66);
    java.util.Collection var74 = var66.getPropostaCollection();
    java.lang.String var75 = var66.toString();
    java.lang.String var76 = var66.getStatus();
    java.lang.String var77 = var66.getStatus();
    java.lang.String var78 = var66.getStatus();
    boolean var79 = var22.equals((java.lang.Object)var66);
    java.lang.String var80 = var22.getStatus();
    boolean var81 = var1.equals((java.lang.Object)var80);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + (-1)+ "'", var8.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + (-1)+ "'", var9.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + ""+ "'", var13.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + (-1)+ "'", var33.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + 100+ "'", var44.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var45 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var46 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var46.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var47 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var47.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var48 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var49 + "' != '" + "hi!"+ "'", var49.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var50);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var51.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var60 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var61);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var67 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var67.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var70);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var73 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var74);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var75 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var75.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var76 + "' != '" + "hi!"+ "'", var76.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var77 + "' != '" + "hi!"+ "'", var77.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var78 + "' != '" + "hi!"+ "'", var78.equals("hi!"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var79 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var80 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var80.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var81 == false);

  }

  public void test226() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test226"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var1.setId((java.lang.Integer)10);
    java.lang.String var4 = var1.toString();
    java.lang.String var5 = var1.toString();
    var1.setId((java.lang.Integer)(-1));
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    boolean var12 = var10.equals((java.lang.Object)(-1.0d));
    java.util.Collection var13 = var10.getPropostaCollection();
    var10.setId((java.lang.Integer)(-1));
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var18 = var1.equals((java.lang.Object)var10);
    java.util.Collection var19 = var10.getPropostaCollection();
    java.lang.String var20 = var10.toString();
    java.util.Collection var21 = var10.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.Integer var25 = var24.getId();
    java.lang.String var26 = var24.getStatus();
    var24.setId((java.lang.Integer)1);
    boolean var29 = var10.equals((java.lang.Object)var24);
    var10.setStatus("hi!");
    java.util.Collection var32 = var10.getPropostaCollection();
    java.lang.String var33 = var10.toString();
    java.lang.Integer var34 = var10.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var5.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var21);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var29 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var33.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var34 + "' != '" + (-1)+ "'", var34.equals((-1)));

  }

  public void test227() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test227"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var22 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var23 = var22.getId();
    boolean var24 = var18.equals((java.lang.Object)var22);
    java.lang.String var25 = var18.getStatus();
    java.lang.String var26 = var18.getStatus();
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.Integer var28 = var18.getId();
    java.util.Collection var29 = var18.getPropostaCollection();
    var18.setId((java.lang.Integer)10);
    java.lang.Integer var32 = var18.getId();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.util.Collection var37 = var18.getPropostaCollection();
    java.lang.Integer var38 = var18.getId();
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.util.Collection var43 = var42.getPropostaCollection();
    java.lang.Integer var44 = var42.getId();
    java.util.Collection var45 = var42.getPropostaCollection();
    var42.setId((java.lang.Integer)1);
    java.lang.String var48 = var42.toString();
    java.lang.String var49 = var42.getStatus();
    boolean var50 = var18.equals((java.lang.Object)var42);
    java.lang.String var51 = var42.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + 100+ "'", var23.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var24 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var25.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var26.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + 1+ "'", var28.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var38 + "' != '" + 10+ "'", var38.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var44 + "' != '" + 1+ "'", var44.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var45);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var48.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var50 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var51.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));

  }

  public void test228() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test228"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "");
    var2.setId((java.lang.Integer)100);
    loja.com.br.entity.PropostaStatus var6 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var6.setId((java.lang.Integer)10);
    java.lang.String var9 = var6.toString();
    boolean var11 = var6.equals((java.lang.Object)(byte)100);
    var6.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    java.lang.String var14 = var6.getStatus();
    java.lang.Integer var15 = var6.getId();
    java.lang.Integer var16 = var6.getId();
    boolean var17 = var2.equals((java.lang.Object)var6);
    java.lang.String var18 = var2.toString();
    java.lang.String var19 = var2.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var11 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 10+ "'", var15.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 10+ "'", var16.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + ""+ "'", var19.equals(""));

  }

  public void test229() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test229"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.Integer var2 = var1.getId();
    java.lang.Integer var3 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    var1.setId((java.lang.Integer)0);
    java.lang.Integer var8 = var1.getId();
    java.lang.String var9 = var1.toString();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + 1+ "'", var2.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var3 + "' != '" + 1+ "'", var3.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 0+ "'", var8.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var9.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test230() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test230"); }


    loja.com.br.entity.PropostaStatus var2 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    loja.com.br.entity.PropostaStatus var3 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var4 = var3.getPropostaCollection();
    var3.setId((java.lang.Integer)100);
    java.lang.Integer var7 = var3.getId();
    boolean var8 = var2.equals((java.lang.Object)var3);
    var2.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.String var11 = var2.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    boolean var15 = var13.equals((java.lang.Object)false);
    java.util.Collection var16 = var13.getPropostaCollection();
    var13.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    loja.com.br.entity.PropostaStatus var21 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.lang.String var22 = var21.toString();
    loja.com.br.entity.PropostaStatus var24 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var24.setStatus("");
    java.lang.Integer var27 = var24.getId();
    java.util.Collection var28 = var24.getPropostaCollection();
    java.lang.String var29 = var24.toString();
    boolean var30 = var21.equals((java.lang.Object)var24);
    loja.com.br.entity.PropostaStatus var33 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var34 = var24.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var35 = var13.equals((java.lang.Object)var24);
    var24.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    boolean var38 = var2.equals((java.lang.Object)var24);
    java.lang.Integer var39 = var24.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var4);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + 100+ "'", var7.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var15 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var16);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var22.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + (-1)+ "'", var27.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var29.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var34 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var35 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var38 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var39 + "' != '" + (-1)+ "'", var39.equals((-1)));

  }

  public void test231() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test231"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=10 ]");
    java.lang.String var4 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.String var11 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var14 = var13.toString();
    boolean var16 = var13.equals((java.lang.Object)"");
    boolean var17 = var1.equals((java.lang.Object)var13);
    java.lang.String var18 = var1.getStatus();
    java.lang.String var19 = var1.toString();
    java.util.Collection var20 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var4 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var4.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var11.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var14.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var19.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);

  }

  public void test232() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test232"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    loja.com.br.entity.PropostaStatus var10 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var10.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var14 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var15 = var14.getId();
    boolean var16 = var10.equals((java.lang.Object)var14);
    loja.com.br.entity.PropostaStatus var18 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)10);
    var18.setId((java.lang.Integer)10);
    java.lang.String var21 = var18.toString();
    boolean var23 = var18.equals((java.lang.Object)(byte)100);
    var18.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    boolean var26 = var10.equals((java.lang.Object)var18);
    boolean var27 = var1.equals((java.lang.Object)var18);
    java.lang.String var28 = var1.toString();
    java.util.Collection var29 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var30 = new loja.com.br.entity.PropostaStatus();
    java.lang.Integer var31 = var30.getId();
    java.lang.Integer var32 = var30.getId();
    java.lang.String var33 = var30.getStatus();
    loja.com.br.entity.PropostaStatus var35 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var36 = var35.getId();
    java.util.Collection var37 = var35.getPropostaCollection();
    var35.setId((java.lang.Integer)10);
    java.lang.String var40 = var35.toString();
    java.util.Collection var41 = var35.getPropostaCollection();
    boolean var42 = var30.equals((java.lang.Object)var35);
    java.lang.String var43 = var30.getStatus();
    boolean var44 = var1.equals((java.lang.Object)var30);
    loja.com.br.entity.PropostaStatus var46 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var46.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var50 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var51 = var50.getId();
    boolean var52 = var46.equals((java.lang.Object)var50);
    java.lang.Integer var53 = var46.getId();
    var46.setId((java.lang.Integer)10);
    java.lang.Integer var56 = var46.getId();
    var46.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var60 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var61 = var60.getId();
    java.util.Collection var62 = var60.getPropostaCollection();
    var60.setId((java.lang.Integer)10);
    var60.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var68 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var68.setStatus("");
    java.lang.Integer var71 = var68.getId();
    java.util.Collection var72 = var68.getPropostaCollection();
    boolean var73 = var60.equals((java.lang.Object)var68);
    var60.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var77 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var77.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var81 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var82 = var81.getId();
    boolean var83 = var77.equals((java.lang.Object)var81);
    java.lang.String var84 = var77.getStatus();
    java.lang.String var85 = var77.getStatus();
    boolean var86 = var60.equals((java.lang.Object)var77);
    var60.setId((java.lang.Integer)1);
    boolean var89 = var46.equals((java.lang.Object)var60);
    java.lang.Integer var90 = var60.getId();
    boolean var91 = var1.equals((java.lang.Object)var90);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var15 + "' != '" + 100+ "'", var15.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var16 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var26 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var28 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var28.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var29);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var31);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var32);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var33);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var36 + "' != '" + (-1)+ "'", var36.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var37);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var40 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var40.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var41);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var42 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var43);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var44 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var51 + "' != '" + 100+ "'", var51.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var53 + "' != '" + 1+ "'", var53.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var56 + "' != '" + 10+ "'", var56.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var61 + "' != '" + (-1)+ "'", var61.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var62);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var71 + "' != '" + (-1)+ "'", var71.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var72);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var73 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var82 + "' != '" + 100+ "'", var82.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var83 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var84 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var84.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var85 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var85.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var86 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var89 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var90 + "' != '" + 1+ "'", var90.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var91 == false);

  }

  public void test233() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test233"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    var1.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var9 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var9.setStatus("");
    java.lang.Integer var12 = var9.getId();
    java.util.Collection var13 = var9.getPropostaCollection();
    boolean var14 = var1.equals((java.lang.Object)var9);
    var1.setId((java.lang.Integer)10);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    java.lang.Integer var19 = var1.getId();
    java.lang.String var20 = var1.getStatus();
    var1.setStatus("hi!");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var14 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var20 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var20.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test234() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test234"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.lang.Integer var8 = var1.getId();
    java.lang.Integer var9 = var1.getId();
    java.lang.String var10 = var1.getStatus();
    java.lang.Integer var11 = var1.getId();
    java.lang.Integer var12 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var9 + "' != '" + 1+ "'", var9.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 1+ "'", var11.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + 1+ "'", var12.equals(1));

  }

  public void test235() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test235"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var8 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100, "loja.com.br.entity.PropostaStatus[ id=10 ]");
    var8.setId((java.lang.Integer)1);
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var13 = var12.getId();
    java.util.Collection var14 = var12.getPropostaCollection();
    var12.setId((java.lang.Integer)10);
    var12.setStatus("hi!");
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var20.setStatus("");
    java.lang.Integer var23 = var20.getId();
    java.util.Collection var24 = var20.getPropostaCollection();
    boolean var25 = var12.equals((java.lang.Object)var20);
    var12.setId((java.lang.Integer)10);
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    boolean var30 = var8.equals((java.lang.Object)var12);
    boolean var31 = var1.equals((java.lang.Object)var12);
    java.lang.Integer var32 = var12.getId();
    java.lang.Integer var33 = var12.getId();
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    java.util.Collection var36 = var12.getPropostaCollection();
    var12.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var13 + "' != '" + (-1)+ "'", var13.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var14);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var23 + "' != '" + (-1)+ "'", var23.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var30 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 10+ "'", var32.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + 10+ "'", var33.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var36);

  }

  public void test236() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test236"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)10);
    java.lang.String var6 = var1.toString();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=100 ]");
    java.util.Collection var9 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var11 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var11.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var15 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var16 = var15.getId();
    boolean var17 = var11.equals((java.lang.Object)var15);
    java.util.Collection var18 = var15.getPropostaCollection();
    java.util.Collection var19 = var15.getPropostaCollection();
    boolean var20 = var1.equals((java.lang.Object)var15);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=1 ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    java.util.Collection var25 = var1.getPropostaCollection();
    java.util.Collection var26 = var1.getPropostaCollection();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var6.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var17 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var19);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var20 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var25);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);

  }

  public void test237() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test237"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    var1.setId((java.lang.Integer)10);
    java.lang.Integer var11 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var11 + "' != '" + 10+ "'", var11.equals(10));

  }

  public void test238() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test238"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.util.Collection var8 = var5.getPropostaCollection();
    java.util.Collection var9 = var5.getPropostaCollection();
    var5.setStatus("loja.com.br.entity.PropostaStatus[ id=0 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var14 = var13.getId();
    java.util.Collection var15 = var13.getPropostaCollection();
    var13.setId((java.lang.Integer)10);
    java.lang.String var18 = var13.toString();
    java.lang.Integer var19 = var13.getId();
    java.util.Collection var20 = var13.getPropostaCollection();
    java.lang.Integer var21 = var13.getId();
    java.lang.String var22 = var13.getStatus();
    boolean var23 = var5.equals((java.lang.Object)var13);
    java.lang.String var24 = var5.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var8);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var15);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var18 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var18.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 10+ "'", var19.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var20);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + 10+ "'", var21.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var22);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var23 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=0 ]"+ "'", var24.equals("loja.com.br.entity.PropostaStatus[ id=0 ]"));

  }

  public void test239() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test239"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.String var2 = var1.toString();
    boolean var4 = var1.equals((java.lang.Object)"");
    java.lang.Integer var5 = var1.getId();
    java.util.Collection var6 = var1.getPropostaCollection();
    java.lang.String var7 = var1.getStatus();
    var1.setId((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var13 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var14 = var13.getId();
    var13.setId((java.lang.Integer)(-1));
    java.util.Collection var17 = var13.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var19 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var19.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var23 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var24 = var23.getId();
    boolean var25 = var19.equals((java.lang.Object)var23);
    java.lang.Integer var26 = var19.getId();
    boolean var27 = var13.equals((java.lang.Object)var26);
    java.util.Collection var28 = var13.getPropostaCollection();
    var13.setStatus("hi!");
    boolean var31 = var1.equals((java.lang.Object)"hi!");
    java.lang.Integer var32 = var1.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var2.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var4 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + 100+ "'", var5.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var6);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var14 + "' != '" + (-1)+ "'", var14.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var17);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var24 + "' != '" + 100+ "'", var24.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var25 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + 1+ "'", var26.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var27 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var28);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var31 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var32 + "' != '" + 1+ "'", var32.equals(1));

  }

  public void test240() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test240"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.util.Collection var2 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var4 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var5 = var4.getId();
    java.lang.Integer var6 = var4.getId();
    java.lang.Integer var7 = var4.getId();
    boolean var8 = var1.equals((java.lang.Object)var4);
    java.lang.String var9 = var1.getStatus();
    var1.setStatus("hi!");
    java.lang.String var12 = var1.getStatus();
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var2);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var5 + "' != '" + (-1)+ "'", var5.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + (-1)+ "'", var7.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var8 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + "hi!"+ "'", var12.equals("hi!"));

  }

  public void test241() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test241"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=-1 ]");
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)100);
    java.lang.Integer var6 = var5.getId();
    boolean var7 = var1.equals((java.lang.Object)var5);
    java.lang.Integer var8 = var1.getId();
    java.util.Collection var9 = var1.getPropostaCollection();
    var1.setId((java.lang.Integer)(-1));
    java.lang.Integer var12 = var1.getId();
    var1.setId((java.lang.Integer)10);
    loja.com.br.entity.PropostaStatus var16 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var17 = var16.getId();
    java.util.Collection var18 = var16.getPropostaCollection();
    var16.setId((java.lang.Integer)10);
    java.lang.String var21 = var16.toString();
    java.lang.Integer var22 = var16.getId();
    var16.setStatus("");
    java.lang.String var25 = var16.getStatus();
    java.lang.String var26 = var16.getStatus();
    java.lang.Integer var27 = var16.getId();
    boolean var28 = var1.equals((java.lang.Object)var16);
    java.lang.Integer var29 = var16.getId();
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + 100+ "'", var6.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var7 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + 1+ "'", var8.equals(1));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var9);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var12 + "' != '" + (-1)+ "'", var12.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + (-1)+ "'", var17.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var21 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var21.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var22 + "' != '" + 10+ "'", var22.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + ""+ "'", var25.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var26 + "' != '" + ""+ "'", var26.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 10+ "'", var27.equals(10));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var28 == true);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var29 + "' != '" + 10+ "'", var29.equals(10));

  }

  public void test242() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test242"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    var1.setId((java.lang.Integer)(-1));
    boolean var6 = var1.equals((java.lang.Object)'4');
    java.lang.String var7 = var1.toString();
    java.lang.String var8 = var1.toString();
    var1.setId((java.lang.Integer)100);
    java.lang.String var11 = var1.getStatus();
    loja.com.br.entity.PropostaStatus var12 = new loja.com.br.entity.PropostaStatus();
    java.util.Collection var13 = var12.getPropostaCollection();
    var12.setId((java.lang.Integer)100);
    java.lang.Integer var16 = var12.getId();
    java.lang.Integer var17 = var12.getId();
    java.lang.String var18 = var12.getStatus();
    loja.com.br.entity.PropostaStatus var20 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    var20.setId((java.lang.Integer)100);
    java.lang.String var23 = var20.getStatus();
    java.util.Collection var24 = var20.getPropostaCollection();
    java.lang.Integer var25 = var20.getId();
    java.util.Collection var26 = var20.getPropostaCollection();
    java.lang.Integer var27 = var20.getId();
    var20.setId((java.lang.Integer)(-1));
    java.lang.String var30 = var20.toString();
    loja.com.br.entity.PropostaStatus var32 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var33 = var32.getId();
    java.util.Collection var34 = var32.getPropostaCollection();
    var32.setId((java.lang.Integer)10);
    java.lang.String var37 = var32.toString();
    java.lang.String var38 = var32.getStatus();
    var32.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var42 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)1);
    java.lang.String var43 = var42.toString();
    loja.com.br.entity.PropostaStatus var45 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    var45.setStatus("");
    java.lang.Integer var48 = var45.getId();
    java.util.Collection var49 = var45.getPropostaCollection();
    java.lang.String var50 = var45.getStatus();
    boolean var51 = var42.equals((java.lang.Object)var45);
    boolean var52 = var32.equals((java.lang.Object)var42);
    java.util.Collection var53 = var32.getPropostaCollection();
    java.util.Collection var54 = var32.getPropostaCollection();
    java.lang.Integer var55 = var32.getId();
    java.lang.String var56 = var32.getStatus();
    boolean var57 = var20.equals((java.lang.Object)var32);
    boolean var58 = var12.equals((java.lang.Object)var57);
    java.lang.String var59 = var12.toString();
    var12.setId((java.lang.Integer)10);
    boolean var62 = var1.equals((java.lang.Object)var12);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var6 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var7 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var7.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var8 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var8.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var13);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var16 + "' != '" + 100+ "'", var16.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var17 + "' != '" + 100+ "'", var17.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var18);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var23);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var24);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var25 + "' != '" + 100+ "'", var25.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var26);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var27 + "' != '" + 100+ "'", var27.equals(100));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var30 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=-1 ]"+ "'", var30.equals("loja.com.br.entity.PropostaStatus[ id=-1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var33 + "' != '" + (-1)+ "'", var33.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var34);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var37 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var37.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var38);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var43 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=1 ]"+ "'", var43.equals("loja.com.br.entity.PropostaStatus[ id=1 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var48 + "' != '" + (-1)+ "'", var48.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var49);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var50 + "' != '" + ""+ "'", var50.equals(""));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var51 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var52 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var53);
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var54);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var55 + "' != '" + 0+ "'", var55.equals(0));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var56);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var57 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var58 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var59 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=100 ]"+ "'", var59.equals("loja.com.br.entity.PropostaStatus[ id=100 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var62 == false);

  }

  public void test243() throws Throwable {

    if (debug) { System.out.println(); System.out.print("PropostaStatus11.test243"); }


    loja.com.br.entity.PropostaStatus var1 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var2 = var1.getId();
    java.util.Collection var3 = var1.getPropostaCollection();
    loja.com.br.entity.PropostaStatus var5 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)(-1));
    java.lang.Integer var6 = var5.getId();
    java.util.Collection var7 = var5.getPropostaCollection();
    var5.setId((java.lang.Integer)10);
    java.lang.String var10 = var5.toString();
    java.lang.String var11 = var5.getStatus();
    boolean var12 = var1.equals((java.lang.Object)var5);
    var1.setId((java.lang.Integer)0);
    loja.com.br.entity.PropostaStatus var17 = new loja.com.br.entity.PropostaStatus((java.lang.Integer)0, "loja.com.br.entity.PropostaStatus[ id=null ]");
    boolean var18 = var1.equals((java.lang.Object)"loja.com.br.entity.PropostaStatus[ id=null ]");
    java.lang.Integer var19 = var1.getId();
    var1.setStatus("loja.com.br.entity.PropostaStatus[ id=null ]");
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var2 + "' != '" + (-1)+ "'", var2.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var3);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var6 + "' != '" + (-1)+ "'", var6.equals((-1)));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var7);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var10 + "' != '" + "loja.com.br.entity.PropostaStatus[ id=10 ]"+ "'", var10.equals("loja.com.br.entity.PropostaStatus[ id=10 ]"));
    
    // Regression assertion (captures the current behavior of the code)
    assertNull(var11);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var12 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue(var18 == false);
    
    // Regression assertion (captures the current behavior of the code)
    assertTrue("'" + var19 + "' != '" + 0+ "'", var19.equals(0));

  }

}
