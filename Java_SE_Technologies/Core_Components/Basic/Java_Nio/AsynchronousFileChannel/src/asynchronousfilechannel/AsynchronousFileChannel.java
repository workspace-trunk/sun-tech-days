/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asynchronousfilechannel;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author Jakob Jenkov
 * @see
 * <a href="http://tutorials.jenkov.com/java-nio/asynchronousfilechannel.html">Java
 * NIO AsynchronousFileChannel</a>
 */
public class AsynchronousFileChannel {

    static private Path path;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    static private void CreatingAnAsynchronousFileChannel() {
        path = Paths.get("/tmp/test.xml");

        AsynchronousFileChannel fileChannel
                = AsynchronousFileChannel.open(path, StandardOpenOption.READ);
    }
}
