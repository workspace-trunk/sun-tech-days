package atomicreferencearray;

import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * @author Jakob Jenkov
 * @see
 * <a href="http://tutorials.jenkov.com/java-util-concurrent/atomicreferencearray.html">AtomicReferenceArray</a>
 *
 */
public class Main {

    static AtomicReferenceArray array;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    static private void CreatingAnAtomicReferenceArray() {
        array = new AtomicReferenceArray(10);

        Object[] source = new Object[10];
        source[5] = "Some string";
        array = new AtomicReferenceArray(source);
    }

    static private void get() {
        Object element = array.get(5);
        element = (String) array.get(5);
    }

    static private void set() {
        array.set(5, "another object");
    }

    static void compareAndSet() {
        String string1 = "string1";
        String string2 = "string2";

        String[] source = new String[10];
        source[5] = string1;

        AtomicReferenceArray<String> array
                = new AtomicReferenceArray<String>(source);

        array.compareAndSet(5, string1, string2);
    }
}
