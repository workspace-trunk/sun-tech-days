package string;

import static org.junit.Assert.*;
import org.junit.Test;

public class PalindromeCheckTest {

    @Test
    public void testIsPalindrome() {
        assertTrue(PalindromeCheck.isPalindrome("ada"));
    }

    @Test
    public void testIsNotPalindrome() {
        assertFalse(PalindromeCheck.isPalindrome("word"));
    }

}
