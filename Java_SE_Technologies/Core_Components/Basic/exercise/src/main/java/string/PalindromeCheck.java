package string;



/**
 *
 * @author tux
 */
public class PalindromeCheck {

    public static boolean isPalindrome(String word) {
        StringBuilder sbf = new StringBuilder(word);
        boolean isPalindrome = word.equals(sbf.reverse().toString());

        System.out.println("Word original: [" + word + "]");
        System.out.println("Word  reverse: [" + sbf + "]");
        System.out.println("Word is " + (isPalindrome ? "" : "not ") + "palindrome!");

        return isPalindrome;
    }

}
