/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package string;

/**
 *
 * @author tux
 */
public class TextWithoutDuplicateCharacter {

    String resultado = "sim";

    public static void main(String[] args) {
        System.err.println(new TextWithoutDuplicateCharacter().textoUnico("Almeida"));
    }

    private String textoUnico(String textoString) {
        boolean naoEstaNulo = !(textoString == null);

        if (naoEstaNulo && textoString.isEmpty()) {
            System.err.println("empty string");
        }

        for (int length = 0; length < textoString.length(); length++) {
            char letra = textoString.charAt(length);

            for (int i = 0; i < textoString.length(); i++) {
                if (i != length && letra == textoString.charAt(i)) {
                    resultado = "não";
                    break;
                }
            }

            if (resultado.equalsIgnoreCase("não")) {
                break;
            }
        }

        return resultado;
    }
}
