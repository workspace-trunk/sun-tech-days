/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * Java Program to show how to use Locks in multi-threading
 * e.g. ReentrantLock, ReentrantReadWriteLock etc.
 *
 * @author Javin Paul
 */
public class LockDemo {

    public static void main(String args[]) {
// Let's create a counter and shared it between three threads
// Since Counter needs a lock to protect its getCount() method
// we are giving it a ReentrantLock.
        final Counter myCounter = new Counter(new ReentrantLock());
// Task to be executed by each thread
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.printf("Count at thread %s is %d %n",
                        Thread.currentThread().getName(),
                        myCounter.getCount());
            }
        };
// Creating three threads
        Thread t0 = new Thread(r, "T0");
        Thread t1 = new Thread(r, "T1");
        Thread t2 = new Thread(r, "T2");
        Thread t3 = new Thread(r, "T3");
        Thread t4 = new Thread(r, "T4");
        Thread t5 = new Thread(r, "T5");
        Thread t6 = new Thread(r, "T6");
        Thread t7 = new Thread(r, "T7");
        Thread t8 = new Thread(r, "T8");
        Thread t9 = new Thread(r, "T9");
//starting all threads
        t0.start();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
    }
}

class Counter {

    private Lock lock; // Lock to protect our counter
    private int count; // Integer to hold count

    public Counter(Lock myLock) {
        this.lock = myLock;
    }

    public final int getCount() {
        lock.lock();
        try {
            count++;
            return count;
        } finally {
            lock.unlock();
        }
    }
}
