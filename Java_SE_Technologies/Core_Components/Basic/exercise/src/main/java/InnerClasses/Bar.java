package InnerClasses;

class Boo {

    Boo(String s) {
    }

    Boo() {
    }
}

public class Bar extends Boo {

    Bar() {
    }

    Bar(String s) {
        super(s);
    }

    void zoo() {
        Boo f = new Bar() {
        };
    }
}
