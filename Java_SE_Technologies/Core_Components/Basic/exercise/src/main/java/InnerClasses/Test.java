package InnerClasses;

class Foo {

    class Bar {
    }
}

public class Test {

    public static void main(String[] args) {
        Foo f = new Foo();
        Foo.Bar b = f.new Bar();
    }
}
