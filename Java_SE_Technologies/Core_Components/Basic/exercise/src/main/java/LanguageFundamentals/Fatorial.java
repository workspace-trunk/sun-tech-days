/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LanguageFundamentals;

/**
 *
 * @author tux
 */
public class Fatorial {

    long f(long n) {
        System.out.println("n=" + n);
        if (n == 1 || n == 0) {
            return n;
        }
        return f(n - 1) + f(n - 2);

    }

    public static void main(String args[]) {
        System.out.println(new Fatorial().f(6));
    }

}
