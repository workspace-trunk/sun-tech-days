/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LanguageFundamentals;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author tux
 */
public class ArrayWhitoutDuplicate {

    public static void main(String[] args) {
        new ArrayWhitoutDuplicate().test();
    }

    private void test() {
        Integer[] arr1 = {1, 2, 4, 9};
        Integer[] arr2 = {1, 5, 6, 7};
        Integer[] arr3 = {9, 2, 7, 8};
        testIntegerArrayCollection(arr1, arr2, arr3);
        testIntegerArray(arr1, arr2, arr3);
    }

    private void testIntegerArrayCollection(Integer[]... matrizes) {
        Set<Integer> n = new HashSet<>();
        List<Integer> duplicates = new Stack<>();
        for (Integer[] numeros : matrizes) {
            for (Integer numero : numeros) {
                if (!n.add(numero)) {
                    duplicates.add(numero);
                }
            }
        }
        Collections.sort(duplicates);
        System.out.println("numeros duplicados: " + duplicates);
    }

    private void testIntegerArray(Integer[]... matrizes) {
        Integer[] todos = new Integer[12];
        Integer[] duplicados = new Integer[12];
        int index = 0;

        // Coleta todos os numeros
        for (Integer[] matriz : matrizes) {
            for (Integer numero : matriz) {
                todos[index++] = numero;
            }
        }

        // ordena o array com todos os numeros
        Arrays.sort(todos);

        // pesquisa por numeros duplicados
        boolean primeiro = true;
        for (int i = 0; i < todos.length; i++) {
            Integer numero = todos[i];
            index = Arrays.binarySearch(todos, numero);
            if (index != i) {
                if (primeiro) {
                    primeiro = false;
                    System.out.print("numeros duplicados: " + numero);
                } else {
                    System.out.print(", " + numero);
                }
            }

        }

    }

}
