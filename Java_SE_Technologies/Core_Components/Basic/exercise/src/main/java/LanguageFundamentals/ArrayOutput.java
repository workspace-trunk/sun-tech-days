/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LanguageFundamentals;

/**
 *
 * @author tux
 */
public class ArrayOutput {

    static void test() {
        int[] arrayv = {2, 3, 5, 7, 11, 13, 17};
        int[] arrayi = {0, 3, 2, 1};
        int[] arrayj = {1, 6, 4, 5};
        int s = 0;
        for (int i = 0; i < arrayi.length; i++) {
            s += arrayv[arrayi[i]] + arrayv[arrayj[i]];
        }
        System.out.println("s = " + s);
    }

    public static void main(String[] args) {
        ArrayOutput.test();
    }
}
