package com.voxxed.optional;

import java.util.Optional;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tux
 * @see
 * <a href="https://www.voxxed.com/blog/2015/05/why-even-use-java-8-optional/">Why
 * Even Use Java 8 Optional?</a>
 */
public class Construction {

    public static void main(String[] args) {
        // an empty 'Optional';
        // before Java 8 you would simply use a null reference here
        Optional<String> empty = Optional.empty();

        // an 'Optional' where you know that it will not contain null;
        // (if the parameter for 'of' is null, a 'NullPointerException' is thrown)
        Optional<String> full = Optional.of("Some String");
        String someOtherString = "abc";

        // an 'Optional' where you don't know whether it will contain null or not
        Optional<String> halfFull = Optional.ofNullable(someOtherString);
    }
}
