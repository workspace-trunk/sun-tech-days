package org.reflection.demo;

import java.io.Serializable;
import java.util.Date;

public class Employee implements Serializable,
                                 Cloneable {

    private static final long serialVersionUID = 1L;
    private String name;
    private Date joinDate;

    public Employee() {
        super();
    }

    public Employee(String name, Date joinDate) {
        super();
        this.name = name;
        this.joinDate = joinDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }
}
