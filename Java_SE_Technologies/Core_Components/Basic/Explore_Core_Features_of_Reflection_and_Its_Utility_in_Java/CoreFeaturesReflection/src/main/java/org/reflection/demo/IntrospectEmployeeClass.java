package org.reflection.demo;

import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;

public class IntrospectEmployeeClass {

    public static void main(String[] args) {
        Class<Employee> e = Employee.class;
        showClassInfo(e);
    }

    public static void showClassInfo(Class<?> c) {
        String str = "";
        int mod = 0;
        if (c.isPrimitive()) {

        } else if (c.isInterface()) {
            mod = c.getModifiers() & Modifier.interfaceModifiers();
            if (c.isAnnotation()) {
                str += " @interface";
            } else {
                str += " interface";
            }
        } else if (c.isEnum()) {
            mod = c.getModifiers() & Modifier.classModifiers();
            str += " enum";
        } else {
            mod = c.getModifiers() & Modifier.classModifiers();
        }

        String finalStr = Modifier.toString(mod) +
        "
         "+str+c.getSimpleName()+getGenericTypeParams(c);
      if (c.getSuperclass() != null) {
            finalStr += " extends " + c.getSuperclass().getSimpleName();
        }

        if (getClassInterfaces(c) != null) {
            finalStr += " implements " + getClassInterfaces(c);
        }

        System.out.println(finalStr);
    }

    public static String getClassInterfaces(Class<?> c) {
        Class<?>[] interfaces = c.getInterfaces();
        String list = null;
        if (interfaces.length > 0) {
            String[] namesOfInterfaces
                    = new String[interfaces.length];
            for (int i = 0; i < interfaces.length; i++) {
                namesOfInterfaces[i]
                        = interfaces[i].getSimpleName();
            }
            list = String.join(", ", namesOfInterfaces);
        }
        return list;
    }

    public static String getGenericTypeParams(Class<?> c) {
        String str = "";
        TypeVariable<?>[] typeParameters
                = c.getTypeParameters();
        if (typeParameters.length > 0) {
            String[] paramNames
                    = new String[typeParameters.length];
            for (int i = 0; i < typeParameters.length; i++) {
                paramNames[i] = typeParameters[i].getTypeName();
            }
            str += "<";
            String parmsList = String.join(",", paramNames);
            str += parmsList;
            str += ">";
        }
        return str;
    }

}
