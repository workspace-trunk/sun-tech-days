package org.reflection.demo;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class IntrospectFieldMethodConstructor {

    public static void main(String[] args) {
        Class<Employee> c = Employee.class;
        showFieldInfo(c);
        showMethodInfo(c);
        showConstructorInfo(c);
    }

    public static void showFieldInfo(Class<?> c) {
        Field[] fields = c.getDeclaredFields();
        for (Field f : fields) {
            System.out.println(Modifier.toString(f.getModifiers()
                    & Modifier.fieldModifiers())
                    + " " + f.getType().getSimpleName()
                    + " " + f.getName());
        }
    }

    public static void showMethodInfo(Class<?> c) {
        Method[] methods = c.getDeclaredMethods();

        for (Method m : methods) {
            Parameter[] params = m.getParameters();
            String str = "";
            for (int i = 0; i < params.length; i++) {
                str += Modifier.toString(params[i].getModifiers()
                        & Modifier.parameterModifiers())
                        + " ";
                str += params[i].getType().getSimpleName() + " ";
                str += params[i].getName();
            }
            System.out.println(Modifier.toString(m.getModifiers()
                    & Modifier.methodModifiers())
                    + " "
                    + m.getReturnType().getSimpleName()
                    + " "
                    + m.getName() + " " + str);
        }
    }

    public staticvoid showConstructorInfo(Class<?> c) {
        Constructor<?>[] constructors
                = c.getDeclaredConstructors();

        for (Constructor<?> con : constructors) {
            Parameter[] params = con.getParameters();
            String str = "";

            for (int i = 0; i < params.length; i++) {
                str += Modifier.toString(params[i].getModifiers()
                        & Modifier.parameterModifiers())
                        + " ";
                str += params[i].getType().getSimpleName() + " ";
                str += params[i].getName();

            }
            System.out.println(Modifier.toString(con.getModifiers()
                    & Modifier.methodModifiers())
                    + " " + con.getName() + " " + str);
        }
    }

}
