package generics.boundedtypeparameter;

public class MaximumTest {

    class MeuTipo implements Comparable<MeuTipo> {

        int id;

        public MeuTipo(int _id) {
            this.id = _id;
        }

        @Override
        public int compareTo(MeuTipo o) {
            return this.id > o.id ? 1 : 0;
        }

        @Override
        public String toString() {
            return String.format("MeuTipo.id: %d", this.id);
        }

    }

    // determines the largest of three Comparable objects
    public static <InstanciasQualquerTipoQueExtendamComparable extends Comparable<InstanciasQualquerTipoQueExtendamComparable>> InstanciasQualquerTipoQueExtendamComparable maximum(InstanciasQualquerTipoQueExtendamComparable x, InstanciasQualquerTipoQueExtendamComparable y, InstanciasQualquerTipoQueExtendamComparable z) {
        InstanciasQualquerTipoQueExtendamComparable max = x; // assume x is initially the largest
        if (y.compareTo(max) > 0) {
            max = y; // y is the largest so far
        }
        if (z.compareTo(max) > 0) {
            max = z; // z is the largest now
        }
        return max; // returns the largest object
    }

    public static void main(String args[]) {
        new MaximumTest().test();
    }

    private void test() {
        MeuTipo meuTipo2 = new MeuTipo(2);
        MeuTipo meuTipo42 = new MeuTipo(42);
        MeuTipo meuTipo32 = new MeuTipo(32);
        System.out.printf("Max of %d, %d and %d is %d\n\n", 3, 4, 5, maximum(3, 4, 5));
        System.out.printf("Maxm of %.1f,%.1f and %.1f is %.1f\n\n", 6.6, 8.8, 7.7, maximum(6.6, 8.8, 7.7));
        System.out.printf("Max of %s, %s and %s is %s\n", "pear", "apple", "orange", maximum("pear", "apple", "orange"));
        System.out.printf("Max of %s, %s and %s is %s\n", "2", "42", "32", maximum(Byte.parseByte("2"), Byte.parseByte("42"), Byte.parseByte("32")));
        System.out.printf("Max of %s, %s and %s is %s\n", meuTipo2, meuTipo42, meuTipo32, maximum(meuTipo2, meuTipo42, meuTipo32));
    }

}
