package multithreading;

public class MyThreadTest {

    private void startTest() {
        System.out.println("Iniciando o teste da thread.");
        MyThread myThread = new MyThread();
        myThread.start();
    }

    public static void main(String[] args) {
        new MyThreadTest().startTest();
    }

    class MyThread implements Runnable {

        @Override
        public void run() {
            System.err.println("Thread running");
        }

        private void start() {
            new Thread(this, "MyThread").start();
        }

    }

}
