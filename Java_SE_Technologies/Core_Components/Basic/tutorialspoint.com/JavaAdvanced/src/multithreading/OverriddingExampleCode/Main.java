package multithreading.OverriddingExampleCode;

/*
 * @author By X Wang
 *
 * The difference between "new Thread(new A(),"name_thread2").run(); "
 * and "new Thread(new A(),"name_thread3").start();"
 * is that the start() method creates a new Thread and executes the run method in that thread.
 * If you invoke the run() method directly, the code in run method will execute in the current thread.
 * This explains why the code prints two lines with the same thread name.
 */
class A implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}

class B implements Runnable {

    @Override
    public void run() {
        new A().run();
        new Thread(new A(), "name_thread2").run();
        new Thread(new A(), "name_thread3").start();
    }
}

public class Main {

    public static void main(String[] args) {
        new Thread(new B(), "name_thread1").start();
    }
}
