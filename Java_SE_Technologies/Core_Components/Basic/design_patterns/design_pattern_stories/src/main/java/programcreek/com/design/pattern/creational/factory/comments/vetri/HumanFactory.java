/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programcreek.com.design.pattern.creational.factory.comments.vetri;

import java.util.Hashtable;

interface Human {

    public void Talk();

    public void Walk();

    public Human createHuman();
}

class Boy implements Human {

    static {
        HumanFactory.getInstance().registerHuman("boy", new Boy());
    }

    @Override
    public Human createHuman() {
        return new Boy();
    }

    @Override
    public void Talk() {
        System.out.println("Boy is talking...");
    }

    @Override
    public void Walk() {
        System.out.println("Boy is walking...");
    }
}

class Girl implements Human {

    static {
        HumanFactory.getInstance().registerHuman("girl", new Girl());
    }

    @Override
    public Human createHuman() {
        return new Girl();
    }

    @Override
    public void Talk() {
        System.out.println("Girl is talking...");
    }

    @Override
    public void Walk() {
        System.out.println("Girl is walking...");
    }
}

public class HumanFactory {

    private static HumanFactory humanfactory;
    private final Hashtable<String, Human> humanGroup;

    public static HumanFactory getInstance() {
        synchronized (HumanFactory.class) {
            if (humanfactory == null) {
                humanfactory = new HumanFactory();
            }
        }
        return humanfactory;
    }

    public HumanFactory() {
        this.humanGroup = new Hashtable<>();
    }

    public void registerHuman(String name, Human human) {
        humanGroup.put(name, human);

    }

    public Human createHuman(String m) {

        Human human = (Human) humanGroup.get(m);
        return human.createHuman();

        /*Human p = null;
         if(m == "girl"){
         p = new Boy();
         }else if(m == "girl"){
         p = new Girl();
         }

         return p;*/
    }

    public static void main(String[] args) {
        HumanFactory factory;
        factory = HumanFactory.getInstance();
        factory.registerHuman("boy", new Boy());
        factory.registerHuman("girl", new Girl());

        Human boy;
        boy = factory.createHuman("boy");
        boy.Talk();
        boy.Walk();

        Human girl;
        girl = factory.createHuman("girl");
        girl.Talk();
        girl.Walk();
    }
}
