/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programcreek.com.design.pattern.creational.strategy;

public interface Strategy {

    //defind a method for police to process speeding case.

    public void processSpeeding(int speed);
}
