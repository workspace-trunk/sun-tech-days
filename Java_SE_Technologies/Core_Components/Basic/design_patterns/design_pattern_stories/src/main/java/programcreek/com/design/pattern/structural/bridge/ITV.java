/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programcreek.com.design.pattern.structural.bridge;

public interface ITV {

    public void on();

    public void off();

    public void switchChannel(int channel);
}
