/*
 Another Implementation of Singleton Pattern

 As private constructor doesn't protect from instantiation via reflection, Joshua Bloch (Effective Java)
 proposes a better implementation of Singleton. If you are not familiar with Enum, here is a good example
 from Oracle.
 */
package programcreek.com.design.pattern.creational.singleton.another;

public enum AmericaPresident {

    INSTANCE;

    public static void doSomething() {
        //do something
    }
}
