/*
 * Os objetos criados pela fábrica pertecem a uma categoria de objetos.
 Exemplos:
 Humanos = menino, menina, adolescente, jovem, adulto, idoso;
 Formas geometricas = circulo, quadrado, triangulo;
 Produto = Notebook, Tablet, Celular.
 */
package livreprogramacao.com.br.design.pattern.creational.factory;

interface Produto {

    void getDetalhes();
}

class Notebook implements Produto {

    @Override
    public void getDetalhes() {
        System.err.println("A bateria do MacBook Air de 11 polegadas dura até 9 horas com uma carga");
    }

}

class Tablet implements Produto {

    @Override
    public void getDetalhes() {
        System.err.println("Fino e leve: com menos de 9,5 mm de espessura, você pode apoiar esse leve tablet no seu colo. Relaxe com seus jogos favoritos baseados em Android ou ponha-o na mochila e saia por aí.");
    }

}

class Celular implements Produto {

    @Override
    public void getDetalhes() {
        System.err.println("O Motorola Moto X 2014 é um smartphone Android com características inovadoras em todos os pontos de vista que permite ser útil para qualquer forma de entretenimento");
    }

}

class ProdutoFactory {

    public Produto createProduto(String _produto) {
        Produto produto = null;
        switch (_produto) {
            case "a":
                produto = new Notebook();
                break;
            case "b":
                produto = new Tablet();
                break;
            case "c":
                produto = new Celular();
                break;
        }
        return produto;
    }
}

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
public class FactoryPatternProdutoDemo {

    public static void main(String[] args) {
        ProdutoFactory factory = new ProdutoFactory();
        factory.createProduto("a").getDetalhes();
        factory.createProduto("b").getDetalhes();
        factory.createProduto("c").getDetalhes();
    }
}
