package avajava.com.design.pattern.creational.abstractfactory;

interface Animal {

    String makeSound();
}

class Cat implements Animal {

    @Override
    public String makeSound() {
        return "Meow";
    }

}

class Dog implements Animal {

    @Override
    public String makeSound() {
        return "Woof";
    }

}

class Snake implements Animal {

    @Override
    public String makeSound() {
        return "Hiss";
    }

}

class Tyrannosaurus implements Animal {

    @Override
    public String makeSound() {
        return "Roar";
    }

}

abstract class SpeciesFactory {

    public abstract Animal getAnimal(String type);
}

class MammalFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {
        if ("dog".equals(type)) {
            return new Dog();
        } else {
            return new Cat();
        }
    }

}

class ReptileFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {
        if ("snake".equals(type)) {
            return new Snake();
        } else {
            return new Tyrannosaurus();
        }
    }

}

class AbstractFactory {

    public SpeciesFactory getSpeciesFactory(String type) {
        if ("mammal".equals(type)) {
            return new MammalFactory();
        } else {
            return new ReptileFactory();
        }
    }

}

/**
 *
 * @author: Deron Eriksson
 * Description: This Java tutorial describes the abstract factory pattern, a popular creational design pattern.
 */
public class AbstractFactorySpeciesDemo {

    public static void main(String[] args) {
        AbstractFactory abstractFactory = new AbstractFactory();

        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("reptile");
        Animal a1 = speciesFactory1.getAnimal("tyrannosaurus");
        System.out.println("tyrannosaurus sound: " + a1.makeSound());
        Animal a2 = speciesFactory1.getAnimal("snake");
        System.out.println("snake sound: " + a2.makeSound());

        SpeciesFactory speciesFactory2 = abstractFactory.getSpeciesFactory("mammal");
        Animal a3 = speciesFactory2.getAnimal("dog");
        System.out.println("dog sound: " + a3.makeSound());
        Animal a4 = speciesFactory2.getAnimal("cat");
        System.out.println("cat sound: " + a4.makeSound());

    }
}
