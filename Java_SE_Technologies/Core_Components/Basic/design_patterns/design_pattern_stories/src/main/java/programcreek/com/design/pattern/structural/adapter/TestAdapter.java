package programcreek.com.design.pattern.structural.adapter;

/*
 Java Design Pattern: Adapter

 Adapter pattern is frequently used in modern Java frameworks.
 It comes into place when you want to use an existing class, and its interface does not match the one you
 need, or you want to create a reusable class that cooperates with unrelated classes with incompatible
 interfaces.
 */
class Apple {

    public void getAColor(String str) {
        System.out.println("Apple color is: " + str);
    }
}

class Orange {

    public void getOColor(String str) {
        System.out.println("Orange color is: " + str);
    }
}

class AppleAdapter extends Apple {

    private Orange orange;

    public AppleAdapter(Orange orange) {
        this.orange = orange;
    }

    public void getAColor(String str) {
        orange.getOColor(str);
    }
}

public class TestAdapter {

    public static void main(String[] args) {
        Apple apple1 = new Apple();
        Apple apple2 = new Apple();
        apple1.getAColor("green");

        Orange orange = new Orange();

        AppleAdapter aa = new AppleAdapter(orange);
        aa.getAColor("red");
    }

}
