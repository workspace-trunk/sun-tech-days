/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programcreek.com.design.pattern.creational.state;

interface State {

    public void saySomething(StateContext sc);
}

class Rich implements State {

    @Override
    public void saySomething(StateContext sc) {
        System.out.println("I'm rick currently, and play a lot.");
        sc.changeState(new Poor());
    }
}

class Poor implements State {

    @Override
    public void saySomething(StateContext sc) {
        System.out.println("I'm poor currently, and spend much time working.");
        sc.changeState(new Rich());
    }
}

public class StateContext {

    private State currentState;

    public StateContext() {
        currentState = new Poor();
    }

    public void changeState(State newState) {
        this.currentState = newState;
    }

    public void saySomething() {
        this.currentState.saySomething(this);
    }
}
