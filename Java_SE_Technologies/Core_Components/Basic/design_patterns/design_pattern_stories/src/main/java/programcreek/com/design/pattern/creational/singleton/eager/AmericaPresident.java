/*
 The Story for Singleton

 Here is a simple use case. A country can have only one president.
 So whenever a president is needed, the only president should be returned instead of creating a new one.
 The getPresident() method will make sure there is always only one president created.
 */
package programcreek.com.design.pattern.creational.singleton.eager;

public class AmericaPresident {

    private static final AmericaPresident thePresident = new AmericaPresident();

    private AmericaPresident() {
    }

    public static AmericaPresident getPresident() {
        return thePresident;
    }
}
