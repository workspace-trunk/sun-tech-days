/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programcreek.com.design.pattern.structural.decorator;

public abstract class GirlDecorator extends Girl {

    public abstract String getDescription();
}
