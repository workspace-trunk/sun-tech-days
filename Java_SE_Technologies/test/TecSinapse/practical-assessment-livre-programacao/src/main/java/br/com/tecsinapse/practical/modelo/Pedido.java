package br.com.tecsinapse.practical.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Pedido {
	private String cnpjCliente = "";
	private String usuarioSolicitante = "";
	private List<ItemPedido> itens = new ArrayList<>();
	private BigDecimal valorTotal = new BigDecimal("0").setScale(0,
			RoundingMode.HALF_EVEN);

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.cnpjCliente);
		hash = 53 * hash + Objects.hashCode(this.usuarioSolicitante);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Pedido other = (Pedido) obj;
		if (!Objects.equals(this.cnpjCliente, other.cnpjCliente)) {
			return false;
		}
		if (!Objects.equals(this.usuarioSolicitante, other.usuarioSolicitante)) {
			return false;
		}
		return true;
	}

	public void adicionaItem(ItemPedido itemPedido) {
		this.getItens().add(itemPedido);
	}

	@Override
	public String toString() {
		return "Pedido{" + "cnpjCliente=" + cnpjCliente + ", usuarioSolicitante="
				+ usuarioSolicitante + ", itens=" + itens + ", valorTotal="
				+ valorTotal + '}';
	}

	public String getCnpjCliente() {
		return cnpjCliente;
	}

	public void setCnpjCliente(String cnpjCliente) {
		this.cnpjCliente = cnpjCliente;
	}

	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	public List<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

}
