package br.com.tecsinapse.practical.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class ItemPedido {
	private String cnpjCliente = "";
	private String usuarioSolicitante = "";
	private String codigoItem = "";
	private int quantidade;
	private BigDecimal valorTotal = new BigDecimal("0").setScale(0,
			RoundingMode.HALF_EVEN);
	;
	private BigDecimal valorUnitario = new BigDecimal("0").setScale(0,
			RoundingMode.HALF_EVEN);

	;

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 59 * hash + Objects.hashCode(this.cnpjCliente);
		hash = 59 * hash + Objects.hashCode(this.usuarioSolicitante);
		hash = 59 * hash + Objects.hashCode(this.codigoItem);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ItemPedido other = (ItemPedido) obj;
		if (!Objects.equals(this.cnpjCliente, other.cnpjCliente)) {
			return false;
		}
		if (!Objects.equals(this.usuarioSolicitante, other.usuarioSolicitante)) {
			return false;
		}
		if (!Objects.equals(this.codigoItem, other.codigoItem)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ItemPedido{" + "cnpjCliente=" + cnpjCliente
				+ ", usuarioSolicitante=" + usuarioSolicitante + ", codigoItem="
				+ codigoItem + ", quantidade=" + quantidade + ", valorTotal="
				+ valorTotal + ", valorUnitario=" + valorUnitario + '}';
	}

	public String getCnpjCliente() {
		return cnpjCliente;
	}

	public void setCnpjCliente(String cnpjCliente) {
		this.cnpjCliente = cnpjCliente;
	}

	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	public String getCodigoItem() {
		return codigoItem;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;

		valorUnitario = valorTotal.divide(BigDecimal.valueOf(quantidade).setScale(
				0, RoundingMode.HALF_EVEN),
				RoundingMode.HALF_EVEN);

		setValorUnitario(valorUnitario);
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario; //.setScale(0, RoundingMode.HALF_EVEN);
	}

}
