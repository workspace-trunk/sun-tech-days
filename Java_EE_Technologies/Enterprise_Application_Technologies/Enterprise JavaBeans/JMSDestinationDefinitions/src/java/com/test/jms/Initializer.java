package com.test.jms;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.Queue;

/**
 *
 * @author Adam Bien
 * @see
 * <a href="http://www.adam-bien.com/roller/abien/entry/auto_creating_jms_destinations_with">AUTO-CREATING
 * JMS DESTINATIONS WITH JMS 2.0 AND JAVA EE 7</a>
 */
@JMSDestinationDefinitions(
        value = {
            @JMSDestinationDefinition(
                    name = "java:/queue/duke-queue",
                    interfaceName = "javax.jms.Queue",
                    destinationName = "duke"
            ),
            @JMSDestinationDefinition(
                    name = "java:/topic/duke-topic",
                    interfaceName = "javax.jms.Topic",
                    destinationName = "duke-topic"
            )
        }
)
@Startup
@Singleton
public class Initializer {

    @Resource(lookup = "java:/queue/duke-queue")
    private Queue queue;

    @Produces
    public Queue expose() {
        return this.queue;
    }
}
