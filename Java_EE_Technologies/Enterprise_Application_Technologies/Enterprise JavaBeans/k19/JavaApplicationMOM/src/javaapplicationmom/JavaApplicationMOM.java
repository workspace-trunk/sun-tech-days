/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplicationmom;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author tux
 */
public class JavaApplicationMOM {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JMSException {

        InitialContext ic;
        ConnectionFactory connectionFactory;
        QueueConnectionFactory queueConnFac;
        Queue queue;
        TopicConnectionFactory topicConnFac;
        Topic topic;

        try {
            ic = new InitialContext();
            connectionFactory = (ConnectionFactory) ic.lookup("jms/K19Factory");

            //factoryTopic = (TopicConnectionFactory) ic.lookup("jms/noticias");
            topic = (Topic) ic.lookup("jms/noticias");

            queueConnFac = (QueueConnectionFactory) ic.lookup("jms/pedidos");
            QueueConnection connection = queueConnFac.createQueueConnection();
            QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            queue = (Queue) ic.lookup("jms/pedidos");
            QueueSender sender = session.createSender(queue);

        } catch (NamingException ex) {
            Logger.getLogger(JavaApplicationMOM.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
