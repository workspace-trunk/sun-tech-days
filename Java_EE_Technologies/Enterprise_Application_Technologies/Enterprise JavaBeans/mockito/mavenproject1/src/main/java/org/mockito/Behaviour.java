/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mockito;

//Let's import Mockito statically so that the code looks clearer
import java.util.List;
import static org.mockito.Mockito.*;

/**
 *
 * @author tux
 */
public class Behaviour {

    /**
     *
     * @param args
     */
    public static void main(String args[]) {

        //mock creation
        List mockedList = mock(List.class);

        //using mock object
        mockedList.add("one");
        mockedList.clear();

        //verification
        verify(mockedList).add("one");
        verify(mockedList).clear();

    }
}
