/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.wasdev.jaxrs;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
@Path("properties")
public class SystemProperties {

    private static final long serialVersionUID = 906892345861L;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, String> getSystemProperties() {
        Map<String, String> result = new HashMap<>();
        Properties props = System.getProperties();
        for (Map.Entry<Object, Object> entry : props.entrySet()) {
            result.put((String) entry.getKey(), (String) entry.getValue());
        }
        return result;
    }
}
