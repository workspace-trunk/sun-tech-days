<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Insert title here</title>
        <script type="text/javascript" src="jquery-1.10.2.js"></script>
    </head>
    <body>

        <script>
            $(function(e) {


                $("#clearBttnId").click(function(e) {
                    $("#content").val("");
                });

                $("#submitBttnId").click(function(e) {
                    var _d = $("#datatypeId").val();
                    var urlReal = $("#pathId").val()

                    appendLine("====================request header=====================");
                    appendLine(new Date());
                    appendLine(_d);
                    appendLine(getMineType(_d));

                    appendLine("====================response =====================");
                    $.ajax({
                        type: $("#typeId").val(),
                        url: urlReal,
                        data: $("#msgId").val(),
                        dataType: _d,
                        mimeType: getMineType(_d),
                        contentType: getMineType(_d)
                    }).done(function(msg, sts, jqXHR) {
                        appendLine("=======sucess=======:");
                        appendLine("sts         :" + sts);
                        appendLine("msg         :" + msg);
                        appendLine("response txt:" + jqXHR.responseText);
                        appendLine("response sts:" + jqXHR.status);


                    }).fail(function(jqXHR, textStatus) {
                        appendLine("=======fail:=======");
                        appendLine("sts         :" + textStatus);
                        appendLine(jqXHR.responseText);
                        appendLine("response sts:" + jqXHR.status);

                    }).always(function(data, textStatus, jqXHR) {
                        appendLine("=======header:=======");
                        appendLine("sts         :" + textStatus);
                        appendLine(jqXHR.getAllResponseHeaders());
                        appendLine("response sts:" + jqXHR.status);
                    });

                });


                var appendLine = function(data) {
                    var ov = $("#content").val();
                    $("#content").val(ov + data + "\r\n");
                }

                var getMineType = function(d) {
                    if (d) {
                        if (d == "text") {
                            return "text/plain"
                        }
                        if (d == "xml") {
                            return "text/xml"
                        }
                        if (d == "html") {
                            return "text/html"
                        }
                        if (d == "json") {
                            return "application/json"
                        }
                    }
                }

            });
        </script>
        <form action="">
            <p>
                <img alt="" src="img/oracle_logo.jpg">
            </p>
            <textarea id="content" style="width:100%; max-height: 500px; height: 300px; overflow: scroll; ">
            </textarea>
            <ul>
                <li>Path:<textarea id="pathId" style="width:90%;">http://10.12.116.60:9091/JDK7WEB/restful/blogs</textarea>
                <li>Data:<textarea id="msgId" style="width:90%;"><Blog id="2"><name>millet test</name></Blog>
                    </textarea>
                <li>Type:
                    <select id="typeId" >
                        <option VALUE="GET" selected="selected">GET</option>
                        <option VALUE="POST">POST</option>
                        <option VALUE="HEAD">HEAD</option>
                        <option VALUE="PUT">PUT</option>
                        <option VALUE="OPTIONS">OPTIONS</option>
                    </select>
                <li>Data Type:
                    <select id="datatypeId" >
                        <option VALUE="text" selected="selected">text</option>
                        <option VALUE="xml">xml</option>
                        <option VALUE="json">json</option>
                        <option VALUE="script">script</option>
                    </select>
                <li><input id="submitBttnId" type="button" value="Submit" ></li>
                <li><input id="clearBttnId" type="button" value="Clear" ></li>
            </ul>

        </form>
    </body>
</html>