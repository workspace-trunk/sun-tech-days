/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.csdn.blog.rsapplication.service;

import java.util.Date;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//import net.csdn.blog.rsapplication.entity.Blog;
import ejb.BlogEJB;

@Path("/blogs")
public class BlogRestful {

    @EJB
    BlogEJB blogEJB;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Blog[] getBlogs() {
        return blogEJB.getBlogs();
    }

    @GET
    @Path("blog/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Blog getBlog(@PathParam("id") Long id) {
        System.out.println(id);
        return blogEJB.getBlog(id);
    }

    @POST
    @Path("blog")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Blog mergeBlog(Blog blog) {
        blog.setStatus("updated at " + new Date());
        return blogEJB.mergeBlog(blog);
    }

    @DELETE
    @Path("blog/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Blog deleteBlog(@PathParam("id") Long id) {
        Blog deleteBlog = blogEJB.deleteBlog(id);
        deleteBlog.setStatus("deleteBlog at " + new Date());
        return deleteBlog;
    }
}
