package com.kenfogel.rest;

import com.kenfogel.beans.FishJpaController;
import com.kenfogel.entities.Fish;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Ken
 * @see
 * <a href="http://netbeans.dzone.com/nb-class-rest-client-and-server?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+zones%2Fnetbeans+%28NetBeans+Zone%29">NetBeans
 * in the Classroom: Using the NetBeans Rest Service Wizard</a>
 */
@Path("fishies")
public class FishRest {

    @Inject
    private FishJpaController fishjpa;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of FishRest
     */
    public FishRest() {
    }

    /**
     * Retrieves all the fish
     *
     * @return an instance of com.kenfogel.entities.Fish
     */
    @GET
    @Produces("application/xml")
    public List getAllFishXml() {
        return fishjpa.findFishEntities();
    }

    /**
     * Retrieves a specific fish
     *
     * @param id
     * @return an instance of com.kenfogel.entities.Fish
     */
    @GET
    @Produces("application/xml")
    @Path("{id}/")
    public Fish getOneFishXml(@PathParam("id") int id) {
        return fishjpa.findFish(id);
    }

    /**
     * PUT method for updating or creating an instance of FishRest
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes("application/xml")
    public void putXml(Fish content) throws Exception {
        fishjpa.create(content);
    }
}
