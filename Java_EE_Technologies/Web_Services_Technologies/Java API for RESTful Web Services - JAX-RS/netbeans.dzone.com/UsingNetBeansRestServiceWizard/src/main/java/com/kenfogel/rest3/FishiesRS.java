package com.kenfogel.rest3;

import com.kenfogel.entities.Fish;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Ken
 * @see
 * <a href="http://netbeans.dzone.com/nb-class-rest-client-and-server?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+zones%2Fnetbeans+%28NetBeans+Zone%29">NetBeans
 * in the Classroom: Using the NetBeans Rest Service Wizard</a>
 */
@Path("fishies")
public class FishiesRS {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of FishiesRS
     */
    public FishiesRS() {
    }

    /**
     * Retrieves representation of an instance of com.kenfogel.rest3.FishiesRS
     *
     * @return an instance of com.kenfogel.entities.Fish
     */
    @GET
    @Produces("application/xml")
    public Fish getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of FishiesRS
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public void putXml(Fish content) {
    }
}
