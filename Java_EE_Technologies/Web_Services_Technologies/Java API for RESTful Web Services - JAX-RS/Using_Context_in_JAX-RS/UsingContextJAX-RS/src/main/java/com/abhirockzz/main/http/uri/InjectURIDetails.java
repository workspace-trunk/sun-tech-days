package com.abhirockzz.main.http.uri;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Abhishek
 * @see
 * <a href="https://abhirockzz.wordpress.com/2015/05/03/using-context-in-jax-rs-part-1/">Using
 * Context in JAX-RS</a>
 */
@Path("testinject")
public class InjectURIDetails {

    //localhost:8080/<root-context>/testinject/uriinfo
    @GET
    @Path("uriinfo")
    public void test(@Context UriInfo uriDetails) {
        System.out.println("ALL query parameters -- " + uriDetails.getQueryParameters().toString());
        System.out.println("'id' query parameter -- " + uriDetails.getQueryParameters().get("id"));
        System.out.println("Complete URI -- " + uriDetails.getRequestUri());
    }
}
