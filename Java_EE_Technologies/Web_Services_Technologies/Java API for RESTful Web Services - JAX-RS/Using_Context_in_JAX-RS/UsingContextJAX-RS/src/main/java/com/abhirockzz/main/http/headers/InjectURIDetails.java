package com.abhirockzz.main.http.headers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

/**
 *
 * @author Abhishek
 * @see
 * <a href="https://abhirockzz.wordpress.com/2015/05/03/using-context-in-jax-rs-part-1/">Using
 * Context in JAX-RS</a>
 */
@Path("testinject")
public class InjectURIDetails {

    //localhost:8080/<root-context>/testinject/httpheaders
    @GET
    @Path("httpheaders")
    public void test(@Context HttpHeaders headers) {
        System.out.println("ALL headers -- " + headers.getRequestHeaders().toString());
        System.out.println("'Accept' header -- " + headers.getHeaderString("Accept"));
        System.out.println("'TestCookie' value -- " + headers.getCookies().get("TestCookie").getValue());
    }
}
