package com.abhirockzz.main.security.context;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Abhishek
 * @see
 * <a href="https://abhirockzz.wordpress.com/2015/05/03/using-context-in-jax-rs-part-1/">Using
 * Context in JAX-RS</a>
 */
@Path("testinject")
public class InjectSecurityContext {

    //localhost:8080/<root-context>/testinject/securitycontext
    @GET
    @Path("securitycontext")
    public void test(@Context SecurityContext secContext) {
        System.out.println("Caller -- " + secContext.getUserPrincipal().getName());
        System.out.println("Authentication Scheme -- " + secContext.getAuthenticationScheme());
        System.out.println("Over HTTPS ? -- " + secContext.isSecure());
        System.out.println("Belongs to 'admin' role? -- " + secContext.isUserInRole("admin"));
    }
}
