# Java API for WebSocket #

The Java API for WebSocket JSR will define a standard API for creating WebSocket applications.

### Informação sobre WebSocket ###

*   [netbeans.org - Using the WebSocket API in a Web Application](https://netbeans.org/kb/docs/javaee/maven-websocketapi.html)
*   [java.net - Project Tyrus](https://tyrus.java.net/)

### Aplicações para aprendizado sobre WebSocket ###

*   Summary of set up
*   Configuration
*   Dependencies
*   Database configuration
*   How to run tests
*   Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact