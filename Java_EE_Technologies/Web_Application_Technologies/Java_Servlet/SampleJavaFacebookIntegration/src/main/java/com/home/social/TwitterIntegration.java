package com.home.social;

import Twitter4j.Query;
import Twitter4j.QueryResult;
import Twitter4j.Status;
import Twitter4j.Twitter;
import Twitter4j.TwitterFactory;
import Twitter4j.conf.ConfigurationBuilder;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

public class TwitterIntegration {

    public static void main(String[] args) throws Exception {
        // Create configuration builder and set key, token etc
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey("xxx");
        cb.setOAuthConsumerSecret("xxxx");
        cb.setOAuthAccessToken("xxxxx");
        cb.setOAuthAccessTokenSecret("xxxx");

        // Create Twitter instance
        Twitter Twitter = new TwitterFactory(cb.build()).getInstance();

        // Create file writer and buffer writer
        FileWriter fstream = new FileWriter("Twitterstream.txt", true);
        BufferedWriter out = new BufferedWriter(fstream);

        // Create Query object and set search string
        Query query = new Query("");
        query.setQuery("#USAirways");

        // Get query result
        QueryResult qr = Twitter.search(query);

        // Get tweets and write in the file
        while (qr.hasNext()) {
            qr.nextQuery();
            List<Status> tweets = qr.getTweets();

            for (Status t : tweets) {
                System.out.println(t.getId() + " - " + t.getCreatedAt() + ": " + t.getText());

                out.write("\n" + t.getId() + ",");
                out.write("\t" + t.getText() + ",");
                out.write("\t" + t.getUser() + ",");
            }
        }
        try {
            Thread.sleep(1000 * 60 * 15);
        } catch (Exception e) {
        }
    }
}
