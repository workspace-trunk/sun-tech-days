package com.home.social;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import facebook4j.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FacebookClientTest {

    Logger log = LoggerFactory.getLogger(FacebookClientTest.class);

    public static void main(String[] args) {
        new FacebookClientTest().login();
    }

    private void login() {

        try {
            FacebookClient facebookClient = new DefaultFacebookClient();

            // You'll notice that logging statements from this call are bridged from java.util.logging to Log4j
            User user = facebookClient.fetchObject("btaylor", User.class);

            // ...and of course this goes straight to Log4j
            log.debug("User:", user);
        } finally {
            // Make sure to tear down when you're done using the bridge
            //SLF4JBridgeHandler.uninstall();
        }
    }
}
