<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<sql:query var="rs" dataSource="jdbc/__loja">
    select id, nome from APP.vendedor
</sql:query>

<html>
    <head>
        <title>DB Test</title>
    </head>
    <body>

        <h2>Lista de(os) vendedor(es)</h2>

        <c:forEach var="row" items="${rs.rows}">
            <p>Id ${row.id} - Nome ${row.nome}<p/>
        </c:forEach>

    </body>
</html>