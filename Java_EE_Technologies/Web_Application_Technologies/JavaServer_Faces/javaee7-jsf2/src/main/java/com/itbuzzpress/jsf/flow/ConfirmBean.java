package com.itbuzzpress.jsf.flow;

 


import java.io.Serializable;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.flow.FlowScoped;
import javax.inject.Named;


@Named
@FlowScoped("confirm")
public class ConfirmBean implements Serializable {
    @PostConstruct
    public void createKey() {
    	secretKey = UUID.randomUUID().toString();
    	System.out.println("Secret Key is "+secretKey);
    }
	public ConfirmBean() {
        
    }
    private String key;
    private String secretKey;
    
    public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
        return this.getClass().getSimpleName();
    }
    
    public String getHomeAction() {
        return "/index";
    }
    public String checkKey() {
        
        if (key.equals(secretKey)) {
            return "confirm2";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "You have to read and accept the license!", "You have to read and accept the license!"));
            return null;
        }
    }
}
