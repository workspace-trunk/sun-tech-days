/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.terra.cisper.flashe.boleto.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import br.com.terra.cisper.flashe.boleto.entity.Boleto;

/**
 *
 * @author tux
 */
@Stateless
public class BoletoFacade extends AbstractFacade<Boleto> {
    @PersistenceContext(unitName = "maven.project_mavenproject3_war_0.1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BoletoFacade() {
        super(Boleto.class);
    }
    
}
