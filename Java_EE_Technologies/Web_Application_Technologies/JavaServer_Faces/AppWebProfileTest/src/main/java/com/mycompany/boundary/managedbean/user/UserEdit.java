/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boundary.managedbean.user;

import com.mycompany.controller.dao.UserDAO;
import com.mycompany.entity.User;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author tux
 */
@Named
@SessionScoped
public class UserEdit implements Serializable {

    private static final long serialVersionUID = -4425622971689352545L;

    @Inject
    private UserDAO userDAO;

    private User user = new User("firstName", "lastName");

    public String saveUser() {
        System.out.println("saveUser(): " + user);
        userDAO.save(this.user);
        addFlashMessage("User " + this.user.getId() + " saved");
        return "users.xhtml";
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    private void addFlashMessage(String msg) {
        Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        flash.put("message", msg);
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);

    }
}
