/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller.dao;

import com.mycompany.entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 */
@Stateless
public class UserDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void save(User user) {
        System.out.println("save: " + user);
        entityManager.persist(user);
    }

    public void update(User user) {
        entityManager.merge(user);
    }

    public List<User> getAll() {
        return entityManager.createNamedQuery("User.getAll", User.class)
                .getResultList();
    }
}
