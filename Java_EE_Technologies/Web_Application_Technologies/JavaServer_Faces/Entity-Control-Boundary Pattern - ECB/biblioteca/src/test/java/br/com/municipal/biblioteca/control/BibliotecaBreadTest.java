/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.municipal.biblioteca.control;

import br.com.municipal.biblioteca.entity.Livro;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tux
 */
public class BibliotecaBreadTest {

    Livro livro;
    Long id = Long.MIN_VALUE;

    public BibliotecaBreadTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        livro = new Livro(id, "teste");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of browse method, of class BibliotecaBread.
     */
    @Test
    public void testBrowse() {
        System.out.println("browse");
        BibliotecaBread instance = new BibliotecaBread();
        Collection<Livro> expResult = null;
        Collection<Livro> result = instance.browse();
        assertEquals("browse", expResult, result);
    }

    /**
     * Test of read method, of class BibliotecaBread.
     */
    @Test
    public void testRead() {
        System.out.println("read");
        BibliotecaBread instance = new BibliotecaBread();
        instance.add(livro);
        Livro result = instance.read(id);
        Livro expResult = livro;
        assertEquals("read", expResult, result);
    }

    /**
     * Test of add method, of class BibliotecaBread.
     */
    @Test
    @Before
    public void testAdd() {
        System.out.println("add");
        BibliotecaBread instance = new BibliotecaBread();
        Livro result = instance.add(livro);
        Livro expResult = instance.read(id);
        assertEquals("add", expResult, result);
    }

    /**
     * Test of edit method, of class BibliotecaBread.
     */
    @Test
    public void testEdit() {
        System.out.println("edit");
        BibliotecaBread instance = new BibliotecaBread();
        instance.add(livro);
        Livro expResult = instance.read(id);
        expResult.setTitulo("edit");
        Livro result = instance.edit(livro);
        assertEquals("edit", expResult, result);
    }

    /**
     * Test of delete method, of class BibliotecaBread.
     */
    @Test
    @After
    public void testDelete() {
        System.out.println("delete");
        BibliotecaBread instance = new BibliotecaBread();
        instance.delete(livro);
    }

}
