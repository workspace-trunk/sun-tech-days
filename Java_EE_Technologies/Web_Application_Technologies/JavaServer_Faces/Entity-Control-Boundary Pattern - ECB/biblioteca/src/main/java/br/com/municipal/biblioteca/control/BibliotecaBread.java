/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.municipal.biblioteca.control;

import br.com.municipal.biblioteca.entity.Livro;
import java.util.Collection;
import javax.ejb.Asynchronous;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 */
public class BibliotecaBread {

    EntityManager em;

    public BibliotecaBread() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("test");
        em = emf.createEntityManager();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc --> @generated @ordered
     */
    public Collection<Livro> browse() {
        // TODO implement me
        return null;
    }

    public Livro read(Long id) {
        return em.find(Livro.class, id);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc --> @generated @ordered
     */
    @Asynchronous
    public Livro add(Livro livro) {
        em.persist(livro);
        return livro;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc --> @generated @ordered
     */
    public Livro edit(Livro livro) {
        em.merge(livro);
        return livro;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc --> @generated @ordered
     */
    public void delete(Livro livro) {
        em.remove(livro);
    }

}
