/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.municipal.biblioteca.presentation;

import br.com.municipal.biblioteca.control.BibliotecaBread;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author tux
 */
@Named
@RequestScoped
public class IncluirLivro {

    @Inject
    BibliotecaBread bread;

    String titulo = "";

    /**
     * Creates a new instance of IncluirLivro
     */
    public IncluirLivro() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void incluir() {
        System.out.println("Incluir o livro com o titulo: " + this.titulo);
    }
}
