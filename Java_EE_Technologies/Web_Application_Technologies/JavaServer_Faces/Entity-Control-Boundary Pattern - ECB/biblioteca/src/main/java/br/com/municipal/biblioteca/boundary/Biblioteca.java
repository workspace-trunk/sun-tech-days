package br.com.municipal.biblioteca.boundary;

import br.com.municipal.biblioteca.control.BibliotecaBread;
import javax.ejb.Stateless;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tux
 */
@Stateless
public class Biblioteca {

    BibliotecaBread bread;

    private String titulo;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void incluir() {
        System.out.println("Incluir o livro com o titulo: " + this.titulo);
    }

}
