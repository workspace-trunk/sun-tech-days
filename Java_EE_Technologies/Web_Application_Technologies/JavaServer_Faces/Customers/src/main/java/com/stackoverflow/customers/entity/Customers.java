/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackoverflow.customers.entity;

import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.JoinColumn;

/**
 *
 * @author http://stackoverflow.com/users/3553476/peterl355
 * @see
 * http://stackoverflow.com/questions/23180454/how-to-manipulate-java-list-with-jsf-requestscope
 */
public class Customers {

    @Column(name = "FIRSTNAME")
    private String firstname;
    @Column(name = "LASTNAME")
    private String lastname;

    @ElementCollection
    @CollectionTable(name = "CUSTOMERS_Phone",
            joinColumns = @JoinColumn(name = "CUSTOMERS_ID"))
    @AttributeOverride(name = "teleNumbers",
            column = @Column(name = "PHONE_NUMBER", length = 30))
    private List<Phone> phone;

}
