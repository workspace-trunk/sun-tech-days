/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackoverflow.customers.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author http://stackoverflow.com/users/3553476/peterl355
 * @see
 * http://stackoverflow.com/questions/23180454/how-to-manipulate-java-list-with-jsf-requestscope
 */
public class Phone {

    @Embeddable
    public class Phone {

        public enum PhoneType {

            Home, Mobile, Work
        }
        @Enumerated(EnumType.STRING)
        @Column(name = "PHONE_TYPE", length = 10)
        private PhoneType phoneType;
        @Column(name = "PHONE_NUM", length = 30)
        private String teleNumbers;
//setter, getter
    }
}
