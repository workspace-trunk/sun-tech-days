/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackoverflow.customers.service;

import com.stackoverflow.customers.entity.Customers;
import com.stackoverflow.customers.entity.Phone;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author http://stackoverflow.com/users/3553476/peterl355
 * @see
 * http://stackoverflow.com/questions/23180454/how-to-manipulate-java-list-with-jsf-requestscope
 */
@Named(value = "editCustomersBeanService")
@RequestScoped
public class EditCustomersBeanService {

    /**
     * Creates a new instance of EditCustomersBeanService
     */
    @PostConstruct
    public void init() {
        ctx = FacesContext.getCurrentInstance();
        customers = new Customers();
        phones = new ArrayList<>();
        customers.setPhone(phones);

    }

    public EditCustomersBeanService() {

    }

    @Inject
    private BusinessSessionCustomers businessSSCustomers;
    private int customerId;
    private List<Phone> phones;
    private Customers customers;

    //setter, getter ...
    //update to DB
    public String updatedCustomers() {
        System.out.println("customer Name: " + customers.getFirstname());
        System.out.println("customer LastName: " + customers.getLastname());
        System.out.print("List of Phones in updatedCustomers: ");
        for (Phone ph : customers.getPhone()) {
            System.out.print(ph.getPhoneType() + ", " + ph.getTeleNumbers());
        }

        businessSSCustomers.mergeToDB(customers);
        return "customers";
    }

}
