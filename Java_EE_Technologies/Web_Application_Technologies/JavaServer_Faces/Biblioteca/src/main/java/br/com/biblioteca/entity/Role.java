package br.com.biblioteca.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.ManyToMany;

public class Role extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -9039331212127089919L;

    private String roledesc;
    private String rolename;

    protected Role() {
    }

    public Role(Integer roleid, String rolename) {
        this.rolename = rolename;
    }

    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    public String getRoledesc() {
        return this.roledesc;
    }

    public void setRoledesc(String roledesc) {
        this.roledesc = roledesc;
    }

    public String getRolename() {
        return this.rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
