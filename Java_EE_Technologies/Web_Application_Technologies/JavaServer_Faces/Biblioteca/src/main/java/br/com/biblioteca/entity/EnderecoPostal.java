package br.com.biblioteca.entity;

import javax.persistence.Entity;

/**
 * Endereço postal.
 *
 * O endereço postal é a localização completa de um destinatário de correio. É
 * escrito no envelope pelo expedidor do correio e permite ao serviço postal
 * encaminhar e distribuir o dito correio. Os endereços postais obedecem a
 * normas de apresentação próprias para cada país.
 *
 * @author tux
 */
@Entity
public class EnderecoPostal extends BaseEntity {

    private static final long serialVersionUID = -553896061066575640L;

    /*
     * Nome do logradouro (rua ou avenida ou praça ou etc.).
     */
    private String logradouro;
    private String descricao;
    /*
     * Número da residência.
     */
    private String numero;

    /*
     * Complemento (geralmente um número de apartamento)
     */
    private String complemento;

    /*
     * Nome do bairro (opcional)
     */
    private String bairro;


    /*
     * Nome do estado
     */
    private String estado;

    /*
     * Nome da cidade
     */
    private String cidade;

    /*
     * Código de Endereçamento Postal - CEP
     */
    private String cep;

    /**
     * Construtor padrão da entidade.
     */
    protected EnderecoPostal() {
    }

    /**
     *
     * @param logradouro Tipo do logradouro (rua ou avenida ou praça ou etc.).
     * @param descricao Nome do logradouro (rua ou avenida ou praça ou etc.).
     * @param numero Número da residência.
     * @param complemento Complemento (geralmente um número de apartamento)
     * @param bairro Nome do bairro (opcional)
     * @param estado Nome do estado
     * @param cidade Nome da cidade
     * @param cep Código de Endereçamento Postal - CEP
     */
    public EnderecoPostal(String logradouro, String descricao, String numero, String complemento, String bairro, String estado, String cidade, String cep) {
        this.logradouro = logradouro;
        this.descricao = descricao;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.estado = estado;
        this.cidade = cidade;
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

}
