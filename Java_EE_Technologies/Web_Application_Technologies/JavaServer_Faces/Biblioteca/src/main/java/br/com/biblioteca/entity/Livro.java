package br.com.biblioteca.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Livro (do latim liber, um termo relacionado com a cortiça da árvore) é um
 * volume transportável, composto por páginas encadernadas, contendo texto
 * manuscrito ou impresso e/ou imagens e que forma uma publicação unitária (ou
 * foi concebido como tal) ou a parte principal de um trabalho literário,
 * científico ou outro, formando um volume.
 *
 * @author tux
 */
@Entity
public class Livro extends BaseEntity {

    private static final long serialVersionUID = 4281859641934090905L;


    /*
     * International Standard Book Number ou Número Padrão Internacional de Livro.
     */
    @Column(length = 20)
    private String isbn;

    /*
     * Pessoa que escreveu o livro.
     */
    @OneToOne(cascade = {CascadeType.ALL})
    private Autor autor;

    /*
     * Nome que o autor deu a sua obra.
     */
    @Column(length = 50)
    private String titulo;

    /*
     * Palavra para categorizar o livro.
     */
    @Column(length = 20)
    private String etiqueta;

    /**
     *
     */
    protected Livro() {
    }

    /**
     *
     * @param isbn International Standard Book Number ou Número Padrão
     * Internacional de Livro.
     * @param autor Pessoa que escreveu o livro.
     * @param titulo Nome que o autor deu a sua obra.
     * @param etiqueta Palavra para categorizar o livro.
     */
    public Livro(String isbn, Autor autor, String titulo, String etiqueta) {
        this.isbn = isbn;
        this.autor = autor;
        this.titulo = titulo;
        this.etiqueta = etiqueta;
    }

    /**
     * Get the value of etiqueta
     *
     * @return the value of etiqueta
     */
    public String getEtiqueta() {
        return etiqueta;
    }

    /**
     * Set the value of etiqueta
     *
     * @param etiqueta new value of etiqueta
     */
    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    /**
     * Get the value of titulo
     *
     * @return the value of titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Set the value of titulo
     *
     * @param titulo new value of titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Get the value of autor
     *
     * @return the value of autor
     */
    public Autor getAutor() {
        return autor;
    }

    /**
     * Set the value of autor
     *
     * @param autor new value of autor
     */
    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    /**
     * Get the value of isbn
     *
     * @return the value of isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * Set the value of isbn
     *
     * @param isbn new value of isbn
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

}
