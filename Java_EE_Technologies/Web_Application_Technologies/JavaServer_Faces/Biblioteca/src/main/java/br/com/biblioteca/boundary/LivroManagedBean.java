package br.com.biblioteca.boundary;

import br.com.biblioteca.controller.LivroFacade;
import br.com.biblioteca.controller.dao.LivroDAO;
import br.com.biblioteca.entity.Autor;
import br.com.biblioteca.entity.EnderecoPostal;
import br.com.biblioteca.entity.Livro;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author tux
 */
@Named
@SessionScoped
public class LivroManagedBean implements Serializable {

    private static final long serialVersionUID = 500511154073977470L;

    @EJB
    private LivroFacade ejbFacade;
    private Livro selected;
    private List<Livro> items = null;

    @Inject
    private LivroDAO dao;

    public LivroManagedBean() {
        String isbn = "000.000.000.000.000";
        Autor autor = new Autor("123.456.789.12", "Seu nome.", new EnderecoPostal("Rua", "dos bobos", "0", "Centro", "MPB", "São Paulo", "SP", "12345-678"));
        String titulo = "Você S/A.";
        String etiqueta = "livro";
        selected = new Livro(isbn, autor, titulo, etiqueta);
    }

    public String saveLivro() {
        String msg = "saveLivro(): " + selected;
        System.out.println(msg);
        addFlashMessage(msg);
        dao.save(selected);
        return "pages/Livro.xhtml";
    }

    private void addFlashMessage(String msg) {
        Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
        flash.put("message", msg);
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);

    }
//    public Livro getSelected() {
//        return selected;
//    }
//
//    public void setSelected(Livro selected) {
//        this.selected = selected;
//    }
//
//    protected void setEmbeddableKeys() {
//    }
//
//    protected void initializeEmbeddableKey() {
//    }
//
//    private LivroFacade getFacade() {
//        return ejbFacade;
//    }
//
//    public Livro prepareCreate() {
//        selected = new Livro();
//        initializeEmbeddableKey();
//        return selected;
//    }
//
//    public void create() {
//        System.out.println("Executando o metodo create().");
//        System.out.println("Registro de livro que será inserido: " + this.selected);
//        persist(PersistAction.CREATE, ResourceBundle.getBundle("/DiarioLivroBundle").getString("LivroCreated"));
//        if (!JsfUtil.isValidationFailed()) {
//            items = null;    // Invalidate list of items to trigger re-query.
//        }
//    }
//
//    public void update() {
//        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/DiarioLivroBundle").getString("LivroUpdated"));
//    }
//
//    public void destroy() {
//        persist(PersistAction.DELETE, ResourceBundle.getBundle("/DiarioLivroBundle").getString("LivroDeleted"));
//        if (!JsfUtil.isValidationFailed()) {
//            selected = null; // Remove selection
//            items = null;    // Invalidate list of items to trigger re-query.
//        }
//    }
//
//    public List<Livro> getItems() {
//        if (items == null) {
//            items = getFacade().findAll();
//        }
//        return items;
//    }
//
//    private void persist(PersistAction persistAction, String successMessage) {
//        if (selected == null) {
//            return;
//        }
//
//        setEmbeddableKeys();
//        try {
//            if (persistAction != PersistAction.DELETE) {
//                getFacade().edit(selected);
//            } else {
//                getFacade().remove(selected);
//            }
//            JsfUtil.addSuccessMessage(successMessage);
//        } catch (EJBException ex) {
//            String msg = "";
//            Throwable cause = ex.getCause();
//            if (cause != null) {
//                msg = cause.getLocalizedMessage();
//            }
//            if (msg.length() > 0) {
//                JsfUtil.addErrorMessage(msg);
//            } else {
//                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/DiarioLivroBundle").getString("PersistenceErrorOccured"));
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//            JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/DiarioLivroBundle").getString("PersistenceErrorOccured"));
//        }
//    }
//
//    public Livro getLivro(java.lang.Long id) {
//        return getFacade().find(id);
//    }
//
//    public List<Livro> getItemsAvailableSelectMany() {
//        return getFacade().findAll();
//    }
//
//    public List<Livro> getItemsAvailableSelectOne() {
//        return getFacade().findAll();
//    }
//
//    @FacesConverter(forClass = Livro.class)
//    public static class LivroManagedBeanConverter implements Converter {
//
//        @Override
//        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
//            if (value == null || value.length() == 0) {
//                return null;
//            }
//            LivroManagedBean controller = (LivroManagedBean) facesContext.getApplication().getELResolver().
//                    getValue(facesContext.getELContext(), null, "livroController");
//            return controller.getLivro(getKey(value));
//        }
//
//        java.lang.Long getKey(String value) {
//            java.lang.Long key;
//            key = Long.valueOf(value);
//            return key;
//        }
//
//        String getStringKey(java.lang.Long value) {
//            StringBuilder sb = new StringBuilder();
//            sb.append(value);
//            return sb.toString();
//        }
//
//        @Override
//        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
//            if (object == null) {
//                return null;
//            }
//            if (object instanceof Livro) {
//                Livro o = (Livro) object;
//                return getStringKey(o.getId());
//            } else {
//                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Livro.class.getName()});
//                return null;
//            }
//        }
//
//    }
//
//    public void onCellEdit(CellEditEvent event) {
//        Object oldValue = event.getOldValue();
//        Object newValue = event.getNewValue();
//        if (!oldValue.equals(newValue)) {
//            // Save to the database
//            DataTable table = (DataTable) event.getSource();
//            Livro livro = (Livro) table.getRowData();
//            ejbFacade.edit(livro);
//            FacesContext.getCurrentInstance().addMessage(null,
//                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Atualizado com sucesso!",
//                            String.format("Valor anterior: %s atualizado para: %s", oldValue, newValue)));
//        }
//    }

    public Livro getSelected() {
        return selected;
    }

    public void setSelected(Livro selected) {
        this.selected = selected;
    }

    public List<Livro> getItems() {
        return items;
    }

    public void setItems(List<Livro> items) {
        this.items = items;
    }

}
