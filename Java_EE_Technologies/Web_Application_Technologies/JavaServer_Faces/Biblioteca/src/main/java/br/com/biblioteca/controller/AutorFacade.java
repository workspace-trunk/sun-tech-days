package br.com.biblioteca.controller;

import br.com.biblioteca.entity.Autor;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 */
@Stateless
public class AutorFacade extends AbstractFacade<Autor> {

    @PersistenceContext(unitName = "derby_pu")
    private EntityManager em;

    public AutorFacade() {
        super(Autor.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
