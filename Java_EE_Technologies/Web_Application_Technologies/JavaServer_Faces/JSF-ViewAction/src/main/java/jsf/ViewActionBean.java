/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.util.logging.Logger;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;  
import javax.enterprise.inject.spi.InjectionPoint;  
import javax.inject.Inject;
  
@Model
public class ViewActionBean {

    @Inject
    Logger log;

    private String flag = "page1";

    public ViewActionBean() {

    }

    public String init() {
        log.info("call init");
        switch (flag) {
            case "page1":
                return "page1";
            default:
                return "page2";
        }
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Produces
    public Logger produceLogger(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }
}
