plugins {
    build-scan
    kotlin("jvm") version "1.2.10" 
}

buildScan {
    setLicenseAgreementUrl("https://gradle.com/terms-of-service") 
    setLicenseAgree("yes")

    publishAlways() 
}

repositories {
    jcenter() 
}

dependencies {
    implementation(kotlin("stdlib", "1.2.10")) 
}