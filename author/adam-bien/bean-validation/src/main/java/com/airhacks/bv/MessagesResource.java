/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.bv;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
@Path("messages")
public class MessagesResource {

    private static final long serialVersionUID = 98243958439556851L;

    @GET
    public Message message() {
        return new Message("duke42");
    }

    @POST
    public void save(@Valid Message message) {
        System.out.println("Got message " + message);
    }
}
