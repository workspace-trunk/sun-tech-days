package com.airhacks.bv;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {

    private static final long serialVersionUID = 56562384584231L;

}
