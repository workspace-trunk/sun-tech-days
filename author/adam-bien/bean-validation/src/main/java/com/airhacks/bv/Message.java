/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.bv;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class Message {

    @Size(min = 2, max = 5)
    private String content;

    private static final long serialVersionUID = 878545458561L;

    public Message() {
    }

    public Message(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Message{" + "content=" + content + '}';
    }

}
