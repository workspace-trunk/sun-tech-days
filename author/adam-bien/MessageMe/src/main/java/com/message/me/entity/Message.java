/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.message.me.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 *
 * @author tux
 */
@Entity
public class Message {

    @Id
    @GeneratedValue
    private Long id;
    @Size(min = 2, max = 140)
    private String content;

    public Message(String content) {
        this.content = content;
    }

    public Message() { /*required by JPA */

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }
}
