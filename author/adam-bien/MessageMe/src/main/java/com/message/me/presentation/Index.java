/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.message.me.presentation;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.message.me.boundary.Messaging;
import com.message.me.entity.Message;

/**
 *
 * @author tux
 */
@Named
@RequestScoped
public class Index {

    @Inject
    Messaging ms;
    private Message message = new Message();

    public Message getMessage() {
        return message;
    }

    public void save() {
        ms.store(message);
    }
}
