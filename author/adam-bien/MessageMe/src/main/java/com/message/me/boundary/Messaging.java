/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.message.me.boundary;

import com.message.me.control.MessageStore;
import com.message.me.entity.Message;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author tux
 */
@Stateless
public class Messaging {

    @Inject
    MessageStore messageStore;

    @Asynchronous
    public void store(Message message) {
        messageStore.store(message);
    }
}
