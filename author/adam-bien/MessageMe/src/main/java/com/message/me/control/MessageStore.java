/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.message.me.control;

import com.message.me.entity.Message;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 */
public class MessageStore {

    @PersistenceContext
    EntityManager em;

    public void store(Message message) {
        em.persist(message);
    }
}
