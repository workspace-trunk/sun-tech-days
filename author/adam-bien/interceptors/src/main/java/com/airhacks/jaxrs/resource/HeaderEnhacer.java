   /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.jaxrs.resource;

import java.io.IOException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;

/**
 *
 * @author tux
 */
@Provider
public class HeaderEnhacer implements WriterInterceptor {

    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        System.out.println("context Media Type: " + context.getMediaType());
        System.out.println("context Type: " + context.getGenericType());
        System.out.println("context Entity: " + context.getEntity());
        context.getHeaders().add("x-airhacks", "helloword");
        context.proceed();
    }

}
