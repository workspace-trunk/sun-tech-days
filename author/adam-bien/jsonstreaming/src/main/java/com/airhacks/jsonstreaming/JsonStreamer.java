/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.jsonstreaming;

import java.io.OutputStream;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
@Stateless
public class JsonStreamer {

    private static final long serialVersionUID = 5643885457921L;

    public void stream(OutputStream stream) {
        JsonGenerator generator = Json.createGenerator(stream);
        generator.writeStartObject();
        for (int i = 0; i < 10; i++) {
            generator.write("key" + i, "value" + i);
        }
        generator.writeEnd();
        generator.flush();
        generator.close();

    }

}
