/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.jsonstreaming;

import java.io.IOException;
import java.io.OutputStream;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
@Stateless
@Path("streams")
public class StreamsResource {

    private static final long serialVersionUID = 8978955899461L;

    @Inject
    JsonStreamer js;

    @GET
    public Response streams() {
        StreamingOutput so = new StreamingOutput() {

            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                js.stream(output);
            }
        };
        return Response.ok(so).build();
    }
;
}
