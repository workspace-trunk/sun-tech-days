/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.jsonstreaming;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Fábio Almeida <livre.programacao@.gmail.com>
 */
@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {

    private static final long serialVersionUID = 45685695681L;

}
