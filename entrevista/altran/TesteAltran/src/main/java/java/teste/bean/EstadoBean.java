package java.teste.bean;

/**
 *
 * @author tux
 */
public class EstadoBean {

    private Integer codigo;

    private String descricao;

    public EstadoBean(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    /**
     * Get the value of descricao
     *
     * @return the value of descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Set the value of descricao
     *
     * @param descricao new value of descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Get the value of codigo
     *
     * @return the value of codigo
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * Set the value of codigo
     *
     * @param codigo new value of codigo
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

}
