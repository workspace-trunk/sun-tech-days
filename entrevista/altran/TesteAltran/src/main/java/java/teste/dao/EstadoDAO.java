package java.teste.dao;

import java.teste.bean.EstadoBean;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author tux
 */
public class EstadoDAO {

    public List<EstadoBean> listar() {
        return Arrays.asList(new EstadoBean(1, "São Paulo"), new EstadoBean(2, "Rio de Janeiro"), new EstadoBean(3, "Minas Gerais"));
    }
}
