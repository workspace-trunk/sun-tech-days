package java.teste.action;

import java.io.IOException;
import java.teste.dao.EstadoDAO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tux
 */
public class EstadoAction extends HttpServlet {

    private static final long serialVersionUID = -6677348451596486092L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp); //To change body of generated methods, choose Tools | Templates.
        resp.sendRedirect("05.jsp");
    }

    /**
     * método responsável pela obtenção dos beans "EstadoBean"
     *
     * @param request
     */
    public void exibir(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("estados", new EstadoDAO().listar());
    }
}
