package br.com.altran.teste.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author tux
 */
public class DateTest09 {

    public DateTest09() {

    }

    public static void main(String[] arg) {
        System.out.println("Data formatada: " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

    }

}
