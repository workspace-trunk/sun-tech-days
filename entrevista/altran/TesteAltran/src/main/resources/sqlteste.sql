drop table autor;
drop table trabalho;
drop table pessoa;

CREATE TABLE pessoa (cpf INT NOT NULL CONSTRAINT pessoa_pk PRIMARY KEY, nome varchar(100), tipo_pessoa varchar(100) CONSTRAINT tipo_pessoa_ck CHECK ( tipo_pessoa IN ('Docente', 'Discente', 'Outros') ));
INSERT INTO pessoa VALUES (0,'teste','Não vai subir ninguêm!!!!');
INSERT INTO pessoa VALUES (1,'Prof. José','Docente'),(2,'teste','Discente'),(3,'teste','Outros'),(4,'teste','Docente'),(5,'teste','Discente'),(6,'teste','Outros');
SELECT * FROM pessoa;

CREATE TABLE trabalho (codigo INT NOT NULL CONSTRAINT trabalho_pk PRIMARY KEY, titulo VARCHAR(100), ano CHAR(4));
INSERT INTO trabalho VALUES (0,'trabalho teste 2000','2000');
INSERT INTO trabalho VALUES (1,'trabalho teste 2001','2001');
INSERT INTO trabalho VALUES (2,'trabalho teste 2002','2002');
INSERT INTO trabalho VALUES (3,'trabalho teste 2003','2003');
INSERT INTO trabalho VALUES (4,'trabalho teste 2004','2004');
INSERT INTO trabalho VALUES (5,'trabalho teste 2005','2005');
INSERT INTO trabalho VALUES (6,'trabalho teste 2006','2006');
INSERT INTO trabalho VALUES (7,'trabalho teste 2007','2007');
INSERT INTO trabalho VALUES (8,'trabalho teste 2008','2008');
INSERT INTO trabalho VALUES (9,'trabalho teste 2009','2009');
SELECT * FROM trabalho;

CREATE TABLE autor (
cpf_id INT NOT NULL
, codigo_id INT NOT NULL
, CONSTRAINT pessoa_fk FOREIGN KEY (cpf_id) REFERENCES pessoa (cpf)
, CONSTRAINT trabalho_fk FOREIGN KEY (codigo_id) REFERENCES trabalho (codigo)
, PRIMARY KEY (cpf_id, codigo_id)
);
INSERT INTO autor VALUES (1,0);
INSERT INTO autor VALUES (1,1);
INSERT INTO autor VALUES (1,2);
INSERT INTO autor VALUES (1,3);
INSERT INTO autor VALUES (1,4);
INSERT INTO autor VALUES (1,5);
INSERT INTO autor VALUES (1,6);
INSERT INTO autor VALUES (1,7);
INSERT INTO autor VALUES (1,8);
INSERT INTO autor VALUES (1,9);
INSERT INTO autor VALUES (2,0);
INSERT INTO autor VALUES (2,1);
INSERT INTO autor VALUES (2,2);
INSERT INTO autor VALUES (2,3);
INSERT INTO autor VALUES (2,4);
INSERT INTO autor VALUES (2,5);
INSERT INTO autor VALUES (2,6);
INSERT INTO autor VALUES (2,7);
INSERT INTO autor VALUES (2,8);
INSERT INTO autor VALUES (2,9);
INSERT INTO autor VALUES (3,0);
INSERT INTO autor VALUES (3,1);
INSERT INTO autor VALUES (3,2);
INSERT INTO autor VALUES (3,3);
INSERT INTO autor VALUES (3,4);
INSERT INTO autor VALUES (3,5);
INSERT INTO autor VALUES (3,6);
INSERT INTO autor VALUES (3,7);
INSERT INTO autor VALUES (3,8);
INSERT INTO autor VALUES (3,9);
INSERT INTO autor VALUES (4,0);
INSERT INTO autor VALUES (4,1);
INSERT INTO autor VALUES (4,2);
INSERT INTO autor VALUES (4,3);
INSERT INTO autor VALUES (4,4);
INSERT INTO autor VALUES (4,5);
INSERT INTO autor VALUES (4,6);
INSERT INTO autor VALUES (4,7);
INSERT INTO autor VALUES (4,8);
INSERT INTO autor VALUES (4,9);
SELECT * FROM autor;

