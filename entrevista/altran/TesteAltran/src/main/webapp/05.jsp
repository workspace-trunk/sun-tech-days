<%--
    Document   : 05
    Created on : Aug 12, 2015, 12:32:12 PM
    Author     : tux
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <head>
        <title>Cadastro de clientes</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form name="cadastro" action="DadosInformados.jsp" onsubmit="return camposObrigatorios()" method="post">
            <label for="codigo">Código</label>
            <input type="text" id="codigo" name="codigo" maxlength="4" />
            <br/>

            <label for="nome">Nome</label>
            <input id="nome" name="nome" maxlength="50" />
            <br/>

            <label for="descricao">Descrição</label>
            <textarea id="descricao" name="descricao" maxlength="250" ></textarea>
            <br/>

            <label for="tipoCadastro">Tipo cadastro</label>
            <select id="tipoCadastro" name="tipoCadastro">
                <option value="pj">Pessoa Jurídica</option>
                <option value="pf">Pessoa Física</option>
            </select>
            <br/>

            <label for="estado">Estado</label>
            <select id="estado" name="estado" multiple="multiple">
                <c:forEach var="estado" items="${ estados }" >
                    <option value="<c:out value="${estado.codigo}"/>"><c:out value="${estado.descricao}"/></option>
                </c:forEach>
                <option value="SP">São Paulo</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="MG">Minas Gerais</option>
            </select>
            <br/>
            <input type="reset" value="Limpar" />
            <input type="submit" value="Enviar" />
        </form>
</html>
