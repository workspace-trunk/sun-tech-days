<%--
    Document   : DadosInformados
    Created on : Aug 13, 2015, 6:36:49 PM
    Author     : tux
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <c:set var="codigoRecebido" value="${ requestScope.codigo }"/>
        <c:set var="codigoRecebido1" value="${ requestSession.codigo }"/>
        <p>codigo = <c:out value="${ codigoRecebido }" /></p>
        <p>codigo = <c:out value="${ codigoRecebido1 }" /></p>
    </body>
</html>
